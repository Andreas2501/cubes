// Compiled by ClojureScript 1.9.229 {}
goog.provide('figwheel.client');
goog.require('cljs.core');
goog.require('goog.userAgent.product');
goog.require('goog.Uri');
goog.require('cljs.core.async');
goog.require('goog.object');
goog.require('figwheel.client.socket');
goog.require('figwheel.client.file_reloading');
goog.require('clojure.string');
goog.require('figwheel.client.utils');
goog.require('cljs.repl');
goog.require('figwheel.client.heads_up');
goog.require('cljs.reader');
figwheel.client._figwheel_version_ = "0.5.10";
figwheel.client.figwheel_repl_print = (function figwheel$client$figwheel_repl_print(var_args){
var args35586 = [];
var len__26974__auto___35589 = arguments.length;
var i__26975__auto___35590 = (0);
while(true){
if((i__26975__auto___35590 < len__26974__auto___35589)){
args35586.push((arguments[i__26975__auto___35590]));

var G__35591 = (i__26975__auto___35590 + (1));
i__26975__auto___35590 = G__35591;
continue;
} else {
}
break;
}

var G__35588 = args35586.length;
switch (G__35588) {
case 2:
return figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 1:
return figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args35586.length)].join('')));

}
});

figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$2 = (function (stream,args){
figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),"figwheel-repl-print",new cljs.core.Keyword(null,"content","content",15833224),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"stream","stream",1534941648),stream,new cljs.core.Keyword(null,"args","args",1315556576),args], null)], null));

return null;
});

figwheel.client.figwheel_repl_print.cljs$core$IFn$_invoke$arity$1 = (function (args){
return figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"out","out",-910545517),args);
});

figwheel.client.figwheel_repl_print.cljs$lang$maxFixedArity = 2;

figwheel.client.console_out_print = (function figwheel$client$console_out_print(args){
return console.log.apply(console,cljs.core.into_array.call(null,args));
});
figwheel.client.console_err_print = (function figwheel$client$console_err_print(args){
return console.error.apply(console,cljs.core.into_array.call(null,args));
});
figwheel.client.repl_out_print_fn = (function figwheel$client$repl_out_print_fn(var_args){
var args__26981__auto__ = [];
var len__26974__auto___35594 = arguments.length;
var i__26975__auto___35595 = (0);
while(true){
if((i__26975__auto___35595 < len__26974__auto___35594)){
args__26981__auto__.push((arguments[i__26975__auto___35595]));

var G__35596 = (i__26975__auto___35595 + (1));
i__26975__auto___35595 = G__35596;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.console_out_print.call(null,args);

figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"out","out",-910545517),args);

return null;
});

figwheel.client.repl_out_print_fn.cljs$lang$maxFixedArity = (0);

figwheel.client.repl_out_print_fn.cljs$lang$applyTo = (function (seq35593){
return figwheel.client.repl_out_print_fn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq35593));
});

figwheel.client.repl_err_print_fn = (function figwheel$client$repl_err_print_fn(var_args){
var args__26981__auto__ = [];
var len__26974__auto___35598 = arguments.length;
var i__26975__auto___35599 = (0);
while(true){
if((i__26975__auto___35599 < len__26974__auto___35598)){
args__26981__auto__.push((arguments[i__26975__auto___35599]));

var G__35600 = (i__26975__auto___35599 + (1));
i__26975__auto___35599 = G__35600;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic = (function (args){
figwheel.client.console_err_print.call(null,args);

figwheel.client.figwheel_repl_print.call(null,new cljs.core.Keyword(null,"err","err",-2089457205),args);

return null;
});

figwheel.client.repl_err_print_fn.cljs$lang$maxFixedArity = (0);

figwheel.client.repl_err_print_fn.cljs$lang$applyTo = (function (seq35597){
return figwheel.client.repl_err_print_fn.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq35597));
});

figwheel.client.enable_repl_print_BANG_ = (function figwheel$client$enable_repl_print_BANG_(){
cljs.core._STAR_print_newline_STAR_ = false;

cljs.core.set_print_fn_BANG_.call(null,figwheel.client.repl_out_print_fn);

cljs.core.set_print_err_fn_BANG_.call(null,figwheel.client.repl_err_print_fn);

return null;
});
figwheel.client.autoload_QMARK_ = (function figwheel$client$autoload_QMARK_(){
return figwheel.client.utils.persistent_config_get.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),true);
});
figwheel.client.toggle_autoload = (function figwheel$client$toggle_autoload(){
var res = figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),cljs.core.not.call(null,figwheel.client.autoload_QMARK_.call(null)));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Toggle autoload deprecated! Use (figwheel.client/set-autoload! false)")].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Figwheel autoloading "),cljs.core.str((cljs.core.truth_(figwheel.client.autoload_QMARK_.call(null))?"ON":"OFF"))].join(''));

return res;
});
goog.exportSymbol('figwheel.client.toggle_autoload', figwheel.client.toggle_autoload);
/**
 * Figwheel by default loads code changes as you work. Sometimes you
 *   just want to work on your code without the ramifications of
 *   autoloading and simply load your code piecemeal in the REPL. You can
 *   turn autoloading on and of with this method. 
 * 
 *   (figwheel.client/set-autoload false)
 * 
 *   NOTE: This is a persistent setting, meaning that it will persist
 *   through browser reloads.
 */
figwheel.client.set_autoload = (function figwheel$client$set_autoload(b){
if((b === true) || (b === false)){
} else {
throw (new Error("Assert failed: (or (true? b) (false? b))"));
}

return figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-autoload","figwheel-autoload",-2044741728),b);
});
goog.exportSymbol('figwheel.client.set_autoload', figwheel.client.set_autoload);
figwheel.client.repl_pprint = (function figwheel$client$repl_pprint(){
return figwheel.client.utils.persistent_config_get.call(null,new cljs.core.Keyword(null,"figwheel-repl-pprint","figwheel-repl-pprint",1076150873),true);
});
goog.exportSymbol('figwheel.client.repl_pprint', figwheel.client.repl_pprint);
/**
 * This method gives you the ability to turn the pretty printing of
 *   the REPL's return value on and off.
 *   
 *   (figwheel.client/set-repl-pprint false)
 * 
 *   NOTE: This is a persistent setting, meaning that it will persist
 *   through browser reloads.
 */
figwheel.client.set_repl_pprint = (function figwheel$client$set_repl_pprint(b){
if((b === true) || (b === false)){
} else {
throw (new Error("Assert failed: (or (true? b) (false? b))"));
}

return figwheel.client.utils.persistent_config_set_BANG_.call(null,new cljs.core.Keyword(null,"figwheel-repl-pprint","figwheel-repl-pprint",1076150873),b);
});
goog.exportSymbol('figwheel.client.set_repl_pprint', figwheel.client.set_repl_pprint);
figwheel.client.repl_result_pr_str = (function figwheel$client$repl_result_pr_str(v){
if(cljs.core.truth_(figwheel.client.repl_pprint.call(null))){
return figwheel.client.utils.pprint_to_string.call(null,v);
} else {
return cljs.core.pr_str.call(null,v);
}
});
goog.exportSymbol('figwheel.client.repl_result_pr_str', figwheel.client.repl_result_pr_str);
figwheel.client.get_essential_messages = (function figwheel$client$get_essential_messages(ed){
if(cljs.core.truth_(ed)){
return cljs.core.cons.call(null,cljs.core.select_keys.call(null,ed,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"message","message",-406056002),new cljs.core.Keyword(null,"class","class",-2030961996)], null)),figwheel$client$get_essential_messages.call(null,new cljs.core.Keyword(null,"cause","cause",231901252).cljs$core$IFn$_invoke$arity$1(ed)));
} else {
return null;
}
});
figwheel.client.error_msg_format = (function figwheel$client$error_msg_format(p__35601){
var map__35604 = p__35601;
var map__35604__$1 = ((((!((map__35604 == null)))?((((map__35604.cljs$lang$protocol_mask$partition0$ & (64))) || (map__35604.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__35604):map__35604);
var message = cljs.core.get.call(null,map__35604__$1,new cljs.core.Keyword(null,"message","message",-406056002));
var class$ = cljs.core.get.call(null,map__35604__$1,new cljs.core.Keyword(null,"class","class",-2030961996));
return [cljs.core.str(class$),cljs.core.str(" : "),cljs.core.str(message)].join('');
});
figwheel.client.format_messages = cljs.core.comp.call(null,cljs.core.partial.call(null,cljs.core.map,figwheel.client.error_msg_format),figwheel.client.get_essential_messages);
figwheel.client.focus_msgs = (function figwheel$client$focus_msgs(name_set,msg_hist){
return cljs.core.cons.call(null,cljs.core.first.call(null,msg_hist),cljs.core.filter.call(null,cljs.core.comp.call(null,name_set,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863)),cljs.core.rest.call(null,msg_hist)));
});
figwheel.client.reload_file_QMARK__STAR_ = (function figwheel$client$reload_file_QMARK__STAR_(msg_name,opts){
var or__25899__auto__ = new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223).cljs$core$IFn$_invoke$arity$1(opts);
if(cljs.core.truth_(or__25899__auto__)){
return or__25899__auto__;
} else {
return cljs.core.not_EQ_.call(null,msg_name,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356));
}
});
figwheel.client.reload_file_state_QMARK_ = (function figwheel$client$reload_file_state_QMARK_(msg_names,opts){
var and__25887__auto__ = cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563));
if(and__25887__auto__){
return figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts);
} else {
return and__25887__auto__;
}
});
figwheel.client.block_reload_file_state_QMARK_ = (function figwheel$client$block_reload_file_state_QMARK_(msg_names,opts){
return (cljs.core._EQ_.call(null,cljs.core.first.call(null,msg_names),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))) && (cljs.core.not.call(null,figwheel.client.reload_file_QMARK__STAR_.call(null,cljs.core.second.call(null,msg_names),opts)));
});
figwheel.client.warning_append_state_QMARK_ = (function figwheel$client$warning_append_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.warning_state_QMARK_ = (function figwheel$client$warning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),cljs.core.first.call(null,msg_names));
});
figwheel.client.rewarning_state_QMARK_ = (function figwheel$client$rewarning_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356)], null),cljs.core.take.call(null,(3),msg_names));
});
figwheel.client.compile_fail_state_QMARK_ = (function figwheel$client$compile_fail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),cljs.core.first.call(null,msg_names));
});
figwheel.client.compile_refail_state_QMARK_ = (function figwheel$client$compile_refail_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289)], null),cljs.core.take.call(null,(2),msg_names));
});
figwheel.client.css_loaded_state_QMARK_ = (function figwheel$client$css_loaded_state_QMARK_(msg_names){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874),cljs.core.first.call(null,msg_names));
});
figwheel.client.file_reloader_plugin = (function figwheel$client$file_reloader_plugin(opts){
var ch = cljs.core.async.chan.call(null);
var c__28077__auto___35766 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___35766,ch){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___35766,ch){
return (function (state_35735){
var state_val_35736 = (state_35735[(1)]);
if((state_val_35736 === (7))){
var inst_35731 = (state_35735[(2)]);
var state_35735__$1 = state_35735;
var statearr_35737_35767 = state_35735__$1;
(statearr_35737_35767[(2)] = inst_35731);

(statearr_35737_35767[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (1))){
var state_35735__$1 = state_35735;
var statearr_35738_35768 = state_35735__$1;
(statearr_35738_35768[(2)] = null);

(statearr_35738_35768[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (4))){
var inst_35688 = (state_35735[(7)]);
var inst_35688__$1 = (state_35735[(2)]);
var state_35735__$1 = (function (){var statearr_35739 = state_35735;
(statearr_35739[(7)] = inst_35688__$1);

return statearr_35739;
})();
if(cljs.core.truth_(inst_35688__$1)){
var statearr_35740_35769 = state_35735__$1;
(statearr_35740_35769[(1)] = (5));

} else {
var statearr_35741_35770 = state_35735__$1;
(statearr_35741_35770[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (15))){
var inst_35695 = (state_35735[(8)]);
var inst_35710 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_35695);
var inst_35711 = cljs.core.first.call(null,inst_35710);
var inst_35712 = new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(inst_35711);
var inst_35713 = [cljs.core.str("Figwheel: Not loading code with warnings - "),cljs.core.str(inst_35712)].join('');
var inst_35714 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),inst_35713);
var state_35735__$1 = state_35735;
var statearr_35742_35771 = state_35735__$1;
(statearr_35742_35771[(2)] = inst_35714);

(statearr_35742_35771[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (13))){
var inst_35719 = (state_35735[(2)]);
var state_35735__$1 = state_35735;
var statearr_35743_35772 = state_35735__$1;
(statearr_35743_35772[(2)] = inst_35719);

(statearr_35743_35772[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (6))){
var state_35735__$1 = state_35735;
var statearr_35744_35773 = state_35735__$1;
(statearr_35744_35773[(2)] = null);

(statearr_35744_35773[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (17))){
var inst_35717 = (state_35735[(2)]);
var state_35735__$1 = state_35735;
var statearr_35745_35774 = state_35735__$1;
(statearr_35745_35774[(2)] = inst_35717);

(statearr_35745_35774[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (3))){
var inst_35733 = (state_35735[(2)]);
var state_35735__$1 = state_35735;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_35735__$1,inst_35733);
} else {
if((state_val_35736 === (12))){
var inst_35694 = (state_35735[(9)]);
var inst_35708 = figwheel.client.block_reload_file_state_QMARK_.call(null,inst_35694,opts);
var state_35735__$1 = state_35735;
if(cljs.core.truth_(inst_35708)){
var statearr_35746_35775 = state_35735__$1;
(statearr_35746_35775[(1)] = (15));

} else {
var statearr_35747_35776 = state_35735__$1;
(statearr_35747_35776[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (2))){
var state_35735__$1 = state_35735;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_35735__$1,(4),ch);
} else {
if((state_val_35736 === (11))){
var inst_35695 = (state_35735[(8)]);
var inst_35700 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_35701 = figwheel.client.file_reloading.reload_js_files.call(null,opts,inst_35695);
var inst_35702 = cljs.core.async.timeout.call(null,(1000));
var inst_35703 = [inst_35701,inst_35702];
var inst_35704 = (new cljs.core.PersistentVector(null,2,(5),inst_35700,inst_35703,null));
var state_35735__$1 = state_35735;
return cljs.core.async.ioc_alts_BANG_.call(null,state_35735__$1,(14),inst_35704);
} else {
if((state_val_35736 === (9))){
var inst_35695 = (state_35735[(8)]);
var inst_35721 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),"Figwheel: code autoloading is OFF");
var inst_35722 = new cljs.core.Keyword(null,"files","files",-472457450).cljs$core$IFn$_invoke$arity$1(inst_35695);
var inst_35723 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_35722);
var inst_35724 = [cljs.core.str("Not loading: "),cljs.core.str(inst_35723)].join('');
var inst_35725 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),inst_35724);
var state_35735__$1 = (function (){var statearr_35748 = state_35735;
(statearr_35748[(10)] = inst_35721);

return statearr_35748;
})();
var statearr_35749_35777 = state_35735__$1;
(statearr_35749_35777[(2)] = inst_35725);

(statearr_35749_35777[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (5))){
var inst_35688 = (state_35735[(7)]);
var inst_35690 = [new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null];
var inst_35691 = (new cljs.core.PersistentArrayMap(null,2,inst_35690,null));
var inst_35692 = (new cljs.core.PersistentHashSet(null,inst_35691,null));
var inst_35693 = figwheel.client.focus_msgs.call(null,inst_35692,inst_35688);
var inst_35694 = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),inst_35693);
var inst_35695 = cljs.core.first.call(null,inst_35693);
var inst_35696 = figwheel.client.autoload_QMARK_.call(null);
var state_35735__$1 = (function (){var statearr_35750 = state_35735;
(statearr_35750[(9)] = inst_35694);

(statearr_35750[(8)] = inst_35695);

return statearr_35750;
})();
if(cljs.core.truth_(inst_35696)){
var statearr_35751_35778 = state_35735__$1;
(statearr_35751_35778[(1)] = (8));

} else {
var statearr_35752_35779 = state_35735__$1;
(statearr_35752_35779[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (14))){
var inst_35706 = (state_35735[(2)]);
var state_35735__$1 = state_35735;
var statearr_35753_35780 = state_35735__$1;
(statearr_35753_35780[(2)] = inst_35706);

(statearr_35753_35780[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (16))){
var state_35735__$1 = state_35735;
var statearr_35754_35781 = state_35735__$1;
(statearr_35754_35781[(2)] = null);

(statearr_35754_35781[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (10))){
var inst_35727 = (state_35735[(2)]);
var state_35735__$1 = (function (){var statearr_35755 = state_35735;
(statearr_35755[(11)] = inst_35727);

return statearr_35755;
})();
var statearr_35756_35782 = state_35735__$1;
(statearr_35756_35782[(2)] = null);

(statearr_35756_35782[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_35736 === (8))){
var inst_35694 = (state_35735[(9)]);
var inst_35698 = figwheel.client.reload_file_state_QMARK_.call(null,inst_35694,opts);
var state_35735__$1 = state_35735;
if(cljs.core.truth_(inst_35698)){
var statearr_35757_35783 = state_35735__$1;
(statearr_35757_35783[(1)] = (11));

} else {
var statearr_35758_35784 = state_35735__$1;
(statearr_35758_35784[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___35766,ch))
;
return ((function (switch__27965__auto__,c__28077__auto___35766,ch){
return (function() {
var figwheel$client$file_reloader_plugin_$_state_machine__27966__auto__ = null;
var figwheel$client$file_reloader_plugin_$_state_machine__27966__auto____0 = (function (){
var statearr_35762 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_35762[(0)] = figwheel$client$file_reloader_plugin_$_state_machine__27966__auto__);

(statearr_35762[(1)] = (1));

return statearr_35762;
});
var figwheel$client$file_reloader_plugin_$_state_machine__27966__auto____1 = (function (state_35735){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_35735);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e35763){if((e35763 instanceof Object)){
var ex__27969__auto__ = e35763;
var statearr_35764_35785 = state_35735;
(statearr_35764_35785[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_35735);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e35763;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__35786 = state_35735;
state_35735 = G__35786;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
figwheel$client$file_reloader_plugin_$_state_machine__27966__auto__ = function(state_35735){
switch(arguments.length){
case 0:
return figwheel$client$file_reloader_plugin_$_state_machine__27966__auto____0.call(this);
case 1:
return figwheel$client$file_reloader_plugin_$_state_machine__27966__auto____1.call(this,state_35735);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloader_plugin_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloader_plugin_$_state_machine__27966__auto____0;
figwheel$client$file_reloader_plugin_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloader_plugin_$_state_machine__27966__auto____1;
return figwheel$client$file_reloader_plugin_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___35766,ch))
})();
var state__28079__auto__ = (function (){var statearr_35765 = f__28078__auto__.call(null);
(statearr_35765[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___35766);

return statearr_35765;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___35766,ch))
);


return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.truncate_stack_trace = (function figwheel$client$truncate_stack_trace(stack_str){
return cljs.core.take_while.call(null,(function (p1__35787_SHARP_){
return cljs.core.not.call(null,cljs.core.re_matches.call(null,/.*eval_javascript_STAR__STAR_.*/,p1__35787_SHARP_));
}),clojure.string.split_lines.call(null,stack_str));
});
figwheel.client.get_ua_product = (function figwheel$client$get_ua_product(){
if(cljs.core.truth_(figwheel.client.utils.node_env_QMARK_.call(null))){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.SAFARI)){
return new cljs.core.Keyword(null,"safari","safari",497115653);
} else {
if(cljs.core.truth_(goog.userAgent.product.CHROME)){
return new cljs.core.Keyword(null,"chrome","chrome",1718738387);
} else {
if(cljs.core.truth_(goog.userAgent.product.FIREFOX)){
return new cljs.core.Keyword(null,"firefox","firefox",1283768880);
} else {
if(cljs.core.truth_(goog.userAgent.product.IE)){
return new cljs.core.Keyword(null,"ie","ie",2038473780);
} else {
return null;
}
}
}
}
}
});
var base_path_35790 = figwheel.client.utils.base_url_path.call(null);
figwheel.client.eval_javascript_STAR__STAR_ = ((function (base_path_35790){
return (function figwheel$client$eval_javascript_STAR__STAR_(code,opts,result_handler){
try{figwheel.client.enable_repl_print_BANG_.call(null);

var result_value = figwheel.client.utils.eval_helper.call(null,code,opts);
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"success","success",1890645906),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),result_value], null));
}catch (e35789){if((e35789 instanceof Error)){
var e = e35789;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 5, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),clojure.string.join.call(null,"\n",figwheel.client.truncate_stack_trace.call(null,e.stack)),new cljs.core.Keyword(null,"base-path","base-path",495760020),base_path_35790], null));
} else {
var e = e35789;
return result_handler.call(null,new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"status","status",-1997798413),new cljs.core.Keyword(null,"exception","exception",-335277064),new cljs.core.Keyword(null,"ua-product","ua-product",938384227),figwheel.client.get_ua_product.call(null),new cljs.core.Keyword(null,"value","value",305978217),cljs.core.pr_str.call(null,e),new cljs.core.Keyword(null,"stacktrace","stacktrace",-95588394),"No stacktrace available."], null));

}
}finally {figwheel.client.enable_repl_print_BANG_.call(null);
}});})(base_path_35790))
;
/**
 * The REPL can disconnect and reconnect lets ensure cljs.user exists at least.
 */
figwheel.client.ensure_cljs_user = (function figwheel$client$ensure_cljs_user(){
if(cljs.core.truth_(cljs.user)){
return null;
} else {
return cljs.user = ({});
}
});
figwheel.client.repl_plugin = (function figwheel$client$repl_plugin(p__35791){
var map__35800 = p__35791;
var map__35800__$1 = ((((!((map__35800 == null)))?((((map__35800.cljs$lang$protocol_mask$partition0$ & (64))) || (map__35800.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__35800):map__35800);
var opts = map__35800__$1;
var build_id = cljs.core.get.call(null,map__35800__$1,new cljs.core.Keyword(null,"build-id","build-id",1642831089));
return ((function (map__35800,map__35800__$1,opts,build_id){
return (function (p__35802){
var vec__35803 = p__35802;
var seq__35804 = cljs.core.seq.call(null,vec__35803);
var first__35805 = cljs.core.first.call(null,seq__35804);
var seq__35804__$1 = cljs.core.next.call(null,seq__35804);
var map__35806 = first__35805;
var map__35806__$1 = ((((!((map__35806 == null)))?((((map__35806.cljs$lang$protocol_mask$partition0$ & (64))) || (map__35806.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__35806):map__35806);
var msg = map__35806__$1;
var msg_name = cljs.core.get.call(null,map__35806__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__35804__$1;
if(cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"repl-eval","repl-eval",-1784727398),msg_name)){
figwheel.client.ensure_cljs_user.call(null);

return figwheel.client.eval_javascript_STAR__STAR_.call(null,new cljs.core.Keyword(null,"code","code",1586293142).cljs$core$IFn$_invoke$arity$1(msg),opts,((function (vec__35803,seq__35804,first__35805,seq__35804__$1,map__35806,map__35806__$1,msg,msg_name,_,map__35800,map__35800__$1,opts,build_id){
return (function (res){
return figwheel.client.socket.send_BANG_.call(null,new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"figwheel-event","figwheel-event",519570592),"callback",new cljs.core.Keyword(null,"callback-name","callback-name",336964714),new cljs.core.Keyword(null,"callback-name","callback-name",336964714).cljs$core$IFn$_invoke$arity$1(msg),new cljs.core.Keyword(null,"content","content",15833224),res], null));
});})(vec__35803,seq__35804,first__35805,seq__35804__$1,map__35806,map__35806__$1,msg,msg_name,_,map__35800,map__35800__$1,opts,build_id))
);
} else {
return null;
}
});
;})(map__35800,map__35800__$1,opts,build_id))
});
figwheel.client.css_reloader_plugin = (function figwheel$client$css_reloader_plugin(opts){
return (function (p__35814){
var vec__35815 = p__35814;
var seq__35816 = cljs.core.seq.call(null,vec__35815);
var first__35817 = cljs.core.first.call(null,seq__35816);
var seq__35816__$1 = cljs.core.next.call(null,seq__35816);
var map__35818 = first__35817;
var map__35818__$1 = ((((!((map__35818 == null)))?((((map__35818.cljs$lang$protocol_mask$partition0$ & (64))) || (map__35818.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__35818):map__35818);
var msg = map__35818__$1;
var msg_name = cljs.core.get.call(null,map__35818__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__35816__$1;
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"css-files-changed","css-files-changed",720773874))){
return figwheel.client.file_reloading.reload_css_files.call(null,opts,msg);
} else {
return null;
}
});
});
figwheel.client.compile_fail_warning_plugin = (function figwheel$client$compile_fail_warning_plugin(p__35820){
var map__35832 = p__35820;
var map__35832__$1 = ((((!((map__35832 == null)))?((((map__35832.cljs$lang$protocol_mask$partition0$ & (64))) || (map__35832.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__35832):map__35832);
var on_compile_warning = cljs.core.get.call(null,map__35832__$1,new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947));
var on_compile_fail = cljs.core.get.call(null,map__35832__$1,new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036));
return ((function (map__35832,map__35832__$1,on_compile_warning,on_compile_fail){
return (function (p__35834){
var vec__35835 = p__35834;
var seq__35836 = cljs.core.seq.call(null,vec__35835);
var first__35837 = cljs.core.first.call(null,seq__35836);
var seq__35836__$1 = cljs.core.next.call(null,seq__35836);
var map__35838 = first__35837;
var map__35838__$1 = ((((!((map__35838 == null)))?((((map__35838.cljs$lang$protocol_mask$partition0$ & (64))) || (map__35838.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__35838):map__35838);
var msg = map__35838__$1;
var msg_name = cljs.core.get.call(null,map__35838__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
var _ = seq__35836__$1;
var pred__35840 = cljs.core._EQ_;
var expr__35841 = msg_name;
if(cljs.core.truth_(pred__35840.call(null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),expr__35841))){
return on_compile_warning.call(null,msg);
} else {
if(cljs.core.truth_(pred__35840.call(null,new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),expr__35841))){
return on_compile_fail.call(null,msg);
} else {
return null;
}
}
});
;})(map__35832,map__35832__$1,on_compile_warning,on_compile_fail))
});
figwheel.client.auto_jump_to_error = (function figwheel$client$auto_jump_to_error(opts,error){
if(cljs.core.truth_(new cljs.core.Keyword(null,"auto-jump-to-source-on-error","auto-jump-to-source-on-error",-960314920).cljs$core$IFn$_invoke$arity$1(opts))){
return figwheel.client.heads_up.auto_notify_source_file_line.call(null,error);
} else {
return null;
}
});
figwheel.client.heads_up_plugin_msg_handler = (function figwheel$client$heads_up_plugin_msg_handler(opts,msg_hist_SINGLEQUOTE_){
var msg_hist = figwheel.client.focus_msgs.call(null,new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"compile-failed","compile-failed",-477639289),null,new cljs.core.Keyword(null,"compile-warning","compile-warning",43425356),null,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563),null], null), null),msg_hist_SINGLEQUOTE_);
var msg_names = cljs.core.map.call(null,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863),msg_hist);
var msg = cljs.core.first.call(null,msg_hist);
var c__28077__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto__,msg_hist,msg_names,msg){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto__,msg_hist,msg_names,msg){
return (function (state_36069){
var state_val_36070 = (state_36069[(1)]);
if((state_val_36070 === (7))){
var inst_35989 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
if(cljs.core.truth_(inst_35989)){
var statearr_36071_36121 = state_36069__$1;
(statearr_36071_36121[(1)] = (8));

} else {
var statearr_36072_36122 = state_36069__$1;
(statearr_36072_36122[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (20))){
var inst_36063 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
var statearr_36073_36123 = state_36069__$1;
(statearr_36073_36123[(2)] = inst_36063);

(statearr_36073_36123[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (27))){
var inst_36059 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
var statearr_36074_36124 = state_36069__$1;
(statearr_36074_36124[(2)] = inst_36059);

(statearr_36074_36124[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (1))){
var inst_35982 = figwheel.client.reload_file_state_QMARK_.call(null,msg_names,opts);
var state_36069__$1 = state_36069;
if(cljs.core.truth_(inst_35982)){
var statearr_36075_36125 = state_36069__$1;
(statearr_36075_36125[(1)] = (2));

} else {
var statearr_36076_36126 = state_36069__$1;
(statearr_36076_36126[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (24))){
var inst_36061 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
var statearr_36077_36127 = state_36069__$1;
(statearr_36077_36127[(2)] = inst_36061);

(statearr_36077_36127[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (4))){
var inst_36067 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_36069__$1,inst_36067);
} else {
if((state_val_36070 === (15))){
var inst_36065 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
var statearr_36078_36128 = state_36069__$1;
(statearr_36078_36128[(2)] = inst_36065);

(statearr_36078_36128[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (21))){
var inst_36018 = (state_36069[(2)]);
var inst_36019 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_36020 = figwheel.client.auto_jump_to_error.call(null,opts,inst_36019);
var state_36069__$1 = (function (){var statearr_36079 = state_36069;
(statearr_36079[(7)] = inst_36018);

return statearr_36079;
})();
var statearr_36080_36129 = state_36069__$1;
(statearr_36080_36129[(2)] = inst_36020);

(statearr_36080_36129[(1)] = (20));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (31))){
var inst_36048 = figwheel.client.css_loaded_state_QMARK_.call(null,msg_names);
var state_36069__$1 = state_36069;
if(cljs.core.truth_(inst_36048)){
var statearr_36081_36130 = state_36069__$1;
(statearr_36081_36130[(1)] = (34));

} else {
var statearr_36082_36131 = state_36069__$1;
(statearr_36082_36131[(1)] = (35));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (32))){
var inst_36057 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
var statearr_36083_36132 = state_36069__$1;
(statearr_36083_36132[(2)] = inst_36057);

(statearr_36083_36132[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (33))){
var inst_36044 = (state_36069[(2)]);
var inst_36045 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_36046 = figwheel.client.auto_jump_to_error.call(null,opts,inst_36045);
var state_36069__$1 = (function (){var statearr_36084 = state_36069;
(statearr_36084[(8)] = inst_36044);

return statearr_36084;
})();
var statearr_36085_36133 = state_36069__$1;
(statearr_36085_36133[(2)] = inst_36046);

(statearr_36085_36133[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (13))){
var inst_36003 = figwheel.client.heads_up.clear.call(null);
var state_36069__$1 = state_36069;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36069__$1,(16),inst_36003);
} else {
if((state_val_36070 === (22))){
var inst_36024 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_36025 = figwheel.client.heads_up.append_warning_message.call(null,inst_36024);
var state_36069__$1 = state_36069;
var statearr_36086_36134 = state_36069__$1;
(statearr_36086_36134[(2)] = inst_36025);

(statearr_36086_36134[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (36))){
var inst_36055 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
var statearr_36087_36135 = state_36069__$1;
(statearr_36087_36135[(2)] = inst_36055);

(statearr_36087_36135[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (29))){
var inst_36035 = (state_36069[(2)]);
var inst_36036 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_36037 = figwheel.client.auto_jump_to_error.call(null,opts,inst_36036);
var state_36069__$1 = (function (){var statearr_36088 = state_36069;
(statearr_36088[(9)] = inst_36035);

return statearr_36088;
})();
var statearr_36089_36136 = state_36069__$1;
(statearr_36089_36136[(2)] = inst_36037);

(statearr_36089_36136[(1)] = (27));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (6))){
var inst_35984 = (state_36069[(10)]);
var state_36069__$1 = state_36069;
var statearr_36090_36137 = state_36069__$1;
(statearr_36090_36137[(2)] = inst_35984);

(statearr_36090_36137[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (28))){
var inst_36031 = (state_36069[(2)]);
var inst_36032 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_36033 = figwheel.client.heads_up.display_warning.call(null,inst_36032);
var state_36069__$1 = (function (){var statearr_36091 = state_36069;
(statearr_36091[(11)] = inst_36031);

return statearr_36091;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36069__$1,(29),inst_36033);
} else {
if((state_val_36070 === (25))){
var inst_36029 = figwheel.client.heads_up.clear.call(null);
var state_36069__$1 = state_36069;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36069__$1,(28),inst_36029);
} else {
if((state_val_36070 === (34))){
var inst_36050 = figwheel.client.heads_up.flash_loaded.call(null);
var state_36069__$1 = state_36069;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36069__$1,(37),inst_36050);
} else {
if((state_val_36070 === (17))){
var inst_36009 = (state_36069[(2)]);
var inst_36010 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_36011 = figwheel.client.auto_jump_to_error.call(null,opts,inst_36010);
var state_36069__$1 = (function (){var statearr_36092 = state_36069;
(statearr_36092[(12)] = inst_36009);

return statearr_36092;
})();
var statearr_36093_36138 = state_36069__$1;
(statearr_36093_36138[(2)] = inst_36011);

(statearr_36093_36138[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (3))){
var inst_36001 = figwheel.client.compile_refail_state_QMARK_.call(null,msg_names);
var state_36069__$1 = state_36069;
if(cljs.core.truth_(inst_36001)){
var statearr_36094_36139 = state_36069__$1;
(statearr_36094_36139[(1)] = (13));

} else {
var statearr_36095_36140 = state_36069__$1;
(statearr_36095_36140[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (12))){
var inst_35997 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
var statearr_36096_36141 = state_36069__$1;
(statearr_36096_36141[(2)] = inst_35997);

(statearr_36096_36141[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (2))){
var inst_35984 = (state_36069[(10)]);
var inst_35984__$1 = figwheel.client.autoload_QMARK_.call(null);
var state_36069__$1 = (function (){var statearr_36097 = state_36069;
(statearr_36097[(10)] = inst_35984__$1);

return statearr_36097;
})();
if(cljs.core.truth_(inst_35984__$1)){
var statearr_36098_36142 = state_36069__$1;
(statearr_36098_36142[(1)] = (5));

} else {
var statearr_36099_36143 = state_36069__$1;
(statearr_36099_36143[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (23))){
var inst_36027 = figwheel.client.rewarning_state_QMARK_.call(null,msg_names);
var state_36069__$1 = state_36069;
if(cljs.core.truth_(inst_36027)){
var statearr_36100_36144 = state_36069__$1;
(statearr_36100_36144[(1)] = (25));

} else {
var statearr_36101_36145 = state_36069__$1;
(statearr_36101_36145[(1)] = (26));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (35))){
var state_36069__$1 = state_36069;
var statearr_36102_36146 = state_36069__$1;
(statearr_36102_36146[(2)] = null);

(statearr_36102_36146[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (19))){
var inst_36022 = figwheel.client.warning_append_state_QMARK_.call(null,msg_names);
var state_36069__$1 = state_36069;
if(cljs.core.truth_(inst_36022)){
var statearr_36103_36147 = state_36069__$1;
(statearr_36103_36147[(1)] = (22));

} else {
var statearr_36104_36148 = state_36069__$1;
(statearr_36104_36148[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (11))){
var inst_35993 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
var statearr_36105_36149 = state_36069__$1;
(statearr_36105_36149[(2)] = inst_35993);

(statearr_36105_36149[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (9))){
var inst_35995 = figwheel.client.heads_up.clear.call(null);
var state_36069__$1 = state_36069;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36069__$1,(12),inst_35995);
} else {
if((state_val_36070 === (5))){
var inst_35986 = new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(opts);
var state_36069__$1 = state_36069;
var statearr_36106_36150 = state_36069__$1;
(statearr_36106_36150[(2)] = inst_35986);

(statearr_36106_36150[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (14))){
var inst_36013 = figwheel.client.compile_fail_state_QMARK_.call(null,msg_names);
var state_36069__$1 = state_36069;
if(cljs.core.truth_(inst_36013)){
var statearr_36107_36151 = state_36069__$1;
(statearr_36107_36151[(1)] = (18));

} else {
var statearr_36108_36152 = state_36069__$1;
(statearr_36108_36152[(1)] = (19));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (26))){
var inst_36039 = figwheel.client.warning_state_QMARK_.call(null,msg_names);
var state_36069__$1 = state_36069;
if(cljs.core.truth_(inst_36039)){
var statearr_36109_36153 = state_36069__$1;
(statearr_36109_36153[(1)] = (30));

} else {
var statearr_36110_36154 = state_36069__$1;
(statearr_36110_36154[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (16))){
var inst_36005 = (state_36069[(2)]);
var inst_36006 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_36007 = figwheel.client.heads_up.display_exception.call(null,inst_36006);
var state_36069__$1 = (function (){var statearr_36111 = state_36069;
(statearr_36111[(13)] = inst_36005);

return statearr_36111;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36069__$1,(17),inst_36007);
} else {
if((state_val_36070 === (30))){
var inst_36041 = new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(msg);
var inst_36042 = figwheel.client.heads_up.display_warning.call(null,inst_36041);
var state_36069__$1 = state_36069;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36069__$1,(33),inst_36042);
} else {
if((state_val_36070 === (10))){
var inst_35999 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
var statearr_36112_36155 = state_36069__$1;
(statearr_36112_36155[(2)] = inst_35999);

(statearr_36112_36155[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (18))){
var inst_36015 = new cljs.core.Keyword(null,"exception-data","exception-data",-512474886).cljs$core$IFn$_invoke$arity$1(msg);
var inst_36016 = figwheel.client.heads_up.display_exception.call(null,inst_36015);
var state_36069__$1 = state_36069;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36069__$1,(21),inst_36016);
} else {
if((state_val_36070 === (37))){
var inst_36052 = (state_36069[(2)]);
var state_36069__$1 = state_36069;
var statearr_36113_36156 = state_36069__$1;
(statearr_36113_36156[(2)] = inst_36052);

(statearr_36113_36156[(1)] = (36));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36070 === (8))){
var inst_35991 = figwheel.client.heads_up.flash_loaded.call(null);
var state_36069__$1 = state_36069;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36069__$1,(11),inst_35991);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto__,msg_hist,msg_names,msg))
;
return ((function (switch__27965__auto__,c__28077__auto__,msg_hist,msg_names,msg){
return (function() {
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto__ = null;
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto____0 = (function (){
var statearr_36117 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_36117[(0)] = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto__);

(statearr_36117[(1)] = (1));

return statearr_36117;
});
var figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto____1 = (function (state_36069){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_36069);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e36118){if((e36118 instanceof Object)){
var ex__27969__auto__ = e36118;
var statearr_36119_36157 = state_36069;
(statearr_36119_36157[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_36069);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e36118;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36158 = state_36069;
state_36069 = G__36158;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto__ = function(state_36069){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto____1.call(this,state_36069);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto____0;
figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto____1;
return figwheel$client$heads_up_plugin_msg_handler_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto__,msg_hist,msg_names,msg))
})();
var state__28079__auto__ = (function (){var statearr_36120 = f__28078__auto__.call(null);
(statearr_36120[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto__);

return statearr_36120;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto__,msg_hist,msg_names,msg))
);

return c__28077__auto__;
});
figwheel.client.heads_up_plugin = (function figwheel$client$heads_up_plugin(opts){
var ch = cljs.core.async.chan.call(null);
figwheel.client.heads_up_config_options_STAR__STAR_ = opts;

var c__28077__auto___36221 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___36221,ch){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___36221,ch){
return (function (state_36204){
var state_val_36205 = (state_36204[(1)]);
if((state_val_36205 === (1))){
var state_36204__$1 = state_36204;
var statearr_36206_36222 = state_36204__$1;
(statearr_36206_36222[(2)] = null);

(statearr_36206_36222[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36205 === (2))){
var state_36204__$1 = state_36204;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36204__$1,(4),ch);
} else {
if((state_val_36205 === (3))){
var inst_36202 = (state_36204[(2)]);
var state_36204__$1 = state_36204;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_36204__$1,inst_36202);
} else {
if((state_val_36205 === (4))){
var inst_36192 = (state_36204[(7)]);
var inst_36192__$1 = (state_36204[(2)]);
var state_36204__$1 = (function (){var statearr_36207 = state_36204;
(statearr_36207[(7)] = inst_36192__$1);

return statearr_36207;
})();
if(cljs.core.truth_(inst_36192__$1)){
var statearr_36208_36223 = state_36204__$1;
(statearr_36208_36223[(1)] = (5));

} else {
var statearr_36209_36224 = state_36204__$1;
(statearr_36209_36224[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36205 === (5))){
var inst_36192 = (state_36204[(7)]);
var inst_36194 = figwheel.client.heads_up_plugin_msg_handler.call(null,opts,inst_36192);
var state_36204__$1 = state_36204;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36204__$1,(8),inst_36194);
} else {
if((state_val_36205 === (6))){
var state_36204__$1 = state_36204;
var statearr_36210_36225 = state_36204__$1;
(statearr_36210_36225[(2)] = null);

(statearr_36210_36225[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36205 === (7))){
var inst_36200 = (state_36204[(2)]);
var state_36204__$1 = state_36204;
var statearr_36211_36226 = state_36204__$1;
(statearr_36211_36226[(2)] = inst_36200);

(statearr_36211_36226[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_36205 === (8))){
var inst_36196 = (state_36204[(2)]);
var state_36204__$1 = (function (){var statearr_36212 = state_36204;
(statearr_36212[(8)] = inst_36196);

return statearr_36212;
})();
var statearr_36213_36227 = state_36204__$1;
(statearr_36213_36227[(2)] = null);

(statearr_36213_36227[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
});})(c__28077__auto___36221,ch))
;
return ((function (switch__27965__auto__,c__28077__auto___36221,ch){
return (function() {
var figwheel$client$heads_up_plugin_$_state_machine__27966__auto__ = null;
var figwheel$client$heads_up_plugin_$_state_machine__27966__auto____0 = (function (){
var statearr_36217 = [null,null,null,null,null,null,null,null,null];
(statearr_36217[(0)] = figwheel$client$heads_up_plugin_$_state_machine__27966__auto__);

(statearr_36217[(1)] = (1));

return statearr_36217;
});
var figwheel$client$heads_up_plugin_$_state_machine__27966__auto____1 = (function (state_36204){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_36204);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e36218){if((e36218 instanceof Object)){
var ex__27969__auto__ = e36218;
var statearr_36219_36228 = state_36204;
(statearr_36219_36228[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_36204);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e36218;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36229 = state_36204;
state_36204 = G__36229;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
figwheel$client$heads_up_plugin_$_state_machine__27966__auto__ = function(state_36204){
switch(arguments.length){
case 0:
return figwheel$client$heads_up_plugin_$_state_machine__27966__auto____0.call(this);
case 1:
return figwheel$client$heads_up_plugin_$_state_machine__27966__auto____1.call(this,state_36204);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$heads_up_plugin_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$heads_up_plugin_$_state_machine__27966__auto____0;
figwheel$client$heads_up_plugin_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$heads_up_plugin_$_state_machine__27966__auto____1;
return figwheel$client$heads_up_plugin_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___36221,ch))
})();
var state__28079__auto__ = (function (){var statearr_36220 = f__28078__auto__.call(null);
(statearr_36220[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___36221);

return statearr_36220;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___36221,ch))
);


figwheel.client.heads_up.ensure_container.call(null);

return ((function (ch){
return (function (msg_hist){
cljs.core.async.put_BANG_.call(null,ch,msg_hist);

return msg_hist;
});
;})(ch))
});
figwheel.client.enforce_project_plugin = (function figwheel$client$enforce_project_plugin(opts){
return (function (msg_hist){
if(((1) < cljs.core.count.call(null,cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"project-id","project-id",206449307),cljs.core.take.call(null,(5),msg_hist)))))){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different project. Shutting socket down.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__28077__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto__){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto__){
return (function (state_36250){
var state_val_36251 = (state_36250[(1)]);
if((state_val_36251 === (1))){
var inst_36245 = cljs.core.async.timeout.call(null,(3000));
var state_36250__$1 = state_36250;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36250__$1,(2),inst_36245);
} else {
if((state_val_36251 === (2))){
var inst_36247 = (state_36250[(2)]);
var inst_36248 = figwheel.client.heads_up.display_system_warning.call(null,"Connection from different project","Shutting connection down!!!!!");
var state_36250__$1 = (function (){var statearr_36252 = state_36250;
(statearr_36252[(7)] = inst_36247);

return statearr_36252;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_36250__$1,inst_36248);
} else {
return null;
}
}
});})(c__28077__auto__))
;
return ((function (switch__27965__auto__,c__28077__auto__){
return (function() {
var figwheel$client$enforce_project_plugin_$_state_machine__27966__auto__ = null;
var figwheel$client$enforce_project_plugin_$_state_machine__27966__auto____0 = (function (){
var statearr_36256 = [null,null,null,null,null,null,null,null];
(statearr_36256[(0)] = figwheel$client$enforce_project_plugin_$_state_machine__27966__auto__);

(statearr_36256[(1)] = (1));

return statearr_36256;
});
var figwheel$client$enforce_project_plugin_$_state_machine__27966__auto____1 = (function (state_36250){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_36250);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e36257){if((e36257 instanceof Object)){
var ex__27969__auto__ = e36257;
var statearr_36258_36260 = state_36250;
(statearr_36258_36260[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_36250);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e36257;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36261 = state_36250;
state_36250 = G__36261;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
figwheel$client$enforce_project_plugin_$_state_machine__27966__auto__ = function(state_36250){
switch(arguments.length){
case 0:
return figwheel$client$enforce_project_plugin_$_state_machine__27966__auto____0.call(this);
case 1:
return figwheel$client$enforce_project_plugin_$_state_machine__27966__auto____1.call(this,state_36250);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$enforce_project_plugin_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_project_plugin_$_state_machine__27966__auto____0;
figwheel$client$enforce_project_plugin_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_project_plugin_$_state_machine__27966__auto____1;
return figwheel$client$enforce_project_plugin_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto__))
})();
var state__28079__auto__ = (function (){var statearr_36259 = f__28078__auto__.call(null);
(statearr_36259[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto__);

return statearr_36259;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto__))
);

return c__28077__auto__;
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.enforce_figwheel_version_plugin = (function figwheel$client$enforce_figwheel_version_plugin(opts){
return (function (msg_hist){
var temp__4657__auto__ = new cljs.core.Keyword(null,"figwheel-version","figwheel-version",1409553832).cljs$core$IFn$_invoke$arity$1(cljs.core.first.call(null,msg_hist));
if(cljs.core.truth_(temp__4657__auto__)){
var figwheel_version = temp__4657__auto__;
if(cljs.core.not_EQ_.call(null,figwheel_version,figwheel.client._figwheel_version_)){
figwheel.client.socket.close_BANG_.call(null);

console.error("Figwheel: message received from different version of Figwheel.");

if(cljs.core.truth_(new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(opts))){
var c__28077__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto__,figwheel_version,temp__4657__auto__){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto__,figwheel_version,temp__4657__auto__){
return (function (state_36284){
var state_val_36285 = (state_36284[(1)]);
if((state_val_36285 === (1))){
var inst_36278 = cljs.core.async.timeout.call(null,(2000));
var state_36284__$1 = state_36284;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_36284__$1,(2),inst_36278);
} else {
if((state_val_36285 === (2))){
var inst_36280 = (state_36284[(2)]);
var inst_36281 = [cljs.core.str("Figwheel Client Version <strong>"),cljs.core.str(figwheel.client._figwheel_version_),cljs.core.str("</strong> is not equal to "),cljs.core.str("Figwheel Sidecar Version <strong>"),cljs.core.str(figwheel_version),cljs.core.str("</strong>"),cljs.core.str(".  Shutting down Websocket Connection!"),cljs.core.str("<h4>To fix try:</h4>"),cljs.core.str("<ol><li>Reload this page and make sure you are not getting a cached version of the client.</li>"),cljs.core.str("<li>You may have to clean (delete compiled assets) and rebuild to make sure that the new client code is being used.</li>"),cljs.core.str("<li>Also, make sure you have consistent Figwheel dependencies.</li></ol>")].join('');
var inst_36282 = figwheel.client.heads_up.display_system_warning.call(null,"Figwheel Client and Server have different versions!!",inst_36281);
var state_36284__$1 = (function (){var statearr_36286 = state_36284;
(statearr_36286[(7)] = inst_36280);

return statearr_36286;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_36284__$1,inst_36282);
} else {
return null;
}
}
});})(c__28077__auto__,figwheel_version,temp__4657__auto__))
;
return ((function (switch__27965__auto__,c__28077__auto__,figwheel_version,temp__4657__auto__){
return (function() {
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto__ = null;
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto____0 = (function (){
var statearr_36290 = [null,null,null,null,null,null,null,null];
(statearr_36290[(0)] = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto__);

(statearr_36290[(1)] = (1));

return statearr_36290;
});
var figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto____1 = (function (state_36284){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_36284);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e36291){if((e36291 instanceof Object)){
var ex__27969__auto__ = e36291;
var statearr_36292_36294 = state_36284;
(statearr_36292_36294[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_36284);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e36291;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__36295 = state_36284;
state_36284 = G__36295;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto__ = function(state_36284){
switch(arguments.length){
case 0:
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto____0.call(this);
case 1:
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto____1.call(this,state_36284);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto____0;
figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto____1;
return figwheel$client$enforce_figwheel_version_plugin_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto__,figwheel_version,temp__4657__auto__))
})();
var state__28079__auto__ = (function (){var statearr_36293 = f__28078__auto__.call(null);
(statearr_36293[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto__);

return statearr_36293;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto__,figwheel_version,temp__4657__auto__))
);

return c__28077__auto__;
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
});
});
figwheel.client.default_on_jsload = cljs.core.identity;
figwheel.client.file_line_column = (function figwheel$client$file_line_column(p__36296){
var map__36300 = p__36296;
var map__36300__$1 = ((((!((map__36300 == null)))?((((map__36300.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36300.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36300):map__36300);
var file = cljs.core.get.call(null,map__36300__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var line = cljs.core.get.call(null,map__36300__$1,new cljs.core.Keyword(null,"line","line",212345235));
var column = cljs.core.get.call(null,map__36300__$1,new cljs.core.Keyword(null,"column","column",2078222095));
var G__36302 = "";
var G__36302__$1 = (cljs.core.truth_(file)?[cljs.core.str(G__36302),cljs.core.str("file "),cljs.core.str(file)].join(''):G__36302);
var G__36302__$2 = (cljs.core.truth_(line)?[cljs.core.str(G__36302__$1),cljs.core.str(" at line "),cljs.core.str(line)].join(''):G__36302__$1);
if(cljs.core.truth_((function (){var and__25887__auto__ = line;
if(cljs.core.truth_(and__25887__auto__)){
return column;
} else {
return and__25887__auto__;
}
})())){
return [cljs.core.str(G__36302__$2),cljs.core.str(", column "),cljs.core.str(column)].join('');
} else {
return G__36302__$2;
}
});
figwheel.client.default_on_compile_fail = (function figwheel$client$default_on_compile_fail(p__36303){
var map__36310 = p__36303;
var map__36310__$1 = ((((!((map__36310 == null)))?((((map__36310.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36310.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36310):map__36310);
var ed = map__36310__$1;
var formatted_exception = cljs.core.get.call(null,map__36310__$1,new cljs.core.Keyword(null,"formatted-exception","formatted-exception",-116489026));
var exception_data = cljs.core.get.call(null,map__36310__$1,new cljs.core.Keyword(null,"exception-data","exception-data",-512474886));
var cause = cljs.core.get.call(null,map__36310__$1,new cljs.core.Keyword(null,"cause","cause",231901252));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: Compile Exception");

var seq__36312_36316 = cljs.core.seq.call(null,figwheel.client.format_messages.call(null,exception_data));
var chunk__36313_36317 = null;
var count__36314_36318 = (0);
var i__36315_36319 = (0);
while(true){
if((i__36315_36319 < count__36314_36318)){
var msg_36320 = cljs.core._nth.call(null,chunk__36313_36317,i__36315_36319);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_36320);

var G__36321 = seq__36312_36316;
var G__36322 = chunk__36313_36317;
var G__36323 = count__36314_36318;
var G__36324 = (i__36315_36319 + (1));
seq__36312_36316 = G__36321;
chunk__36313_36317 = G__36322;
count__36314_36318 = G__36323;
i__36315_36319 = G__36324;
continue;
} else {
var temp__4657__auto___36325 = cljs.core.seq.call(null,seq__36312_36316);
if(temp__4657__auto___36325){
var seq__36312_36326__$1 = temp__4657__auto___36325;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__36312_36326__$1)){
var c__26710__auto___36327 = cljs.core.chunk_first.call(null,seq__36312_36326__$1);
var G__36328 = cljs.core.chunk_rest.call(null,seq__36312_36326__$1);
var G__36329 = c__26710__auto___36327;
var G__36330 = cljs.core.count.call(null,c__26710__auto___36327);
var G__36331 = (0);
seq__36312_36316 = G__36328;
chunk__36313_36317 = G__36329;
count__36314_36318 = G__36330;
i__36315_36319 = G__36331;
continue;
} else {
var msg_36332 = cljs.core.first.call(null,seq__36312_36326__$1);
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),msg_36332);

var G__36333 = cljs.core.next.call(null,seq__36312_36326__$1);
var G__36334 = null;
var G__36335 = (0);
var G__36336 = (0);
seq__36312_36316 = G__36333;
chunk__36313_36317 = G__36334;
count__36314_36318 = G__36335;
i__36315_36319 = G__36336;
continue;
}
} else {
}
}
break;
}

if(cljs.core.truth_(cause)){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),[cljs.core.str("Error on "),cljs.core.str(figwheel.client.file_line_column.call(null,ed))].join(''));
} else {
}

return ed;
});
figwheel.client.default_on_compile_warning = (function figwheel$client$default_on_compile_warning(p__36337){
var map__36340 = p__36337;
var map__36340__$1 = ((((!((map__36340 == null)))?((((map__36340.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36340.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36340):map__36340);
var w = map__36340__$1;
var message = cljs.core.get.call(null,map__36340__$1,new cljs.core.Keyword(null,"message","message",-406056002));
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"warn","warn",-436710552),[cljs.core.str("Figwheel: Compile Warning - "),cljs.core.str(new cljs.core.Keyword(null,"message","message",-406056002).cljs$core$IFn$_invoke$arity$1(message)),cljs.core.str(" in "),cljs.core.str(figwheel.client.file_line_column.call(null,message))].join(''));

return w;
});
figwheel.client.default_before_load = (function figwheel$client$default_before_load(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: notified of file changes");

return files;
});
figwheel.client.default_on_cssload = (function figwheel$client$default_on_cssload(files){
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded CSS files");

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"info","info",-317069002),cljs.core.pr_str.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files)));

return files;
});
if(typeof figwheel.client.config_defaults !== 'undefined'){
} else {
figwheel.client.config_defaults = cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"on-compile-warning","on-compile-warning",-1195585947),new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430),new cljs.core.Keyword(null,"on-compile-fail","on-compile-fail",728013036),new cljs.core.Keyword(null,"debug","debug",-1608172596),new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202),new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938),new cljs.core.Keyword(null,"auto-jump-to-source-on-error","auto-jump-to-source-on-error",-960314920),new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128),new cljs.core.Keyword(null,"load-warninged-code","load-warninged-code",-2030345223),new cljs.core.Keyword(null,"eval-fn","eval-fn",-1111644294),new cljs.core.Keyword(null,"retry-count","retry-count",1936122875),new cljs.core.Keyword(null,"autoload","autoload",-354122500),new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318)],[new cljs.core.Var(function(){return figwheel.client.default_on_compile_warning;},new cljs.core.Symbol("figwheel.client","default-on-compile-warning","figwheel.client/default-on-compile-warning",584144208,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"figwheel.client","figwheel.client",-538710252,null),new cljs.core.Symbol(null,"default-on-compile-warning","default-on-compile-warning",-18911586,null),"resources\\public\\js\\compiled\\out\\figwheel\\client.cljs",33,1,357,357,cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keys","keys",1068423698),new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"message","message",1234475525,null)], null),new cljs.core.Keyword(null,"as","as",1148689641),new cljs.core.Symbol(null,"w","w",1994700528,null)], null)], null)),null,(cljs.core.truth_(figwheel.client.default_on_compile_warning)?figwheel.client.default_on_compile_warning.cljs$lang$test:null)])),figwheel.client.default_on_jsload,true,new cljs.core.Var(function(){return figwheel.client.default_on_compile_fail;},new cljs.core.Symbol("figwheel.client","default-on-compile-fail","figwheel.client/default-on-compile-fail",1384826337,null),cljs.core.PersistentHashMap.fromArrays([new cljs.core.Keyword(null,"ns","ns",441598760),new cljs.core.Keyword(null,"name","name",1843675177),new cljs.core.Keyword(null,"file","file",-1269645878),new cljs.core.Keyword(null,"end-column","end-column",1425389514),new cljs.core.Keyword(null,"column","column",2078222095),new cljs.core.Keyword(null,"line","line",212345235),new cljs.core.Keyword(null,"end-line","end-line",1837326455),new cljs.core.Keyword(null,"arglists","arglists",1661989754),new cljs.core.Keyword(null,"doc","doc",1913296891),new cljs.core.Keyword(null,"test","test",577538877)],[new cljs.core.Symbol(null,"figwheel.client","figwheel.client",-538710252,null),new cljs.core.Symbol(null,"default-on-compile-fail","default-on-compile-fail",-158814813,null),"resources\\public\\js\\compiled\\out\\figwheel\\client.cljs",30,1,349,349,cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"keys","keys",1068423698),new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"formatted-exception","formatted-exception",1524042501,null),new cljs.core.Symbol(null,"exception-data","exception-data",1128056641,null),new cljs.core.Symbol(null,"cause","cause",1872432779,null)], null),new cljs.core.Keyword(null,"as","as",1148689641),new cljs.core.Symbol(null,"ed","ed",2076825751,null)], null)], null)),null,(cljs.core.truth_(figwheel.client.default_on_compile_fail)?figwheel.client.default_on_compile_fail.cljs$lang$test:null)])),false,true,[cljs.core.str("ws://"),cljs.core.str((cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))?location.host:"localhost:3449")),cljs.core.str("/figwheel-ws")].join(''),false,figwheel.client.default_before_load,false,false,(100),true,figwheel.client.default_on_cssload]);
}
figwheel.client.handle_deprecated_jsload_callback = (function figwheel$client$handle_deprecated_jsload_callback(config){
if(cljs.core.truth_(new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config))){
return cljs.core.dissoc.call(null,cljs.core.assoc.call(null,config,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369).cljs$core$IFn$_invoke$arity$1(config)),new cljs.core.Keyword(null,"jsload-callback","jsload-callback",-1949628369));
} else {
return config;
}
});
figwheel.client.fill_url_template = (function figwheel$client$fill_url_template(config){
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
return cljs.core.update_in.call(null,config,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"websocket-url","websocket-url",-490444938)], null),(function (x){
return clojure.string.replace.call(null,clojure.string.replace.call(null,x,"[[client-hostname]]",location.hostname),"[[client-port]]",location.port);
}));
} else {
return config;
}
});
figwheel.client.base_plugins = (function figwheel$client$base_plugins(system_options){
var base = new cljs.core.PersistentArrayMap(null, 6, [new cljs.core.Keyword(null,"enforce-project-plugin","enforce-project-plugin",959402899),figwheel.client.enforce_project_plugin,new cljs.core.Keyword(null,"enforce-figwheel-version-plugin","enforce-figwheel-version-plugin",-1916185220),figwheel.client.enforce_figwheel_version_plugin,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),figwheel.client.file_reloader_plugin,new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),figwheel.client.compile_fail_warning_plugin,new cljs.core.Keyword(null,"css-reloader-plugin","css-reloader-plugin",2002032904),figwheel.client.css_reloader_plugin,new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371),figwheel.client.repl_plugin], null);
var base__$1 = ((cljs.core.not.call(null,figwheel.client.utils.html_env_QMARK_.call(null)))?cljs.core.select_keys.call(null,base,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733),new cljs.core.Keyword(null,"comp-fail-warning-plugin","comp-fail-warning-plugin",634311),new cljs.core.Keyword(null,"repl-plugin","repl-plugin",-1138952371)], null)):base);
var base__$2 = ((new cljs.core.Keyword(null,"autoload","autoload",-354122500).cljs$core$IFn$_invoke$arity$1(system_options) === false)?cljs.core.dissoc.call(null,base__$1,new cljs.core.Keyword(null,"file-reloader-plugin","file-reloader-plugin",-1792964733)):base__$1);
if(cljs.core.truth_((function (){var and__25887__auto__ = new cljs.core.Keyword(null,"heads-up-display","heads-up-display",-896577202).cljs$core$IFn$_invoke$arity$1(system_options);
if(cljs.core.truth_(and__25887__auto__)){
return figwheel.client.utils.html_env_QMARK_.call(null);
} else {
return and__25887__auto__;
}
})())){
return cljs.core.assoc.call(null,base__$2,new cljs.core.Keyword(null,"heads-up-display-plugin","heads-up-display-plugin",1745207501),figwheel.client.heads_up_plugin);
} else {
return base__$2;
}
});
figwheel.client.add_message_watch = (function figwheel$client$add_message_watch(key,callback){
return cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,key,(function (_,___$1,___$2,msg_hist){
return callback.call(null,cljs.core.first.call(null,msg_hist));
}));
});
figwheel.client.add_plugins = (function figwheel$client$add_plugins(plugins,system_options){
var seq__36352 = cljs.core.seq.call(null,plugins);
var chunk__36353 = null;
var count__36354 = (0);
var i__36355 = (0);
while(true){
if((i__36355 < count__36354)){
var vec__36356 = cljs.core._nth.call(null,chunk__36353,i__36355);
var k = cljs.core.nth.call(null,vec__36356,(0),null);
var plugin = cljs.core.nth.call(null,vec__36356,(1),null);
if(cljs.core.truth_(plugin)){
var pl_36362 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__36352,chunk__36353,count__36354,i__36355,pl_36362,vec__36356,k,plugin){
return (function (_,___$1,___$2,msg_hist){
return pl_36362.call(null,msg_hist);
});})(seq__36352,chunk__36353,count__36354,i__36355,pl_36362,vec__36356,k,plugin))
);
} else {
}

var G__36363 = seq__36352;
var G__36364 = chunk__36353;
var G__36365 = count__36354;
var G__36366 = (i__36355 + (1));
seq__36352 = G__36363;
chunk__36353 = G__36364;
count__36354 = G__36365;
i__36355 = G__36366;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__36352);
if(temp__4657__auto__){
var seq__36352__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__36352__$1)){
var c__26710__auto__ = cljs.core.chunk_first.call(null,seq__36352__$1);
var G__36367 = cljs.core.chunk_rest.call(null,seq__36352__$1);
var G__36368 = c__26710__auto__;
var G__36369 = cljs.core.count.call(null,c__26710__auto__);
var G__36370 = (0);
seq__36352 = G__36367;
chunk__36353 = G__36368;
count__36354 = G__36369;
i__36355 = G__36370;
continue;
} else {
var vec__36359 = cljs.core.first.call(null,seq__36352__$1);
var k = cljs.core.nth.call(null,vec__36359,(0),null);
var plugin = cljs.core.nth.call(null,vec__36359,(1),null);
if(cljs.core.truth_(plugin)){
var pl_36371 = plugin.call(null,system_options);
cljs.core.add_watch.call(null,figwheel.client.socket.message_history_atom,k,((function (seq__36352,chunk__36353,count__36354,i__36355,pl_36371,vec__36359,k,plugin,seq__36352__$1,temp__4657__auto__){
return (function (_,___$1,___$2,msg_hist){
return pl_36371.call(null,msg_hist);
});})(seq__36352,chunk__36353,count__36354,i__36355,pl_36371,vec__36359,k,plugin,seq__36352__$1,temp__4657__auto__))
);
} else {
}

var G__36372 = cljs.core.next.call(null,seq__36352__$1);
var G__36373 = null;
var G__36374 = (0);
var G__36375 = (0);
seq__36352 = G__36372;
chunk__36353 = G__36373;
count__36354 = G__36374;
i__36355 = G__36375;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.start = (function figwheel$client$start(var_args){
var args36376 = [];
var len__26974__auto___36383 = arguments.length;
var i__26975__auto___36384 = (0);
while(true){
if((i__26975__auto___36384 < len__26974__auto___36383)){
args36376.push((arguments[i__26975__auto___36384]));

var G__36385 = (i__26975__auto___36384 + (1));
i__26975__auto___36384 = G__36385;
continue;
} else {
}
break;
}

var G__36378 = args36376.length;
switch (G__36378) {
case 1:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 0:
return figwheel.client.start.cljs$core$IFn$_invoke$arity$0();

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args36376.length)].join('')));

}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$1 = (function (opts){
if((goog.dependencies_ == null)){
return null;
} else {
if(typeof figwheel.client.__figwheel_start_once__ !== 'undefined'){
return null;
} else {
figwheel.client.__figwheel_start_once__ = setTimeout((function (){
var plugins_SINGLEQUOTE_ = new cljs.core.Keyword(null,"plugins","plugins",1900073717).cljs$core$IFn$_invoke$arity$1(opts);
var merge_plugins = new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370).cljs$core$IFn$_invoke$arity$1(opts);
var system_options = figwheel.client.fill_url_template.call(null,figwheel.client.handle_deprecated_jsload_callback.call(null,cljs.core.merge.call(null,figwheel.client.config_defaults,cljs.core.dissoc.call(null,opts,new cljs.core.Keyword(null,"plugins","plugins",1900073717),new cljs.core.Keyword(null,"merge-plugins","merge-plugins",-1193912370)))));
var plugins = (cljs.core.truth_(plugins_SINGLEQUOTE_)?plugins_SINGLEQUOTE_:cljs.core.merge.call(null,figwheel.client.base_plugins.call(null,system_options),merge_plugins));
figwheel.client.utils._STAR_print_debug_STAR_ = new cljs.core.Keyword(null,"debug","debug",-1608172596).cljs$core$IFn$_invoke$arity$1(opts);

figwheel.client.enable_repl_print_BANG_.call(null);

figwheel.client.add_plugins.call(null,plugins,system_options);

figwheel.client.file_reloading.patch_goog_base.call(null);

var seq__36379_36387 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"initial-messages","initial-messages",2057377771).cljs$core$IFn$_invoke$arity$1(system_options));
var chunk__36380_36388 = null;
var count__36381_36389 = (0);
var i__36382_36390 = (0);
while(true){
if((i__36382_36390 < count__36381_36389)){
var msg_36391 = cljs.core._nth.call(null,chunk__36380_36388,i__36382_36390);
figwheel.client.socket.handle_incoming_message.call(null,msg_36391);

var G__36392 = seq__36379_36387;
var G__36393 = chunk__36380_36388;
var G__36394 = count__36381_36389;
var G__36395 = (i__36382_36390 + (1));
seq__36379_36387 = G__36392;
chunk__36380_36388 = G__36393;
count__36381_36389 = G__36394;
i__36382_36390 = G__36395;
continue;
} else {
var temp__4657__auto___36396 = cljs.core.seq.call(null,seq__36379_36387);
if(temp__4657__auto___36396){
var seq__36379_36397__$1 = temp__4657__auto___36396;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__36379_36397__$1)){
var c__26710__auto___36398 = cljs.core.chunk_first.call(null,seq__36379_36397__$1);
var G__36399 = cljs.core.chunk_rest.call(null,seq__36379_36397__$1);
var G__36400 = c__26710__auto___36398;
var G__36401 = cljs.core.count.call(null,c__26710__auto___36398);
var G__36402 = (0);
seq__36379_36387 = G__36399;
chunk__36380_36388 = G__36400;
count__36381_36389 = G__36401;
i__36382_36390 = G__36402;
continue;
} else {
var msg_36403 = cljs.core.first.call(null,seq__36379_36397__$1);
figwheel.client.socket.handle_incoming_message.call(null,msg_36403);

var G__36404 = cljs.core.next.call(null,seq__36379_36397__$1);
var G__36405 = null;
var G__36406 = (0);
var G__36407 = (0);
seq__36379_36387 = G__36404;
chunk__36380_36388 = G__36405;
count__36381_36389 = G__36406;
i__36382_36390 = G__36407;
continue;
}
} else {
}
}
break;
}

return figwheel.client.socket.open.call(null,system_options);
}));
}
}
});

figwheel.client.start.cljs$core$IFn$_invoke$arity$0 = (function (){
return figwheel.client.start.call(null,cljs.core.PersistentArrayMap.EMPTY);
});

figwheel.client.start.cljs$lang$maxFixedArity = 1;

figwheel.client.watch_and_reload_with_opts = figwheel.client.start;
figwheel.client.watch_and_reload = (function figwheel$client$watch_and_reload(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36412 = arguments.length;
var i__26975__auto___36413 = (0);
while(true){
if((i__26975__auto___36413 < len__26974__auto___36412)){
args__26981__auto__.push((arguments[i__26975__auto___36413]));

var G__36414 = (i__26975__auto___36413 + (1));
i__26975__auto___36413 = G__36414;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic = (function (p__36409){
var map__36410 = p__36409;
var map__36410__$1 = ((((!((map__36410 == null)))?((((map__36410.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36410.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36410):map__36410);
var opts = map__36410__$1;
return figwheel.client.start.call(null,opts);
});

figwheel.client.watch_and_reload.cljs$lang$maxFixedArity = (0);

figwheel.client.watch_and_reload.cljs$lang$applyTo = (function (seq36408){
return figwheel.client.watch_and_reload.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36408));
});

figwheel.client.fetch_data_from_env = (function figwheel$client$fetch_data_from_env(){
try{return cljs.reader.read_string.call(null,goog.object.get(window,"FIGWHEEL_CLIENT_CONFIGURATION"));
}catch (e36416){if((e36416 instanceof Error)){
var e = e36416;
cljs.core._STAR_print_err_fn_STAR_.call(null,"Unable to load FIGWHEEL_CLIENT_CONFIGURATION from the environment");

return new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"autoload","autoload",-354122500),false], null);
} else {
throw e36416;

}
}});
figwheel.client.console_intro_message = "Figwheel has compiled a temporary helper application to your :output-file.\n\nThe code currently in your configured output file does not\nrepresent the code that you are trying to compile.\n\nThis temporary application is intended to help you continue to get\nfeedback from Figwheel until the build you are working on compiles\ncorrectly.\n\nWhen your ClojureScript source code compiles correctly this helper\napplication will auto-reload and pick up your freshly compiled\nClojureScript program.";
figwheel.client.bad_compile_helper_app = (function figwheel$client$bad_compile_helper_app(){
cljs.core.enable_console_print_BANG_.call(null);

var config = figwheel.client.fetch_data_from_env.call(null);
cljs.core.println.call(null,figwheel.client.console_intro_message);

figwheel.client.heads_up.bad_compile_screen.call(null);

if(cljs.core.truth_(goog.dependencies_)){
} else {
goog.dependencies_ = true;
}

figwheel.client.start.call(null,config);

return figwheel.client.add_message_watch.call(null,new cljs.core.Keyword(null,"listen-for-successful-compile","listen-for-successful-compile",-995277603),((function (config){
return (function (p__36420){
var map__36421 = p__36420;
var map__36421__$1 = ((((!((map__36421 == null)))?((((map__36421.cljs$lang$protocol_mask$partition0$ & (64))) || (map__36421.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__36421):map__36421);
var msg_name = cljs.core.get.call(null,map__36421__$1,new cljs.core.Keyword(null,"msg-name","msg-name",-353709863));
if(cljs.core._EQ_.call(null,msg_name,new cljs.core.Keyword(null,"files-changed","files-changed",-1418200563))){
return location.href = location.href;
} else {
return null;
}
});})(config))
);
});

//# sourceMappingURL=client.js.map?rel=1496955727634