// Compiled by ClojureScript 1.9.229 {}
goog.provide('figwheel.client.file_reloading');
goog.require('cljs.core');
goog.require('goog.string');
goog.require('goog.async.Deferred');
goog.require('goog.Uri');
goog.require('goog.net.jsloader');
goog.require('cljs.core.async');
goog.require('goog.object');
goog.require('clojure.set');
goog.require('clojure.string');
goog.require('figwheel.client.utils');
if(typeof figwheel.client.file_reloading.figwheel_meta_pragmas !== 'undefined'){
} else {
figwheel.client.file_reloading.figwheel_meta_pragmas = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
figwheel.client.file_reloading.on_jsload_custom_event = (function figwheel$client$file_reloading$on_jsload_custom_event(url){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.js-reload",url);
});
figwheel.client.file_reloading.before_jsload_custom_event = (function figwheel$client$file_reloading$before_jsload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.before-js-reload",files);
});
figwheel.client.file_reloading.on_cssload_custom_event = (function figwheel$client$file_reloading$on_cssload_custom_event(files){
return figwheel.client.utils.dispatch_custom_event.call(null,"figwheel.css-reload",files);
});
figwheel.client.file_reloading.namespace_file_map_QMARK_ = (function figwheel$client$file_reloading$namespace_file_map_QMARK_(m){
var or__25899__auto__ = (cljs.core.map_QMARK_.call(null,m)) && (typeof new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(m) === 'string') && (((new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) == null)) || (typeof new cljs.core.Keyword(null,"file","file",-1269645878).cljs$core$IFn$_invoke$arity$1(m) === 'string')) && (cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"type","type",1174270348).cljs$core$IFn$_invoke$arity$1(m),new cljs.core.Keyword(null,"namespace","namespace",-377510372)));
if(or__25899__auto__){
return or__25899__auto__;
} else {
cljs.core.println.call(null,"Error not namespace-file-map",cljs.core.pr_str.call(null,m));

return false;
}
});
figwheel.client.file_reloading.add_cache_buster = (function figwheel$client$file_reloading$add_cache_buster(url){

return goog.Uri.parse(url).makeUnique();
});
figwheel.client.file_reloading.name__GT_path = (function figwheel$client$file_reloading$name__GT_path(ns){

return (goog.dependencies_.nameToPath[ns]);
});
figwheel.client.file_reloading.provided_QMARK_ = (function figwheel$client$file_reloading$provided_QMARK_(ns){
return (goog.dependencies_.written[figwheel.client.file_reloading.name__GT_path.call(null,ns)]);
});
figwheel.client.file_reloading.immutable_ns_QMARK_ = (function figwheel$client$file_reloading$immutable_ns_QMARK_(name){
var or__25899__auto__ = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 10, ["svgpan.SvgPan",null,"far.out",null,"cljs.nodejs",null,"testDep.bar",null,"someprotopackage.TestPackageTypes",null,"goog",null,"an.existing.path",null,"cljs.core",null,"ns",null,"dup.base",null], null), null).call(null,name);
if(cljs.core.truth_(or__25899__auto__)){
return or__25899__auto__;
} else {
return cljs.core.some.call(null,cljs.core.partial.call(null,goog.string.startsWith,name),new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, ["goog.","cljs.","clojure.","fake.","proto2."], null));
}
});
figwheel.client.file_reloading.get_requires = (function figwheel$client$file_reloading$get_requires(ns){
return cljs.core.set.call(null,cljs.core.filter.call(null,(function (p1__32811_SHARP_){
return cljs.core.not.call(null,figwheel.client.file_reloading.immutable_ns_QMARK_.call(null,p1__32811_SHARP_));
}),goog.object.getKeys((goog.dependencies_.requires[figwheel.client.file_reloading.name__GT_path.call(null,ns)]))));
});
if(typeof figwheel.client.file_reloading.dependency_data !== 'undefined'){
} else {
figwheel.client.file_reloading.dependency_data = cljs.core.atom.call(null,new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),cljs.core.PersistentArrayMap.EMPTY,new cljs.core.Keyword(null,"dependents","dependents",136812837),cljs.core.PersistentArrayMap.EMPTY], null));
}
figwheel.client.file_reloading.path_to_name_BANG_ = (function figwheel$client$file_reloading$path_to_name_BANG_(path,name){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.fromArray([name], true));
});
/**
 * Setup a path to name dependencies map.
 * That goes from path -> #{ ns-names }
 */
figwheel.client.file_reloading.setup_path__GT_name_BANG_ = (function figwheel$client$file_reloading$setup_path__GT_name_BANG_(){
var nameToPath = goog.object.filter(goog.dependencies_.nameToPath,(function (v,k,o){
return goog.string.startsWith(v,"../");
}));
return goog.object.forEach(nameToPath,((function (nameToPath){
return (function (v,k,o){
return figwheel.client.file_reloading.path_to_name_BANG_.call(null,v,k);
});})(nameToPath))
);
});
/**
 * returns a set of namespaces defined by a path
 */
figwheel.client.file_reloading.path__GT_name = (function figwheel$client$file_reloading$path__GT_name(path){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"pathToName","pathToName",-1236616181),path], null));
});
figwheel.client.file_reloading.name_to_parent_BANG_ = (function figwheel$client$file_reloading$name_to_parent_BANG_(ns,parent_ns){
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependency_data,cljs.core.update_in,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null),cljs.core.fnil.call(null,clojure.set.union,cljs.core.PersistentHashSet.EMPTY),cljs.core.PersistentHashSet.fromArray([parent_ns], true));
});
/**
 * This reverses the goog.dependencies_.requires for looking up ns-dependents.
 */
figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_ = (function figwheel$client$file_reloading$setup_ns__GT_dependents_BANG_(){
var requires = goog.object.filter(goog.dependencies_.requires,(function (v,k,o){
return goog.string.startsWith(k,"../");
}));
return goog.object.forEach(requires,((function (requires){
return (function (v,k,_){
return goog.object.forEach(v,((function (requires){
return (function (v_SINGLEQUOTE_,k_SINGLEQUOTE_,___$1){
var seq__32816 = cljs.core.seq.call(null,figwheel.client.file_reloading.path__GT_name.call(null,k));
var chunk__32817 = null;
var count__32818 = (0);
var i__32819 = (0);
while(true){
if((i__32819 < count__32818)){
var n = cljs.core._nth.call(null,chunk__32817,i__32819);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__32820 = seq__32816;
var G__32821 = chunk__32817;
var G__32822 = count__32818;
var G__32823 = (i__32819 + (1));
seq__32816 = G__32820;
chunk__32817 = G__32821;
count__32818 = G__32822;
i__32819 = G__32823;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__32816);
if(temp__4657__auto__){
var seq__32816__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32816__$1)){
var c__26710__auto__ = cljs.core.chunk_first.call(null,seq__32816__$1);
var G__32824 = cljs.core.chunk_rest.call(null,seq__32816__$1);
var G__32825 = c__26710__auto__;
var G__32826 = cljs.core.count.call(null,c__26710__auto__);
var G__32827 = (0);
seq__32816 = G__32824;
chunk__32817 = G__32825;
count__32818 = G__32826;
i__32819 = G__32827;
continue;
} else {
var n = cljs.core.first.call(null,seq__32816__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,k_SINGLEQUOTE_,n);

var G__32828 = cljs.core.next.call(null,seq__32816__$1);
var G__32829 = null;
var G__32830 = (0);
var G__32831 = (0);
seq__32816 = G__32828;
chunk__32817 = G__32829;
count__32818 = G__32830;
i__32819 = G__32831;
continue;
}
} else {
return null;
}
}
break;
}
});})(requires))
);
});})(requires))
);
});
figwheel.client.file_reloading.ns__GT_dependents = (function figwheel$client$file_reloading$ns__GT_dependents(ns){
return cljs.core.get_in.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.dependency_data),new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"dependents","dependents",136812837),ns], null));
});
figwheel.client.file_reloading.build_topo_sort = (function figwheel$client$file_reloading$build_topo_sort(get_deps){
var get_deps__$1 = cljs.core.memoize.call(null,get_deps);
var topo_sort_helper_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_topo_sort_helper_STAR_(x,depth,state){
var deps = get_deps__$1.call(null,x);
if(cljs.core.empty_QMARK_.call(null,deps)){
return null;
} else {
return topo_sort_STAR_.call(null,deps,depth,state);
}
});})(get_deps__$1))
;
var topo_sort_STAR_ = ((function (get_deps__$1){
return (function() {
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = null;
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1 = (function (deps){
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.call(null,deps,(0),cljs.core.atom.call(null,cljs.core.sorted_map.call(null)));
});
var figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3 = (function (deps,depth,state){
cljs.core.swap_BANG_.call(null,state,cljs.core.update_in,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [depth], null),cljs.core.fnil.call(null,cljs.core.into,cljs.core.PersistentHashSet.EMPTY),deps);

var seq__32882_32893 = cljs.core.seq.call(null,deps);
var chunk__32883_32894 = null;
var count__32884_32895 = (0);
var i__32885_32896 = (0);
while(true){
if((i__32885_32896 < count__32884_32895)){
var dep_32897 = cljs.core._nth.call(null,chunk__32883_32894,i__32885_32896);
topo_sort_helper_STAR_.call(null,dep_32897,(depth + (1)),state);

var G__32898 = seq__32882_32893;
var G__32899 = chunk__32883_32894;
var G__32900 = count__32884_32895;
var G__32901 = (i__32885_32896 + (1));
seq__32882_32893 = G__32898;
chunk__32883_32894 = G__32899;
count__32884_32895 = G__32900;
i__32885_32896 = G__32901;
continue;
} else {
var temp__4657__auto___32902 = cljs.core.seq.call(null,seq__32882_32893);
if(temp__4657__auto___32902){
var seq__32882_32903__$1 = temp__4657__auto___32902;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32882_32903__$1)){
var c__26710__auto___32904 = cljs.core.chunk_first.call(null,seq__32882_32903__$1);
var G__32905 = cljs.core.chunk_rest.call(null,seq__32882_32903__$1);
var G__32906 = c__26710__auto___32904;
var G__32907 = cljs.core.count.call(null,c__26710__auto___32904);
var G__32908 = (0);
seq__32882_32893 = G__32905;
chunk__32883_32894 = G__32906;
count__32884_32895 = G__32907;
i__32885_32896 = G__32908;
continue;
} else {
var dep_32909 = cljs.core.first.call(null,seq__32882_32903__$1);
topo_sort_helper_STAR_.call(null,dep_32909,(depth + (1)),state);

var G__32910 = cljs.core.next.call(null,seq__32882_32903__$1);
var G__32911 = null;
var G__32912 = (0);
var G__32913 = (0);
seq__32882_32893 = G__32910;
chunk__32883_32894 = G__32911;
count__32884_32895 = G__32912;
i__32885_32896 = G__32913;
continue;
}
} else {
}
}
break;
}

if(cljs.core._EQ_.call(null,depth,(0))){
return elim_dups_STAR_.call(null,cljs.core.reverse.call(null,cljs.core.vals.call(null,cljs.core.deref.call(null,state))));
} else {
return null;
}
});
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_ = function(deps,depth,state){
switch(arguments.length){
case 1:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1.call(this,deps);
case 3:
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3.call(this,deps,depth,state);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___1;
figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_.cljs$core$IFn$_invoke$arity$3 = figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR___3;
return figwheel$client$file_reloading$build_topo_sort_$_topo_sort_STAR_;
})()
;})(get_deps__$1))
;
var elim_dups_STAR_ = ((function (get_deps__$1){
return (function figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_(p__32886){
var vec__32890 = p__32886;
var seq__32891 = cljs.core.seq.call(null,vec__32890);
var first__32892 = cljs.core.first.call(null,seq__32891);
var seq__32891__$1 = cljs.core.next.call(null,seq__32891);
var x = first__32892;
var xs = seq__32891__$1;
if((x == null)){
return cljs.core.List.EMPTY;
} else {
return cljs.core.cons.call(null,x,figwheel$client$file_reloading$build_topo_sort_$_elim_dups_STAR_.call(null,cljs.core.map.call(null,((function (vec__32890,seq__32891,first__32892,seq__32891__$1,x,xs,get_deps__$1){
return (function (p1__32832_SHARP_){
return clojure.set.difference.call(null,p1__32832_SHARP_,x);
});})(vec__32890,seq__32891,first__32892,seq__32891__$1,x,xs,get_deps__$1))
,xs)));
}
});})(get_deps__$1))
;
return topo_sort_STAR_;
});
figwheel.client.file_reloading.get_all_dependencies = (function figwheel$client$file_reloading$get_all_dependencies(ns){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.get_requires);
return cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [ns], null))));
});
figwheel.client.file_reloading.get_all_dependents = (function figwheel$client$file_reloading$get_all_dependents(nss){
var topo_sort_SINGLEQUOTE_ = figwheel.client.file_reloading.build_topo_sort.call(null,figwheel.client.file_reloading.ns__GT_dependents);
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,figwheel.client.file_reloading.immutable_ns_QMARK_),cljs.core.reverse.call(null,cljs.core.apply.call(null,cljs.core.concat,topo_sort_SINGLEQUOTE_.call(null,cljs.core.set.call(null,nss)))));
});
figwheel.client.file_reloading.unprovide_BANG_ = (function figwheel$client$file_reloading$unprovide_BANG_(ns){
var path = figwheel.client.file_reloading.name__GT_path.call(null,ns);
goog.object.remove(goog.dependencies_.visited,path);

goog.object.remove(goog.dependencies_.written,path);

return goog.object.remove(goog.dependencies_.written,[cljs.core.str(goog.basePath),cljs.core.str(path)].join(''));
});
figwheel.client.file_reloading.resolve_ns = (function figwheel$client$file_reloading$resolve_ns(ns){
return [cljs.core.str(goog.basePath),cljs.core.str(figwheel.client.file_reloading.name__GT_path.call(null,ns))].join('');
});
figwheel.client.file_reloading.addDependency = (function figwheel$client$file_reloading$addDependency(path,provides,requires){
var seq__32926 = cljs.core.seq.call(null,provides);
var chunk__32927 = null;
var count__32928 = (0);
var i__32929 = (0);
while(true){
if((i__32929 < count__32928)){
var prov = cljs.core._nth.call(null,chunk__32927,i__32929);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__32930_32938 = cljs.core.seq.call(null,requires);
var chunk__32931_32939 = null;
var count__32932_32940 = (0);
var i__32933_32941 = (0);
while(true){
if((i__32933_32941 < count__32932_32940)){
var req_32942 = cljs.core._nth.call(null,chunk__32931_32939,i__32933_32941);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_32942,prov);

var G__32943 = seq__32930_32938;
var G__32944 = chunk__32931_32939;
var G__32945 = count__32932_32940;
var G__32946 = (i__32933_32941 + (1));
seq__32930_32938 = G__32943;
chunk__32931_32939 = G__32944;
count__32932_32940 = G__32945;
i__32933_32941 = G__32946;
continue;
} else {
var temp__4657__auto___32947 = cljs.core.seq.call(null,seq__32930_32938);
if(temp__4657__auto___32947){
var seq__32930_32948__$1 = temp__4657__auto___32947;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32930_32948__$1)){
var c__26710__auto___32949 = cljs.core.chunk_first.call(null,seq__32930_32948__$1);
var G__32950 = cljs.core.chunk_rest.call(null,seq__32930_32948__$1);
var G__32951 = c__26710__auto___32949;
var G__32952 = cljs.core.count.call(null,c__26710__auto___32949);
var G__32953 = (0);
seq__32930_32938 = G__32950;
chunk__32931_32939 = G__32951;
count__32932_32940 = G__32952;
i__32933_32941 = G__32953;
continue;
} else {
var req_32954 = cljs.core.first.call(null,seq__32930_32948__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_32954,prov);

var G__32955 = cljs.core.next.call(null,seq__32930_32948__$1);
var G__32956 = null;
var G__32957 = (0);
var G__32958 = (0);
seq__32930_32938 = G__32955;
chunk__32931_32939 = G__32956;
count__32932_32940 = G__32957;
i__32933_32941 = G__32958;
continue;
}
} else {
}
}
break;
}

var G__32959 = seq__32926;
var G__32960 = chunk__32927;
var G__32961 = count__32928;
var G__32962 = (i__32929 + (1));
seq__32926 = G__32959;
chunk__32927 = G__32960;
count__32928 = G__32961;
i__32929 = G__32962;
continue;
} else {
var temp__4657__auto__ = cljs.core.seq.call(null,seq__32926);
if(temp__4657__auto__){
var seq__32926__$1 = temp__4657__auto__;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32926__$1)){
var c__26710__auto__ = cljs.core.chunk_first.call(null,seq__32926__$1);
var G__32963 = cljs.core.chunk_rest.call(null,seq__32926__$1);
var G__32964 = c__26710__auto__;
var G__32965 = cljs.core.count.call(null,c__26710__auto__);
var G__32966 = (0);
seq__32926 = G__32963;
chunk__32927 = G__32964;
count__32928 = G__32965;
i__32929 = G__32966;
continue;
} else {
var prov = cljs.core.first.call(null,seq__32926__$1);
figwheel.client.file_reloading.path_to_name_BANG_.call(null,path,prov);

var seq__32934_32967 = cljs.core.seq.call(null,requires);
var chunk__32935_32968 = null;
var count__32936_32969 = (0);
var i__32937_32970 = (0);
while(true){
if((i__32937_32970 < count__32936_32969)){
var req_32971 = cljs.core._nth.call(null,chunk__32935_32968,i__32937_32970);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_32971,prov);

var G__32972 = seq__32934_32967;
var G__32973 = chunk__32935_32968;
var G__32974 = count__32936_32969;
var G__32975 = (i__32937_32970 + (1));
seq__32934_32967 = G__32972;
chunk__32935_32968 = G__32973;
count__32936_32969 = G__32974;
i__32937_32970 = G__32975;
continue;
} else {
var temp__4657__auto___32976__$1 = cljs.core.seq.call(null,seq__32934_32967);
if(temp__4657__auto___32976__$1){
var seq__32934_32977__$1 = temp__4657__auto___32976__$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32934_32977__$1)){
var c__26710__auto___32978 = cljs.core.chunk_first.call(null,seq__32934_32977__$1);
var G__32979 = cljs.core.chunk_rest.call(null,seq__32934_32977__$1);
var G__32980 = c__26710__auto___32978;
var G__32981 = cljs.core.count.call(null,c__26710__auto___32978);
var G__32982 = (0);
seq__32934_32967 = G__32979;
chunk__32935_32968 = G__32980;
count__32936_32969 = G__32981;
i__32937_32970 = G__32982;
continue;
} else {
var req_32983 = cljs.core.first.call(null,seq__32934_32977__$1);
figwheel.client.file_reloading.name_to_parent_BANG_.call(null,req_32983,prov);

var G__32984 = cljs.core.next.call(null,seq__32934_32977__$1);
var G__32985 = null;
var G__32986 = (0);
var G__32987 = (0);
seq__32934_32967 = G__32984;
chunk__32935_32968 = G__32985;
count__32936_32969 = G__32986;
i__32937_32970 = G__32987;
continue;
}
} else {
}
}
break;
}

var G__32988 = cljs.core.next.call(null,seq__32926__$1);
var G__32989 = null;
var G__32990 = (0);
var G__32991 = (0);
seq__32926 = G__32988;
chunk__32927 = G__32989;
count__32928 = G__32990;
i__32929 = G__32991;
continue;
}
} else {
return null;
}
}
break;
}
});
figwheel.client.file_reloading.figwheel_require = (function figwheel$client$file_reloading$figwheel_require(src,reload){
goog.require = figwheel$client$file_reloading$figwheel_require;

if(cljs.core._EQ_.call(null,reload,"reload-all")){
var seq__32996_33000 = cljs.core.seq.call(null,figwheel.client.file_reloading.get_all_dependencies.call(null,src));
var chunk__32997_33001 = null;
var count__32998_33002 = (0);
var i__32999_33003 = (0);
while(true){
if((i__32999_33003 < count__32998_33002)){
var ns_33004 = cljs.core._nth.call(null,chunk__32997_33001,i__32999_33003);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_33004);

var G__33005 = seq__32996_33000;
var G__33006 = chunk__32997_33001;
var G__33007 = count__32998_33002;
var G__33008 = (i__32999_33003 + (1));
seq__32996_33000 = G__33005;
chunk__32997_33001 = G__33006;
count__32998_33002 = G__33007;
i__32999_33003 = G__33008;
continue;
} else {
var temp__4657__auto___33009 = cljs.core.seq.call(null,seq__32996_33000);
if(temp__4657__auto___33009){
var seq__32996_33010__$1 = temp__4657__auto___33009;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__32996_33010__$1)){
var c__26710__auto___33011 = cljs.core.chunk_first.call(null,seq__32996_33010__$1);
var G__33012 = cljs.core.chunk_rest.call(null,seq__32996_33010__$1);
var G__33013 = c__26710__auto___33011;
var G__33014 = cljs.core.count.call(null,c__26710__auto___33011);
var G__33015 = (0);
seq__32996_33000 = G__33012;
chunk__32997_33001 = G__33013;
count__32998_33002 = G__33014;
i__32999_33003 = G__33015;
continue;
} else {
var ns_33016 = cljs.core.first.call(null,seq__32996_33010__$1);
figwheel.client.file_reloading.unprovide_BANG_.call(null,ns_33016);

var G__33017 = cljs.core.next.call(null,seq__32996_33010__$1);
var G__33018 = null;
var G__33019 = (0);
var G__33020 = (0);
seq__32996_33000 = G__33017;
chunk__32997_33001 = G__33018;
count__32998_33002 = G__33019;
i__32999_33003 = G__33020;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(reload)){
figwheel.client.file_reloading.unprovide_BANG_.call(null,src);
} else {
}

return goog.require_figwheel_backup_(src);
});
/**
 * Reusable browser REPL bootstrapping. Patches the essential functions
 *   in goog.base to support re-loading of namespaces after page load.
 */
figwheel.client.file_reloading.bootstrap_goog_base = (function figwheel$client$file_reloading$bootstrap_goog_base(){
if(cljs.core.truth_(COMPILED)){
return null;
} else {
goog.require_figwheel_backup_ = (function (){var or__25899__auto__ = goog.require__;
if(cljs.core.truth_(or__25899__auto__)){
return or__25899__auto__;
} else {
return goog.require;
}
})();

goog.isProvided_ = (function (name){
return false;
});

figwheel.client.file_reloading.setup_path__GT_name_BANG_.call(null);

figwheel.client.file_reloading.setup_ns__GT_dependents_BANG_.call(null);

goog.addDependency_figwheel_backup_ = goog.addDependency;

goog.addDependency = (function() { 
var G__33021__delegate = function (args){
cljs.core.apply.call(null,figwheel.client.file_reloading.addDependency,args);

return cljs.core.apply.call(null,goog.addDependency_figwheel_backup_,args);
};
var G__33021 = function (var_args){
var args = null;
if (arguments.length > 0) {
var G__33022__i = 0, G__33022__a = new Array(arguments.length -  0);
while (G__33022__i < G__33022__a.length) {G__33022__a[G__33022__i] = arguments[G__33022__i + 0]; ++G__33022__i;}
  args = new cljs.core.IndexedSeq(G__33022__a,0);
} 
return G__33021__delegate.call(this,args);};
G__33021.cljs$lang$maxFixedArity = 0;
G__33021.cljs$lang$applyTo = (function (arglist__33023){
var args = cljs.core.seq(arglist__33023);
return G__33021__delegate(args);
});
G__33021.cljs$core$IFn$_invoke$arity$variadic = G__33021__delegate;
return G__33021;
})()
;

goog.constructNamespace_("cljs.user");

goog.global.CLOSURE_IMPORT_SCRIPT = figwheel.client.file_reloading.queued_file_reload;

return goog.require = figwheel.client.file_reloading.figwheel_require;
}
});
figwheel.client.file_reloading.patch_goog_base = (function figwheel$client$file_reloading$patch_goog_base(){
if(typeof figwheel.client.file_reloading.bootstrapped_cljs !== 'undefined'){
return null;
} else {
figwheel.client.file_reloading.bootstrapped_cljs = (function (){
figwheel.client.file_reloading.bootstrap_goog_base.call(null);

return true;
})()
;
}
});
figwheel.client.file_reloading.reload_file_in_html_env = (function figwheel$client$file_reloading$reload_file_in_html_env(request_url,callback){

var deferred = goog.net.jsloader.load(figwheel.client.file_reloading.add_cache_buster.call(null,request_url),({"cleanupWhenDone": true}));
deferred.addCallback(((function (deferred){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [true], null));
});})(deferred))
);

return deferred.addErrback(((function (deferred){
return (function (){
return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [false], null));
});})(deferred))
);
});
figwheel.client.file_reloading.reload_file_STAR_ = (function (){var pred__33024 = cljs.core._EQ_;
var expr__33025 = figwheel.client.utils.host_env_QMARK_.call(null);
if(cljs.core.truth_(pred__33024.call(null,new cljs.core.Keyword(null,"node","node",581201198),expr__33025))){
var node_path_lib = require("path");
var util_pattern = [cljs.core.str(node_path_lib.sep),cljs.core.str(node_path_lib.join("goog","bootstrap","nodejs.js"))].join('');
var util_path = goog.object.findKey(require.cache,((function (node_path_lib,util_pattern,pred__33024,expr__33025){
return (function (v,k,o){
return goog.string.endsWith(k,util_pattern);
});})(node_path_lib,util_pattern,pred__33024,expr__33025))
);
var parts = cljs.core.pop.call(null,cljs.core.pop.call(null,clojure.string.split.call(null,util_path,/[\/\\]/)));
var root_path = clojure.string.join.call(null,node_path_lib.sep,parts);
return ((function (node_path_lib,util_pattern,util_path,parts,root_path,pred__33024,expr__33025){
return (function (request_url,callback){

var cache_path = node_path_lib.resolve(root_path,request_url);
goog.object.remove(require.cache,cache_path);

return callback.call(null,(function (){try{return require(cache_path);
}catch (e33027){if((e33027 instanceof Error)){
var e = e33027;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(cache_path)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e33027;

}
}})());
});
;})(node_path_lib,util_pattern,util_path,parts,root_path,pred__33024,expr__33025))
} else {
if(cljs.core.truth_(pred__33024.call(null,new cljs.core.Keyword(null,"html","html",-998796897),expr__33025))){
return figwheel.client.file_reloading.reload_file_in_html_env;
} else {
if(cljs.core.truth_(pred__33024.call(null,new cljs.core.Keyword(null,"react-native","react-native",-1543085138),expr__33025))){
return figwheel.client.file_reloading.reload_file_in_html_env;
} else {
if(cljs.core.truth_(pred__33024.call(null,new cljs.core.Keyword(null,"worker","worker",938239996),expr__33025))){
return ((function (pred__33024,expr__33025){
return (function (request_url,callback){

return callback.call(null,(function (){try{self.importScripts(figwheel.client.file_reloading.add_cache_buster.call(null,request_url));

return true;
}catch (e33028){if((e33028 instanceof Error)){
var e = e33028;
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(request_url)].join(''));

figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),e.stack);

return false;
} else {
throw e33028;

}
}})());
});
;})(pred__33024,expr__33025))
} else {
return ((function (pred__33024,expr__33025){
return (function (a,b){
throw "Reload not defined for this platform";
});
;})(pred__33024,expr__33025))
}
}
}
}
})();
figwheel.client.file_reloading.reload_file = (function figwheel$client$file_reloading$reload_file(p__33029,callback){
var map__33032 = p__33029;
var map__33032__$1 = ((((!((map__33032 == null)))?((((map__33032.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33032.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33032):map__33032);
var file_msg = map__33032__$1;
var request_url = cljs.core.get.call(null,map__33032__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));

figwheel.client.utils.debug_prn.call(null,[cljs.core.str("FigWheel: Attempting to load "),cljs.core.str(request_url)].join(''));

return figwheel.client.file_reloading.reload_file_STAR_.call(null,request_url,((function (map__33032,map__33032__$1,file_msg,request_url){
return (function (success_QMARK_){
if(cljs.core.truth_(success_QMARK_)){
figwheel.client.utils.debug_prn.call(null,[cljs.core.str("FigWheel: Successfully loaded "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.assoc.call(null,file_msg,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),true)], null));
} else {
figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Figwheel: Error loading file "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});})(map__33032,map__33032__$1,file_msg,request_url))
);
});
if(typeof figwheel.client.file_reloading.reload_chan !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_chan = cljs.core.async.chan.call(null);
}
if(typeof figwheel.client.file_reloading.on_load_callbacks !== 'undefined'){
} else {
figwheel.client.file_reloading.on_load_callbacks = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
}
if(typeof figwheel.client.file_reloading.dependencies_loaded !== 'undefined'){
} else {
figwheel.client.file_reloading.dependencies_loaded = cljs.core.atom.call(null,cljs.core.PersistentVector.EMPTY);
}
figwheel.client.file_reloading.blocking_load = (function figwheel$client$file_reloading$blocking_load(url){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.reload_file.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"request-url","request-url",2100346596),url], null),((function (out){
return (function (file_msg){
cljs.core.async.put_BANG_.call(null,out,file_msg);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
if(typeof figwheel.client.file_reloading.reloader_loop !== 'undefined'){
} else {
figwheel.client.file_reloading.reloader_loop = (function (){var c__28077__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto__){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto__){
return (function (state_33056){
var state_val_33057 = (state_33056[(1)]);
if((state_val_33057 === (7))){
var inst_33052 = (state_33056[(2)]);
var state_33056__$1 = state_33056;
var statearr_33058_33078 = state_33056__$1;
(statearr_33058_33078[(2)] = inst_33052);

(statearr_33058_33078[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33057 === (1))){
var state_33056__$1 = state_33056;
var statearr_33059_33079 = state_33056__$1;
(statearr_33059_33079[(2)] = null);

(statearr_33059_33079[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33057 === (4))){
var inst_33036 = (state_33056[(7)]);
var inst_33036__$1 = (state_33056[(2)]);
var state_33056__$1 = (function (){var statearr_33060 = state_33056;
(statearr_33060[(7)] = inst_33036__$1);

return statearr_33060;
})();
if(cljs.core.truth_(inst_33036__$1)){
var statearr_33061_33080 = state_33056__$1;
(statearr_33061_33080[(1)] = (5));

} else {
var statearr_33062_33081 = state_33056__$1;
(statearr_33062_33081[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33057 === (6))){
var state_33056__$1 = state_33056;
var statearr_33063_33082 = state_33056__$1;
(statearr_33063_33082[(2)] = null);

(statearr_33063_33082[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33057 === (3))){
var inst_33054 = (state_33056[(2)]);
var state_33056__$1 = state_33056;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33056__$1,inst_33054);
} else {
if((state_val_33057 === (2))){
var state_33056__$1 = state_33056;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33056__$1,(4),figwheel.client.file_reloading.reload_chan);
} else {
if((state_val_33057 === (11))){
var inst_33048 = (state_33056[(2)]);
var state_33056__$1 = (function (){var statearr_33064 = state_33056;
(statearr_33064[(8)] = inst_33048);

return statearr_33064;
})();
var statearr_33065_33083 = state_33056__$1;
(statearr_33065_33083[(2)] = null);

(statearr_33065_33083[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33057 === (9))){
var inst_33042 = (state_33056[(9)]);
var inst_33040 = (state_33056[(10)]);
var inst_33044 = inst_33042.call(null,inst_33040);
var state_33056__$1 = state_33056;
var statearr_33066_33084 = state_33056__$1;
(statearr_33066_33084[(2)] = inst_33044);

(statearr_33066_33084[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33057 === (5))){
var inst_33036 = (state_33056[(7)]);
var inst_33038 = figwheel.client.file_reloading.blocking_load.call(null,inst_33036);
var state_33056__$1 = state_33056;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33056__$1,(8),inst_33038);
} else {
if((state_val_33057 === (10))){
var inst_33040 = (state_33056[(10)]);
var inst_33046 = cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,cljs.core.conj,inst_33040);
var state_33056__$1 = state_33056;
var statearr_33067_33085 = state_33056__$1;
(statearr_33067_33085[(2)] = inst_33046);

(statearr_33067_33085[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33057 === (8))){
var inst_33036 = (state_33056[(7)]);
var inst_33042 = (state_33056[(9)]);
var inst_33040 = (state_33056[(2)]);
var inst_33041 = cljs.core.deref.call(null,figwheel.client.file_reloading.on_load_callbacks);
var inst_33042__$1 = cljs.core.get.call(null,inst_33041,inst_33036);
var state_33056__$1 = (function (){var statearr_33068 = state_33056;
(statearr_33068[(9)] = inst_33042__$1);

(statearr_33068[(10)] = inst_33040);

return statearr_33068;
})();
if(cljs.core.truth_(inst_33042__$1)){
var statearr_33069_33086 = state_33056__$1;
(statearr_33069_33086[(1)] = (9));

} else {
var statearr_33070_33087 = state_33056__$1;
(statearr_33070_33087[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto__))
;
return ((function (switch__27965__auto__,c__28077__auto__){
return (function() {
var figwheel$client$file_reloading$state_machine__27966__auto__ = null;
var figwheel$client$file_reloading$state_machine__27966__auto____0 = (function (){
var statearr_33074 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_33074[(0)] = figwheel$client$file_reloading$state_machine__27966__auto__);

(statearr_33074[(1)] = (1));

return statearr_33074;
});
var figwheel$client$file_reloading$state_machine__27966__auto____1 = (function (state_33056){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_33056);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e33075){if((e33075 instanceof Object)){
var ex__27969__auto__ = e33075;
var statearr_33076_33088 = state_33056;
(statearr_33076_33088[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33056);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33075;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33089 = state_33056;
state_33056 = G__33089;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
figwheel$client$file_reloading$state_machine__27966__auto__ = function(state_33056){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$state_machine__27966__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$state_machine__27966__auto____1.call(this,state_33056);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$state_machine__27966__auto____0;
figwheel$client$file_reloading$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$state_machine__27966__auto____1;
return figwheel$client$file_reloading$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto__))
})();
var state__28079__auto__ = (function (){var statearr_33077 = f__28078__auto__.call(null);
(statearr_33077[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto__);

return statearr_33077;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto__))
);

return c__28077__auto__;
})();
}
figwheel.client.file_reloading.queued_file_reload = (function figwheel$client$file_reloading$queued_file_reload(url){
return cljs.core.async.put_BANG_.call(null,figwheel.client.file_reloading.reload_chan,url);
});
figwheel.client.file_reloading.require_with_callback = (function figwheel$client$file_reloading$require_with_callback(p__33090,callback){
var map__33093 = p__33090;
var map__33093__$1 = ((((!((map__33093 == null)))?((((map__33093.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33093.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33093):map__33093);
var file_msg = map__33093__$1;
var namespace = cljs.core.get.call(null,map__33093__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var request_url = figwheel.client.file_reloading.resolve_ns.call(null,namespace);
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.assoc,request_url,((function (request_url,map__33093,map__33093__$1,file_msg,namespace){
return (function (file_msg_SINGLEQUOTE_){
cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.on_load_callbacks,cljs.core.dissoc,request_url);

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.merge.call(null,file_msg,cljs.core.select_keys.call(null,file_msg_SINGLEQUOTE_,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375)], null)))], null));
});})(request_url,map__33093,map__33093__$1,file_msg,namespace))
);

return figwheel.client.file_reloading.figwheel_require.call(null,cljs.core.name.call(null,namespace),true);
});
figwheel.client.file_reloading.figwheel_no_load_QMARK_ = (function figwheel$client$file_reloading$figwheel_no_load_QMARK_(p__33095){
var map__33098 = p__33095;
var map__33098__$1 = ((((!((map__33098 == null)))?((((map__33098.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33098.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33098):map__33098);
var file_msg = map__33098__$1;
var namespace = cljs.core.get.call(null,map__33098__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
});
figwheel.client.file_reloading.reload_file_QMARK_ = (function figwheel$client$file_reloading$reload_file_QMARK_(p__33100){
var map__33103 = p__33100;
var map__33103__$1 = ((((!((map__33103 == null)))?((((map__33103.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33103.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33103):map__33103);
var file_msg = map__33103__$1;
var namespace = cljs.core.get.call(null,map__33103__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

var meta_pragmas = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
var and__25887__auto__ = cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,file_msg));
if(and__25887__auto__){
var or__25899__auto__ = new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__25899__auto__)){
return or__25899__auto__;
} else {
var or__25899__auto____$1 = new cljs.core.Keyword(null,"figwheel-load","figwheel-load",1316089175).cljs$core$IFn$_invoke$arity$1(meta_pragmas);
if(cljs.core.truth_(or__25899__auto____$1)){
return or__25899__auto____$1;
} else {
return figwheel.client.file_reloading.provided_QMARK_.call(null,cljs.core.name.call(null,namespace));
}
}
} else {
return and__25887__auto__;
}
});
figwheel.client.file_reloading.js_reload = (function figwheel$client$file_reloading$js_reload(p__33105,callback){
var map__33108 = p__33105;
var map__33108__$1 = ((((!((map__33108 == null)))?((((map__33108.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33108.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33108):map__33108);
var file_msg = map__33108__$1;
var request_url = cljs.core.get.call(null,map__33108__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
var namespace = cljs.core.get.call(null,map__33108__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));

if(cljs.core.truth_(figwheel.client.file_reloading.reload_file_QMARK_.call(null,file_msg))){
return figwheel.client.file_reloading.require_with_callback.call(null,file_msg,callback);
} else {
figwheel.client.utils.debug_prn.call(null,[cljs.core.str("Figwheel: Not trying to load file "),cljs.core.str(request_url)].join(''));

return cljs.core.apply.call(null,callback,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [file_msg], null));
}
});
figwheel.client.file_reloading.reload_js_file = (function figwheel$client$file_reloading$reload_js_file(file_msg){
var out = cljs.core.async.chan.call(null);
figwheel.client.file_reloading.js_reload.call(null,file_msg,((function (out){
return (function (url){
cljs.core.async.put_BANG_.call(null,out,url);

return cljs.core.async.close_BANG_.call(null,out);
});})(out))
);

return out;
});
/**
 * Returns a chanel with one collection of loaded filenames on it.
 */
figwheel.client.file_reloading.load_all_js_files = (function figwheel$client$file_reloading$load_all_js_files(files){
var out = cljs.core.async.chan.call(null);
var c__28077__auto___33212 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___33212,out){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___33212,out){
return (function (state_33194){
var state_val_33195 = (state_33194[(1)]);
if((state_val_33195 === (1))){
var inst_33168 = cljs.core.seq.call(null,files);
var inst_33169 = cljs.core.first.call(null,inst_33168);
var inst_33170 = cljs.core.next.call(null,inst_33168);
var inst_33171 = files;
var state_33194__$1 = (function (){var statearr_33196 = state_33194;
(statearr_33196[(7)] = inst_33169);

(statearr_33196[(8)] = inst_33171);

(statearr_33196[(9)] = inst_33170);

return statearr_33196;
})();
var statearr_33197_33213 = state_33194__$1;
(statearr_33197_33213[(2)] = null);

(statearr_33197_33213[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33195 === (2))){
var inst_33177 = (state_33194[(10)]);
var inst_33171 = (state_33194[(8)]);
var inst_33176 = cljs.core.seq.call(null,inst_33171);
var inst_33177__$1 = cljs.core.first.call(null,inst_33176);
var inst_33178 = cljs.core.next.call(null,inst_33176);
var inst_33179 = (inst_33177__$1 == null);
var inst_33180 = cljs.core.not.call(null,inst_33179);
var state_33194__$1 = (function (){var statearr_33198 = state_33194;
(statearr_33198[(10)] = inst_33177__$1);

(statearr_33198[(11)] = inst_33178);

return statearr_33198;
})();
if(inst_33180){
var statearr_33199_33214 = state_33194__$1;
(statearr_33199_33214[(1)] = (4));

} else {
var statearr_33200_33215 = state_33194__$1;
(statearr_33200_33215[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33195 === (3))){
var inst_33192 = (state_33194[(2)]);
var state_33194__$1 = state_33194;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33194__$1,inst_33192);
} else {
if((state_val_33195 === (4))){
var inst_33177 = (state_33194[(10)]);
var inst_33182 = figwheel.client.file_reloading.reload_js_file.call(null,inst_33177);
var state_33194__$1 = state_33194;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33194__$1,(7),inst_33182);
} else {
if((state_val_33195 === (5))){
var inst_33188 = cljs.core.async.close_BANG_.call(null,out);
var state_33194__$1 = state_33194;
var statearr_33201_33216 = state_33194__$1;
(statearr_33201_33216[(2)] = inst_33188);

(statearr_33201_33216[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33195 === (6))){
var inst_33190 = (state_33194[(2)]);
var state_33194__$1 = state_33194;
var statearr_33202_33217 = state_33194__$1;
(statearr_33202_33217[(2)] = inst_33190);

(statearr_33202_33217[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33195 === (7))){
var inst_33178 = (state_33194[(11)]);
var inst_33184 = (state_33194[(2)]);
var inst_33185 = cljs.core.async.put_BANG_.call(null,out,inst_33184);
var inst_33171 = inst_33178;
var state_33194__$1 = (function (){var statearr_33203 = state_33194;
(statearr_33203[(8)] = inst_33171);

(statearr_33203[(12)] = inst_33185);

return statearr_33203;
})();
var statearr_33204_33218 = state_33194__$1;
(statearr_33204_33218[(2)] = null);

(statearr_33204_33218[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(c__28077__auto___33212,out))
;
return ((function (switch__27965__auto__,c__28077__auto___33212,out){
return (function() {
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto__ = null;
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto____0 = (function (){
var statearr_33208 = [null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33208[(0)] = figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto__);

(statearr_33208[(1)] = (1));

return statearr_33208;
});
var figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto____1 = (function (state_33194){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_33194);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e33209){if((e33209 instanceof Object)){
var ex__27969__auto__ = e33209;
var statearr_33210_33219 = state_33194;
(statearr_33210_33219[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33194);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33209;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33220 = state_33194;
state_33194 = G__33220;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto__ = function(state_33194){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto____1.call(this,state_33194);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto____0;
figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto____1;
return figwheel$client$file_reloading$load_all_js_files_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___33212,out))
})();
var state__28079__auto__ = (function (){var statearr_33211 = f__28078__auto__.call(null);
(statearr_33211[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___33212);

return statearr_33211;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___33212,out))
);


return cljs.core.async.into.call(null,cljs.core.PersistentVector.EMPTY,out);
});
figwheel.client.file_reloading.eval_body = (function figwheel$client$file_reloading$eval_body(p__33221,opts){
var map__33225 = p__33221;
var map__33225__$1 = ((((!((map__33225 == null)))?((((map__33225.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33225.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33225):map__33225);
var eval_body__$1 = cljs.core.get.call(null,map__33225__$1,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883));
var file = cljs.core.get.call(null,map__33225__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_((function (){var and__25887__auto__ = eval_body__$1;
if(cljs.core.truth_(and__25887__auto__)){
return typeof eval_body__$1 === 'string';
} else {
return and__25887__auto__;
}
})())){
var code = eval_body__$1;
try{figwheel.client.utils.debug_prn.call(null,[cljs.core.str("Evaling file "),cljs.core.str(file)].join(''));

return figwheel.client.utils.eval_helper.call(null,code,opts);
}catch (e33227){var e = e33227;
return figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"error","error",-978969032),[cljs.core.str("Unable to evaluate "),cljs.core.str(file)].join(''));
}} else {
return null;
}
});
figwheel.client.file_reloading.expand_files = (function figwheel$client$file_reloading$expand_files(files){
var deps = figwheel.client.file_reloading.get_all_dependents.call(null,cljs.core.map.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,cljs.core.not,cljs.core.partial.call(null,cljs.core.re_matches,/figwheel\.connect.*/),new cljs.core.Keyword(null,"namespace","namespace",-377510372)),cljs.core.map.call(null,((function (deps){
return (function (n){
var temp__4655__auto__ = cljs.core.first.call(null,cljs.core.filter.call(null,((function (deps){
return (function (p1__33228_SHARP_){
return cljs.core._EQ_.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__33228_SHARP_),n);
});})(deps))
,files));
if(cljs.core.truth_(temp__4655__auto__)){
var file_msg = temp__4655__auto__;
return file_msg;
} else {
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372),new cljs.core.Keyword(null,"namespace","namespace",-377510372),n], null);
}
});})(deps))
,deps));
});
figwheel.client.file_reloading.sort_files = (function figwheel$client$file_reloading$sort_files(files){
if((cljs.core.count.call(null,files) <= (1))){
return files;
} else {
var keep_files = cljs.core.set.call(null,cljs.core.keep.call(null,new cljs.core.Keyword(null,"namespace","namespace",-377510372),files));
return cljs.core.filter.call(null,cljs.core.comp.call(null,keep_files,new cljs.core.Keyword(null,"namespace","namespace",-377510372)),figwheel.client.file_reloading.expand_files.call(null,files));
}
});
figwheel.client.file_reloading.get_figwheel_always = (function figwheel$client$file_reloading$get_figwheel_always(){
return cljs.core.map.call(null,(function (p__33237){
var vec__33238 = p__33237;
var k = cljs.core.nth.call(null,vec__33238,(0),null);
var v = cljs.core.nth.call(null,vec__33238,(1),null);
return new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"namespace","namespace",-377510372),k,new cljs.core.Keyword(null,"type","type",1174270348),new cljs.core.Keyword(null,"namespace","namespace",-377510372)], null);
}),cljs.core.filter.call(null,(function (p__33241){
var vec__33242 = p__33241;
var k = cljs.core.nth.call(null,vec__33242,(0),null);
var v = cljs.core.nth.call(null,vec__33242,(1),null);
return new cljs.core.Keyword(null,"figwheel-always","figwheel-always",799819691).cljs$core$IFn$_invoke$arity$1(v);
}),cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas)));
});
figwheel.client.file_reloading.reload_js_files = (function figwheel$client$file_reloading$reload_js_files(p__33248,p__33249){
var map__33496 = p__33248;
var map__33496__$1 = ((((!((map__33496 == null)))?((((map__33496.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33496.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33496):map__33496);
var opts = map__33496__$1;
var before_jsload = cljs.core.get.call(null,map__33496__$1,new cljs.core.Keyword(null,"before-jsload","before-jsload",-847513128));
var on_jsload = cljs.core.get.call(null,map__33496__$1,new cljs.core.Keyword(null,"on-jsload","on-jsload",-395756602));
var reload_dependents = cljs.core.get.call(null,map__33496__$1,new cljs.core.Keyword(null,"reload-dependents","reload-dependents",-956865430));
var map__33497 = p__33249;
var map__33497__$1 = ((((!((map__33497 == null)))?((((map__33497.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33497.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33497):map__33497);
var msg = map__33497__$1;
var files = cljs.core.get.call(null,map__33497__$1,new cljs.core.Keyword(null,"files","files",-472457450));
var figwheel_meta = cljs.core.get.call(null,map__33497__$1,new cljs.core.Keyword(null,"figwheel-meta","figwheel-meta",-225970237));
var recompile_dependents = cljs.core.get.call(null,map__33497__$1,new cljs.core.Keyword(null,"recompile-dependents","recompile-dependents",523804171));
if(cljs.core.empty_QMARK_.call(null,figwheel_meta)){
} else {
cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas,figwheel_meta);
}

var c__28077__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (state_33650){
var state_val_33651 = (state_33650[(1)]);
if((state_val_33651 === (7))){
var inst_33514 = (state_33650[(7)]);
var inst_33512 = (state_33650[(8)]);
var inst_33513 = (state_33650[(9)]);
var inst_33511 = (state_33650[(10)]);
var inst_33519 = cljs.core._nth.call(null,inst_33512,inst_33514);
var inst_33520 = figwheel.client.file_reloading.eval_body.call(null,inst_33519,opts);
var inst_33521 = (inst_33514 + (1));
var tmp33652 = inst_33512;
var tmp33653 = inst_33513;
var tmp33654 = inst_33511;
var inst_33511__$1 = tmp33654;
var inst_33512__$1 = tmp33652;
var inst_33513__$1 = tmp33653;
var inst_33514__$1 = inst_33521;
var state_33650__$1 = (function (){var statearr_33655 = state_33650;
(statearr_33655[(7)] = inst_33514__$1);

(statearr_33655[(8)] = inst_33512__$1);

(statearr_33655[(9)] = inst_33513__$1);

(statearr_33655[(10)] = inst_33511__$1);

(statearr_33655[(11)] = inst_33520);

return statearr_33655;
})();
var statearr_33656_33742 = state_33650__$1;
(statearr_33656_33742[(2)] = null);

(statearr_33656_33742[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (20))){
var inst_33554 = (state_33650[(12)]);
var inst_33562 = figwheel.client.file_reloading.sort_files.call(null,inst_33554);
var state_33650__$1 = state_33650;
var statearr_33657_33743 = state_33650__$1;
(statearr_33657_33743[(2)] = inst_33562);

(statearr_33657_33743[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (27))){
var state_33650__$1 = state_33650;
var statearr_33658_33744 = state_33650__$1;
(statearr_33658_33744[(2)] = null);

(statearr_33658_33744[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (1))){
var inst_33503 = (state_33650[(13)]);
var inst_33500 = before_jsload.call(null,files);
var inst_33501 = figwheel.client.file_reloading.before_jsload_custom_event.call(null,files);
var inst_33502 = (function (){return ((function (inst_33503,inst_33500,inst_33501,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__33245_SHARP_){
return new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__33245_SHARP_);
});
;})(inst_33503,inst_33500,inst_33501,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_33503__$1 = cljs.core.filter.call(null,inst_33502,files);
var inst_33504 = cljs.core.not_empty.call(null,inst_33503__$1);
var state_33650__$1 = (function (){var statearr_33659 = state_33650;
(statearr_33659[(14)] = inst_33500);

(statearr_33659[(15)] = inst_33501);

(statearr_33659[(13)] = inst_33503__$1);

return statearr_33659;
})();
if(cljs.core.truth_(inst_33504)){
var statearr_33660_33745 = state_33650__$1;
(statearr_33660_33745[(1)] = (2));

} else {
var statearr_33661_33746 = state_33650__$1;
(statearr_33661_33746[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (24))){
var state_33650__$1 = state_33650;
var statearr_33662_33747 = state_33650__$1;
(statearr_33662_33747[(2)] = null);

(statearr_33662_33747[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (39))){
var inst_33604 = (state_33650[(16)]);
var state_33650__$1 = state_33650;
var statearr_33663_33748 = state_33650__$1;
(statearr_33663_33748[(2)] = inst_33604);

(statearr_33663_33748[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (46))){
var inst_33645 = (state_33650[(2)]);
var state_33650__$1 = state_33650;
var statearr_33664_33749 = state_33650__$1;
(statearr_33664_33749[(2)] = inst_33645);

(statearr_33664_33749[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (4))){
var inst_33548 = (state_33650[(2)]);
var inst_33549 = cljs.core.List.EMPTY;
var inst_33550 = cljs.core.reset_BANG_.call(null,figwheel.client.file_reloading.dependencies_loaded,inst_33549);
var inst_33551 = (function (){return ((function (inst_33548,inst_33549,inst_33550,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__33246_SHARP_){
var and__25887__auto__ = new cljs.core.Keyword(null,"namespace","namespace",-377510372).cljs$core$IFn$_invoke$arity$1(p1__33246_SHARP_);
if(cljs.core.truth_(and__25887__auto__)){
return (cljs.core.not.call(null,new cljs.core.Keyword(null,"eval-body","eval-body",-907279883).cljs$core$IFn$_invoke$arity$1(p1__33246_SHARP_))) && (cljs.core.not.call(null,figwheel.client.file_reloading.figwheel_no_load_QMARK_.call(null,p1__33246_SHARP_)));
} else {
return and__25887__auto__;
}
});
;})(inst_33548,inst_33549,inst_33550,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_33552 = cljs.core.filter.call(null,inst_33551,files);
var inst_33553 = figwheel.client.file_reloading.get_figwheel_always.call(null);
var inst_33554 = cljs.core.concat.call(null,inst_33552,inst_33553);
var state_33650__$1 = (function (){var statearr_33665 = state_33650;
(statearr_33665[(17)] = inst_33548);

(statearr_33665[(18)] = inst_33550);

(statearr_33665[(12)] = inst_33554);

return statearr_33665;
})();
if(cljs.core.truth_(reload_dependents)){
var statearr_33666_33750 = state_33650__$1;
(statearr_33666_33750[(1)] = (16));

} else {
var statearr_33667_33751 = state_33650__$1;
(statearr_33667_33751[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (15))){
var inst_33538 = (state_33650[(2)]);
var state_33650__$1 = state_33650;
var statearr_33668_33752 = state_33650__$1;
(statearr_33668_33752[(2)] = inst_33538);

(statearr_33668_33752[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (21))){
var inst_33564 = (state_33650[(19)]);
var inst_33564__$1 = (state_33650[(2)]);
var inst_33565 = figwheel.client.file_reloading.load_all_js_files.call(null,inst_33564__$1);
var state_33650__$1 = (function (){var statearr_33669 = state_33650;
(statearr_33669[(19)] = inst_33564__$1);

return statearr_33669;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_33650__$1,(22),inst_33565);
} else {
if((state_val_33651 === (31))){
var inst_33648 = (state_33650[(2)]);
var state_33650__$1 = state_33650;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_33650__$1,inst_33648);
} else {
if((state_val_33651 === (32))){
var inst_33604 = (state_33650[(16)]);
var inst_33609 = inst_33604.cljs$lang$protocol_mask$partition0$;
var inst_33610 = (inst_33609 & (64));
var inst_33611 = inst_33604.cljs$core$ISeq$;
var inst_33612 = (inst_33610) || (inst_33611);
var state_33650__$1 = state_33650;
if(cljs.core.truth_(inst_33612)){
var statearr_33670_33753 = state_33650__$1;
(statearr_33670_33753[(1)] = (35));

} else {
var statearr_33671_33754 = state_33650__$1;
(statearr_33671_33754[(1)] = (36));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (40))){
var inst_33625 = (state_33650[(20)]);
var inst_33624 = (state_33650[(2)]);
var inst_33625__$1 = cljs.core.get.call(null,inst_33624,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179));
var inst_33626 = cljs.core.get.call(null,inst_33624,new cljs.core.Keyword(null,"not-required","not-required",-950359114));
var inst_33627 = cljs.core.not_empty.call(null,inst_33625__$1);
var state_33650__$1 = (function (){var statearr_33672 = state_33650;
(statearr_33672[(21)] = inst_33626);

(statearr_33672[(20)] = inst_33625__$1);

return statearr_33672;
})();
if(cljs.core.truth_(inst_33627)){
var statearr_33673_33755 = state_33650__$1;
(statearr_33673_33755[(1)] = (41));

} else {
var statearr_33674_33756 = state_33650__$1;
(statearr_33674_33756[(1)] = (42));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (33))){
var state_33650__$1 = state_33650;
var statearr_33675_33757 = state_33650__$1;
(statearr_33675_33757[(2)] = false);

(statearr_33675_33757[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (13))){
var inst_33524 = (state_33650[(22)]);
var inst_33528 = cljs.core.chunk_first.call(null,inst_33524);
var inst_33529 = cljs.core.chunk_rest.call(null,inst_33524);
var inst_33530 = cljs.core.count.call(null,inst_33528);
var inst_33511 = inst_33529;
var inst_33512 = inst_33528;
var inst_33513 = inst_33530;
var inst_33514 = (0);
var state_33650__$1 = (function (){var statearr_33676 = state_33650;
(statearr_33676[(7)] = inst_33514);

(statearr_33676[(8)] = inst_33512);

(statearr_33676[(9)] = inst_33513);

(statearr_33676[(10)] = inst_33511);

return statearr_33676;
})();
var statearr_33677_33758 = state_33650__$1;
(statearr_33677_33758[(2)] = null);

(statearr_33677_33758[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (22))){
var inst_33572 = (state_33650[(23)]);
var inst_33564 = (state_33650[(19)]);
var inst_33567 = (state_33650[(24)]);
var inst_33568 = (state_33650[(25)]);
var inst_33567__$1 = (state_33650[(2)]);
var inst_33568__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_33567__$1);
var inst_33569 = (function (){var all_files = inst_33564;
var res_SINGLEQUOTE_ = inst_33567__$1;
var res = inst_33568__$1;
return ((function (all_files,res_SINGLEQUOTE_,res,inst_33572,inst_33564,inst_33567,inst_33568,inst_33567__$1,inst_33568__$1,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p1__33247_SHARP_){
return cljs.core.not.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375).cljs$core$IFn$_invoke$arity$1(p1__33247_SHARP_));
});
;})(all_files,res_SINGLEQUOTE_,res,inst_33572,inst_33564,inst_33567,inst_33568,inst_33567__$1,inst_33568__$1,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_33570 = cljs.core.filter.call(null,inst_33569,inst_33567__$1);
var inst_33571 = cljs.core.deref.call(null,figwheel.client.file_reloading.dependencies_loaded);
var inst_33572__$1 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded-file","loaded-file",-168399375),inst_33571);
var inst_33573 = cljs.core.not_empty.call(null,inst_33572__$1);
var state_33650__$1 = (function (){var statearr_33678 = state_33650;
(statearr_33678[(26)] = inst_33570);

(statearr_33678[(23)] = inst_33572__$1);

(statearr_33678[(24)] = inst_33567__$1);

(statearr_33678[(25)] = inst_33568__$1);

return statearr_33678;
})();
if(cljs.core.truth_(inst_33573)){
var statearr_33679_33759 = state_33650__$1;
(statearr_33679_33759[(1)] = (23));

} else {
var statearr_33680_33760 = state_33650__$1;
(statearr_33680_33760[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (36))){
var state_33650__$1 = state_33650;
var statearr_33681_33761 = state_33650__$1;
(statearr_33681_33761[(2)] = false);

(statearr_33681_33761[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (41))){
var inst_33625 = (state_33650[(20)]);
var inst_33629 = cljs.core.comp.call(null,figwheel.client.file_reloading.name__GT_path,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var inst_33630 = cljs.core.map.call(null,inst_33629,inst_33625);
var inst_33631 = cljs.core.pr_str.call(null,inst_33630);
var inst_33632 = [cljs.core.str("figwheel-no-load meta-data: "),cljs.core.str(inst_33631)].join('');
var inst_33633 = figwheel.client.utils.log.call(null,inst_33632);
var state_33650__$1 = state_33650;
var statearr_33682_33762 = state_33650__$1;
(statearr_33682_33762[(2)] = inst_33633);

(statearr_33682_33762[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (43))){
var inst_33626 = (state_33650[(21)]);
var inst_33636 = (state_33650[(2)]);
var inst_33637 = cljs.core.not_empty.call(null,inst_33626);
var state_33650__$1 = (function (){var statearr_33683 = state_33650;
(statearr_33683[(27)] = inst_33636);

return statearr_33683;
})();
if(cljs.core.truth_(inst_33637)){
var statearr_33684_33763 = state_33650__$1;
(statearr_33684_33763[(1)] = (44));

} else {
var statearr_33685_33764 = state_33650__$1;
(statearr_33685_33764[(1)] = (45));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (29))){
var inst_33570 = (state_33650[(26)]);
var inst_33572 = (state_33650[(23)]);
var inst_33564 = (state_33650[(19)]);
var inst_33604 = (state_33650[(16)]);
var inst_33567 = (state_33650[(24)]);
var inst_33568 = (state_33650[(25)]);
var inst_33600 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: NOT loading these files ");
var inst_33603 = (function (){var all_files = inst_33564;
var res_SINGLEQUOTE_ = inst_33567;
var res = inst_33568;
var files_not_loaded = inst_33570;
var dependencies_that_loaded = inst_33572;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_33570,inst_33572,inst_33564,inst_33604,inst_33567,inst_33568,inst_33600,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__33602){
var map__33686 = p__33602;
var map__33686__$1 = ((((!((map__33686 == null)))?((((map__33686.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33686.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33686):map__33686);
var namespace = cljs.core.get.call(null,map__33686__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var meta_data = cljs.core.get.call(null,cljs.core.deref.call(null,figwheel.client.file_reloading.figwheel_meta_pragmas),cljs.core.name.call(null,namespace));
if((meta_data == null)){
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);
} else {
if(cljs.core.truth_(meta_data.call(null,new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179)))){
return new cljs.core.Keyword(null,"figwheel-no-load","figwheel-no-load",-555840179);
} else {
return new cljs.core.Keyword(null,"not-required","not-required",-950359114);

}
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_33570,inst_33572,inst_33564,inst_33604,inst_33567,inst_33568,inst_33600,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_33604__$1 = cljs.core.group_by.call(null,inst_33603,inst_33570);
var inst_33606 = (inst_33604__$1 == null);
var inst_33607 = cljs.core.not.call(null,inst_33606);
var state_33650__$1 = (function (){var statearr_33688 = state_33650;
(statearr_33688[(28)] = inst_33600);

(statearr_33688[(16)] = inst_33604__$1);

return statearr_33688;
})();
if(inst_33607){
var statearr_33689_33765 = state_33650__$1;
(statearr_33689_33765[(1)] = (32));

} else {
var statearr_33690_33766 = state_33650__$1;
(statearr_33690_33766[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (44))){
var inst_33626 = (state_33650[(21)]);
var inst_33639 = cljs.core.map.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),inst_33626);
var inst_33640 = cljs.core.pr_str.call(null,inst_33639);
var inst_33641 = [cljs.core.str("not required: "),cljs.core.str(inst_33640)].join('');
var inst_33642 = figwheel.client.utils.log.call(null,inst_33641);
var state_33650__$1 = state_33650;
var statearr_33691_33767 = state_33650__$1;
(statearr_33691_33767[(2)] = inst_33642);

(statearr_33691_33767[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (6))){
var inst_33545 = (state_33650[(2)]);
var state_33650__$1 = state_33650;
var statearr_33692_33768 = state_33650__$1;
(statearr_33692_33768[(2)] = inst_33545);

(statearr_33692_33768[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (28))){
var inst_33570 = (state_33650[(26)]);
var inst_33597 = (state_33650[(2)]);
var inst_33598 = cljs.core.not_empty.call(null,inst_33570);
var state_33650__$1 = (function (){var statearr_33693 = state_33650;
(statearr_33693[(29)] = inst_33597);

return statearr_33693;
})();
if(cljs.core.truth_(inst_33598)){
var statearr_33694_33769 = state_33650__$1;
(statearr_33694_33769[(1)] = (29));

} else {
var statearr_33695_33770 = state_33650__$1;
(statearr_33695_33770[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (25))){
var inst_33568 = (state_33650[(25)]);
var inst_33584 = (state_33650[(2)]);
var inst_33585 = cljs.core.not_empty.call(null,inst_33568);
var state_33650__$1 = (function (){var statearr_33696 = state_33650;
(statearr_33696[(30)] = inst_33584);

return statearr_33696;
})();
if(cljs.core.truth_(inst_33585)){
var statearr_33697_33771 = state_33650__$1;
(statearr_33697_33771[(1)] = (26));

} else {
var statearr_33698_33772 = state_33650__$1;
(statearr_33698_33772[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (34))){
var inst_33619 = (state_33650[(2)]);
var state_33650__$1 = state_33650;
if(cljs.core.truth_(inst_33619)){
var statearr_33699_33773 = state_33650__$1;
(statearr_33699_33773[(1)] = (38));

} else {
var statearr_33700_33774 = state_33650__$1;
(statearr_33700_33774[(1)] = (39));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (17))){
var state_33650__$1 = state_33650;
var statearr_33701_33775 = state_33650__$1;
(statearr_33701_33775[(2)] = recompile_dependents);

(statearr_33701_33775[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (3))){
var state_33650__$1 = state_33650;
var statearr_33702_33776 = state_33650__$1;
(statearr_33702_33776[(2)] = null);

(statearr_33702_33776[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (12))){
var inst_33541 = (state_33650[(2)]);
var state_33650__$1 = state_33650;
var statearr_33703_33777 = state_33650__$1;
(statearr_33703_33777[(2)] = inst_33541);

(statearr_33703_33777[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (2))){
var inst_33503 = (state_33650[(13)]);
var inst_33510 = cljs.core.seq.call(null,inst_33503);
var inst_33511 = inst_33510;
var inst_33512 = null;
var inst_33513 = (0);
var inst_33514 = (0);
var state_33650__$1 = (function (){var statearr_33704 = state_33650;
(statearr_33704[(7)] = inst_33514);

(statearr_33704[(8)] = inst_33512);

(statearr_33704[(9)] = inst_33513);

(statearr_33704[(10)] = inst_33511);

return statearr_33704;
})();
var statearr_33705_33778 = state_33650__$1;
(statearr_33705_33778[(2)] = null);

(statearr_33705_33778[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (23))){
var inst_33570 = (state_33650[(26)]);
var inst_33572 = (state_33650[(23)]);
var inst_33564 = (state_33650[(19)]);
var inst_33567 = (state_33650[(24)]);
var inst_33568 = (state_33650[(25)]);
var inst_33575 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these dependencies");
var inst_33577 = (function (){var all_files = inst_33564;
var res_SINGLEQUOTE_ = inst_33567;
var res = inst_33568;
var files_not_loaded = inst_33570;
var dependencies_that_loaded = inst_33572;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_33570,inst_33572,inst_33564,inst_33567,inst_33568,inst_33575,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__33576){
var map__33706 = p__33576;
var map__33706__$1 = ((((!((map__33706 == null)))?((((map__33706.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33706.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33706):map__33706);
var request_url = cljs.core.get.call(null,map__33706__$1,new cljs.core.Keyword(null,"request-url","request-url",2100346596));
return clojure.string.replace.call(null,request_url,goog.basePath,"");
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_33570,inst_33572,inst_33564,inst_33567,inst_33568,inst_33575,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_33578 = cljs.core.reverse.call(null,inst_33572);
var inst_33579 = cljs.core.map.call(null,inst_33577,inst_33578);
var inst_33580 = cljs.core.pr_str.call(null,inst_33579);
var inst_33581 = figwheel.client.utils.log.call(null,inst_33580);
var state_33650__$1 = (function (){var statearr_33708 = state_33650;
(statearr_33708[(31)] = inst_33575);

return statearr_33708;
})();
var statearr_33709_33779 = state_33650__$1;
(statearr_33709_33779[(2)] = inst_33581);

(statearr_33709_33779[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (35))){
var state_33650__$1 = state_33650;
var statearr_33710_33780 = state_33650__$1;
(statearr_33710_33780[(2)] = true);

(statearr_33710_33780[(1)] = (37));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (19))){
var inst_33554 = (state_33650[(12)]);
var inst_33560 = figwheel.client.file_reloading.expand_files.call(null,inst_33554);
var state_33650__$1 = state_33650;
var statearr_33711_33781 = state_33650__$1;
(statearr_33711_33781[(2)] = inst_33560);

(statearr_33711_33781[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (11))){
var state_33650__$1 = state_33650;
var statearr_33712_33782 = state_33650__$1;
(statearr_33712_33782[(2)] = null);

(statearr_33712_33782[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (9))){
var inst_33543 = (state_33650[(2)]);
var state_33650__$1 = state_33650;
var statearr_33713_33783 = state_33650__$1;
(statearr_33713_33783[(2)] = inst_33543);

(statearr_33713_33783[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (5))){
var inst_33514 = (state_33650[(7)]);
var inst_33513 = (state_33650[(9)]);
var inst_33516 = (inst_33514 < inst_33513);
var inst_33517 = inst_33516;
var state_33650__$1 = state_33650;
if(cljs.core.truth_(inst_33517)){
var statearr_33714_33784 = state_33650__$1;
(statearr_33714_33784[(1)] = (7));

} else {
var statearr_33715_33785 = state_33650__$1;
(statearr_33715_33785[(1)] = (8));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (14))){
var inst_33524 = (state_33650[(22)]);
var inst_33533 = cljs.core.first.call(null,inst_33524);
var inst_33534 = figwheel.client.file_reloading.eval_body.call(null,inst_33533,opts);
var inst_33535 = cljs.core.next.call(null,inst_33524);
var inst_33511 = inst_33535;
var inst_33512 = null;
var inst_33513 = (0);
var inst_33514 = (0);
var state_33650__$1 = (function (){var statearr_33716 = state_33650;
(statearr_33716[(7)] = inst_33514);

(statearr_33716[(8)] = inst_33512);

(statearr_33716[(9)] = inst_33513);

(statearr_33716[(10)] = inst_33511);

(statearr_33716[(32)] = inst_33534);

return statearr_33716;
})();
var statearr_33717_33786 = state_33650__$1;
(statearr_33717_33786[(2)] = null);

(statearr_33717_33786[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (45))){
var state_33650__$1 = state_33650;
var statearr_33718_33787 = state_33650__$1;
(statearr_33718_33787[(2)] = null);

(statearr_33718_33787[(1)] = (46));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (26))){
var inst_33570 = (state_33650[(26)]);
var inst_33572 = (state_33650[(23)]);
var inst_33564 = (state_33650[(19)]);
var inst_33567 = (state_33650[(24)]);
var inst_33568 = (state_33650[(25)]);
var inst_33587 = figwheel.client.utils.log.call(null,new cljs.core.Keyword(null,"debug","debug",-1608172596),"Figwheel: loaded these files");
var inst_33589 = (function (){var all_files = inst_33564;
var res_SINGLEQUOTE_ = inst_33567;
var res = inst_33568;
var files_not_loaded = inst_33570;
var dependencies_that_loaded = inst_33572;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_33570,inst_33572,inst_33564,inst_33567,inst_33568,inst_33587,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (p__33588){
var map__33719 = p__33588;
var map__33719__$1 = ((((!((map__33719 == null)))?((((map__33719.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33719.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33719):map__33719);
var namespace = cljs.core.get.call(null,map__33719__$1,new cljs.core.Keyword(null,"namespace","namespace",-377510372));
var file = cljs.core.get.call(null,map__33719__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
if(cljs.core.truth_(namespace)){
return figwheel.client.file_reloading.name__GT_path.call(null,cljs.core.name.call(null,namespace));
} else {
return file;
}
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_33570,inst_33572,inst_33564,inst_33567,inst_33568,inst_33587,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_33590 = cljs.core.map.call(null,inst_33589,inst_33568);
var inst_33591 = cljs.core.pr_str.call(null,inst_33590);
var inst_33592 = figwheel.client.utils.log.call(null,inst_33591);
var inst_33593 = (function (){var all_files = inst_33564;
var res_SINGLEQUOTE_ = inst_33567;
var res = inst_33568;
var files_not_loaded = inst_33570;
var dependencies_that_loaded = inst_33572;
return ((function (all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_33570,inst_33572,inst_33564,inst_33567,inst_33568,inst_33587,inst_33589,inst_33590,inst_33591,inst_33592,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents){
return (function (){
figwheel.client.file_reloading.on_jsload_custom_event.call(null,res);

return cljs.core.apply.call(null,on_jsload,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [res], null));
});
;})(all_files,res_SINGLEQUOTE_,res,files_not_loaded,dependencies_that_loaded,inst_33570,inst_33572,inst_33564,inst_33567,inst_33568,inst_33587,inst_33589,inst_33590,inst_33591,inst_33592,state_val_33651,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var inst_33594 = setTimeout(inst_33593,(10));
var state_33650__$1 = (function (){var statearr_33721 = state_33650;
(statearr_33721[(33)] = inst_33587);

(statearr_33721[(34)] = inst_33592);

return statearr_33721;
})();
var statearr_33722_33788 = state_33650__$1;
(statearr_33722_33788[(2)] = inst_33594);

(statearr_33722_33788[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (16))){
var state_33650__$1 = state_33650;
var statearr_33723_33789 = state_33650__$1;
(statearr_33723_33789[(2)] = reload_dependents);

(statearr_33723_33789[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (38))){
var inst_33604 = (state_33650[(16)]);
var inst_33621 = cljs.core.apply.call(null,cljs.core.hash_map,inst_33604);
var state_33650__$1 = state_33650;
var statearr_33724_33790 = state_33650__$1;
(statearr_33724_33790[(2)] = inst_33621);

(statearr_33724_33790[(1)] = (40));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (30))){
var state_33650__$1 = state_33650;
var statearr_33725_33791 = state_33650__$1;
(statearr_33725_33791[(2)] = null);

(statearr_33725_33791[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (10))){
var inst_33524 = (state_33650[(22)]);
var inst_33526 = cljs.core.chunked_seq_QMARK_.call(null,inst_33524);
var state_33650__$1 = state_33650;
if(inst_33526){
var statearr_33726_33792 = state_33650__$1;
(statearr_33726_33792[(1)] = (13));

} else {
var statearr_33727_33793 = state_33650__$1;
(statearr_33727_33793[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (18))){
var inst_33558 = (state_33650[(2)]);
var state_33650__$1 = state_33650;
if(cljs.core.truth_(inst_33558)){
var statearr_33728_33794 = state_33650__$1;
(statearr_33728_33794[(1)] = (19));

} else {
var statearr_33729_33795 = state_33650__$1;
(statearr_33729_33795[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (42))){
var state_33650__$1 = state_33650;
var statearr_33730_33796 = state_33650__$1;
(statearr_33730_33796[(2)] = null);

(statearr_33730_33796[(1)] = (43));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (37))){
var inst_33616 = (state_33650[(2)]);
var state_33650__$1 = state_33650;
var statearr_33731_33797 = state_33650__$1;
(statearr_33731_33797[(2)] = inst_33616);

(statearr_33731_33797[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_33651 === (8))){
var inst_33524 = (state_33650[(22)]);
var inst_33511 = (state_33650[(10)]);
var inst_33524__$1 = cljs.core.seq.call(null,inst_33511);
var state_33650__$1 = (function (){var statearr_33732 = state_33650;
(statearr_33732[(22)] = inst_33524__$1);

return statearr_33732;
})();
if(inst_33524__$1){
var statearr_33733_33798 = state_33650__$1;
(statearr_33733_33798[(1)] = (10));

} else {
var statearr_33734_33799 = state_33650__$1;
(statearr_33734_33799[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents))
;
return ((function (switch__27965__auto__,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents){
return (function() {
var figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto__ = null;
var figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto____0 = (function (){
var statearr_33738 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_33738[(0)] = figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto__);

(statearr_33738[(1)] = (1));

return statearr_33738;
});
var figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto____1 = (function (state_33650){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_33650);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e33739){if((e33739 instanceof Object)){
var ex__27969__auto__ = e33739;
var statearr_33740_33800 = state_33650;
(statearr_33740_33800[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_33650);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e33739;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__33801 = state_33650;
state_33650 = G__33801;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto__ = function(state_33650){
switch(arguments.length){
case 0:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto____0.call(this);
case 1:
return figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto____1.call(this,state_33650);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto____0;
figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto____1;
return figwheel$client$file_reloading$reload_js_files_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents))
})();
var state__28079__auto__ = (function (){var statearr_33741 = f__28078__auto__.call(null);
(statearr_33741[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto__);

return statearr_33741;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto__,map__33496,map__33496__$1,opts,before_jsload,on_jsload,reload_dependents,map__33497,map__33497__$1,msg,files,figwheel_meta,recompile_dependents))
);

return c__28077__auto__;
});
figwheel.client.file_reloading.current_links = (function figwheel$client$file_reloading$current_links(){
return Array.prototype.slice.call(document.getElementsByTagName("link"));
});
figwheel.client.file_reloading.truncate_url = (function figwheel$client$file_reloading$truncate_url(url){
return clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,clojure.string.replace_first.call(null,cljs.core.first.call(null,clojure.string.split.call(null,url,/\?/)),[cljs.core.str(location.protocol),cljs.core.str("//")].join(''),""),".*://",""),/^\/\//,""),/[^\\/]*/,"");
});
figwheel.client.file_reloading.matches_file_QMARK_ = (function figwheel$client$file_reloading$matches_file_QMARK_(p__33804,link){
var map__33807 = p__33804;
var map__33807__$1 = ((((!((map__33807 == null)))?((((map__33807.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33807.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33807):map__33807);
var file = cljs.core.get.call(null,map__33807__$1,new cljs.core.Keyword(null,"file","file",-1269645878));
var temp__4657__auto__ = link.href;
if(cljs.core.truth_(temp__4657__auto__)){
var link_href = temp__4657__auto__;
var match = clojure.string.join.call(null,"/",cljs.core.take_while.call(null,cljs.core.identity,cljs.core.map.call(null,((function (link_href,temp__4657__auto__,map__33807,map__33807__$1,file){
return (function (p1__33802_SHARP_,p2__33803_SHARP_){
if(cljs.core._EQ_.call(null,p1__33802_SHARP_,p2__33803_SHARP_)){
return p1__33802_SHARP_;
} else {
return false;
}
});})(link_href,temp__4657__auto__,map__33807,map__33807__$1,file))
,cljs.core.reverse.call(null,clojure.string.split.call(null,file,"/")),cljs.core.reverse.call(null,clojure.string.split.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href),"/")))));
var match_length = cljs.core.count.call(null,match);
var file_name_length = cljs.core.count.call(null,cljs.core.last.call(null,clojure.string.split.call(null,file,"/")));
if((match_length >= file_name_length)){
return new cljs.core.PersistentArrayMap(null, 4, [new cljs.core.Keyword(null,"link","link",-1769163468),link,new cljs.core.Keyword(null,"link-href","link-href",-250644450),link_href,new cljs.core.Keyword(null,"match-length","match-length",1101537310),match_length,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083),cljs.core.count.call(null,figwheel.client.file_reloading.truncate_url.call(null,link_href))], null);
} else {
return null;
}
} else {
return null;
}
});
figwheel.client.file_reloading.get_correct_link = (function figwheel$client$file_reloading$get_correct_link(f_data){
var temp__4657__auto__ = cljs.core.first.call(null,cljs.core.sort_by.call(null,(function (p__33813){
var map__33814 = p__33813;
var map__33814__$1 = ((((!((map__33814 == null)))?((((map__33814.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33814.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33814):map__33814);
var match_length = cljs.core.get.call(null,map__33814__$1,new cljs.core.Keyword(null,"match-length","match-length",1101537310));
var current_url_length = cljs.core.get.call(null,map__33814__$1,new cljs.core.Keyword(null,"current-url-length","current-url-length",380404083));
return (current_url_length - match_length);
}),cljs.core.keep.call(null,(function (p1__33809_SHARP_){
return figwheel.client.file_reloading.matches_file_QMARK_.call(null,f_data,p1__33809_SHARP_);
}),figwheel.client.file_reloading.current_links.call(null))));
if(cljs.core.truth_(temp__4657__auto__)){
var res = temp__4657__auto__;
return new cljs.core.Keyword(null,"link","link",-1769163468).cljs$core$IFn$_invoke$arity$1(res);
} else {
return null;
}
});
figwheel.client.file_reloading.clone_link = (function figwheel$client$file_reloading$clone_link(link,url){
var clone = document.createElement("link");
clone.rel = "stylesheet";

clone.media = link.media;

clone.disabled = link.disabled;

clone.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return clone;
});
figwheel.client.file_reloading.create_link = (function figwheel$client$file_reloading$create_link(url){
var link = document.createElement("link");
link.rel = "stylesheet";

link.href = figwheel.client.file_reloading.add_cache_buster.call(null,url);

return link;
});
figwheel.client.file_reloading.distinctify = (function figwheel$client$file_reloading$distinctify(key,seqq){
return cljs.core.vals.call(null,cljs.core.reduce.call(null,(function (p1__33816_SHARP_,p2__33817_SHARP_){
return cljs.core.assoc.call(null,p1__33816_SHARP_,cljs.core.get.call(null,p2__33817_SHARP_,key),p2__33817_SHARP_);
}),cljs.core.PersistentArrayMap.EMPTY,seqq));
});
figwheel.client.file_reloading.add_link_to_document = (function figwheel$client$file_reloading$add_link_to_document(orig_link,klone,finished_fn){
var parent = orig_link.parentNode;
if(cljs.core._EQ_.call(null,orig_link,parent.lastChild)){
parent.appendChild(klone);
} else {
parent.insertBefore(klone,orig_link.nextSibling);
}

return setTimeout(((function (parent){
return (function (){
parent.removeChild(orig_link);

return finished_fn.call(null);
});})(parent))
,(300));
});
if(typeof figwheel.client.file_reloading.reload_css_deferred_chain !== 'undefined'){
} else {
figwheel.client.file_reloading.reload_css_deferred_chain = cljs.core.atom.call(null,goog.async.Deferred.succeed());
}
figwheel.client.file_reloading.reload_css_file = (function figwheel$client$file_reloading$reload_css_file(f_data,fin){
var temp__4655__auto__ = figwheel.client.file_reloading.get_correct_link.call(null,f_data);
if(cljs.core.truth_(temp__4655__auto__)){
var link = temp__4655__auto__;
return figwheel.client.file_reloading.add_link_to_document.call(null,link,figwheel.client.file_reloading.clone_link.call(null,link,link.href),((function (link,temp__4655__auto__){
return (function (){
return fin.call(null,cljs.core.assoc.call(null,f_data,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),true));
});})(link,temp__4655__auto__))
);
} else {
return fin.call(null,f_data);
}
});
figwheel.client.file_reloading.reload_css_files_STAR_ = (function figwheel$client$file_reloading$reload_css_files_STAR_(deferred,f_datas,on_cssload){
return figwheel.client.utils.liftContD.call(null,figwheel.client.utils.mapConcatD.call(null,deferred,figwheel.client.file_reloading.reload_css_file,f_datas),(function (f_datas_SINGLEQUOTE_,fin){
var loaded_f_datas_33818 = cljs.core.filter.call(null,new cljs.core.Keyword(null,"loaded","loaded",-1246482293),f_datas_SINGLEQUOTE_);
figwheel.client.file_reloading.on_cssload_custom_event.call(null,loaded_f_datas_33818);

if(cljs.core.fn_QMARK_.call(null,on_cssload)){
on_cssload.call(null,loaded_f_datas_33818);
} else {
}

return fin.call(null);
}));
});
figwheel.client.file_reloading.reload_css_files = (function figwheel$client$file_reloading$reload_css_files(p__33819,p__33820){
var map__33825 = p__33819;
var map__33825__$1 = ((((!((map__33825 == null)))?((((map__33825.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33825.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33825):map__33825);
var on_cssload = cljs.core.get.call(null,map__33825__$1,new cljs.core.Keyword(null,"on-cssload","on-cssload",1825432318));
var map__33826 = p__33820;
var map__33826__$1 = ((((!((map__33826 == null)))?((((map__33826.cljs$lang$protocol_mask$partition0$ & (64))) || (map__33826.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__33826):map__33826);
var files_msg = map__33826__$1;
var files = cljs.core.get.call(null,map__33826__$1,new cljs.core.Keyword(null,"files","files",-472457450));
if(cljs.core.truth_(figwheel.client.utils.html_env_QMARK_.call(null))){
var temp__4657__auto__ = cljs.core.not_empty.call(null,figwheel.client.file_reloading.distinctify.call(null,new cljs.core.Keyword(null,"file","file",-1269645878),files));
if(cljs.core.truth_(temp__4657__auto__)){
var f_datas = temp__4657__auto__;
return cljs.core.swap_BANG_.call(null,figwheel.client.file_reloading.reload_css_deferred_chain,figwheel.client.file_reloading.reload_css_files_STAR_,f_datas,on_cssload);
} else {
return null;
}
} else {
return null;
}
});

//# sourceMappingURL=file_reloading.js.map?rel=1496955723983