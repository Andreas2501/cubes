// Compiled by ClojureScript 1.9.229 {}
goog.provide('devtools.format');
goog.require('cljs.core');
goog.require('devtools.context');

/**
 * @interface
 */
devtools.format.IDevtoolsFormat = function(){};

devtools.format._header = (function devtools$format$_header(value){
if((!((value == null))) && (!((value.devtools$format$IDevtoolsFormat$_header$arity$1 == null)))){
return value.devtools$format$IDevtoolsFormat$_header$arity$1(value);
} else {
var x__26562__auto__ = (((value == null))?null:value);
var m__26563__auto__ = (devtools.format._header[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,value);
} else {
var m__26563__auto____$1 = (devtools.format._header["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,value);
} else {
throw cljs.core.missing_protocol.call(null,"IDevtoolsFormat.-header",value);
}
}
}
});

devtools.format._has_body = (function devtools$format$_has_body(value){
if((!((value == null))) && (!((value.devtools$format$IDevtoolsFormat$_has_body$arity$1 == null)))){
return value.devtools$format$IDevtoolsFormat$_has_body$arity$1(value);
} else {
var x__26562__auto__ = (((value == null))?null:value);
var m__26563__auto__ = (devtools.format._has_body[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,value);
} else {
var m__26563__auto____$1 = (devtools.format._has_body["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,value);
} else {
throw cljs.core.missing_protocol.call(null,"IDevtoolsFormat.-has-body",value);
}
}
}
});

devtools.format._body = (function devtools$format$_body(value){
if((!((value == null))) && (!((value.devtools$format$IDevtoolsFormat$_body$arity$1 == null)))){
return value.devtools$format$IDevtoolsFormat$_body$arity$1(value);
} else {
var x__26562__auto__ = (((value == null))?null:value);
var m__26563__auto__ = (devtools.format._body[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,value);
} else {
var m__26563__auto____$1 = (devtools.format._body["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,value);
} else {
throw cljs.core.missing_protocol.call(null,"IDevtoolsFormat.-body",value);
}
}
}
});

devtools.format.setup_BANG_ = (function devtools$format$setup_BANG_(){
if(cljs.core.truth_(devtools.format._STAR_setup_done_STAR_)){
return null;
} else {
devtools.format._STAR_setup_done_STAR_ = true;

devtools.format.make_template_fn = (function (){var temp__4657__auto__ = goog.object.get(devtools.context.get_root.call(null),"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__36556__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__36556__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__36556__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__36556__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__36555__auto__ = temp__4657__auto____$2;
return goog.object.get(o__36555__auto__,"make_template");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.make_group_fn = (function (){var temp__4657__auto__ = goog.object.get(devtools.context.get_root.call(null),"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__36556__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__36556__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__36556__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__36556__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__36555__auto__ = temp__4657__auto____$2;
return goog.object.get(o__36555__auto__,"make_group");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.make_reference_fn = (function (){var temp__4657__auto__ = goog.object.get(devtools.context.get_root.call(null),"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__36556__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__36556__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__36556__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__36556__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__36555__auto__ = temp__4657__auto____$2;
return goog.object.get(o__36555__auto__,"make_reference");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.make_surrogate_fn = (function (){var temp__4657__auto__ = goog.object.get(devtools.context.get_root.call(null),"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__36556__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__36556__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__36556__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__36556__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__36555__auto__ = temp__4657__auto____$2;
return goog.object.get(o__36555__auto__,"make_surrogate");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format.render_markup_fn = (function (){var temp__4657__auto__ = goog.object.get(devtools.context.get_root.call(null),"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__36556__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__36556__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__36556__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__36556__auto____$1,"templating");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__36555__auto__ = temp__4657__auto____$2;
return goog.object.get(o__36555__auto__,"render_markup");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format._LT_header_GT__fn = (function (){var temp__4657__auto__ = goog.object.get(devtools.context.get_root.call(null),"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__36556__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__36556__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__36556__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__36556__auto____$1,"markup");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__36555__auto__ = temp__4657__auto____$2;
return goog.object.get(o__36555__auto__,"_LT_header_GT_");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

devtools.format._LT_standard_body_GT__fn = (function (){var temp__4657__auto__ = goog.object.get(devtools.context.get_root.call(null),"devtools");
if(cljs.core.truth_(temp__4657__auto__)){
var o__36556__auto__ = temp__4657__auto__;
var temp__4657__auto____$1 = goog.object.get(o__36556__auto__,"formatters");
if(cljs.core.truth_(temp__4657__auto____$1)){
var o__36556__auto____$1 = temp__4657__auto____$1;
var temp__4657__auto____$2 = goog.object.get(o__36556__auto____$1,"markup");
if(cljs.core.truth_(temp__4657__auto____$2)){
var o__36555__auto__ = temp__4657__auto____$2;
return goog.object.get(o__36555__auto__,"_LT_standard_body_GT_");
} else {
return null;
}
} else {
return null;
}
} else {
return null;
}
})();

if(cljs.core.truth_(devtools.format.make_template_fn)){
} else {
throw (new Error("Assert failed: make-template-fn"));
}

if(cljs.core.truth_(devtools.format.make_group_fn)){
} else {
throw (new Error("Assert failed: make-group-fn"));
}

if(cljs.core.truth_(devtools.format.make_reference_fn)){
} else {
throw (new Error("Assert failed: make-reference-fn"));
}

if(cljs.core.truth_(devtools.format.make_surrogate_fn)){
} else {
throw (new Error("Assert failed: make-surrogate-fn"));
}

if(cljs.core.truth_(devtools.format.render_markup_fn)){
} else {
throw (new Error("Assert failed: render-markup-fn"));
}

if(cljs.core.truth_(devtools.format._LT_header_GT__fn)){
} else {
throw (new Error("Assert failed: <header>-fn"));
}

if(cljs.core.truth_(devtools.format._LT_standard_body_GT__fn)){
return null;
} else {
throw (new Error("Assert failed: <standard-body>-fn"));
}
}
});
devtools.format.render_markup = (function devtools$format$render_markup(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36577 = arguments.length;
var i__26975__auto___36578 = (0);
while(true){
if((i__26975__auto___36578 < len__26974__auto___36577)){
args__26981__auto__.push((arguments[i__26975__auto___36578]));

var G__36579 = (i__26975__auto___36578 + (1));
i__26975__auto___36578 = G__36579;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return devtools.format.render_markup.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

devtools.format.render_markup.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.render_markup_fn,args);
});

devtools.format.render_markup.cljs$lang$maxFixedArity = (0);

devtools.format.render_markup.cljs$lang$applyTo = (function (seq36576){
return devtools.format.render_markup.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36576));
});

devtools.format.make_template = (function devtools$format$make_template(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36581 = arguments.length;
var i__26975__auto___36582 = (0);
while(true){
if((i__26975__auto___36582 < len__26974__auto___36581)){
args__26981__auto__.push((arguments[i__26975__auto___36582]));

var G__36583 = (i__26975__auto___36582 + (1));
i__26975__auto___36582 = G__36583;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return devtools.format.make_template.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

devtools.format.make_template.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_template_fn,args);
});

devtools.format.make_template.cljs$lang$maxFixedArity = (0);

devtools.format.make_template.cljs$lang$applyTo = (function (seq36580){
return devtools.format.make_template.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36580));
});

devtools.format.make_group = (function devtools$format$make_group(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36585 = arguments.length;
var i__26975__auto___36586 = (0);
while(true){
if((i__26975__auto___36586 < len__26974__auto___36585)){
args__26981__auto__.push((arguments[i__26975__auto___36586]));

var G__36587 = (i__26975__auto___36586 + (1));
i__26975__auto___36586 = G__36587;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return devtools.format.make_group.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

devtools.format.make_group.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_group_fn,args);
});

devtools.format.make_group.cljs$lang$maxFixedArity = (0);

devtools.format.make_group.cljs$lang$applyTo = (function (seq36584){
return devtools.format.make_group.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36584));
});

devtools.format.make_surrogate = (function devtools$format$make_surrogate(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36589 = arguments.length;
var i__26975__auto___36590 = (0);
while(true){
if((i__26975__auto___36590 < len__26974__auto___36589)){
args__26981__auto__.push((arguments[i__26975__auto___36590]));

var G__36591 = (i__26975__auto___36590 + (1));
i__26975__auto___36590 = G__36591;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return devtools.format.make_surrogate.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

devtools.format.make_surrogate.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_surrogate_fn,args);
});

devtools.format.make_surrogate.cljs$lang$maxFixedArity = (0);

devtools.format.make_surrogate.cljs$lang$applyTo = (function (seq36588){
return devtools.format.make_surrogate.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36588));
});

devtools.format.template = (function devtools$format$template(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36593 = arguments.length;
var i__26975__auto___36594 = (0);
while(true){
if((i__26975__auto___36594 < len__26974__auto___36593)){
args__26981__auto__.push((arguments[i__26975__auto___36594]));

var G__36595 = (i__26975__auto___36594 + (1));
i__26975__auto___36594 = G__36595;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return devtools.format.template.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

devtools.format.template.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_template_fn,args);
});

devtools.format.template.cljs$lang$maxFixedArity = (0);

devtools.format.template.cljs$lang$applyTo = (function (seq36592){
return devtools.format.template.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36592));
});

devtools.format.group = (function devtools$format$group(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36597 = arguments.length;
var i__26975__auto___36598 = (0);
while(true){
if((i__26975__auto___36598 < len__26974__auto___36597)){
args__26981__auto__.push((arguments[i__26975__auto___36598]));

var G__36599 = (i__26975__auto___36598 + (1));
i__26975__auto___36598 = G__36599;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return devtools.format.group.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

devtools.format.group.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_group_fn,args);
});

devtools.format.group.cljs$lang$maxFixedArity = (0);

devtools.format.group.cljs$lang$applyTo = (function (seq36596){
return devtools.format.group.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36596));
});

devtools.format.surrogate = (function devtools$format$surrogate(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36601 = arguments.length;
var i__26975__auto___36602 = (0);
while(true){
if((i__26975__auto___36602 < len__26974__auto___36601)){
args__26981__auto__.push((arguments[i__26975__auto___36602]));

var G__36603 = (i__26975__auto___36602 + (1));
i__26975__auto___36602 = G__36603;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return devtools.format.surrogate.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

devtools.format.surrogate.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_surrogate_fn,args);
});

devtools.format.surrogate.cljs$lang$maxFixedArity = (0);

devtools.format.surrogate.cljs$lang$applyTo = (function (seq36600){
return devtools.format.surrogate.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36600));
});

devtools.format.reference = (function devtools$format$reference(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36611 = arguments.length;
var i__26975__auto___36612 = (0);
while(true){
if((i__26975__auto___36612 < len__26974__auto___36611)){
args__26981__auto__.push((arguments[i__26975__auto___36612]));

var G__36613 = (i__26975__auto___36612 + (1));
i__26975__auto___36612 = G__36613;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((1) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((1)),(0),null)):null);
return devtools.format.reference.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__26982__auto__);
});

devtools.format.reference.cljs$core$IFn$_invoke$arity$variadic = (function (object,p__36607){
var vec__36608 = p__36607;
var state_override = cljs.core.nth.call(null,vec__36608,(0),null);
devtools.format.setup_BANG_.call(null);

return cljs.core.apply.call(null,devtools.format.make_reference_fn,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [object,((function (vec__36608,state_override){
return (function (p1__36604_SHARP_){
return cljs.core.merge.call(null,p1__36604_SHARP_,state_override);
});})(vec__36608,state_override))
], null));
});

devtools.format.reference.cljs$lang$maxFixedArity = (1);

devtools.format.reference.cljs$lang$applyTo = (function (seq36605){
var G__36606 = cljs.core.first.call(null,seq36605);
var seq36605__$1 = cljs.core.next.call(null,seq36605);
return devtools.format.reference.cljs$core$IFn$_invoke$arity$variadic(G__36606,seq36605__$1);
});

devtools.format.standard_reference = (function devtools$format$standard_reference(target){
devtools.format.setup_BANG_.call(null);

return devtools.format.make_template_fn.call(null,new cljs.core.Keyword(null,"ol","ol",932524051),new cljs.core.Keyword(null,"standard-ol-style","standard-ol-style",2143825615),devtools.format.make_template_fn.call(null,new cljs.core.Keyword(null,"li","li",723558921),new cljs.core.Keyword(null,"standard-li-style","standard-li-style",413442955),devtools.format.make_reference_fn.call(null,target)));
});
devtools.format.build_header = (function devtools$format$build_header(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36615 = arguments.length;
var i__26975__auto___36616 = (0);
while(true){
if((i__26975__auto___36616 < len__26974__auto___36615)){
args__26981__auto__.push((arguments[i__26975__auto___36616]));

var G__36617 = (i__26975__auto___36616 + (1));
i__26975__auto___36616 = G__36617;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return devtools.format.build_header.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

devtools.format.build_header.cljs$core$IFn$_invoke$arity$variadic = (function (args){
devtools.format.setup_BANG_.call(null);

return devtools.format.render_markup.call(null,cljs.core.apply.call(null,devtools.format._LT_header_GT__fn,args));
});

devtools.format.build_header.cljs$lang$maxFixedArity = (0);

devtools.format.build_header.cljs$lang$applyTo = (function (seq36614){
return devtools.format.build_header.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq36614));
});

devtools.format.standard_body_template = (function devtools$format$standard_body_template(var_args){
var args__26981__auto__ = [];
var len__26974__auto___36620 = arguments.length;
var i__26975__auto___36621 = (0);
while(true){
if((i__26975__auto___36621 < len__26974__auto___36620)){
args__26981__auto__.push((arguments[i__26975__auto___36621]));

var G__36622 = (i__26975__auto___36621 + (1));
i__26975__auto___36621 = G__36622;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((1) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((1)),(0),null)):null);
return devtools.format.standard_body_template.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__26982__auto__);
});

devtools.format.standard_body_template.cljs$core$IFn$_invoke$arity$variadic = (function (lines,rest){
devtools.format.setup_BANG_.call(null);

var args = cljs.core.concat.call(null,new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.map.call(null,(function (x){
return new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [x], null);
}),lines)], null),rest);
return devtools.format.render_markup.call(null,cljs.core.apply.call(null,devtools.format._LT_standard_body_GT__fn,args));
});

devtools.format.standard_body_template.cljs$lang$maxFixedArity = (1);

devtools.format.standard_body_template.cljs$lang$applyTo = (function (seq36618){
var G__36619 = cljs.core.first.call(null,seq36618);
var seq36618__$1 = cljs.core.next.call(null,seq36618);
return devtools.format.standard_body_template.cljs$core$IFn$_invoke$arity$variadic(G__36619,seq36618__$1);
});


//# sourceMappingURL=format.js.map?rel=1496955728292