// Compiled by ClojureScript 1.9.229 {}
goog.provide('cljs.core.async');
goog.require('cljs.core');
goog.require('cljs.core.async.impl.channels');
goog.require('cljs.core.async.impl.dispatch');
goog.require('cljs.core.async.impl.ioc_helpers');
goog.require('cljs.core.async.impl.protocols');
goog.require('cljs.core.async.impl.buffers');
goog.require('cljs.core.async.impl.timers');
cljs.core.async.fn_handler = (function cljs$core$async$fn_handler(var_args){
var args28122 = [];
var len__26974__auto___28128 = arguments.length;
var i__26975__auto___28129 = (0);
while(true){
if((i__26975__auto___28129 < len__26974__auto___28128)){
args28122.push((arguments[i__26975__auto___28129]));

var G__28130 = (i__26975__auto___28129 + (1));
i__26975__auto___28129 = G__28130;
continue;
} else {
}
break;
}

var G__28124 = args28122.length;
switch (G__28124) {
case 1:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28122.length)].join('')));

}
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$1 = (function (f){
return cljs.core.async.fn_handler.call(null,f,true);
});

cljs.core.async.fn_handler.cljs$core$IFn$_invoke$arity$2 = (function (f,blockable){
if(typeof cljs.core.async.t_cljs$core$async28125 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async28125 = (function (f,blockable,meta28126){
this.f = f;
this.blockable = blockable;
this.meta28126 = meta28126;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async28125.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_28127,meta28126__$1){
var self__ = this;
var _28127__$1 = this;
return (new cljs.core.async.t_cljs$core$async28125(self__.f,self__.blockable,meta28126__$1));
});

cljs.core.async.t_cljs$core$async28125.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_28127){
var self__ = this;
var _28127__$1 = this;
return self__.meta28126;
});

cljs.core.async.t_cljs$core$async28125.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async28125.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async28125.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.blockable;
});

cljs.core.async.t_cljs$core$async28125.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return self__.f;
});

cljs.core.async.t_cljs$core$async28125.getBasis = (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"blockable","blockable",-28395259,null),new cljs.core.Symbol(null,"meta28126","meta28126",-779395752,null)], null);
});

cljs.core.async.t_cljs$core$async28125.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async28125.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async28125";

cljs.core.async.t_cljs$core$async28125.cljs$lang$ctorPrWriter = (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.core.async/t_cljs$core$async28125");
});

cljs.core.async.__GT_t_cljs$core$async28125 = (function cljs$core$async$__GT_t_cljs$core$async28125(f__$1,blockable__$1,meta28126){
return (new cljs.core.async.t_cljs$core$async28125(f__$1,blockable__$1,meta28126));
});

}

return (new cljs.core.async.t_cljs$core$async28125(f,blockable,cljs.core.PersistentArrayMap.EMPTY));
});

cljs.core.async.fn_handler.cljs$lang$maxFixedArity = 2;

/**
 * Returns a fixed buffer of size n. When full, puts will block/park.
 */
cljs.core.async.buffer = (function cljs$core$async$buffer(n){
return cljs.core.async.impl.buffers.fixed_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete but
 *   val will be dropped (no transfer).
 */
cljs.core.async.dropping_buffer = (function cljs$core$async$dropping_buffer(n){
return cljs.core.async.impl.buffers.dropping_buffer.call(null,n);
});
/**
 * Returns a buffer of size n. When full, puts will complete, and be
 *   buffered, but oldest elements in buffer will be dropped (not
 *   transferred).
 */
cljs.core.async.sliding_buffer = (function cljs$core$async$sliding_buffer(n){
return cljs.core.async.impl.buffers.sliding_buffer.call(null,n);
});
/**
 * Returns true if a channel created with buff will never block. That is to say,
 * puts into this buffer will never cause the buffer to be full. 
 */
cljs.core.async.unblocking_buffer_QMARK_ = (function cljs$core$async$unblocking_buffer_QMARK_(buff){
if(!((buff == null))){
if((false) || (buff.cljs$core$async$impl$protocols$UnblockingBuffer$)){
return true;
} else {
if((!buff.cljs$lang$protocol_mask$partition$)){
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
} else {
return false;
}
}
} else {
return cljs.core.native_satisfies_QMARK_.call(null,cljs.core.async.impl.protocols.UnblockingBuffer,buff);
}
});
/**
 * Creates a channel with an optional buffer, an optional transducer (like (map f),
 *   (filter p) etc or a composition thereof), and an optional exception handler.
 *   If buf-or-n is a number, will create and use a fixed buffer of that size. If a
 *   transducer is supplied a buffer must be specified. ex-handler must be a
 *   fn of one argument - if an exception occurs during transformation it will be called
 *   with the thrown value as an argument, and any non-nil return value will be placed
 *   in the channel.
 */
cljs.core.async.chan = (function cljs$core$async$chan(var_args){
var args28134 = [];
var len__26974__auto___28137 = arguments.length;
var i__26975__auto___28138 = (0);
while(true){
if((i__26975__auto___28138 < len__26974__auto___28137)){
args28134.push((arguments[i__26975__auto___28138]));

var G__28139 = (i__26975__auto___28138 + (1));
i__26975__auto___28138 = G__28139;
continue;
} else {
}
break;
}

var G__28136 = args28134.length;
switch (G__28136) {
case 0:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28134.length)].join('')));

}
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.chan.call(null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$1 = (function (buf_or_n){
return cljs.core.async.chan.call(null,buf_or_n,null,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$2 = (function (buf_or_n,xform){
return cljs.core.async.chan.call(null,buf_or_n,xform,null);
});

cljs.core.async.chan.cljs$core$IFn$_invoke$arity$3 = (function (buf_or_n,xform,ex_handler){
var buf_or_n__$1 = ((cljs.core._EQ_.call(null,buf_or_n,(0)))?null:buf_or_n);
if(cljs.core.truth_(xform)){
if(cljs.core.truth_(buf_or_n__$1)){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str("buffer must be supplied when transducer is"),cljs.core.str("\n"),cljs.core.str("buf-or-n")].join('')));
}
} else {
}

return cljs.core.async.impl.channels.chan.call(null,((typeof buf_or_n__$1 === 'number')?cljs.core.async.buffer.call(null,buf_or_n__$1):buf_or_n__$1),xform,ex_handler);
});

cljs.core.async.chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates a promise channel with an optional transducer, and an optional
 *   exception-handler. A promise channel can take exactly one value that consumers
 *   will receive. Once full, puts complete but val is dropped (no transfer).
 *   Consumers will block until either a value is placed in the channel or the
 *   channel is closed. See chan for the semantics of xform and ex-handler.
 */
cljs.core.async.promise_chan = (function cljs$core$async$promise_chan(var_args){
var args28141 = [];
var len__26974__auto___28144 = arguments.length;
var i__26975__auto___28145 = (0);
while(true){
if((i__26975__auto___28145 < len__26974__auto___28144)){
args28141.push((arguments[i__26975__auto___28145]));

var G__28146 = (i__26975__auto___28145 + (1));
i__26975__auto___28145 = G__28146;
continue;
} else {
}
break;
}

var G__28143 = args28141.length;
switch (G__28143) {
case 0:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0();

break;
case 1:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28141.length)].join('')));

}
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$0 = (function (){
return cljs.core.async.promise_chan.call(null,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$1 = (function (xform){
return cljs.core.async.promise_chan.call(null,xform,null);
});

cljs.core.async.promise_chan.cljs$core$IFn$_invoke$arity$2 = (function (xform,ex_handler){
return cljs.core.async.chan.call(null,cljs.core.async.impl.buffers.promise_buffer.call(null),xform,ex_handler);
});

cljs.core.async.promise_chan.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel that will close after msecs
 */
cljs.core.async.timeout = (function cljs$core$async$timeout(msecs){
return cljs.core.async.impl.timers.timeout.call(null,msecs);
});
/**
 * takes a val from port. Must be called inside a (go ...) block. Will
 *   return nil if closed. Will park if nothing is available.
 *   Returns true unless port is already closed
 */
cljs.core.async._LT__BANG_ = (function cljs$core$async$_LT__BANG_(port){
throw (new Error("<! used not in (go ...) block"));
});
/**
 * Asynchronously takes a val from port, passing to fn1. Will pass nil
 * if closed. If on-caller? (default true) is true, and value is
 * immediately available, will call fn1 on calling thread.
 * Returns nil.
 */
cljs.core.async.take_BANG_ = (function cljs$core$async$take_BANG_(var_args){
var args28148 = [];
var len__26974__auto___28151 = arguments.length;
var i__26975__auto___28152 = (0);
while(true){
if((i__26975__auto___28152 < len__26974__auto___28151)){
args28148.push((arguments[i__26975__auto___28152]));

var G__28153 = (i__26975__auto___28152 + (1));
i__26975__auto___28152 = G__28153;
continue;
} else {
}
break;
}

var G__28150 = args28148.length;
switch (G__28150) {
case 2:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28148.length)].join('')));

}
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,fn1){
return cljs.core.async.take_BANG_.call(null,port,fn1,true);
});

cljs.core.async.take_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,fn1,on_caller_QMARK_){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(ret)){
var val_28155 = cljs.core.deref.call(null,ret);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,val_28155);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (val_28155,ret){
return (function (){
return fn1.call(null,val_28155);
});})(val_28155,ret))
);
}
} else {
}

return null;
});

cljs.core.async.take_BANG_.cljs$lang$maxFixedArity = 3;

cljs.core.async.nop = (function cljs$core$async$nop(_){
return null;
});
cljs.core.async.fhnop = cljs.core.async.fn_handler.call(null,cljs.core.async.nop);
/**
 * puts a val into port. nil values are not allowed. Must be called
 *   inside a (go ...) block. Will park if no buffer space is available.
 *   Returns true unless port is already closed.
 */
cljs.core.async._GT__BANG_ = (function cljs$core$async$_GT__BANG_(port,val){
throw (new Error(">! used not in (go ...) block"));
});
/**
 * Asynchronously puts a val into port, calling fn0 (if supplied) when
 * complete. nil values are not allowed. Will throw if closed. If
 * on-caller? (default true) is true, and the put is immediately
 * accepted, will call fn0 on calling thread.  Returns nil.
 */
cljs.core.async.put_BANG_ = (function cljs$core$async$put_BANG_(var_args){
var args28156 = [];
var len__26974__auto___28159 = arguments.length;
var i__26975__auto___28160 = (0);
while(true){
if((i__26975__auto___28160 < len__26974__auto___28159)){
args28156.push((arguments[i__26975__auto___28160]));

var G__28161 = (i__26975__auto___28160 + (1));
i__26975__auto___28160 = G__28161;
continue;
} else {
}
break;
}

var G__28158 = args28156.length;
switch (G__28158) {
case 2:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28156.length)].join('')));

}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$2 = (function (port,val){
var temp__4655__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fhnop);
if(cljs.core.truth_(temp__4655__auto__)){
var ret = temp__4655__auto__;
return cljs.core.deref.call(null,ret);
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$3 = (function (port,val,fn1){
return cljs.core.async.put_BANG_.call(null,port,val,fn1,true);
});

cljs.core.async.put_BANG_.cljs$core$IFn$_invoke$arity$4 = (function (port,val,fn1,on_caller_QMARK_){
var temp__4655__auto__ = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,fn1));
if(cljs.core.truth_(temp__4655__auto__)){
var retb = temp__4655__auto__;
var ret = cljs.core.deref.call(null,retb);
if(cljs.core.truth_(on_caller_QMARK_)){
fn1.call(null,ret);
} else {
cljs.core.async.impl.dispatch.run.call(null,((function (ret,retb,temp__4655__auto__){
return (function (){
return fn1.call(null,ret);
});})(ret,retb,temp__4655__auto__))
);
}

return ret;
} else {
return true;
}
});

cljs.core.async.put_BANG_.cljs$lang$maxFixedArity = 4;

cljs.core.async.close_BANG_ = (function cljs$core$async$close_BANG_(port){
return cljs.core.async.impl.protocols.close_BANG_.call(null,port);
});
cljs.core.async.random_array = (function cljs$core$async$random_array(n){
var a = (new Array(n));
var n__26814__auto___28163 = n;
var x_28164 = (0);
while(true){
if((x_28164 < n__26814__auto___28163)){
(a[x_28164] = (0));

var G__28165 = (x_28164 + (1));
x_28164 = G__28165;
continue;
} else {
}
break;
}

var i = (1);
while(true){
if(cljs.core._EQ_.call(null,i,n)){
return a;
} else {
var j = cljs.core.rand_int.call(null,i);
(a[i] = (a[j]));

(a[j] = i);

var G__28166 = (i + (1));
i = G__28166;
continue;
}
break;
}
});
cljs.core.async.alt_flag = (function cljs$core$async$alt_flag(){
var flag = cljs.core.atom.call(null,true);
if(typeof cljs.core.async.t_cljs$core$async28170 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async28170 = (function (alt_flag,flag,meta28171){
this.alt_flag = alt_flag;
this.flag = flag;
this.meta28171 = meta28171;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async28170.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (flag){
return (function (_28172,meta28171__$1){
var self__ = this;
var _28172__$1 = this;
return (new cljs.core.async.t_cljs$core$async28170(self__.alt_flag,self__.flag,meta28171__$1));
});})(flag))
;

cljs.core.async.t_cljs$core$async28170.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (flag){
return (function (_28172){
var self__ = this;
var _28172__$1 = this;
return self__.meta28171;
});})(flag))
;

cljs.core.async.t_cljs$core$async28170.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async28170.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.deref.call(null,self__.flag);
});})(flag))
;

cljs.core.async.t_cljs$core$async28170.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async28170.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (flag){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.flag,null);

return true;
});})(flag))
;

cljs.core.async.t_cljs$core$async28170.getBasis = ((function (flag){
return (function (){
return new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"alt-flag","alt-flag",-1794972754,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"private","private",-558947994),true,new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(cljs.core.PersistentVector.EMPTY))], null)),new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"meta28171","meta28171",154949277,null)], null);
});})(flag))
;

cljs.core.async.t_cljs$core$async28170.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async28170.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async28170";

cljs.core.async.t_cljs$core$async28170.cljs$lang$ctorPrWriter = ((function (flag){
return (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.core.async/t_cljs$core$async28170");
});})(flag))
;

cljs.core.async.__GT_t_cljs$core$async28170 = ((function (flag){
return (function cljs$core$async$alt_flag_$___GT_t_cljs$core$async28170(alt_flag__$1,flag__$1,meta28171){
return (new cljs.core.async.t_cljs$core$async28170(alt_flag__$1,flag__$1,meta28171));
});})(flag))
;

}

return (new cljs.core.async.t_cljs$core$async28170(cljs$core$async$alt_flag,flag,cljs.core.PersistentArrayMap.EMPTY));
});
cljs.core.async.alt_handler = (function cljs$core$async$alt_handler(flag,cb){
if(typeof cljs.core.async.t_cljs$core$async28176 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async28176 = (function (alt_handler,flag,cb,meta28177){
this.alt_handler = alt_handler;
this.flag = flag;
this.cb = cb;
this.meta28177 = meta28177;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async28176.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_28178,meta28177__$1){
var self__ = this;
var _28178__$1 = this;
return (new cljs.core.async.t_cljs$core$async28176(self__.alt_handler,self__.flag,self__.cb,meta28177__$1));
});

cljs.core.async.t_cljs$core$async28176.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_28178){
var self__ = this;
var _28178__$1 = this;
return self__.meta28177;
});

cljs.core.async.t_cljs$core$async28176.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async28176.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.flag);
});

cljs.core.async.t_cljs$core$async28176.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return true;
});

cljs.core.async.t_cljs$core$async28176.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.async.impl.protocols.commit.call(null,self__.flag);

return self__.cb;
});

cljs.core.async.t_cljs$core$async28176.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"alt-handler","alt-handler",963786170,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"private","private",-558947994),true,new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null)], null)))], null)),new cljs.core.Symbol(null,"flag","flag",-1565787888,null),new cljs.core.Symbol(null,"cb","cb",-2064487928,null),new cljs.core.Symbol(null,"meta28177","meta28177",680352649,null)], null);
});

cljs.core.async.t_cljs$core$async28176.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async28176.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async28176";

cljs.core.async.t_cljs$core$async28176.cljs$lang$ctorPrWriter = (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.core.async/t_cljs$core$async28176");
});

cljs.core.async.__GT_t_cljs$core$async28176 = (function cljs$core$async$alt_handler_$___GT_t_cljs$core$async28176(alt_handler__$1,flag__$1,cb__$1,meta28177){
return (new cljs.core.async.t_cljs$core$async28176(alt_handler__$1,flag__$1,cb__$1,meta28177));
});

}

return (new cljs.core.async.t_cljs$core$async28176(cljs$core$async$alt_handler,flag,cb,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * returns derefable [val port] if immediate, nil if enqueued
 */
cljs.core.async.do_alts = (function cljs$core$async$do_alts(fret,ports,opts){
var flag = cljs.core.async.alt_flag.call(null);
var n = cljs.core.count.call(null,ports);
var idxs = cljs.core.async.random_array.call(null,n);
var priority = new cljs.core.Keyword(null,"priority","priority",1431093715).cljs$core$IFn$_invoke$arity$1(opts);
var ret = (function (){var i = (0);
while(true){
if((i < n)){
var idx = (cljs.core.truth_(priority)?i:(idxs[i]));
var port = cljs.core.nth.call(null,ports,idx);
var wport = ((cljs.core.vector_QMARK_.call(null,port))?port.call(null,(0)):null);
var vbox = (cljs.core.truth_(wport)?(function (){var val = port.call(null,(1));
return cljs.core.async.impl.protocols.put_BANG_.call(null,wport,val,cljs.core.async.alt_handler.call(null,flag,((function (i,val,idx,port,wport,flag,n,idxs,priority){
return (function (p1__28179_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__28179_SHARP_,wport], null));
});})(i,val,idx,port,wport,flag,n,idxs,priority))
));
})():cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.alt_handler.call(null,flag,((function (i,idx,port,wport,flag,n,idxs,priority){
return (function (p1__28180_SHARP_){
return fret.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [p1__28180_SHARP_,port], null));
});})(i,idx,port,wport,flag,n,idxs,priority))
)));
if(cljs.core.truth_(vbox)){
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.deref.call(null,vbox),(function (){var or__25899__auto__ = wport;
if(cljs.core.truth_(or__25899__auto__)){
return or__25899__auto__;
} else {
return port;
}
})()], null));
} else {
var G__28181 = (i + (1));
i = G__28181;
continue;
}
} else {
return null;
}
break;
}
})();
var or__25899__auto__ = ret;
if(cljs.core.truth_(or__25899__auto__)){
return or__25899__auto__;
} else {
if(cljs.core.contains_QMARK_.call(null,opts,new cljs.core.Keyword(null,"default","default",-1987822328))){
var temp__4657__auto__ = (function (){var and__25887__auto__ = cljs.core.async.impl.protocols.active_QMARK_.call(null,flag);
if(cljs.core.truth_(and__25887__auto__)){
return cljs.core.async.impl.protocols.commit.call(null,flag);
} else {
return and__25887__auto__;
}
})();
if(cljs.core.truth_(temp__4657__auto__)){
var got = temp__4657__auto__;
return cljs.core.async.impl.channels.box.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"default","default",-1987822328).cljs$core$IFn$_invoke$arity$1(opts),new cljs.core.Keyword(null,"default","default",-1987822328)], null));
} else {
return null;
}
} else {
return null;
}
}
});
/**
 * Completes at most one of several channel operations. Must be called
 * inside a (go ...) block. ports is a vector of channel endpoints,
 * which can be either a channel to take from or a vector of
 *   [channel-to-put-to val-to-put], in any combination. Takes will be
 *   made as if by <!, and puts will be made as if by >!. Unless
 *   the :priority option is true, if more than one port operation is
 *   ready a non-deterministic choice will be made. If no operation is
 *   ready and a :default value is supplied, [default-val :default] will
 *   be returned, otherwise alts! will park until the first operation to
 *   become ready completes. Returns [val port] of the completed
 *   operation, where val is the value taken for takes, and a
 *   boolean (true unless already closed, as per put!) for puts.
 * 
 *   opts are passed as :key val ... Supported options:
 * 
 *   :default val - the value to use if none of the operations are immediately ready
 *   :priority true - (default nil) when true, the operations will be tried in order.
 * 
 *   Note: there is no guarantee that the port exps or val exprs will be
 *   used, nor in what order should they be, so they should not be
 *   depended upon for side effects.
 */
cljs.core.async.alts_BANG_ = (function cljs$core$async$alts_BANG_(var_args){
var args__26981__auto__ = [];
var len__26974__auto___28187 = arguments.length;
var i__26975__auto___28188 = (0);
while(true){
if((i__26975__auto___28188 < len__26974__auto___28187)){
args__26981__auto__.push((arguments[i__26975__auto___28188]));

var G__28189 = (i__26975__auto___28188 + (1));
i__26975__auto___28188 = G__28189;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((1) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((1)),(0),null)):null);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),argseq__26982__auto__);
});

cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (ports,p__28184){
var map__28185 = p__28184;
var map__28185__$1 = ((((!((map__28185 == null)))?((((map__28185.cljs$lang$protocol_mask$partition0$ & (64))) || (map__28185.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__28185):map__28185);
var opts = map__28185__$1;
throw (new Error("alts! used not in (go ...) block"));
});

cljs.core.async.alts_BANG_.cljs$lang$maxFixedArity = (1);

cljs.core.async.alts_BANG_.cljs$lang$applyTo = (function (seq28182){
var G__28183 = cljs.core.first.call(null,seq28182);
var seq28182__$1 = cljs.core.next.call(null,seq28182);
return cljs.core.async.alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__28183,seq28182__$1);
});

/**
 * Puts a val into port if it's possible to do so immediately.
 *   nil values are not allowed. Never blocks. Returns true if offer succeeds.
 */
cljs.core.async.offer_BANG_ = (function cljs$core$async$offer_BANG_(port,val){
var ret = cljs.core.async.impl.protocols.put_BANG_.call(null,port,val,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes a val from port if it's possible to do so immediately.
 *   Never blocks. Returns value if successful, nil otherwise.
 */
cljs.core.async.poll_BANG_ = (function cljs$core$async$poll_BANG_(port){
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,port,cljs.core.async.fn_handler.call(null,cljs.core.async.nop,false));
if(cljs.core.truth_(ret)){
return cljs.core.deref.call(null,ret);
} else {
return null;
}
});
/**
 * Takes elements from the from channel and supplies them to the to
 * channel. By default, the to channel will be closed when the from
 * channel closes, but can be determined by the close?  parameter. Will
 * stop consuming the from channel if the to channel closes
 */
cljs.core.async.pipe = (function cljs$core$async$pipe(var_args){
var args28190 = [];
var len__26974__auto___28240 = arguments.length;
var i__26975__auto___28241 = (0);
while(true){
if((i__26975__auto___28241 < len__26974__auto___28240)){
args28190.push((arguments[i__26975__auto___28241]));

var G__28242 = (i__26975__auto___28241 + (1));
i__26975__auto___28241 = G__28242;
continue;
} else {
}
break;
}

var G__28192 = args28190.length;
switch (G__28192) {
case 2:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28190.length)].join('')));

}
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$2 = (function (from,to){
return cljs.core.async.pipe.call(null,from,to,true);
});

cljs.core.async.pipe.cljs$core$IFn$_invoke$arity$3 = (function (from,to,close_QMARK_){
var c__28077__auto___28244 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___28244){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___28244){
return (function (state_28216){
var state_val_28217 = (state_28216[(1)]);
if((state_val_28217 === (7))){
var inst_28212 = (state_28216[(2)]);
var state_28216__$1 = state_28216;
var statearr_28218_28245 = state_28216__$1;
(statearr_28218_28245[(2)] = inst_28212);

(statearr_28218_28245[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28217 === (1))){
var state_28216__$1 = state_28216;
var statearr_28219_28246 = state_28216__$1;
(statearr_28219_28246[(2)] = null);

(statearr_28219_28246[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28217 === (4))){
var inst_28195 = (state_28216[(7)]);
var inst_28195__$1 = (state_28216[(2)]);
var inst_28196 = (inst_28195__$1 == null);
var state_28216__$1 = (function (){var statearr_28220 = state_28216;
(statearr_28220[(7)] = inst_28195__$1);

return statearr_28220;
})();
if(cljs.core.truth_(inst_28196)){
var statearr_28221_28247 = state_28216__$1;
(statearr_28221_28247[(1)] = (5));

} else {
var statearr_28222_28248 = state_28216__$1;
(statearr_28222_28248[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28217 === (13))){
var state_28216__$1 = state_28216;
var statearr_28223_28249 = state_28216__$1;
(statearr_28223_28249[(2)] = null);

(statearr_28223_28249[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28217 === (6))){
var inst_28195 = (state_28216[(7)]);
var state_28216__$1 = state_28216;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28216__$1,(11),to,inst_28195);
} else {
if((state_val_28217 === (3))){
var inst_28214 = (state_28216[(2)]);
var state_28216__$1 = state_28216;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28216__$1,inst_28214);
} else {
if((state_val_28217 === (12))){
var state_28216__$1 = state_28216;
var statearr_28224_28250 = state_28216__$1;
(statearr_28224_28250[(2)] = null);

(statearr_28224_28250[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28217 === (2))){
var state_28216__$1 = state_28216;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28216__$1,(4),from);
} else {
if((state_val_28217 === (11))){
var inst_28205 = (state_28216[(2)]);
var state_28216__$1 = state_28216;
if(cljs.core.truth_(inst_28205)){
var statearr_28225_28251 = state_28216__$1;
(statearr_28225_28251[(1)] = (12));

} else {
var statearr_28226_28252 = state_28216__$1;
(statearr_28226_28252[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28217 === (9))){
var state_28216__$1 = state_28216;
var statearr_28227_28253 = state_28216__$1;
(statearr_28227_28253[(2)] = null);

(statearr_28227_28253[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28217 === (5))){
var state_28216__$1 = state_28216;
if(cljs.core.truth_(close_QMARK_)){
var statearr_28228_28254 = state_28216__$1;
(statearr_28228_28254[(1)] = (8));

} else {
var statearr_28229_28255 = state_28216__$1;
(statearr_28229_28255[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28217 === (14))){
var inst_28210 = (state_28216[(2)]);
var state_28216__$1 = state_28216;
var statearr_28230_28256 = state_28216__$1;
(statearr_28230_28256[(2)] = inst_28210);

(statearr_28230_28256[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28217 === (10))){
var inst_28202 = (state_28216[(2)]);
var state_28216__$1 = state_28216;
var statearr_28231_28257 = state_28216__$1;
(statearr_28231_28257[(2)] = inst_28202);

(statearr_28231_28257[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28217 === (8))){
var inst_28199 = cljs.core.async.close_BANG_.call(null,to);
var state_28216__$1 = state_28216;
var statearr_28232_28258 = state_28216__$1;
(statearr_28232_28258[(2)] = inst_28199);

(statearr_28232_28258[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___28244))
;
return ((function (switch__27965__auto__,c__28077__auto___28244){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_28236 = [null,null,null,null,null,null,null,null];
(statearr_28236[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_28236[(1)] = (1));

return statearr_28236;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_28216){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_28216);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e28237){if((e28237 instanceof Object)){
var ex__27969__auto__ = e28237;
var statearr_28238_28259 = state_28216;
(statearr_28238_28259[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28216);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28237;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28260 = state_28216;
state_28216 = G__28260;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_28216){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_28216);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___28244))
})();
var state__28079__auto__ = (function (){var statearr_28239 = f__28078__auto__.call(null);
(statearr_28239[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___28244);

return statearr_28239;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___28244))
);


return to;
});

cljs.core.async.pipe.cljs$lang$maxFixedArity = 3;

cljs.core.async.pipeline_STAR_ = (function cljs$core$async$pipeline_STAR_(n,to,xf,from,close_QMARK_,ex_handler,type){
if((n > (0))){
} else {
throw (new Error("Assert failed: (pos? n)"));
}

var jobs = cljs.core.async.chan.call(null,n);
var results = cljs.core.async.chan.call(null,n);
var process = ((function (jobs,results){
return (function (p__28448){
var vec__28449 = p__28448;
var v = cljs.core.nth.call(null,vec__28449,(0),null);
var p = cljs.core.nth.call(null,vec__28449,(1),null);
var job = vec__28449;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1),xf,ex_handler);
var c__28077__auto___28635 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___28635,res,vec__28449,v,p,job,jobs,results){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___28635,res,vec__28449,v,p,job,jobs,results){
return (function (state_28456){
var state_val_28457 = (state_28456[(1)]);
if((state_val_28457 === (1))){
var state_28456__$1 = state_28456;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28456__$1,(2),res,v);
} else {
if((state_val_28457 === (2))){
var inst_28453 = (state_28456[(2)]);
var inst_28454 = cljs.core.async.close_BANG_.call(null,res);
var state_28456__$1 = (function (){var statearr_28458 = state_28456;
(statearr_28458[(7)] = inst_28453);

return statearr_28458;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28456__$1,inst_28454);
} else {
return null;
}
}
});})(c__28077__auto___28635,res,vec__28449,v,p,job,jobs,results))
;
return ((function (switch__27965__auto__,c__28077__auto___28635,res,vec__28449,v,p,job,jobs,results){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0 = (function (){
var statearr_28462 = [null,null,null,null,null,null,null,null];
(statearr_28462[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__);

(statearr_28462[(1)] = (1));

return statearr_28462;
});
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1 = (function (state_28456){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_28456);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e28463){if((e28463 instanceof Object)){
var ex__27969__auto__ = e28463;
var statearr_28464_28636 = state_28456;
(statearr_28464_28636[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28456);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28463;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28637 = state_28456;
state_28456 = G__28637;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__ = function(state_28456){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1.call(this,state_28456);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___28635,res,vec__28449,v,p,job,jobs,results))
})();
var state__28079__auto__ = (function (){var statearr_28465 = f__28078__auto__.call(null);
(statearr_28465[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___28635);

return statearr_28465;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___28635,res,vec__28449,v,p,job,jobs,results))
);


cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results))
;
var async = ((function (jobs,results,process){
return (function (p__28466){
var vec__28467 = p__28466;
var v = cljs.core.nth.call(null,vec__28467,(0),null);
var p = cljs.core.nth.call(null,vec__28467,(1),null);
var job = vec__28467;
if((job == null)){
cljs.core.async.close_BANG_.call(null,results);

return null;
} else {
var res = cljs.core.async.chan.call(null,(1));
xf.call(null,v,res);

cljs.core.async.put_BANG_.call(null,p,res);

return true;
}
});})(jobs,results,process))
;
var n__26814__auto___28638 = n;
var __28639 = (0);
while(true){
if((__28639 < n__26814__auto___28638)){
var G__28470_28640 = (((type instanceof cljs.core.Keyword))?type.fqn:null);
switch (G__28470_28640) {
case "compute":
var c__28077__auto___28642 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__28639,c__28077__auto___28642,G__28470_28640,n__26814__auto___28638,jobs,results,process,async){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (__28639,c__28077__auto___28642,G__28470_28640,n__26814__auto___28638,jobs,results,process,async){
return (function (state_28483){
var state_val_28484 = (state_28483[(1)]);
if((state_val_28484 === (1))){
var state_28483__$1 = state_28483;
var statearr_28485_28643 = state_28483__$1;
(statearr_28485_28643[(2)] = null);

(statearr_28485_28643[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28484 === (2))){
var state_28483__$1 = state_28483;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28483__$1,(4),jobs);
} else {
if((state_val_28484 === (3))){
var inst_28481 = (state_28483[(2)]);
var state_28483__$1 = state_28483;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28483__$1,inst_28481);
} else {
if((state_val_28484 === (4))){
var inst_28473 = (state_28483[(2)]);
var inst_28474 = process.call(null,inst_28473);
var state_28483__$1 = state_28483;
if(cljs.core.truth_(inst_28474)){
var statearr_28486_28644 = state_28483__$1;
(statearr_28486_28644[(1)] = (5));

} else {
var statearr_28487_28645 = state_28483__$1;
(statearr_28487_28645[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28484 === (5))){
var state_28483__$1 = state_28483;
var statearr_28488_28646 = state_28483__$1;
(statearr_28488_28646[(2)] = null);

(statearr_28488_28646[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28484 === (6))){
var state_28483__$1 = state_28483;
var statearr_28489_28647 = state_28483__$1;
(statearr_28489_28647[(2)] = null);

(statearr_28489_28647[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28484 === (7))){
var inst_28479 = (state_28483[(2)]);
var state_28483__$1 = state_28483;
var statearr_28490_28648 = state_28483__$1;
(statearr_28490_28648[(2)] = inst_28479);

(statearr_28490_28648[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__28639,c__28077__auto___28642,G__28470_28640,n__26814__auto___28638,jobs,results,process,async))
;
return ((function (__28639,switch__27965__auto__,c__28077__auto___28642,G__28470_28640,n__26814__auto___28638,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0 = (function (){
var statearr_28494 = [null,null,null,null,null,null,null];
(statearr_28494[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__);

(statearr_28494[(1)] = (1));

return statearr_28494;
});
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1 = (function (state_28483){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_28483);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e28495){if((e28495 instanceof Object)){
var ex__27969__auto__ = e28495;
var statearr_28496_28649 = state_28483;
(statearr_28496_28649[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28483);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28495;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28650 = state_28483;
state_28483 = G__28650;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__ = function(state_28483){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1.call(this,state_28483);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__;
})()
;})(__28639,switch__27965__auto__,c__28077__auto___28642,G__28470_28640,n__26814__auto___28638,jobs,results,process,async))
})();
var state__28079__auto__ = (function (){var statearr_28497 = f__28078__auto__.call(null);
(statearr_28497[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___28642);

return statearr_28497;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(__28639,c__28077__auto___28642,G__28470_28640,n__26814__auto___28638,jobs,results,process,async))
);


break;
case "async":
var c__28077__auto___28651 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (__28639,c__28077__auto___28651,G__28470_28640,n__26814__auto___28638,jobs,results,process,async){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (__28639,c__28077__auto___28651,G__28470_28640,n__26814__auto___28638,jobs,results,process,async){
return (function (state_28510){
var state_val_28511 = (state_28510[(1)]);
if((state_val_28511 === (1))){
var state_28510__$1 = state_28510;
var statearr_28512_28652 = state_28510__$1;
(statearr_28512_28652[(2)] = null);

(statearr_28512_28652[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28511 === (2))){
var state_28510__$1 = state_28510;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28510__$1,(4),jobs);
} else {
if((state_val_28511 === (3))){
var inst_28508 = (state_28510[(2)]);
var state_28510__$1 = state_28510;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28510__$1,inst_28508);
} else {
if((state_val_28511 === (4))){
var inst_28500 = (state_28510[(2)]);
var inst_28501 = async.call(null,inst_28500);
var state_28510__$1 = state_28510;
if(cljs.core.truth_(inst_28501)){
var statearr_28513_28653 = state_28510__$1;
(statearr_28513_28653[(1)] = (5));

} else {
var statearr_28514_28654 = state_28510__$1;
(statearr_28514_28654[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28511 === (5))){
var state_28510__$1 = state_28510;
var statearr_28515_28655 = state_28510__$1;
(statearr_28515_28655[(2)] = null);

(statearr_28515_28655[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28511 === (6))){
var state_28510__$1 = state_28510;
var statearr_28516_28656 = state_28510__$1;
(statearr_28516_28656[(2)] = null);

(statearr_28516_28656[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28511 === (7))){
var inst_28506 = (state_28510[(2)]);
var state_28510__$1 = state_28510;
var statearr_28517_28657 = state_28510__$1;
(statearr_28517_28657[(2)] = inst_28506);

(statearr_28517_28657[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
});})(__28639,c__28077__auto___28651,G__28470_28640,n__26814__auto___28638,jobs,results,process,async))
;
return ((function (__28639,switch__27965__auto__,c__28077__auto___28651,G__28470_28640,n__26814__auto___28638,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0 = (function (){
var statearr_28521 = [null,null,null,null,null,null,null];
(statearr_28521[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__);

(statearr_28521[(1)] = (1));

return statearr_28521;
});
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1 = (function (state_28510){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_28510);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e28522){if((e28522 instanceof Object)){
var ex__27969__auto__ = e28522;
var statearr_28523_28658 = state_28510;
(statearr_28523_28658[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28510);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28522;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28659 = state_28510;
state_28510 = G__28659;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__ = function(state_28510){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1.call(this,state_28510);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__;
})()
;})(__28639,switch__27965__auto__,c__28077__auto___28651,G__28470_28640,n__26814__auto___28638,jobs,results,process,async))
})();
var state__28079__auto__ = (function (){var statearr_28524 = f__28078__auto__.call(null);
(statearr_28524[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___28651);

return statearr_28524;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(__28639,c__28077__auto___28651,G__28470_28640,n__26814__auto___28638,jobs,results,process,async))
);


break;
default:
throw (new Error([cljs.core.str("No matching clause: "),cljs.core.str(type)].join('')));

}

var G__28660 = (__28639 + (1));
__28639 = G__28660;
continue;
} else {
}
break;
}

var c__28077__auto___28661 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___28661,jobs,results,process,async){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___28661,jobs,results,process,async){
return (function (state_28546){
var state_val_28547 = (state_28546[(1)]);
if((state_val_28547 === (1))){
var state_28546__$1 = state_28546;
var statearr_28548_28662 = state_28546__$1;
(statearr_28548_28662[(2)] = null);

(statearr_28548_28662[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28547 === (2))){
var state_28546__$1 = state_28546;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28546__$1,(4),from);
} else {
if((state_val_28547 === (3))){
var inst_28544 = (state_28546[(2)]);
var state_28546__$1 = state_28546;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28546__$1,inst_28544);
} else {
if((state_val_28547 === (4))){
var inst_28527 = (state_28546[(7)]);
var inst_28527__$1 = (state_28546[(2)]);
var inst_28528 = (inst_28527__$1 == null);
var state_28546__$1 = (function (){var statearr_28549 = state_28546;
(statearr_28549[(7)] = inst_28527__$1);

return statearr_28549;
})();
if(cljs.core.truth_(inst_28528)){
var statearr_28550_28663 = state_28546__$1;
(statearr_28550_28663[(1)] = (5));

} else {
var statearr_28551_28664 = state_28546__$1;
(statearr_28551_28664[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28547 === (5))){
var inst_28530 = cljs.core.async.close_BANG_.call(null,jobs);
var state_28546__$1 = state_28546;
var statearr_28552_28665 = state_28546__$1;
(statearr_28552_28665[(2)] = inst_28530);

(statearr_28552_28665[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28547 === (6))){
var inst_28532 = (state_28546[(8)]);
var inst_28527 = (state_28546[(7)]);
var inst_28532__$1 = cljs.core.async.chan.call(null,(1));
var inst_28533 = cljs.core.PersistentVector.EMPTY_NODE;
var inst_28534 = [inst_28527,inst_28532__$1];
var inst_28535 = (new cljs.core.PersistentVector(null,2,(5),inst_28533,inst_28534,null));
var state_28546__$1 = (function (){var statearr_28553 = state_28546;
(statearr_28553[(8)] = inst_28532__$1);

return statearr_28553;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28546__$1,(8),jobs,inst_28535);
} else {
if((state_val_28547 === (7))){
var inst_28542 = (state_28546[(2)]);
var state_28546__$1 = state_28546;
var statearr_28554_28666 = state_28546__$1;
(statearr_28554_28666[(2)] = inst_28542);

(statearr_28554_28666[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28547 === (8))){
var inst_28532 = (state_28546[(8)]);
var inst_28537 = (state_28546[(2)]);
var state_28546__$1 = (function (){var statearr_28555 = state_28546;
(statearr_28555[(9)] = inst_28537);

return statearr_28555;
})();
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28546__$1,(9),results,inst_28532);
} else {
if((state_val_28547 === (9))){
var inst_28539 = (state_28546[(2)]);
var state_28546__$1 = (function (){var statearr_28556 = state_28546;
(statearr_28556[(10)] = inst_28539);

return statearr_28556;
})();
var statearr_28557_28667 = state_28546__$1;
(statearr_28557_28667[(2)] = null);

(statearr_28557_28667[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___28661,jobs,results,process,async))
;
return ((function (switch__27965__auto__,c__28077__auto___28661,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0 = (function (){
var statearr_28561 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_28561[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__);

(statearr_28561[(1)] = (1));

return statearr_28561;
});
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1 = (function (state_28546){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_28546);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e28562){if((e28562 instanceof Object)){
var ex__27969__auto__ = e28562;
var statearr_28563_28668 = state_28546;
(statearr_28563_28668[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28546);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28562;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28669 = state_28546;
state_28546 = G__28669;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__ = function(state_28546){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1.call(this,state_28546);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___28661,jobs,results,process,async))
})();
var state__28079__auto__ = (function (){var statearr_28564 = f__28078__auto__.call(null);
(statearr_28564[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___28661);

return statearr_28564;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___28661,jobs,results,process,async))
);


var c__28077__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto__,jobs,results,process,async){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto__,jobs,results,process,async){
return (function (state_28602){
var state_val_28603 = (state_28602[(1)]);
if((state_val_28603 === (7))){
var inst_28598 = (state_28602[(2)]);
var state_28602__$1 = state_28602;
var statearr_28604_28670 = state_28602__$1;
(statearr_28604_28670[(2)] = inst_28598);

(statearr_28604_28670[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (20))){
var state_28602__$1 = state_28602;
var statearr_28605_28671 = state_28602__$1;
(statearr_28605_28671[(2)] = null);

(statearr_28605_28671[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (1))){
var state_28602__$1 = state_28602;
var statearr_28606_28672 = state_28602__$1;
(statearr_28606_28672[(2)] = null);

(statearr_28606_28672[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (4))){
var inst_28567 = (state_28602[(7)]);
var inst_28567__$1 = (state_28602[(2)]);
var inst_28568 = (inst_28567__$1 == null);
var state_28602__$1 = (function (){var statearr_28607 = state_28602;
(statearr_28607[(7)] = inst_28567__$1);

return statearr_28607;
})();
if(cljs.core.truth_(inst_28568)){
var statearr_28608_28673 = state_28602__$1;
(statearr_28608_28673[(1)] = (5));

} else {
var statearr_28609_28674 = state_28602__$1;
(statearr_28609_28674[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (15))){
var inst_28580 = (state_28602[(8)]);
var state_28602__$1 = state_28602;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28602__$1,(18),to,inst_28580);
} else {
if((state_val_28603 === (21))){
var inst_28593 = (state_28602[(2)]);
var state_28602__$1 = state_28602;
var statearr_28610_28675 = state_28602__$1;
(statearr_28610_28675[(2)] = inst_28593);

(statearr_28610_28675[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (13))){
var inst_28595 = (state_28602[(2)]);
var state_28602__$1 = (function (){var statearr_28611 = state_28602;
(statearr_28611[(9)] = inst_28595);

return statearr_28611;
})();
var statearr_28612_28676 = state_28602__$1;
(statearr_28612_28676[(2)] = null);

(statearr_28612_28676[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (6))){
var inst_28567 = (state_28602[(7)]);
var state_28602__$1 = state_28602;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28602__$1,(11),inst_28567);
} else {
if((state_val_28603 === (17))){
var inst_28588 = (state_28602[(2)]);
var state_28602__$1 = state_28602;
if(cljs.core.truth_(inst_28588)){
var statearr_28613_28677 = state_28602__$1;
(statearr_28613_28677[(1)] = (19));

} else {
var statearr_28614_28678 = state_28602__$1;
(statearr_28614_28678[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (3))){
var inst_28600 = (state_28602[(2)]);
var state_28602__$1 = state_28602;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28602__$1,inst_28600);
} else {
if((state_val_28603 === (12))){
var inst_28577 = (state_28602[(10)]);
var state_28602__$1 = state_28602;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28602__$1,(14),inst_28577);
} else {
if((state_val_28603 === (2))){
var state_28602__$1 = state_28602;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28602__$1,(4),results);
} else {
if((state_val_28603 === (19))){
var state_28602__$1 = state_28602;
var statearr_28615_28679 = state_28602__$1;
(statearr_28615_28679[(2)] = null);

(statearr_28615_28679[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (11))){
var inst_28577 = (state_28602[(2)]);
var state_28602__$1 = (function (){var statearr_28616 = state_28602;
(statearr_28616[(10)] = inst_28577);

return statearr_28616;
})();
var statearr_28617_28680 = state_28602__$1;
(statearr_28617_28680[(2)] = null);

(statearr_28617_28680[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (9))){
var state_28602__$1 = state_28602;
var statearr_28618_28681 = state_28602__$1;
(statearr_28618_28681[(2)] = null);

(statearr_28618_28681[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (5))){
var state_28602__$1 = state_28602;
if(cljs.core.truth_(close_QMARK_)){
var statearr_28619_28682 = state_28602__$1;
(statearr_28619_28682[(1)] = (8));

} else {
var statearr_28620_28683 = state_28602__$1;
(statearr_28620_28683[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (14))){
var inst_28582 = (state_28602[(11)]);
var inst_28580 = (state_28602[(8)]);
var inst_28580__$1 = (state_28602[(2)]);
var inst_28581 = (inst_28580__$1 == null);
var inst_28582__$1 = cljs.core.not.call(null,inst_28581);
var state_28602__$1 = (function (){var statearr_28621 = state_28602;
(statearr_28621[(11)] = inst_28582__$1);

(statearr_28621[(8)] = inst_28580__$1);

return statearr_28621;
})();
if(inst_28582__$1){
var statearr_28622_28684 = state_28602__$1;
(statearr_28622_28684[(1)] = (15));

} else {
var statearr_28623_28685 = state_28602__$1;
(statearr_28623_28685[(1)] = (16));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (16))){
var inst_28582 = (state_28602[(11)]);
var state_28602__$1 = state_28602;
var statearr_28624_28686 = state_28602__$1;
(statearr_28624_28686[(2)] = inst_28582);

(statearr_28624_28686[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (10))){
var inst_28574 = (state_28602[(2)]);
var state_28602__$1 = state_28602;
var statearr_28625_28687 = state_28602__$1;
(statearr_28625_28687[(2)] = inst_28574);

(statearr_28625_28687[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (18))){
var inst_28585 = (state_28602[(2)]);
var state_28602__$1 = state_28602;
var statearr_28626_28688 = state_28602__$1;
(statearr_28626_28688[(2)] = inst_28585);

(statearr_28626_28688[(1)] = (17));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28603 === (8))){
var inst_28571 = cljs.core.async.close_BANG_.call(null,to);
var state_28602__$1 = state_28602;
var statearr_28627_28689 = state_28602__$1;
(statearr_28627_28689[(2)] = inst_28571);

(statearr_28627_28689[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto__,jobs,results,process,async))
;
return ((function (switch__27965__auto__,c__28077__auto__,jobs,results,process,async){
return (function() {
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__ = null;
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0 = (function (){
var statearr_28631 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_28631[(0)] = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__);

(statearr_28631[(1)] = (1));

return statearr_28631;
});
var cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1 = (function (state_28602){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_28602);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e28632){if((e28632 instanceof Object)){
var ex__27969__auto__ = e28632;
var statearr_28633_28690 = state_28602;
(statearr_28633_28690[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28602);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28632;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28691 = state_28602;
state_28602 = G__28691;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__ = function(state_28602){
switch(arguments.length){
case 0:
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1.call(this,state_28602);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____0;
cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$pipeline_STAR__$_state_machine__27966__auto____1;
return cljs$core$async$pipeline_STAR__$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto__,jobs,results,process,async))
})();
var state__28079__auto__ = (function (){var statearr_28634 = f__28078__auto__.call(null);
(statearr_28634[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto__);

return statearr_28634;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto__,jobs,results,process,async))
);

return c__28077__auto__;
});
/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the async function af, with parallelism n. af
 *   must be a function of two arguments, the first an input value and
 *   the second a channel on which to place the result(s). af must close!
 *   the channel before returning.  The presumption is that af will
 *   return immediately, having launched some asynchronous operation
 *   whose completion/callback will manipulate the result channel. Outputs
 *   will be returned in order relative to  the inputs. By default, the to
 *   channel will be closed when the from channel closes, but can be
 *   determined by the close?  parameter. Will stop consuming the from
 *   channel if the to channel closes.
 */
cljs.core.async.pipeline_async = (function cljs$core$async$pipeline_async(var_args){
var args28692 = [];
var len__26974__auto___28695 = arguments.length;
var i__26975__auto___28696 = (0);
while(true){
if((i__26975__auto___28696 < len__26974__auto___28695)){
args28692.push((arguments[i__26975__auto___28696]));

var G__28697 = (i__26975__auto___28696 + (1));
i__26975__auto___28696 = G__28697;
continue;
} else {
}
break;
}

var G__28694 = args28692.length;
switch (G__28694) {
case 4:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28692.length)].join('')));

}
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$4 = (function (n,to,af,from){
return cljs.core.async.pipeline_async.call(null,n,to,af,from,true);
});

cljs.core.async.pipeline_async.cljs$core$IFn$_invoke$arity$5 = (function (n,to,af,from,close_QMARK_){
return cljs.core.async.pipeline_STAR_.call(null,n,to,af,from,close_QMARK_,null,new cljs.core.Keyword(null,"async","async",1050769601));
});

cljs.core.async.pipeline_async.cljs$lang$maxFixedArity = 5;

/**
 * Takes elements from the from channel and supplies them to the to
 *   channel, subject to the transducer xf, with parallelism n. Because
 *   it is parallel, the transducer will be applied independently to each
 *   element, not across elements, and may produce zero or more outputs
 *   per input.  Outputs will be returned in order relative to the
 *   inputs. By default, the to channel will be closed when the from
 *   channel closes, but can be determined by the close?  parameter. Will
 *   stop consuming the from channel if the to channel closes.
 * 
 *   Note this is supplied for API compatibility with the Clojure version.
 *   Values of N > 1 will not result in actual concurrency in a
 *   single-threaded runtime.
 */
cljs.core.async.pipeline = (function cljs$core$async$pipeline(var_args){
var args28699 = [];
var len__26974__auto___28702 = arguments.length;
var i__26975__auto___28703 = (0);
while(true){
if((i__26975__auto___28703 < len__26974__auto___28702)){
args28699.push((arguments[i__26975__auto___28703]));

var G__28704 = (i__26975__auto___28703 + (1));
i__26975__auto___28703 = G__28704;
continue;
} else {
}
break;
}

var G__28701 = args28699.length;
switch (G__28701) {
case 4:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
case 5:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]));

break;
case 6:
return cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]),(arguments[(4)]),(arguments[(5)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28699.length)].join('')));

}
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$4 = (function (n,to,xf,from){
return cljs.core.async.pipeline.call(null,n,to,xf,from,true);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$5 = (function (n,to,xf,from,close_QMARK_){
return cljs.core.async.pipeline.call(null,n,to,xf,from,close_QMARK_,null);
});

cljs.core.async.pipeline.cljs$core$IFn$_invoke$arity$6 = (function (n,to,xf,from,close_QMARK_,ex_handler){
return cljs.core.async.pipeline_STAR_.call(null,n,to,xf,from,close_QMARK_,ex_handler,new cljs.core.Keyword(null,"compute","compute",1555393130));
});

cljs.core.async.pipeline.cljs$lang$maxFixedArity = 6;

/**
 * Takes a predicate and a source channel and returns a vector of two
 *   channels, the first of which will contain the values for which the
 *   predicate returned true, the second those for which it returned
 *   false.
 * 
 *   The out channels will be unbuffered by default, or two buf-or-ns can
 *   be supplied. The channels will close after the source channel has
 *   closed.
 */
cljs.core.async.split = (function cljs$core$async$split(var_args){
var args28706 = [];
var len__26974__auto___28759 = arguments.length;
var i__26975__auto___28760 = (0);
while(true){
if((i__26975__auto___28760 < len__26974__auto___28759)){
args28706.push((arguments[i__26975__auto___28760]));

var G__28761 = (i__26975__auto___28760 + (1));
i__26975__auto___28760 = G__28761;
continue;
} else {
}
break;
}

var G__28708 = args28706.length;
switch (G__28708) {
case 2:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 4:
return cljs.core.async.split.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28706.length)].join('')));

}
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.split.call(null,p,ch,null,null);
});

cljs.core.async.split.cljs$core$IFn$_invoke$arity$4 = (function (p,ch,t_buf_or_n,f_buf_or_n){
var tc = cljs.core.async.chan.call(null,t_buf_or_n);
var fc = cljs.core.async.chan.call(null,f_buf_or_n);
var c__28077__auto___28763 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___28763,tc,fc){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___28763,tc,fc){
return (function (state_28734){
var state_val_28735 = (state_28734[(1)]);
if((state_val_28735 === (7))){
var inst_28730 = (state_28734[(2)]);
var state_28734__$1 = state_28734;
var statearr_28736_28764 = state_28734__$1;
(statearr_28736_28764[(2)] = inst_28730);

(statearr_28736_28764[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28735 === (1))){
var state_28734__$1 = state_28734;
var statearr_28737_28765 = state_28734__$1;
(statearr_28737_28765[(2)] = null);

(statearr_28737_28765[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28735 === (4))){
var inst_28711 = (state_28734[(7)]);
var inst_28711__$1 = (state_28734[(2)]);
var inst_28712 = (inst_28711__$1 == null);
var state_28734__$1 = (function (){var statearr_28738 = state_28734;
(statearr_28738[(7)] = inst_28711__$1);

return statearr_28738;
})();
if(cljs.core.truth_(inst_28712)){
var statearr_28739_28766 = state_28734__$1;
(statearr_28739_28766[(1)] = (5));

} else {
var statearr_28740_28767 = state_28734__$1;
(statearr_28740_28767[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28735 === (13))){
var state_28734__$1 = state_28734;
var statearr_28741_28768 = state_28734__$1;
(statearr_28741_28768[(2)] = null);

(statearr_28741_28768[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28735 === (6))){
var inst_28711 = (state_28734[(7)]);
var inst_28717 = p.call(null,inst_28711);
var state_28734__$1 = state_28734;
if(cljs.core.truth_(inst_28717)){
var statearr_28742_28769 = state_28734__$1;
(statearr_28742_28769[(1)] = (9));

} else {
var statearr_28743_28770 = state_28734__$1;
(statearr_28743_28770[(1)] = (10));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28735 === (3))){
var inst_28732 = (state_28734[(2)]);
var state_28734__$1 = state_28734;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28734__$1,inst_28732);
} else {
if((state_val_28735 === (12))){
var state_28734__$1 = state_28734;
var statearr_28744_28771 = state_28734__$1;
(statearr_28744_28771[(2)] = null);

(statearr_28744_28771[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28735 === (2))){
var state_28734__$1 = state_28734;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28734__$1,(4),ch);
} else {
if((state_val_28735 === (11))){
var inst_28711 = (state_28734[(7)]);
var inst_28721 = (state_28734[(2)]);
var state_28734__$1 = state_28734;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28734__$1,(8),inst_28721,inst_28711);
} else {
if((state_val_28735 === (9))){
var state_28734__$1 = state_28734;
var statearr_28745_28772 = state_28734__$1;
(statearr_28745_28772[(2)] = tc);

(statearr_28745_28772[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28735 === (5))){
var inst_28714 = cljs.core.async.close_BANG_.call(null,tc);
var inst_28715 = cljs.core.async.close_BANG_.call(null,fc);
var state_28734__$1 = (function (){var statearr_28746 = state_28734;
(statearr_28746[(8)] = inst_28714);

return statearr_28746;
})();
var statearr_28747_28773 = state_28734__$1;
(statearr_28747_28773[(2)] = inst_28715);

(statearr_28747_28773[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28735 === (14))){
var inst_28728 = (state_28734[(2)]);
var state_28734__$1 = state_28734;
var statearr_28748_28774 = state_28734__$1;
(statearr_28748_28774[(2)] = inst_28728);

(statearr_28748_28774[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28735 === (10))){
var state_28734__$1 = state_28734;
var statearr_28749_28775 = state_28734__$1;
(statearr_28749_28775[(2)] = fc);

(statearr_28749_28775[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28735 === (8))){
var inst_28723 = (state_28734[(2)]);
var state_28734__$1 = state_28734;
if(cljs.core.truth_(inst_28723)){
var statearr_28750_28776 = state_28734__$1;
(statearr_28750_28776[(1)] = (12));

} else {
var statearr_28751_28777 = state_28734__$1;
(statearr_28751_28777[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___28763,tc,fc))
;
return ((function (switch__27965__auto__,c__28077__auto___28763,tc,fc){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_28755 = [null,null,null,null,null,null,null,null,null];
(statearr_28755[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_28755[(1)] = (1));

return statearr_28755;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_28734){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_28734);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e28756){if((e28756 instanceof Object)){
var ex__27969__auto__ = e28756;
var statearr_28757_28778 = state_28734;
(statearr_28757_28778[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28734);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28756;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28779 = state_28734;
state_28734 = G__28779;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_28734){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_28734);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___28763,tc,fc))
})();
var state__28079__auto__ = (function (){var statearr_28758 = f__28078__auto__.call(null);
(statearr_28758[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___28763);

return statearr_28758;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___28763,tc,fc))
);


return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [tc,fc], null);
});

cljs.core.async.split.cljs$lang$maxFixedArity = 4;

/**
 * f should be a function of 2 arguments. Returns a channel containing
 *   the single result of applying f to init and the first item from the
 *   channel, then applying f to that result and the 2nd item, etc. If
 *   the channel closes without yielding items, returns init and f is not
 *   called. ch must close before reduce produces a result.
 */
cljs.core.async.reduce = (function cljs$core$async$reduce(f,init,ch){
var c__28077__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto__){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto__){
return (function (state_28843){
var state_val_28844 = (state_28843[(1)]);
if((state_val_28844 === (7))){
var inst_28839 = (state_28843[(2)]);
var state_28843__$1 = state_28843;
var statearr_28845_28866 = state_28843__$1;
(statearr_28845_28866[(2)] = inst_28839);

(statearr_28845_28866[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28844 === (1))){
var inst_28823 = init;
var state_28843__$1 = (function (){var statearr_28846 = state_28843;
(statearr_28846[(7)] = inst_28823);

return statearr_28846;
})();
var statearr_28847_28867 = state_28843__$1;
(statearr_28847_28867[(2)] = null);

(statearr_28847_28867[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28844 === (4))){
var inst_28826 = (state_28843[(8)]);
var inst_28826__$1 = (state_28843[(2)]);
var inst_28827 = (inst_28826__$1 == null);
var state_28843__$1 = (function (){var statearr_28848 = state_28843;
(statearr_28848[(8)] = inst_28826__$1);

return statearr_28848;
})();
if(cljs.core.truth_(inst_28827)){
var statearr_28849_28868 = state_28843__$1;
(statearr_28849_28868[(1)] = (5));

} else {
var statearr_28850_28869 = state_28843__$1;
(statearr_28850_28869[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28844 === (6))){
var inst_28826 = (state_28843[(8)]);
var inst_28823 = (state_28843[(7)]);
var inst_28830 = (state_28843[(9)]);
var inst_28830__$1 = f.call(null,inst_28823,inst_28826);
var inst_28831 = cljs.core.reduced_QMARK_.call(null,inst_28830__$1);
var state_28843__$1 = (function (){var statearr_28851 = state_28843;
(statearr_28851[(9)] = inst_28830__$1);

return statearr_28851;
})();
if(inst_28831){
var statearr_28852_28870 = state_28843__$1;
(statearr_28852_28870[(1)] = (8));

} else {
var statearr_28853_28871 = state_28843__$1;
(statearr_28853_28871[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28844 === (3))){
var inst_28841 = (state_28843[(2)]);
var state_28843__$1 = state_28843;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28843__$1,inst_28841);
} else {
if((state_val_28844 === (2))){
var state_28843__$1 = state_28843;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28843__$1,(4),ch);
} else {
if((state_val_28844 === (9))){
var inst_28830 = (state_28843[(9)]);
var inst_28823 = inst_28830;
var state_28843__$1 = (function (){var statearr_28854 = state_28843;
(statearr_28854[(7)] = inst_28823);

return statearr_28854;
})();
var statearr_28855_28872 = state_28843__$1;
(statearr_28855_28872[(2)] = null);

(statearr_28855_28872[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28844 === (5))){
var inst_28823 = (state_28843[(7)]);
var state_28843__$1 = state_28843;
var statearr_28856_28873 = state_28843__$1;
(statearr_28856_28873[(2)] = inst_28823);

(statearr_28856_28873[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28844 === (10))){
var inst_28837 = (state_28843[(2)]);
var state_28843__$1 = state_28843;
var statearr_28857_28874 = state_28843__$1;
(statearr_28857_28874[(2)] = inst_28837);

(statearr_28857_28874[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28844 === (8))){
var inst_28830 = (state_28843[(9)]);
var inst_28833 = cljs.core.deref.call(null,inst_28830);
var state_28843__$1 = state_28843;
var statearr_28858_28875 = state_28843__$1;
(statearr_28858_28875[(2)] = inst_28833);

(statearr_28858_28875[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto__))
;
return ((function (switch__27965__auto__,c__28077__auto__){
return (function() {
var cljs$core$async$reduce_$_state_machine__27966__auto__ = null;
var cljs$core$async$reduce_$_state_machine__27966__auto____0 = (function (){
var statearr_28862 = [null,null,null,null,null,null,null,null,null,null];
(statearr_28862[(0)] = cljs$core$async$reduce_$_state_machine__27966__auto__);

(statearr_28862[(1)] = (1));

return statearr_28862;
});
var cljs$core$async$reduce_$_state_machine__27966__auto____1 = (function (state_28843){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_28843);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e28863){if((e28863 instanceof Object)){
var ex__27969__auto__ = e28863;
var statearr_28864_28876 = state_28843;
(statearr_28864_28876[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28843);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28863;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28877 = state_28843;
state_28843 = G__28877;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$reduce_$_state_machine__27966__auto__ = function(state_28843){
switch(arguments.length){
case 0:
return cljs$core$async$reduce_$_state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$reduce_$_state_machine__27966__auto____1.call(this,state_28843);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$reduce_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$reduce_$_state_machine__27966__auto____0;
cljs$core$async$reduce_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$reduce_$_state_machine__27966__auto____1;
return cljs$core$async$reduce_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto__))
})();
var state__28079__auto__ = (function (){var statearr_28865 = f__28078__auto__.call(null);
(statearr_28865[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto__);

return statearr_28865;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto__))
);

return c__28077__auto__;
});
/**
 * async/reduces a channel with a transformation (xform f).
 *   Returns a channel containing the result.  ch must close before
 *   transduce produces a result.
 */
cljs.core.async.transduce = (function cljs$core$async$transduce(xform,f,init,ch){
var f__$1 = xform.call(null,f);
var c__28077__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto__,f__$1){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto__,f__$1){
return (function (state_28897){
var state_val_28898 = (state_28897[(1)]);
if((state_val_28898 === (1))){
var inst_28892 = cljs.core.async.reduce.call(null,f__$1,init,ch);
var state_28897__$1 = state_28897;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_28897__$1,(2),inst_28892);
} else {
if((state_val_28898 === (2))){
var inst_28894 = (state_28897[(2)]);
var inst_28895 = f__$1.call(null,inst_28894);
var state_28897__$1 = state_28897;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28897__$1,inst_28895);
} else {
return null;
}
}
});})(c__28077__auto__,f__$1))
;
return ((function (switch__27965__auto__,c__28077__auto__,f__$1){
return (function() {
var cljs$core$async$transduce_$_state_machine__27966__auto__ = null;
var cljs$core$async$transduce_$_state_machine__27966__auto____0 = (function (){
var statearr_28902 = [null,null,null,null,null,null,null];
(statearr_28902[(0)] = cljs$core$async$transduce_$_state_machine__27966__auto__);

(statearr_28902[(1)] = (1));

return statearr_28902;
});
var cljs$core$async$transduce_$_state_machine__27966__auto____1 = (function (state_28897){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_28897);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e28903){if((e28903 instanceof Object)){
var ex__27969__auto__ = e28903;
var statearr_28904_28906 = state_28897;
(statearr_28904_28906[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28897);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28903;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28907 = state_28897;
state_28897 = G__28907;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$transduce_$_state_machine__27966__auto__ = function(state_28897){
switch(arguments.length){
case 0:
return cljs$core$async$transduce_$_state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$transduce_$_state_machine__27966__auto____1.call(this,state_28897);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$transduce_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$transduce_$_state_machine__27966__auto____0;
cljs$core$async$transduce_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$transduce_$_state_machine__27966__auto____1;
return cljs$core$async$transduce_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto__,f__$1))
})();
var state__28079__auto__ = (function (){var statearr_28905 = f__28078__auto__.call(null);
(statearr_28905[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto__);

return statearr_28905;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto__,f__$1))
);

return c__28077__auto__;
});
/**
 * Puts the contents of coll into the supplied channel.
 * 
 *   By default the channel will be closed after the items are copied,
 *   but can be determined by the close? parameter.
 * 
 *   Returns a channel which will close after the items are copied.
 */
cljs.core.async.onto_chan = (function cljs$core$async$onto_chan(var_args){
var args28908 = [];
var len__26974__auto___28960 = arguments.length;
var i__26975__auto___28961 = (0);
while(true){
if((i__26975__auto___28961 < len__26974__auto___28960)){
args28908.push((arguments[i__26975__auto___28961]));

var G__28962 = (i__26975__auto___28961 + (1));
i__26975__auto___28961 = G__28962;
continue;
} else {
}
break;
}

var G__28910 = args28908.length;
switch (G__28910) {
case 2:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args28908.length)].join('')));

}
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$2 = (function (ch,coll){
return cljs.core.async.onto_chan.call(null,ch,coll,true);
});

cljs.core.async.onto_chan.cljs$core$IFn$_invoke$arity$3 = (function (ch,coll,close_QMARK_){
var c__28077__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto__){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto__){
return (function (state_28935){
var state_val_28936 = (state_28935[(1)]);
if((state_val_28936 === (7))){
var inst_28917 = (state_28935[(2)]);
var state_28935__$1 = state_28935;
var statearr_28937_28964 = state_28935__$1;
(statearr_28937_28964[(2)] = inst_28917);

(statearr_28937_28964[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28936 === (1))){
var inst_28911 = cljs.core.seq.call(null,coll);
var inst_28912 = inst_28911;
var state_28935__$1 = (function (){var statearr_28938 = state_28935;
(statearr_28938[(7)] = inst_28912);

return statearr_28938;
})();
var statearr_28939_28965 = state_28935__$1;
(statearr_28939_28965[(2)] = null);

(statearr_28939_28965[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28936 === (4))){
var inst_28912 = (state_28935[(7)]);
var inst_28915 = cljs.core.first.call(null,inst_28912);
var state_28935__$1 = state_28935;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_28935__$1,(7),ch,inst_28915);
} else {
if((state_val_28936 === (13))){
var inst_28929 = (state_28935[(2)]);
var state_28935__$1 = state_28935;
var statearr_28940_28966 = state_28935__$1;
(statearr_28940_28966[(2)] = inst_28929);

(statearr_28940_28966[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28936 === (6))){
var inst_28920 = (state_28935[(2)]);
var state_28935__$1 = state_28935;
if(cljs.core.truth_(inst_28920)){
var statearr_28941_28967 = state_28935__$1;
(statearr_28941_28967[(1)] = (8));

} else {
var statearr_28942_28968 = state_28935__$1;
(statearr_28942_28968[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28936 === (3))){
var inst_28933 = (state_28935[(2)]);
var state_28935__$1 = state_28935;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_28935__$1,inst_28933);
} else {
if((state_val_28936 === (12))){
var state_28935__$1 = state_28935;
var statearr_28943_28969 = state_28935__$1;
(statearr_28943_28969[(2)] = null);

(statearr_28943_28969[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28936 === (2))){
var inst_28912 = (state_28935[(7)]);
var state_28935__$1 = state_28935;
if(cljs.core.truth_(inst_28912)){
var statearr_28944_28970 = state_28935__$1;
(statearr_28944_28970[(1)] = (4));

} else {
var statearr_28945_28971 = state_28935__$1;
(statearr_28945_28971[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28936 === (11))){
var inst_28926 = cljs.core.async.close_BANG_.call(null,ch);
var state_28935__$1 = state_28935;
var statearr_28946_28972 = state_28935__$1;
(statearr_28946_28972[(2)] = inst_28926);

(statearr_28946_28972[(1)] = (13));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28936 === (9))){
var state_28935__$1 = state_28935;
if(cljs.core.truth_(close_QMARK_)){
var statearr_28947_28973 = state_28935__$1;
(statearr_28947_28973[(1)] = (11));

} else {
var statearr_28948_28974 = state_28935__$1;
(statearr_28948_28974[(1)] = (12));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28936 === (5))){
var inst_28912 = (state_28935[(7)]);
var state_28935__$1 = state_28935;
var statearr_28949_28975 = state_28935__$1;
(statearr_28949_28975[(2)] = inst_28912);

(statearr_28949_28975[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28936 === (10))){
var inst_28931 = (state_28935[(2)]);
var state_28935__$1 = state_28935;
var statearr_28950_28976 = state_28935__$1;
(statearr_28950_28976[(2)] = inst_28931);

(statearr_28950_28976[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_28936 === (8))){
var inst_28912 = (state_28935[(7)]);
var inst_28922 = cljs.core.next.call(null,inst_28912);
var inst_28912__$1 = inst_28922;
var state_28935__$1 = (function (){var statearr_28951 = state_28935;
(statearr_28951[(7)] = inst_28912__$1);

return statearr_28951;
})();
var statearr_28952_28977 = state_28935__$1;
(statearr_28952_28977[(2)] = null);

(statearr_28952_28977[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto__))
;
return ((function (switch__27965__auto__,c__28077__auto__){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_28956 = [null,null,null,null,null,null,null,null];
(statearr_28956[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_28956[(1)] = (1));

return statearr_28956;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_28935){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_28935);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e28957){if((e28957 instanceof Object)){
var ex__27969__auto__ = e28957;
var statearr_28958_28978 = state_28935;
(statearr_28958_28978[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_28935);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e28957;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__28979 = state_28935;
state_28935 = G__28979;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_28935){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_28935);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto__))
})();
var state__28079__auto__ = (function (){var statearr_28959 = f__28078__auto__.call(null);
(statearr_28959[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto__);

return statearr_28959;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto__))
);

return c__28077__auto__;
});

cljs.core.async.onto_chan.cljs$lang$maxFixedArity = 3;

/**
 * Creates and returns a channel which contains the contents of coll,
 *   closing when exhausted.
 */
cljs.core.async.to_chan = (function cljs$core$async$to_chan(coll){
var ch = cljs.core.async.chan.call(null,cljs.core.bounded_count.call(null,(100),coll));
cljs.core.async.onto_chan.call(null,ch,coll);

return ch;
});

/**
 * @interface
 */
cljs.core.async.Mux = function(){};

cljs.core.async.muxch_STAR_ = (function cljs$core$async$muxch_STAR_(_){
if((!((_ == null))) && (!((_.cljs$core$async$Mux$muxch_STAR_$arity$1 == null)))){
return _.cljs$core$async$Mux$muxch_STAR_$arity$1(_);
} else {
var x__26562__auto__ = (((_ == null))?null:_);
var m__26563__auto__ = (cljs.core.async.muxch_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,_);
} else {
var m__26563__auto____$1 = (cljs.core.async.muxch_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,_);
} else {
throw cljs.core.missing_protocol.call(null,"Mux.muxch*",_);
}
}
}
});


/**
 * @interface
 */
cljs.core.async.Mult = function(){};

cljs.core.async.tap_STAR_ = (function cljs$core$async$tap_STAR_(m,ch,close_QMARK_){
if((!((m == null))) && (!((m.cljs$core$async$Mult$tap_STAR_$arity$3 == null)))){
return m.cljs$core$async$Mult$tap_STAR_$arity$3(m,ch,close_QMARK_);
} else {
var x__26562__auto__ = (((m == null))?null:m);
var m__26563__auto__ = (cljs.core.async.tap_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,m,ch,close_QMARK_);
} else {
var m__26563__auto____$1 = (cljs.core.async.tap_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,m,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.tap*",m);
}
}
}
});

cljs.core.async.untap_STAR_ = (function cljs$core$async$untap_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mult$untap_STAR_$arity$2(m,ch);
} else {
var x__26562__auto__ = (((m == null))?null:m);
var m__26563__auto__ = (cljs.core.async.untap_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,m,ch);
} else {
var m__26563__auto____$1 = (cljs.core.async.untap_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap*",m);
}
}
}
});

cljs.core.async.untap_all_STAR_ = (function cljs$core$async$untap_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mult$untap_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mult$untap_all_STAR_$arity$1(m);
} else {
var x__26562__auto__ = (((m == null))?null:m);
var m__26563__auto__ = (cljs.core.async.untap_all_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,m);
} else {
var m__26563__auto____$1 = (cljs.core.async.untap_all_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mult.untap-all*",m);
}
}
}
});

/**
 * Creates and returns a mult(iple) of the supplied channel. Channels
 *   containing copies of the channel can be created with 'tap', and
 *   detached with 'untap'.
 * 
 *   Each item is distributed to all taps in parallel and synchronously,
 *   i.e. each tap must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow taps from holding up the mult.
 * 
 *   Items received when there are no taps get dropped.
 * 
 *   If a tap puts to a closed channel, it will be removed from the mult.
 */
cljs.core.async.mult = (function cljs$core$async$mult(ch){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async29205 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Mult}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async29205 = (function (mult,ch,cs,meta29206){
this.mult = mult;
this.ch = ch;
this.cs = cs;
this.meta29206 = meta29206;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async29205.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs){
return (function (_29207,meta29206__$1){
var self__ = this;
var _29207__$1 = this;
return (new cljs.core.async.t_cljs$core$async29205(self__.mult,self__.ch,self__.cs,meta29206__$1));
});})(cs))
;

cljs.core.async.t_cljs$core$async29205.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs){
return (function (_29207){
var self__ = this;
var _29207__$1 = this;
return self__.meta29206;
});})(cs))
;

cljs.core.async.t_cljs$core$async29205.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t_cljs$core$async29205.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(cs))
;

cljs.core.async.t_cljs$core$async29205.prototype.cljs$core$async$Mult$ = true;

cljs.core.async.t_cljs$core$async29205.prototype.cljs$core$async$Mult$tap_STAR_$arity$3 = ((function (cs){
return (function (_,ch__$1,close_QMARK_){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch__$1,close_QMARK_);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async29205.prototype.cljs$core$async$Mult$untap_STAR_$arity$2 = ((function (cs){
return (function (_,ch__$1){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch__$1);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async29205.prototype.cljs$core$async$Mult$untap_all_STAR_$arity$1 = ((function (cs){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return null;
});})(cs))
;

cljs.core.async.t_cljs$core$async29205.getBasis = ((function (cs){
return (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"mult","mult",-1187640995,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Creates and returns a mult(iple) of the supplied channel. Channels\n  containing copies of the channel can be created with 'tap', and\n  detached with 'untap'.\n\n  Each item is distributed to all taps in parallel and synchronously,\n  i.e. each tap must accept before the next item is distributed. Use\n  buffering/windowing to prevent slow taps from holding up the mult.\n\n  Items received when there are no taps get dropped.\n\n  If a tap puts to a closed channel, it will be removed from the mult."], null)),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"meta29206","meta29206",830950690,null)], null);
});})(cs))
;

cljs.core.async.t_cljs$core$async29205.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async29205.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async29205";

cljs.core.async.t_cljs$core$async29205.cljs$lang$ctorPrWriter = ((function (cs){
return (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.core.async/t_cljs$core$async29205");
});})(cs))
;

cljs.core.async.__GT_t_cljs$core$async29205 = ((function (cs){
return (function cljs$core$async$mult_$___GT_t_cljs$core$async29205(mult__$1,ch__$1,cs__$1,meta29206){
return (new cljs.core.async.t_cljs$core$async29205(mult__$1,ch__$1,cs__$1,meta29206));
});})(cs))
;

}

return (new cljs.core.async.t_cljs$core$async29205(cljs$core$async$mult,ch,cs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = ((function (cs,m,dchan,dctr){
return (function (_){
if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,true);
} else {
return null;
}
});})(cs,m,dchan,dctr))
;
var c__28077__auto___29430 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___29430,cs,m,dchan,dctr,done){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___29430,cs,m,dchan,dctr,done){
return (function (state_29342){
var state_val_29343 = (state_29342[(1)]);
if((state_val_29343 === (7))){
var inst_29338 = (state_29342[(2)]);
var state_29342__$1 = state_29342;
var statearr_29344_29431 = state_29342__$1;
(statearr_29344_29431[(2)] = inst_29338);

(statearr_29344_29431[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (20))){
var inst_29241 = (state_29342[(7)]);
var inst_29253 = cljs.core.first.call(null,inst_29241);
var inst_29254 = cljs.core.nth.call(null,inst_29253,(0),null);
var inst_29255 = cljs.core.nth.call(null,inst_29253,(1),null);
var state_29342__$1 = (function (){var statearr_29345 = state_29342;
(statearr_29345[(8)] = inst_29254);

return statearr_29345;
})();
if(cljs.core.truth_(inst_29255)){
var statearr_29346_29432 = state_29342__$1;
(statearr_29346_29432[(1)] = (22));

} else {
var statearr_29347_29433 = state_29342__$1;
(statearr_29347_29433[(1)] = (23));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (27))){
var inst_29283 = (state_29342[(9)]);
var inst_29290 = (state_29342[(10)]);
var inst_29285 = (state_29342[(11)]);
var inst_29210 = (state_29342[(12)]);
var inst_29290__$1 = cljs.core._nth.call(null,inst_29283,inst_29285);
var inst_29291 = cljs.core.async.put_BANG_.call(null,inst_29290__$1,inst_29210,done);
var state_29342__$1 = (function (){var statearr_29348 = state_29342;
(statearr_29348[(10)] = inst_29290__$1);

return statearr_29348;
})();
if(cljs.core.truth_(inst_29291)){
var statearr_29349_29434 = state_29342__$1;
(statearr_29349_29434[(1)] = (30));

} else {
var statearr_29350_29435 = state_29342__$1;
(statearr_29350_29435[(1)] = (31));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (1))){
var state_29342__$1 = state_29342;
var statearr_29351_29436 = state_29342__$1;
(statearr_29351_29436[(2)] = null);

(statearr_29351_29436[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (24))){
var inst_29241 = (state_29342[(7)]);
var inst_29260 = (state_29342[(2)]);
var inst_29261 = cljs.core.next.call(null,inst_29241);
var inst_29219 = inst_29261;
var inst_29220 = null;
var inst_29221 = (0);
var inst_29222 = (0);
var state_29342__$1 = (function (){var statearr_29352 = state_29342;
(statearr_29352[(13)] = inst_29260);

(statearr_29352[(14)] = inst_29219);

(statearr_29352[(15)] = inst_29220);

(statearr_29352[(16)] = inst_29222);

(statearr_29352[(17)] = inst_29221);

return statearr_29352;
})();
var statearr_29353_29437 = state_29342__$1;
(statearr_29353_29437[(2)] = null);

(statearr_29353_29437[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (39))){
var state_29342__$1 = state_29342;
var statearr_29357_29438 = state_29342__$1;
(statearr_29357_29438[(2)] = null);

(statearr_29357_29438[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (4))){
var inst_29210 = (state_29342[(12)]);
var inst_29210__$1 = (state_29342[(2)]);
var inst_29211 = (inst_29210__$1 == null);
var state_29342__$1 = (function (){var statearr_29358 = state_29342;
(statearr_29358[(12)] = inst_29210__$1);

return statearr_29358;
})();
if(cljs.core.truth_(inst_29211)){
var statearr_29359_29439 = state_29342__$1;
(statearr_29359_29439[(1)] = (5));

} else {
var statearr_29360_29440 = state_29342__$1;
(statearr_29360_29440[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (15))){
var inst_29219 = (state_29342[(14)]);
var inst_29220 = (state_29342[(15)]);
var inst_29222 = (state_29342[(16)]);
var inst_29221 = (state_29342[(17)]);
var inst_29237 = (state_29342[(2)]);
var inst_29238 = (inst_29222 + (1));
var tmp29354 = inst_29219;
var tmp29355 = inst_29220;
var tmp29356 = inst_29221;
var inst_29219__$1 = tmp29354;
var inst_29220__$1 = tmp29355;
var inst_29221__$1 = tmp29356;
var inst_29222__$1 = inst_29238;
var state_29342__$1 = (function (){var statearr_29361 = state_29342;
(statearr_29361[(14)] = inst_29219__$1);

(statearr_29361[(15)] = inst_29220__$1);

(statearr_29361[(16)] = inst_29222__$1);

(statearr_29361[(17)] = inst_29221__$1);

(statearr_29361[(18)] = inst_29237);

return statearr_29361;
})();
var statearr_29362_29441 = state_29342__$1;
(statearr_29362_29441[(2)] = null);

(statearr_29362_29441[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (21))){
var inst_29264 = (state_29342[(2)]);
var state_29342__$1 = state_29342;
var statearr_29366_29442 = state_29342__$1;
(statearr_29366_29442[(2)] = inst_29264);

(statearr_29366_29442[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (31))){
var inst_29290 = (state_29342[(10)]);
var inst_29294 = done.call(null,null);
var inst_29295 = cljs.core.async.untap_STAR_.call(null,m,inst_29290);
var state_29342__$1 = (function (){var statearr_29367 = state_29342;
(statearr_29367[(19)] = inst_29294);

return statearr_29367;
})();
var statearr_29368_29443 = state_29342__$1;
(statearr_29368_29443[(2)] = inst_29295);

(statearr_29368_29443[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (32))){
var inst_29282 = (state_29342[(20)]);
var inst_29283 = (state_29342[(9)]);
var inst_29284 = (state_29342[(21)]);
var inst_29285 = (state_29342[(11)]);
var inst_29297 = (state_29342[(2)]);
var inst_29298 = (inst_29285 + (1));
var tmp29363 = inst_29282;
var tmp29364 = inst_29283;
var tmp29365 = inst_29284;
var inst_29282__$1 = tmp29363;
var inst_29283__$1 = tmp29364;
var inst_29284__$1 = tmp29365;
var inst_29285__$1 = inst_29298;
var state_29342__$1 = (function (){var statearr_29369 = state_29342;
(statearr_29369[(20)] = inst_29282__$1);

(statearr_29369[(9)] = inst_29283__$1);

(statearr_29369[(21)] = inst_29284__$1);

(statearr_29369[(11)] = inst_29285__$1);

(statearr_29369[(22)] = inst_29297);

return statearr_29369;
})();
var statearr_29370_29444 = state_29342__$1;
(statearr_29370_29444[(2)] = null);

(statearr_29370_29444[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (40))){
var inst_29310 = (state_29342[(23)]);
var inst_29314 = done.call(null,null);
var inst_29315 = cljs.core.async.untap_STAR_.call(null,m,inst_29310);
var state_29342__$1 = (function (){var statearr_29371 = state_29342;
(statearr_29371[(24)] = inst_29314);

return statearr_29371;
})();
var statearr_29372_29445 = state_29342__$1;
(statearr_29372_29445[(2)] = inst_29315);

(statearr_29372_29445[(1)] = (41));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (33))){
var inst_29301 = (state_29342[(25)]);
var inst_29303 = cljs.core.chunked_seq_QMARK_.call(null,inst_29301);
var state_29342__$1 = state_29342;
if(inst_29303){
var statearr_29373_29446 = state_29342__$1;
(statearr_29373_29446[(1)] = (36));

} else {
var statearr_29374_29447 = state_29342__$1;
(statearr_29374_29447[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (13))){
var inst_29231 = (state_29342[(26)]);
var inst_29234 = cljs.core.async.close_BANG_.call(null,inst_29231);
var state_29342__$1 = state_29342;
var statearr_29375_29448 = state_29342__$1;
(statearr_29375_29448[(2)] = inst_29234);

(statearr_29375_29448[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (22))){
var inst_29254 = (state_29342[(8)]);
var inst_29257 = cljs.core.async.close_BANG_.call(null,inst_29254);
var state_29342__$1 = state_29342;
var statearr_29376_29449 = state_29342__$1;
(statearr_29376_29449[(2)] = inst_29257);

(statearr_29376_29449[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (36))){
var inst_29301 = (state_29342[(25)]);
var inst_29305 = cljs.core.chunk_first.call(null,inst_29301);
var inst_29306 = cljs.core.chunk_rest.call(null,inst_29301);
var inst_29307 = cljs.core.count.call(null,inst_29305);
var inst_29282 = inst_29306;
var inst_29283 = inst_29305;
var inst_29284 = inst_29307;
var inst_29285 = (0);
var state_29342__$1 = (function (){var statearr_29377 = state_29342;
(statearr_29377[(20)] = inst_29282);

(statearr_29377[(9)] = inst_29283);

(statearr_29377[(21)] = inst_29284);

(statearr_29377[(11)] = inst_29285);

return statearr_29377;
})();
var statearr_29378_29450 = state_29342__$1;
(statearr_29378_29450[(2)] = null);

(statearr_29378_29450[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (41))){
var inst_29301 = (state_29342[(25)]);
var inst_29317 = (state_29342[(2)]);
var inst_29318 = cljs.core.next.call(null,inst_29301);
var inst_29282 = inst_29318;
var inst_29283 = null;
var inst_29284 = (0);
var inst_29285 = (0);
var state_29342__$1 = (function (){var statearr_29379 = state_29342;
(statearr_29379[(20)] = inst_29282);

(statearr_29379[(9)] = inst_29283);

(statearr_29379[(21)] = inst_29284);

(statearr_29379[(11)] = inst_29285);

(statearr_29379[(27)] = inst_29317);

return statearr_29379;
})();
var statearr_29380_29451 = state_29342__$1;
(statearr_29380_29451[(2)] = null);

(statearr_29380_29451[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (43))){
var state_29342__$1 = state_29342;
var statearr_29381_29452 = state_29342__$1;
(statearr_29381_29452[(2)] = null);

(statearr_29381_29452[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (29))){
var inst_29326 = (state_29342[(2)]);
var state_29342__$1 = state_29342;
var statearr_29382_29453 = state_29342__$1;
(statearr_29382_29453[(2)] = inst_29326);

(statearr_29382_29453[(1)] = (26));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (44))){
var inst_29335 = (state_29342[(2)]);
var state_29342__$1 = (function (){var statearr_29383 = state_29342;
(statearr_29383[(28)] = inst_29335);

return statearr_29383;
})();
var statearr_29384_29454 = state_29342__$1;
(statearr_29384_29454[(2)] = null);

(statearr_29384_29454[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (6))){
var inst_29274 = (state_29342[(29)]);
var inst_29273 = cljs.core.deref.call(null,cs);
var inst_29274__$1 = cljs.core.keys.call(null,inst_29273);
var inst_29275 = cljs.core.count.call(null,inst_29274__$1);
var inst_29276 = cljs.core.reset_BANG_.call(null,dctr,inst_29275);
var inst_29281 = cljs.core.seq.call(null,inst_29274__$1);
var inst_29282 = inst_29281;
var inst_29283 = null;
var inst_29284 = (0);
var inst_29285 = (0);
var state_29342__$1 = (function (){var statearr_29385 = state_29342;
(statearr_29385[(20)] = inst_29282);

(statearr_29385[(29)] = inst_29274__$1);

(statearr_29385[(9)] = inst_29283);

(statearr_29385[(21)] = inst_29284);

(statearr_29385[(11)] = inst_29285);

(statearr_29385[(30)] = inst_29276);

return statearr_29385;
})();
var statearr_29386_29455 = state_29342__$1;
(statearr_29386_29455[(2)] = null);

(statearr_29386_29455[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (28))){
var inst_29282 = (state_29342[(20)]);
var inst_29301 = (state_29342[(25)]);
var inst_29301__$1 = cljs.core.seq.call(null,inst_29282);
var state_29342__$1 = (function (){var statearr_29387 = state_29342;
(statearr_29387[(25)] = inst_29301__$1);

return statearr_29387;
})();
if(inst_29301__$1){
var statearr_29388_29456 = state_29342__$1;
(statearr_29388_29456[(1)] = (33));

} else {
var statearr_29389_29457 = state_29342__$1;
(statearr_29389_29457[(1)] = (34));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (25))){
var inst_29284 = (state_29342[(21)]);
var inst_29285 = (state_29342[(11)]);
var inst_29287 = (inst_29285 < inst_29284);
var inst_29288 = inst_29287;
var state_29342__$1 = state_29342;
if(cljs.core.truth_(inst_29288)){
var statearr_29390_29458 = state_29342__$1;
(statearr_29390_29458[(1)] = (27));

} else {
var statearr_29391_29459 = state_29342__$1;
(statearr_29391_29459[(1)] = (28));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (34))){
var state_29342__$1 = state_29342;
var statearr_29392_29460 = state_29342__$1;
(statearr_29392_29460[(2)] = null);

(statearr_29392_29460[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (17))){
var state_29342__$1 = state_29342;
var statearr_29393_29461 = state_29342__$1;
(statearr_29393_29461[(2)] = null);

(statearr_29393_29461[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (3))){
var inst_29340 = (state_29342[(2)]);
var state_29342__$1 = state_29342;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_29342__$1,inst_29340);
} else {
if((state_val_29343 === (12))){
var inst_29269 = (state_29342[(2)]);
var state_29342__$1 = state_29342;
var statearr_29394_29462 = state_29342__$1;
(statearr_29394_29462[(2)] = inst_29269);

(statearr_29394_29462[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (2))){
var state_29342__$1 = state_29342;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_29342__$1,(4),ch);
} else {
if((state_val_29343 === (23))){
var state_29342__$1 = state_29342;
var statearr_29395_29463 = state_29342__$1;
(statearr_29395_29463[(2)] = null);

(statearr_29395_29463[(1)] = (24));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (35))){
var inst_29324 = (state_29342[(2)]);
var state_29342__$1 = state_29342;
var statearr_29396_29464 = state_29342__$1;
(statearr_29396_29464[(2)] = inst_29324);

(statearr_29396_29464[(1)] = (29));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (19))){
var inst_29241 = (state_29342[(7)]);
var inst_29245 = cljs.core.chunk_first.call(null,inst_29241);
var inst_29246 = cljs.core.chunk_rest.call(null,inst_29241);
var inst_29247 = cljs.core.count.call(null,inst_29245);
var inst_29219 = inst_29246;
var inst_29220 = inst_29245;
var inst_29221 = inst_29247;
var inst_29222 = (0);
var state_29342__$1 = (function (){var statearr_29397 = state_29342;
(statearr_29397[(14)] = inst_29219);

(statearr_29397[(15)] = inst_29220);

(statearr_29397[(16)] = inst_29222);

(statearr_29397[(17)] = inst_29221);

return statearr_29397;
})();
var statearr_29398_29465 = state_29342__$1;
(statearr_29398_29465[(2)] = null);

(statearr_29398_29465[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (11))){
var inst_29219 = (state_29342[(14)]);
var inst_29241 = (state_29342[(7)]);
var inst_29241__$1 = cljs.core.seq.call(null,inst_29219);
var state_29342__$1 = (function (){var statearr_29399 = state_29342;
(statearr_29399[(7)] = inst_29241__$1);

return statearr_29399;
})();
if(inst_29241__$1){
var statearr_29400_29466 = state_29342__$1;
(statearr_29400_29466[(1)] = (16));

} else {
var statearr_29401_29467 = state_29342__$1;
(statearr_29401_29467[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (9))){
var inst_29271 = (state_29342[(2)]);
var state_29342__$1 = state_29342;
var statearr_29402_29468 = state_29342__$1;
(statearr_29402_29468[(2)] = inst_29271);

(statearr_29402_29468[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (5))){
var inst_29217 = cljs.core.deref.call(null,cs);
var inst_29218 = cljs.core.seq.call(null,inst_29217);
var inst_29219 = inst_29218;
var inst_29220 = null;
var inst_29221 = (0);
var inst_29222 = (0);
var state_29342__$1 = (function (){var statearr_29403 = state_29342;
(statearr_29403[(14)] = inst_29219);

(statearr_29403[(15)] = inst_29220);

(statearr_29403[(16)] = inst_29222);

(statearr_29403[(17)] = inst_29221);

return statearr_29403;
})();
var statearr_29404_29469 = state_29342__$1;
(statearr_29404_29469[(2)] = null);

(statearr_29404_29469[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (14))){
var state_29342__$1 = state_29342;
var statearr_29405_29470 = state_29342__$1;
(statearr_29405_29470[(2)] = null);

(statearr_29405_29470[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (45))){
var inst_29332 = (state_29342[(2)]);
var state_29342__$1 = state_29342;
var statearr_29406_29471 = state_29342__$1;
(statearr_29406_29471[(2)] = inst_29332);

(statearr_29406_29471[(1)] = (44));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (26))){
var inst_29274 = (state_29342[(29)]);
var inst_29328 = (state_29342[(2)]);
var inst_29329 = cljs.core.seq.call(null,inst_29274);
var state_29342__$1 = (function (){var statearr_29407 = state_29342;
(statearr_29407[(31)] = inst_29328);

return statearr_29407;
})();
if(inst_29329){
var statearr_29408_29472 = state_29342__$1;
(statearr_29408_29472[(1)] = (42));

} else {
var statearr_29409_29473 = state_29342__$1;
(statearr_29409_29473[(1)] = (43));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (16))){
var inst_29241 = (state_29342[(7)]);
var inst_29243 = cljs.core.chunked_seq_QMARK_.call(null,inst_29241);
var state_29342__$1 = state_29342;
if(inst_29243){
var statearr_29410_29474 = state_29342__$1;
(statearr_29410_29474[(1)] = (19));

} else {
var statearr_29411_29475 = state_29342__$1;
(statearr_29411_29475[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (38))){
var inst_29321 = (state_29342[(2)]);
var state_29342__$1 = state_29342;
var statearr_29412_29476 = state_29342__$1;
(statearr_29412_29476[(2)] = inst_29321);

(statearr_29412_29476[(1)] = (35));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (30))){
var state_29342__$1 = state_29342;
var statearr_29413_29477 = state_29342__$1;
(statearr_29413_29477[(2)] = null);

(statearr_29413_29477[(1)] = (32));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (10))){
var inst_29220 = (state_29342[(15)]);
var inst_29222 = (state_29342[(16)]);
var inst_29230 = cljs.core._nth.call(null,inst_29220,inst_29222);
var inst_29231 = cljs.core.nth.call(null,inst_29230,(0),null);
var inst_29232 = cljs.core.nth.call(null,inst_29230,(1),null);
var state_29342__$1 = (function (){var statearr_29414 = state_29342;
(statearr_29414[(26)] = inst_29231);

return statearr_29414;
})();
if(cljs.core.truth_(inst_29232)){
var statearr_29415_29478 = state_29342__$1;
(statearr_29415_29478[(1)] = (13));

} else {
var statearr_29416_29479 = state_29342__$1;
(statearr_29416_29479[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (18))){
var inst_29267 = (state_29342[(2)]);
var state_29342__$1 = state_29342;
var statearr_29417_29480 = state_29342__$1;
(statearr_29417_29480[(2)] = inst_29267);

(statearr_29417_29480[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (42))){
var state_29342__$1 = state_29342;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_29342__$1,(45),dchan);
} else {
if((state_val_29343 === (37))){
var inst_29301 = (state_29342[(25)]);
var inst_29210 = (state_29342[(12)]);
var inst_29310 = (state_29342[(23)]);
var inst_29310__$1 = cljs.core.first.call(null,inst_29301);
var inst_29311 = cljs.core.async.put_BANG_.call(null,inst_29310__$1,inst_29210,done);
var state_29342__$1 = (function (){var statearr_29418 = state_29342;
(statearr_29418[(23)] = inst_29310__$1);

return statearr_29418;
})();
if(cljs.core.truth_(inst_29311)){
var statearr_29419_29481 = state_29342__$1;
(statearr_29419_29481[(1)] = (39));

} else {
var statearr_29420_29482 = state_29342__$1;
(statearr_29420_29482[(1)] = (40));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29343 === (8))){
var inst_29222 = (state_29342[(16)]);
var inst_29221 = (state_29342[(17)]);
var inst_29224 = (inst_29222 < inst_29221);
var inst_29225 = inst_29224;
var state_29342__$1 = state_29342;
if(cljs.core.truth_(inst_29225)){
var statearr_29421_29483 = state_29342__$1;
(statearr_29421_29483[(1)] = (10));

} else {
var statearr_29422_29484 = state_29342__$1;
(statearr_29422_29484[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___29430,cs,m,dchan,dctr,done))
;
return ((function (switch__27965__auto__,c__28077__auto___29430,cs,m,dchan,dctr,done){
return (function() {
var cljs$core$async$mult_$_state_machine__27966__auto__ = null;
var cljs$core$async$mult_$_state_machine__27966__auto____0 = (function (){
var statearr_29426 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_29426[(0)] = cljs$core$async$mult_$_state_machine__27966__auto__);

(statearr_29426[(1)] = (1));

return statearr_29426;
});
var cljs$core$async$mult_$_state_machine__27966__auto____1 = (function (state_29342){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_29342);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e29427){if((e29427 instanceof Object)){
var ex__27969__auto__ = e29427;
var statearr_29428_29485 = state_29342;
(statearr_29428_29485[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_29342);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e29427;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29486 = state_29342;
state_29342 = G__29486;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$mult_$_state_machine__27966__auto__ = function(state_29342){
switch(arguments.length){
case 0:
return cljs$core$async$mult_$_state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$mult_$_state_machine__27966__auto____1.call(this,state_29342);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mult_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mult_$_state_machine__27966__auto____0;
cljs$core$async$mult_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mult_$_state_machine__27966__auto____1;
return cljs$core$async$mult_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___29430,cs,m,dchan,dctr,done))
})();
var state__28079__auto__ = (function (){var statearr_29429 = f__28078__auto__.call(null);
(statearr_29429[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___29430);

return statearr_29429;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___29430,cs,m,dchan,dctr,done))
);


return m;
});
/**
 * Copies the mult source onto the supplied channel.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.tap = (function cljs$core$async$tap(var_args){
var args29487 = [];
var len__26974__auto___29490 = arguments.length;
var i__26975__auto___29491 = (0);
while(true){
if((i__26975__auto___29491 < len__26974__auto___29490)){
args29487.push((arguments[i__26975__auto___29491]));

var G__29492 = (i__26975__auto___29491 + (1));
i__26975__auto___29491 = G__29492;
continue;
} else {
}
break;
}

var G__29489 = args29487.length;
switch (G__29489) {
case 2:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args29487.length)].join('')));

}
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$2 = (function (mult,ch){
return cljs.core.async.tap.call(null,mult,ch,true);
});

cljs.core.async.tap.cljs$core$IFn$_invoke$arity$3 = (function (mult,ch,close_QMARK_){
cljs.core.async.tap_STAR_.call(null,mult,ch,close_QMARK_);

return ch;
});

cljs.core.async.tap.cljs$lang$maxFixedArity = 3;

/**
 * Disconnects a target channel from a mult
 */
cljs.core.async.untap = (function cljs$core$async$untap(mult,ch){
return cljs.core.async.untap_STAR_.call(null,mult,ch);
});
/**
 * Disconnects all target channels from a mult
 */
cljs.core.async.untap_all = (function cljs$core$async$untap_all(mult){
return cljs.core.async.untap_all_STAR_.call(null,mult);
});

/**
 * @interface
 */
cljs.core.async.Mix = function(){};

cljs.core.async.admix_STAR_ = (function cljs$core$async$admix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$admix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$admix_STAR_$arity$2(m,ch);
} else {
var x__26562__auto__ = (((m == null))?null:m);
var m__26563__auto__ = (cljs.core.async.admix_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,m,ch);
} else {
var m__26563__auto____$1 = (cljs.core.async.admix_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.admix*",m);
}
}
}
});

cljs.core.async.unmix_STAR_ = (function cljs$core$async$unmix_STAR_(m,ch){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$unmix_STAR_$arity$2(m,ch);
} else {
var x__26562__auto__ = (((m == null))?null:m);
var m__26563__auto__ = (cljs.core.async.unmix_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,m,ch);
} else {
var m__26563__auto____$1 = (cljs.core.async.unmix_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,m,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix*",m);
}
}
}
});

cljs.core.async.unmix_all_STAR_ = (function cljs$core$async$unmix_all_STAR_(m){
if((!((m == null))) && (!((m.cljs$core$async$Mix$unmix_all_STAR_$arity$1 == null)))){
return m.cljs$core$async$Mix$unmix_all_STAR_$arity$1(m);
} else {
var x__26562__auto__ = (((m == null))?null:m);
var m__26563__auto__ = (cljs.core.async.unmix_all_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,m);
} else {
var m__26563__auto____$1 = (cljs.core.async.unmix_all_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,m);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.unmix-all*",m);
}
}
}
});

cljs.core.async.toggle_STAR_ = (function cljs$core$async$toggle_STAR_(m,state_map){
if((!((m == null))) && (!((m.cljs$core$async$Mix$toggle_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$toggle_STAR_$arity$2(m,state_map);
} else {
var x__26562__auto__ = (((m == null))?null:m);
var m__26563__auto__ = (cljs.core.async.toggle_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,m,state_map);
} else {
var m__26563__auto____$1 = (cljs.core.async.toggle_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,m,state_map);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.toggle*",m);
}
}
}
});

cljs.core.async.solo_mode_STAR_ = (function cljs$core$async$solo_mode_STAR_(m,mode){
if((!((m == null))) && (!((m.cljs$core$async$Mix$solo_mode_STAR_$arity$2 == null)))){
return m.cljs$core$async$Mix$solo_mode_STAR_$arity$2(m,mode);
} else {
var x__26562__auto__ = (((m == null))?null:m);
var m__26563__auto__ = (cljs.core.async.solo_mode_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,m,mode);
} else {
var m__26563__auto____$1 = (cljs.core.async.solo_mode_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,m,mode);
} else {
throw cljs.core.missing_protocol.call(null,"Mix.solo-mode*",m);
}
}
}
});

cljs.core.async.ioc_alts_BANG_ = (function cljs$core$async$ioc_alts_BANG_(var_args){
var args__26981__auto__ = [];
var len__26974__auto___29504 = arguments.length;
var i__26975__auto___29505 = (0);
while(true){
if((i__26975__auto___29505 < len__26974__auto___29504)){
args__26981__auto__.push((arguments[i__26975__auto___29505]));

var G__29506 = (i__26975__auto___29505 + (1));
i__26975__auto___29505 = G__29506;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((3) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((3)),(0),null)):null);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),argseq__26982__auto__);
});

cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic = (function (state,cont_block,ports,p__29498){
var map__29499 = p__29498;
var map__29499__$1 = ((((!((map__29499 == null)))?((((map__29499.cljs$lang$protocol_mask$partition0$ & (64))) || (map__29499.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__29499):map__29499);
var opts = map__29499__$1;
var statearr_29501_29507 = state;
(statearr_29501_29507[cljs.core.async.impl.ioc_helpers.STATE_IDX] = cont_block);


var temp__4657__auto__ = cljs.core.async.do_alts.call(null,((function (map__29499,map__29499__$1,opts){
return (function (val){
var statearr_29502_29508 = state;
(statearr_29502_29508[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = val);


return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state);
});})(map__29499,map__29499__$1,opts))
,ports,opts);
if(cljs.core.truth_(temp__4657__auto__)){
var cb = temp__4657__auto__;
var statearr_29503_29509 = state;
(statearr_29503_29509[cljs.core.async.impl.ioc_helpers.VALUE_IDX] = cljs.core.deref.call(null,cb));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
});

cljs.core.async.ioc_alts_BANG_.cljs$lang$maxFixedArity = (3);

cljs.core.async.ioc_alts_BANG_.cljs$lang$applyTo = (function (seq29494){
var G__29495 = cljs.core.first.call(null,seq29494);
var seq29494__$1 = cljs.core.next.call(null,seq29494);
var G__29496 = cljs.core.first.call(null,seq29494__$1);
var seq29494__$2 = cljs.core.next.call(null,seq29494__$1);
var G__29497 = cljs.core.first.call(null,seq29494__$2);
var seq29494__$3 = cljs.core.next.call(null,seq29494__$2);
return cljs.core.async.ioc_alts_BANG_.cljs$core$IFn$_invoke$arity$variadic(G__29495,G__29496,G__29497,seq29494__$3);
});

/**
 * Creates and returns a mix of one or more input channels which will
 *   be put on the supplied out channel. Input sources can be added to
 *   the mix with 'admix', and removed with 'unmix'. A mix supports
 *   soloing, muting and pausing multiple inputs atomically using
 *   'toggle', and can solo using either muting or pausing as determined
 *   by 'solo-mode'.
 * 
 *   Each channel can have zero or more boolean modes set via 'toggle':
 * 
 *   :solo - when true, only this (ond other soloed) channel(s) will appear
 *        in the mix output channel. :mute and :pause states of soloed
 *        channels are ignored. If solo-mode is :mute, non-soloed
 *        channels are muted, if :pause, non-soloed channels are
 *        paused.
 * 
 *   :mute - muted channels will have their contents consumed but not included in the mix
 *   :pause - paused channels will not have their contents consumed (and thus also not included in the mix)
 */
cljs.core.async.mix = (function cljs$core$async$mix(out){
var cs = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var solo_modes = new cljs.core.PersistentHashSet(null, new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"pause","pause",-2095325672),null,new cljs.core.Keyword(null,"mute","mute",1151223646),null], null), null);
var attrs = cljs.core.conj.call(null,solo_modes,new cljs.core.Keyword(null,"solo","solo",-316350075));
var solo_mode = cljs.core.atom.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646));
var change = cljs.core.async.chan.call(null);
var changed = ((function (cs,solo_modes,attrs,solo_mode,change){
return (function (){
return cljs.core.async.put_BANG_.call(null,change,true);
});})(cs,solo_modes,attrs,solo_mode,change))
;
var pick = ((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (attr,chs){
return cljs.core.reduce_kv.call(null,((function (cs,solo_modes,attrs,solo_mode,change,changed){
return (function (ret,c,v){
if(cljs.core.truth_(attr.call(null,v))){
return cljs.core.conj.call(null,ret,c);
} else {
return ret;
}
});})(cs,solo_modes,attrs,solo_mode,change,changed))
,cljs.core.PersistentHashSet.EMPTY,chs);
});})(cs,solo_modes,attrs,solo_mode,change,changed))
;
var calc_state = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick){
return (function (){
var chs = cljs.core.deref.call(null,cs);
var mode = cljs.core.deref.call(null,solo_mode);
var solos = pick.call(null,new cljs.core.Keyword(null,"solo","solo",-316350075),chs);
var pauses = pick.call(null,new cljs.core.Keyword(null,"pause","pause",-2095325672),chs);
return new cljs.core.PersistentArrayMap(null, 3, [new cljs.core.Keyword(null,"solos","solos",1441458643),solos,new cljs.core.Keyword(null,"mutes","mutes",1068806309),pick.call(null,new cljs.core.Keyword(null,"mute","mute",1151223646),chs),new cljs.core.Keyword(null,"reads","reads",-1215067361),cljs.core.conj.call(null,(((cljs.core._EQ_.call(null,mode,new cljs.core.Keyword(null,"pause","pause",-2095325672))) && (!(cljs.core.empty_QMARK_.call(null,solos))))?cljs.core.vec.call(null,solos):cljs.core.vec.call(null,cljs.core.remove.call(null,pauses,cljs.core.keys.call(null,chs)))),change)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick))
;
var m = (function (){
if(typeof cljs.core.async.t_cljs$core$async29675 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mix}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async29675 = (function (change,mix,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,meta29676){
this.change = change;
this.mix = mix;
this.solo_mode = solo_mode;
this.pick = pick;
this.cs = cs;
this.calc_state = calc_state;
this.out = out;
this.changed = changed;
this.solo_modes = solo_modes;
this.attrs = attrs;
this.meta29676 = meta29676;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async29675.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_29677,meta29676__$1){
var self__ = this;
var _29677__$1 = this;
return (new cljs.core.async.t_cljs$core$async29675(self__.change,self__.mix,self__.solo_mode,self__.pick,self__.cs,self__.calc_state,self__.out,self__.changed,self__.solo_modes,self__.attrs,meta29676__$1));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async29675.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_29677){
var self__ = this;
var _29677__$1 = this;
return self__.meta29676;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async29675.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t_cljs$core$async29675.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.out;
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async29675.prototype.cljs$core$async$Mix$ = true;

cljs.core.async.t_cljs$core$async29675.prototype.cljs$core$async$Mix$admix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.assoc,ch,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async29675.prototype.cljs$core$async$Mix$unmix_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,ch){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.dissoc,ch);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async29675.prototype.cljs$core$async$Mix$unmix_all_STAR_$arity$1 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_){
var self__ = this;
var ___$1 = this;
cljs.core.reset_BANG_.call(null,self__.cs,cljs.core.PersistentArrayMap.EMPTY);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async29675.prototype.cljs$core$async$Mix$toggle_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,state_map){
var self__ = this;
var ___$1 = this;
cljs.core.swap_BANG_.call(null,self__.cs,cljs.core.partial.call(null,cljs.core.merge_with,cljs.core.merge),state_map);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async29675.prototype.cljs$core$async$Mix$solo_mode_STAR_$arity$2 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (_,mode){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.solo_modes.call(null,mode))){
} else {
throw (new Error([cljs.core.str("Assert failed: "),cljs.core.str([cljs.core.str("mode must be one of: "),cljs.core.str(self__.solo_modes)].join('')),cljs.core.str("\n"),cljs.core.str("(solo-modes mode)")].join('')));
}

cljs.core.reset_BANG_.call(null,self__.solo_mode,mode);

return self__.changed.call(null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async29675.getBasis = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (){
return new cljs.core.PersistentVector(null, 11, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"change","change",477485025,null),cljs.core.with_meta(new cljs.core.Symbol(null,"mix","mix",2121373763,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 1, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"out","out",729986010,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Creates and returns a mix of one or more input channels which will\n  be put on the supplied out channel. Input sources can be added to\n  the mix with 'admix', and removed with 'unmix'. A mix supports\n  soloing, muting and pausing multiple inputs atomically using\n  'toggle', and can solo using either muting or pausing as determined\n  by 'solo-mode'.\n\n  Each channel can have zero or more boolean modes set via 'toggle':\n\n  :solo - when true, only this (ond other soloed) channel(s) will appear\n          in the mix output channel. :mute and :pause states of soloed\n          channels are ignored. If solo-mode is :mute, non-soloed\n          channels are muted, if :pause, non-soloed channels are\n          paused.\n\n  :mute - muted channels will have their contents consumed but not included in the mix\n  :pause - paused channels will not have their contents consumed (and thus also not included in the mix)\n"], null)),new cljs.core.Symbol(null,"solo-mode","solo-mode",2031788074,null),new cljs.core.Symbol(null,"pick","pick",1300068175,null),new cljs.core.Symbol(null,"cs","cs",-117024463,null),new cljs.core.Symbol(null,"calc-state","calc-state",-349968968,null),new cljs.core.Symbol(null,"out","out",729986010,null),new cljs.core.Symbol(null,"changed","changed",-2083710852,null),new cljs.core.Symbol(null,"solo-modes","solo-modes",882180540,null),new cljs.core.Symbol(null,"attrs","attrs",-450137186,null),new cljs.core.Symbol(null,"meta29676","meta29676",-878467886,null)], null);
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.t_cljs$core$async29675.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async29675.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async29675";

cljs.core.async.t_cljs$core$async29675.cljs$lang$ctorPrWriter = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.core.async/t_cljs$core$async29675");
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

cljs.core.async.__GT_t_cljs$core$async29675 = ((function (cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state){
return (function cljs$core$async$mix_$___GT_t_cljs$core$async29675(change__$1,mix__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta29676){
return (new cljs.core.async.t_cljs$core$async29675(change__$1,mix__$1,solo_mode__$1,pick__$1,cs__$1,calc_state__$1,out__$1,changed__$1,solo_modes__$1,attrs__$1,meta29676));
});})(cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state))
;

}

return (new cljs.core.async.t_cljs$core$async29675(change,cljs$core$async$mix,solo_mode,pick,cs,calc_state,out,changed,solo_modes,attrs,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__28077__auto___29840 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___29840,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___29840,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function (state_29777){
var state_val_29778 = (state_29777[(1)]);
if((state_val_29778 === (7))){
var inst_29693 = (state_29777[(2)]);
var state_29777__$1 = state_29777;
var statearr_29779_29841 = state_29777__$1;
(statearr_29779_29841[(2)] = inst_29693);

(statearr_29779_29841[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (20))){
var inst_29705 = (state_29777[(7)]);
var state_29777__$1 = state_29777;
var statearr_29780_29842 = state_29777__$1;
(statearr_29780_29842[(2)] = inst_29705);

(statearr_29780_29842[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (27))){
var state_29777__$1 = state_29777;
var statearr_29781_29843 = state_29777__$1;
(statearr_29781_29843[(2)] = null);

(statearr_29781_29843[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (1))){
var inst_29681 = (state_29777[(8)]);
var inst_29681__$1 = calc_state.call(null);
var inst_29683 = (inst_29681__$1 == null);
var inst_29684 = cljs.core.not.call(null,inst_29683);
var state_29777__$1 = (function (){var statearr_29782 = state_29777;
(statearr_29782[(8)] = inst_29681__$1);

return statearr_29782;
})();
if(inst_29684){
var statearr_29783_29844 = state_29777__$1;
(statearr_29783_29844[(1)] = (2));

} else {
var statearr_29784_29845 = state_29777__$1;
(statearr_29784_29845[(1)] = (3));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (24))){
var inst_29737 = (state_29777[(9)]);
var inst_29751 = (state_29777[(10)]);
var inst_29728 = (state_29777[(11)]);
var inst_29751__$1 = inst_29728.call(null,inst_29737);
var state_29777__$1 = (function (){var statearr_29785 = state_29777;
(statearr_29785[(10)] = inst_29751__$1);

return statearr_29785;
})();
if(cljs.core.truth_(inst_29751__$1)){
var statearr_29786_29846 = state_29777__$1;
(statearr_29786_29846[(1)] = (29));

} else {
var statearr_29787_29847 = state_29777__$1;
(statearr_29787_29847[(1)] = (30));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (4))){
var inst_29696 = (state_29777[(2)]);
var state_29777__$1 = state_29777;
if(cljs.core.truth_(inst_29696)){
var statearr_29788_29848 = state_29777__$1;
(statearr_29788_29848[(1)] = (8));

} else {
var statearr_29789_29849 = state_29777__$1;
(statearr_29789_29849[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (15))){
var inst_29722 = (state_29777[(2)]);
var state_29777__$1 = state_29777;
if(cljs.core.truth_(inst_29722)){
var statearr_29790_29850 = state_29777__$1;
(statearr_29790_29850[(1)] = (19));

} else {
var statearr_29791_29851 = state_29777__$1;
(statearr_29791_29851[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (21))){
var inst_29727 = (state_29777[(12)]);
var inst_29727__$1 = (state_29777[(2)]);
var inst_29728 = cljs.core.get.call(null,inst_29727__$1,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_29729 = cljs.core.get.call(null,inst_29727__$1,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_29730 = cljs.core.get.call(null,inst_29727__$1,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var state_29777__$1 = (function (){var statearr_29792 = state_29777;
(statearr_29792[(13)] = inst_29729);

(statearr_29792[(12)] = inst_29727__$1);

(statearr_29792[(11)] = inst_29728);

return statearr_29792;
})();
return cljs.core.async.ioc_alts_BANG_.call(null,state_29777__$1,(22),inst_29730);
} else {
if((state_val_29778 === (31))){
var inst_29759 = (state_29777[(2)]);
var state_29777__$1 = state_29777;
if(cljs.core.truth_(inst_29759)){
var statearr_29793_29852 = state_29777__$1;
(statearr_29793_29852[(1)] = (32));

} else {
var statearr_29794_29853 = state_29777__$1;
(statearr_29794_29853[(1)] = (33));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (32))){
var inst_29736 = (state_29777[(14)]);
var state_29777__$1 = state_29777;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_29777__$1,(35),out,inst_29736);
} else {
if((state_val_29778 === (33))){
var inst_29727 = (state_29777[(12)]);
var inst_29705 = inst_29727;
var state_29777__$1 = (function (){var statearr_29795 = state_29777;
(statearr_29795[(7)] = inst_29705);

return statearr_29795;
})();
var statearr_29796_29854 = state_29777__$1;
(statearr_29796_29854[(2)] = null);

(statearr_29796_29854[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (13))){
var inst_29705 = (state_29777[(7)]);
var inst_29712 = inst_29705.cljs$lang$protocol_mask$partition0$;
var inst_29713 = (inst_29712 & (64));
var inst_29714 = inst_29705.cljs$core$ISeq$;
var inst_29715 = (inst_29713) || (inst_29714);
var state_29777__$1 = state_29777;
if(cljs.core.truth_(inst_29715)){
var statearr_29797_29855 = state_29777__$1;
(statearr_29797_29855[(1)] = (16));

} else {
var statearr_29798_29856 = state_29777__$1;
(statearr_29798_29856[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (22))){
var inst_29736 = (state_29777[(14)]);
var inst_29737 = (state_29777[(9)]);
var inst_29735 = (state_29777[(2)]);
var inst_29736__$1 = cljs.core.nth.call(null,inst_29735,(0),null);
var inst_29737__$1 = cljs.core.nth.call(null,inst_29735,(1),null);
var inst_29738 = (inst_29736__$1 == null);
var inst_29739 = cljs.core._EQ_.call(null,inst_29737__$1,change);
var inst_29740 = (inst_29738) || (inst_29739);
var state_29777__$1 = (function (){var statearr_29799 = state_29777;
(statearr_29799[(14)] = inst_29736__$1);

(statearr_29799[(9)] = inst_29737__$1);

return statearr_29799;
})();
if(cljs.core.truth_(inst_29740)){
var statearr_29800_29857 = state_29777__$1;
(statearr_29800_29857[(1)] = (23));

} else {
var statearr_29801_29858 = state_29777__$1;
(statearr_29801_29858[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (36))){
var inst_29727 = (state_29777[(12)]);
var inst_29705 = inst_29727;
var state_29777__$1 = (function (){var statearr_29802 = state_29777;
(statearr_29802[(7)] = inst_29705);

return statearr_29802;
})();
var statearr_29803_29859 = state_29777__$1;
(statearr_29803_29859[(2)] = null);

(statearr_29803_29859[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (29))){
var inst_29751 = (state_29777[(10)]);
var state_29777__$1 = state_29777;
var statearr_29804_29860 = state_29777__$1;
(statearr_29804_29860[(2)] = inst_29751);

(statearr_29804_29860[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (6))){
var state_29777__$1 = state_29777;
var statearr_29805_29861 = state_29777__$1;
(statearr_29805_29861[(2)] = false);

(statearr_29805_29861[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (28))){
var inst_29747 = (state_29777[(2)]);
var inst_29748 = calc_state.call(null);
var inst_29705 = inst_29748;
var state_29777__$1 = (function (){var statearr_29806 = state_29777;
(statearr_29806[(15)] = inst_29747);

(statearr_29806[(7)] = inst_29705);

return statearr_29806;
})();
var statearr_29807_29862 = state_29777__$1;
(statearr_29807_29862[(2)] = null);

(statearr_29807_29862[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (25))){
var inst_29773 = (state_29777[(2)]);
var state_29777__$1 = state_29777;
var statearr_29808_29863 = state_29777__$1;
(statearr_29808_29863[(2)] = inst_29773);

(statearr_29808_29863[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (34))){
var inst_29771 = (state_29777[(2)]);
var state_29777__$1 = state_29777;
var statearr_29809_29864 = state_29777__$1;
(statearr_29809_29864[(2)] = inst_29771);

(statearr_29809_29864[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (17))){
var state_29777__$1 = state_29777;
var statearr_29810_29865 = state_29777__$1;
(statearr_29810_29865[(2)] = false);

(statearr_29810_29865[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (3))){
var state_29777__$1 = state_29777;
var statearr_29811_29866 = state_29777__$1;
(statearr_29811_29866[(2)] = false);

(statearr_29811_29866[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (12))){
var inst_29775 = (state_29777[(2)]);
var state_29777__$1 = state_29777;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_29777__$1,inst_29775);
} else {
if((state_val_29778 === (2))){
var inst_29681 = (state_29777[(8)]);
var inst_29686 = inst_29681.cljs$lang$protocol_mask$partition0$;
var inst_29687 = (inst_29686 & (64));
var inst_29688 = inst_29681.cljs$core$ISeq$;
var inst_29689 = (inst_29687) || (inst_29688);
var state_29777__$1 = state_29777;
if(cljs.core.truth_(inst_29689)){
var statearr_29812_29867 = state_29777__$1;
(statearr_29812_29867[(1)] = (5));

} else {
var statearr_29813_29868 = state_29777__$1;
(statearr_29813_29868[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (23))){
var inst_29736 = (state_29777[(14)]);
var inst_29742 = (inst_29736 == null);
var state_29777__$1 = state_29777;
if(cljs.core.truth_(inst_29742)){
var statearr_29814_29869 = state_29777__$1;
(statearr_29814_29869[(1)] = (26));

} else {
var statearr_29815_29870 = state_29777__$1;
(statearr_29815_29870[(1)] = (27));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (35))){
var inst_29762 = (state_29777[(2)]);
var state_29777__$1 = state_29777;
if(cljs.core.truth_(inst_29762)){
var statearr_29816_29871 = state_29777__$1;
(statearr_29816_29871[(1)] = (36));

} else {
var statearr_29817_29872 = state_29777__$1;
(statearr_29817_29872[(1)] = (37));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (19))){
var inst_29705 = (state_29777[(7)]);
var inst_29724 = cljs.core.apply.call(null,cljs.core.hash_map,inst_29705);
var state_29777__$1 = state_29777;
var statearr_29818_29873 = state_29777__$1;
(statearr_29818_29873[(2)] = inst_29724);

(statearr_29818_29873[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (11))){
var inst_29705 = (state_29777[(7)]);
var inst_29709 = (inst_29705 == null);
var inst_29710 = cljs.core.not.call(null,inst_29709);
var state_29777__$1 = state_29777;
if(inst_29710){
var statearr_29819_29874 = state_29777__$1;
(statearr_29819_29874[(1)] = (13));

} else {
var statearr_29820_29875 = state_29777__$1;
(statearr_29820_29875[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (9))){
var inst_29681 = (state_29777[(8)]);
var state_29777__$1 = state_29777;
var statearr_29821_29876 = state_29777__$1;
(statearr_29821_29876[(2)] = inst_29681);

(statearr_29821_29876[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (5))){
var state_29777__$1 = state_29777;
var statearr_29822_29877 = state_29777__$1;
(statearr_29822_29877[(2)] = true);

(statearr_29822_29877[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (14))){
var state_29777__$1 = state_29777;
var statearr_29823_29878 = state_29777__$1;
(statearr_29823_29878[(2)] = false);

(statearr_29823_29878[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (26))){
var inst_29737 = (state_29777[(9)]);
var inst_29744 = cljs.core.swap_BANG_.call(null,cs,cljs.core.dissoc,inst_29737);
var state_29777__$1 = state_29777;
var statearr_29824_29879 = state_29777__$1;
(statearr_29824_29879[(2)] = inst_29744);

(statearr_29824_29879[(1)] = (28));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (16))){
var state_29777__$1 = state_29777;
var statearr_29825_29880 = state_29777__$1;
(statearr_29825_29880[(2)] = true);

(statearr_29825_29880[(1)] = (18));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (38))){
var inst_29767 = (state_29777[(2)]);
var state_29777__$1 = state_29777;
var statearr_29826_29881 = state_29777__$1;
(statearr_29826_29881[(2)] = inst_29767);

(statearr_29826_29881[(1)] = (34));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (30))){
var inst_29737 = (state_29777[(9)]);
var inst_29729 = (state_29777[(13)]);
var inst_29728 = (state_29777[(11)]);
var inst_29754 = cljs.core.empty_QMARK_.call(null,inst_29728);
var inst_29755 = inst_29729.call(null,inst_29737);
var inst_29756 = cljs.core.not.call(null,inst_29755);
var inst_29757 = (inst_29754) && (inst_29756);
var state_29777__$1 = state_29777;
var statearr_29827_29882 = state_29777__$1;
(statearr_29827_29882[(2)] = inst_29757);

(statearr_29827_29882[(1)] = (31));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (10))){
var inst_29681 = (state_29777[(8)]);
var inst_29701 = (state_29777[(2)]);
var inst_29702 = cljs.core.get.call(null,inst_29701,new cljs.core.Keyword(null,"solos","solos",1441458643));
var inst_29703 = cljs.core.get.call(null,inst_29701,new cljs.core.Keyword(null,"mutes","mutes",1068806309));
var inst_29704 = cljs.core.get.call(null,inst_29701,new cljs.core.Keyword(null,"reads","reads",-1215067361));
var inst_29705 = inst_29681;
var state_29777__$1 = (function (){var statearr_29828 = state_29777;
(statearr_29828[(16)] = inst_29703);

(statearr_29828[(17)] = inst_29702);

(statearr_29828[(18)] = inst_29704);

(statearr_29828[(7)] = inst_29705);

return statearr_29828;
})();
var statearr_29829_29883 = state_29777__$1;
(statearr_29829_29883[(2)] = null);

(statearr_29829_29883[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (18))){
var inst_29719 = (state_29777[(2)]);
var state_29777__$1 = state_29777;
var statearr_29830_29884 = state_29777__$1;
(statearr_29830_29884[(2)] = inst_29719);

(statearr_29830_29884[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (37))){
var state_29777__$1 = state_29777;
var statearr_29831_29885 = state_29777__$1;
(statearr_29831_29885[(2)] = null);

(statearr_29831_29885[(1)] = (38));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29778 === (8))){
var inst_29681 = (state_29777[(8)]);
var inst_29698 = cljs.core.apply.call(null,cljs.core.hash_map,inst_29681);
var state_29777__$1 = state_29777;
var statearr_29832_29886 = state_29777__$1;
(statearr_29832_29886[(2)] = inst_29698);

(statearr_29832_29886[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___29840,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
;
return ((function (switch__27965__auto__,c__28077__auto___29840,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m){
return (function() {
var cljs$core$async$mix_$_state_machine__27966__auto__ = null;
var cljs$core$async$mix_$_state_machine__27966__auto____0 = (function (){
var statearr_29836 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_29836[(0)] = cljs$core$async$mix_$_state_machine__27966__auto__);

(statearr_29836[(1)] = (1));

return statearr_29836;
});
var cljs$core$async$mix_$_state_machine__27966__auto____1 = (function (state_29777){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_29777);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e29837){if((e29837 instanceof Object)){
var ex__27969__auto__ = e29837;
var statearr_29838_29887 = state_29777;
(statearr_29838_29887[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_29777);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e29837;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__29888 = state_29777;
state_29777 = G__29888;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$mix_$_state_machine__27966__auto__ = function(state_29777){
switch(arguments.length){
case 0:
return cljs$core$async$mix_$_state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$mix_$_state_machine__27966__auto____1.call(this,state_29777);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mix_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mix_$_state_machine__27966__auto____0;
cljs$core$async$mix_$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mix_$_state_machine__27966__auto____1;
return cljs$core$async$mix_$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___29840,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
})();
var state__28079__auto__ = (function (){var statearr_29839 = f__28078__auto__.call(null);
(statearr_29839[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___29840);

return statearr_29839;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___29840,cs,solo_modes,attrs,solo_mode,change,changed,pick,calc_state,m))
);


return m;
});
/**
 * Adds ch as an input to the mix
 */
cljs.core.async.admix = (function cljs$core$async$admix(mix,ch){
return cljs.core.async.admix_STAR_.call(null,mix,ch);
});
/**
 * Removes ch as an input to the mix
 */
cljs.core.async.unmix = (function cljs$core$async$unmix(mix,ch){
return cljs.core.async.unmix_STAR_.call(null,mix,ch);
});
/**
 * removes all inputs from the mix
 */
cljs.core.async.unmix_all = (function cljs$core$async$unmix_all(mix){
return cljs.core.async.unmix_all_STAR_.call(null,mix);
});
/**
 * Atomically sets the state(s) of one or more channels in a mix. The
 *   state map is a map of channels -> channel-state-map. A
 *   channel-state-map is a map of attrs -> boolean, where attr is one or
 *   more of :mute, :pause or :solo. Any states supplied are merged with
 *   the current state.
 * 
 *   Note that channels can be added to a mix via toggle, which can be
 *   used to add channels in a particular (e.g. paused) state.
 */
cljs.core.async.toggle = (function cljs$core$async$toggle(mix,state_map){
return cljs.core.async.toggle_STAR_.call(null,mix,state_map);
});
/**
 * Sets the solo mode of the mix. mode must be one of :mute or :pause
 */
cljs.core.async.solo_mode = (function cljs$core$async$solo_mode(mix,mode){
return cljs.core.async.solo_mode_STAR_.call(null,mix,mode);
});

/**
 * @interface
 */
cljs.core.async.Pub = function(){};

cljs.core.async.sub_STAR_ = (function cljs$core$async$sub_STAR_(p,v,ch,close_QMARK_){
if((!((p == null))) && (!((p.cljs$core$async$Pub$sub_STAR_$arity$4 == null)))){
return p.cljs$core$async$Pub$sub_STAR_$arity$4(p,v,ch,close_QMARK_);
} else {
var x__26562__auto__ = (((p == null))?null:p);
var m__26563__auto__ = (cljs.core.async.sub_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,p,v,ch,close_QMARK_);
} else {
var m__26563__auto____$1 = (cljs.core.async.sub_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,p,v,ch,close_QMARK_);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.sub*",p);
}
}
}
});

cljs.core.async.unsub_STAR_ = (function cljs$core$async$unsub_STAR_(p,v,ch){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_STAR_$arity$3 == null)))){
return p.cljs$core$async$Pub$unsub_STAR_$arity$3(p,v,ch);
} else {
var x__26562__auto__ = (((p == null))?null:p);
var m__26563__auto__ = (cljs.core.async.unsub_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,p,v,ch);
} else {
var m__26563__auto____$1 = (cljs.core.async.unsub_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,p,v,ch);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_ = (function cljs$core$async$unsub_all_STAR_(var_args){
var args29889 = [];
var len__26974__auto___29892 = arguments.length;
var i__26975__auto___29893 = (0);
while(true){
if((i__26975__auto___29893 < len__26974__auto___29892)){
args29889.push((arguments[i__26975__auto___29893]));

var G__29894 = (i__26975__auto___29893 + (1));
i__26975__auto___29893 = G__29894;
continue;
} else {
}
break;
}

var G__29891 = args29889.length;
switch (G__29891) {
case 1:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args29889.length)].join('')));

}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$1 = (function (p){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$1 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$1(p);
} else {
var x__26562__auto__ = (((p == null))?null:p);
var m__26563__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,p);
} else {
var m__26563__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,p);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$core$IFn$_invoke$arity$2 = (function (p,v){
if((!((p == null))) && (!((p.cljs$core$async$Pub$unsub_all_STAR_$arity$2 == null)))){
return p.cljs$core$async$Pub$unsub_all_STAR_$arity$2(p,v);
} else {
var x__26562__auto__ = (((p == null))?null:p);
var m__26563__auto__ = (cljs.core.async.unsub_all_STAR_[goog.typeOf(x__26562__auto__)]);
if(!((m__26563__auto__ == null))){
return m__26563__auto__.call(null,p,v);
} else {
var m__26563__auto____$1 = (cljs.core.async.unsub_all_STAR_["_"]);
if(!((m__26563__auto____$1 == null))){
return m__26563__auto____$1.call(null,p,v);
} else {
throw cljs.core.missing_protocol.call(null,"Pub.unsub-all*",p);
}
}
}
});

cljs.core.async.unsub_all_STAR_.cljs$lang$maxFixedArity = 2;


/**
 * Creates and returns a pub(lication) of the supplied channel,
 *   partitioned into topics by the topic-fn. topic-fn will be applied to
 *   each value on the channel and the result will determine the 'topic'
 *   on which that value will be put. Channels can be subscribed to
 *   receive copies of topics using 'sub', and unsubscribed using
 *   'unsub'. Each topic will be handled by an internal mult on a
 *   dedicated channel. By default these internal channels are
 *   unbuffered, but a buf-fn can be supplied which, given a topic,
 *   creates a buffer with desired properties.
 * 
 *   Each item is distributed to all subs in parallel and synchronously,
 *   i.e. each sub must accept before the next item is distributed. Use
 *   buffering/windowing to prevent slow subs from holding up the pub.
 * 
 *   Items received when there are no matching subs get dropped.
 * 
 *   Note that if buf-fns are used then each topic is handled
 *   asynchronously, i.e. if a channel is subscribed to more than one
 *   topic it should not expect them to be interleaved identically with
 *   the source.
 */
cljs.core.async.pub = (function cljs$core$async$pub(var_args){
var args29897 = [];
var len__26974__auto___30022 = arguments.length;
var i__26975__auto___30023 = (0);
while(true){
if((i__26975__auto___30023 < len__26974__auto___30022)){
args29897.push((arguments[i__26975__auto___30023]));

var G__30024 = (i__26975__auto___30023 + (1));
i__26975__auto___30023 = G__30024;
continue;
} else {
}
break;
}

var G__29899 = args29897.length;
switch (G__29899) {
case 2:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args29897.length)].join('')));

}
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$2 = (function (ch,topic_fn){
return cljs.core.async.pub.call(null,ch,topic_fn,cljs.core.constantly.call(null,null));
});

cljs.core.async.pub.cljs$core$IFn$_invoke$arity$3 = (function (ch,topic_fn,buf_fn){
var mults = cljs.core.atom.call(null,cljs.core.PersistentArrayMap.EMPTY);
var ensure_mult = ((function (mults){
return (function (topic){
var or__25899__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,mults),topic);
if(cljs.core.truth_(or__25899__auto__)){
return or__25899__auto__;
} else {
return cljs.core.get.call(null,cljs.core.swap_BANG_.call(null,mults,((function (or__25899__auto__,mults){
return (function (p1__29896_SHARP_){
if(cljs.core.truth_(p1__29896_SHARP_.call(null,topic))){
return p1__29896_SHARP_;
} else {
return cljs.core.assoc.call(null,p1__29896_SHARP_,topic,cljs.core.async.mult.call(null,cljs.core.async.chan.call(null,buf_fn.call(null,topic))));
}
});})(or__25899__auto__,mults))
),topic);
}
});})(mults))
;
var p = (function (){
if(typeof cljs.core.async.t_cljs$core$async29900 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.Pub}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.async.Mux}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async29900 = (function (ch,topic_fn,buf_fn,mults,ensure_mult,meta29901){
this.ch = ch;
this.topic_fn = topic_fn;
this.buf_fn = buf_fn;
this.mults = mults;
this.ensure_mult = ensure_mult;
this.meta29901 = meta29901;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async29900.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (mults,ensure_mult){
return (function (_29902,meta29901__$1){
var self__ = this;
var _29902__$1 = this;
return (new cljs.core.async.t_cljs$core$async29900(self__.ch,self__.topic_fn,self__.buf_fn,self__.mults,self__.ensure_mult,meta29901__$1));
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async29900.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (mults,ensure_mult){
return (function (_29902){
var self__ = this;
var _29902__$1 = this;
return self__.meta29901;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async29900.prototype.cljs$core$async$Mux$ = true;

cljs.core.async.t_cljs$core$async29900.prototype.cljs$core$async$Mux$muxch_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return self__.ch;
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async29900.prototype.cljs$core$async$Pub$ = true;

cljs.core.async.t_cljs$core$async29900.prototype.cljs$core$async$Pub$sub_STAR_$arity$4 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1,close_QMARK_){
var self__ = this;
var p__$1 = this;
var m = self__.ensure_mult.call(null,topic);
return cljs.core.async.tap.call(null,m,ch__$1,close_QMARK_);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async29900.prototype.cljs$core$async$Pub$unsub_STAR_$arity$3 = ((function (mults,ensure_mult){
return (function (p,topic,ch__$1){
var self__ = this;
var p__$1 = this;
var temp__4657__auto__ = cljs.core.get.call(null,cljs.core.deref.call(null,self__.mults),topic);
if(cljs.core.truth_(temp__4657__auto__)){
var m = temp__4657__auto__;
return cljs.core.async.untap.call(null,m,ch__$1);
} else {
return null;
}
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async29900.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$1 = ((function (mults,ensure_mult){
return (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.reset_BANG_.call(null,self__.mults,cljs.core.PersistentArrayMap.EMPTY);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async29900.prototype.cljs$core$async$Pub$unsub_all_STAR_$arity$2 = ((function (mults,ensure_mult){
return (function (_,topic){
var self__ = this;
var ___$1 = this;
return cljs.core.swap_BANG_.call(null,self__.mults,cljs.core.dissoc,topic);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async29900.getBasis = ((function (mults,ensure_mult){
return (function (){
return new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"topic-fn","topic-fn",-862449736,null),new cljs.core.Symbol(null,"buf-fn","buf-fn",-1200281591,null),new cljs.core.Symbol(null,"mults","mults",-461114485,null),new cljs.core.Symbol(null,"ensure-mult","ensure-mult",1796584816,null),new cljs.core.Symbol(null,"meta29901","meta29901",-2083029093,null)], null);
});})(mults,ensure_mult))
;

cljs.core.async.t_cljs$core$async29900.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async29900.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async29900";

cljs.core.async.t_cljs$core$async29900.cljs$lang$ctorPrWriter = ((function (mults,ensure_mult){
return (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.core.async/t_cljs$core$async29900");
});})(mults,ensure_mult))
;

cljs.core.async.__GT_t_cljs$core$async29900 = ((function (mults,ensure_mult){
return (function cljs$core$async$__GT_t_cljs$core$async29900(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta29901){
return (new cljs.core.async.t_cljs$core$async29900(ch__$1,topic_fn__$1,buf_fn__$1,mults__$1,ensure_mult__$1,meta29901));
});})(mults,ensure_mult))
;

}

return (new cljs.core.async.t_cljs$core$async29900(ch,topic_fn,buf_fn,mults,ensure_mult,cljs.core.PersistentArrayMap.EMPTY));
})()
;
var c__28077__auto___30026 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___30026,mults,ensure_mult,p){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___30026,mults,ensure_mult,p){
return (function (state_29974){
var state_val_29975 = (state_29974[(1)]);
if((state_val_29975 === (7))){
var inst_29970 = (state_29974[(2)]);
var state_29974__$1 = state_29974;
var statearr_29976_30027 = state_29974__$1;
(statearr_29976_30027[(2)] = inst_29970);

(statearr_29976_30027[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (20))){
var state_29974__$1 = state_29974;
var statearr_29977_30028 = state_29974__$1;
(statearr_29977_30028[(2)] = null);

(statearr_29977_30028[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (1))){
var state_29974__$1 = state_29974;
var statearr_29978_30029 = state_29974__$1;
(statearr_29978_30029[(2)] = null);

(statearr_29978_30029[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (24))){
var inst_29953 = (state_29974[(7)]);
var inst_29962 = cljs.core.swap_BANG_.call(null,mults,cljs.core.dissoc,inst_29953);
var state_29974__$1 = state_29974;
var statearr_29979_30030 = state_29974__$1;
(statearr_29979_30030[(2)] = inst_29962);

(statearr_29979_30030[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (4))){
var inst_29905 = (state_29974[(8)]);
var inst_29905__$1 = (state_29974[(2)]);
var inst_29906 = (inst_29905__$1 == null);
var state_29974__$1 = (function (){var statearr_29980 = state_29974;
(statearr_29980[(8)] = inst_29905__$1);

return statearr_29980;
})();
if(cljs.core.truth_(inst_29906)){
var statearr_29981_30031 = state_29974__$1;
(statearr_29981_30031[(1)] = (5));

} else {
var statearr_29982_30032 = state_29974__$1;
(statearr_29982_30032[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (15))){
var inst_29947 = (state_29974[(2)]);
var state_29974__$1 = state_29974;
var statearr_29983_30033 = state_29974__$1;
(statearr_29983_30033[(2)] = inst_29947);

(statearr_29983_30033[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (21))){
var inst_29967 = (state_29974[(2)]);
var state_29974__$1 = (function (){var statearr_29984 = state_29974;
(statearr_29984[(9)] = inst_29967);

return statearr_29984;
})();
var statearr_29985_30034 = state_29974__$1;
(statearr_29985_30034[(2)] = null);

(statearr_29985_30034[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (13))){
var inst_29929 = (state_29974[(10)]);
var inst_29931 = cljs.core.chunked_seq_QMARK_.call(null,inst_29929);
var state_29974__$1 = state_29974;
if(inst_29931){
var statearr_29986_30035 = state_29974__$1;
(statearr_29986_30035[(1)] = (16));

} else {
var statearr_29987_30036 = state_29974__$1;
(statearr_29987_30036[(1)] = (17));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (22))){
var inst_29959 = (state_29974[(2)]);
var state_29974__$1 = state_29974;
if(cljs.core.truth_(inst_29959)){
var statearr_29988_30037 = state_29974__$1;
(statearr_29988_30037[(1)] = (23));

} else {
var statearr_29989_30038 = state_29974__$1;
(statearr_29989_30038[(1)] = (24));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (6))){
var inst_29955 = (state_29974[(11)]);
var inst_29953 = (state_29974[(7)]);
var inst_29905 = (state_29974[(8)]);
var inst_29953__$1 = topic_fn.call(null,inst_29905);
var inst_29954 = cljs.core.deref.call(null,mults);
var inst_29955__$1 = cljs.core.get.call(null,inst_29954,inst_29953__$1);
var state_29974__$1 = (function (){var statearr_29990 = state_29974;
(statearr_29990[(11)] = inst_29955__$1);

(statearr_29990[(7)] = inst_29953__$1);

return statearr_29990;
})();
if(cljs.core.truth_(inst_29955__$1)){
var statearr_29991_30039 = state_29974__$1;
(statearr_29991_30039[(1)] = (19));

} else {
var statearr_29992_30040 = state_29974__$1;
(statearr_29992_30040[(1)] = (20));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (25))){
var inst_29964 = (state_29974[(2)]);
var state_29974__$1 = state_29974;
var statearr_29993_30041 = state_29974__$1;
(statearr_29993_30041[(2)] = inst_29964);

(statearr_29993_30041[(1)] = (21));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (17))){
var inst_29929 = (state_29974[(10)]);
var inst_29938 = cljs.core.first.call(null,inst_29929);
var inst_29939 = cljs.core.async.muxch_STAR_.call(null,inst_29938);
var inst_29940 = cljs.core.async.close_BANG_.call(null,inst_29939);
var inst_29941 = cljs.core.next.call(null,inst_29929);
var inst_29915 = inst_29941;
var inst_29916 = null;
var inst_29917 = (0);
var inst_29918 = (0);
var state_29974__$1 = (function (){var statearr_29994 = state_29974;
(statearr_29994[(12)] = inst_29917);

(statearr_29994[(13)] = inst_29940);

(statearr_29994[(14)] = inst_29916);

(statearr_29994[(15)] = inst_29915);

(statearr_29994[(16)] = inst_29918);

return statearr_29994;
})();
var statearr_29995_30042 = state_29974__$1;
(statearr_29995_30042[(2)] = null);

(statearr_29995_30042[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (3))){
var inst_29972 = (state_29974[(2)]);
var state_29974__$1 = state_29974;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_29974__$1,inst_29972);
} else {
if((state_val_29975 === (12))){
var inst_29949 = (state_29974[(2)]);
var state_29974__$1 = state_29974;
var statearr_29996_30043 = state_29974__$1;
(statearr_29996_30043[(2)] = inst_29949);

(statearr_29996_30043[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (2))){
var state_29974__$1 = state_29974;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_29974__$1,(4),ch);
} else {
if((state_val_29975 === (23))){
var state_29974__$1 = state_29974;
var statearr_29997_30044 = state_29974__$1;
(statearr_29997_30044[(2)] = null);

(statearr_29997_30044[(1)] = (25));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (19))){
var inst_29955 = (state_29974[(11)]);
var inst_29905 = (state_29974[(8)]);
var inst_29957 = cljs.core.async.muxch_STAR_.call(null,inst_29955);
var state_29974__$1 = state_29974;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_29974__$1,(22),inst_29957,inst_29905);
} else {
if((state_val_29975 === (11))){
var inst_29929 = (state_29974[(10)]);
var inst_29915 = (state_29974[(15)]);
var inst_29929__$1 = cljs.core.seq.call(null,inst_29915);
var state_29974__$1 = (function (){var statearr_29998 = state_29974;
(statearr_29998[(10)] = inst_29929__$1);

return statearr_29998;
})();
if(inst_29929__$1){
var statearr_29999_30045 = state_29974__$1;
(statearr_29999_30045[(1)] = (13));

} else {
var statearr_30000_30046 = state_29974__$1;
(statearr_30000_30046[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (9))){
var inst_29951 = (state_29974[(2)]);
var state_29974__$1 = state_29974;
var statearr_30001_30047 = state_29974__$1;
(statearr_30001_30047[(2)] = inst_29951);

(statearr_30001_30047[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (5))){
var inst_29912 = cljs.core.deref.call(null,mults);
var inst_29913 = cljs.core.vals.call(null,inst_29912);
var inst_29914 = cljs.core.seq.call(null,inst_29913);
var inst_29915 = inst_29914;
var inst_29916 = null;
var inst_29917 = (0);
var inst_29918 = (0);
var state_29974__$1 = (function (){var statearr_30002 = state_29974;
(statearr_30002[(12)] = inst_29917);

(statearr_30002[(14)] = inst_29916);

(statearr_30002[(15)] = inst_29915);

(statearr_30002[(16)] = inst_29918);

return statearr_30002;
})();
var statearr_30003_30048 = state_29974__$1;
(statearr_30003_30048[(2)] = null);

(statearr_30003_30048[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (14))){
var state_29974__$1 = state_29974;
var statearr_30007_30049 = state_29974__$1;
(statearr_30007_30049[(2)] = null);

(statearr_30007_30049[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (16))){
var inst_29929 = (state_29974[(10)]);
var inst_29933 = cljs.core.chunk_first.call(null,inst_29929);
var inst_29934 = cljs.core.chunk_rest.call(null,inst_29929);
var inst_29935 = cljs.core.count.call(null,inst_29933);
var inst_29915 = inst_29934;
var inst_29916 = inst_29933;
var inst_29917 = inst_29935;
var inst_29918 = (0);
var state_29974__$1 = (function (){var statearr_30008 = state_29974;
(statearr_30008[(12)] = inst_29917);

(statearr_30008[(14)] = inst_29916);

(statearr_30008[(15)] = inst_29915);

(statearr_30008[(16)] = inst_29918);

return statearr_30008;
})();
var statearr_30009_30050 = state_29974__$1;
(statearr_30009_30050[(2)] = null);

(statearr_30009_30050[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (10))){
var inst_29917 = (state_29974[(12)]);
var inst_29916 = (state_29974[(14)]);
var inst_29915 = (state_29974[(15)]);
var inst_29918 = (state_29974[(16)]);
var inst_29923 = cljs.core._nth.call(null,inst_29916,inst_29918);
var inst_29924 = cljs.core.async.muxch_STAR_.call(null,inst_29923);
var inst_29925 = cljs.core.async.close_BANG_.call(null,inst_29924);
var inst_29926 = (inst_29918 + (1));
var tmp30004 = inst_29917;
var tmp30005 = inst_29916;
var tmp30006 = inst_29915;
var inst_29915__$1 = tmp30006;
var inst_29916__$1 = tmp30005;
var inst_29917__$1 = tmp30004;
var inst_29918__$1 = inst_29926;
var state_29974__$1 = (function (){var statearr_30010 = state_29974;
(statearr_30010[(12)] = inst_29917__$1);

(statearr_30010[(14)] = inst_29916__$1);

(statearr_30010[(15)] = inst_29915__$1);

(statearr_30010[(16)] = inst_29918__$1);

(statearr_30010[(17)] = inst_29925);

return statearr_30010;
})();
var statearr_30011_30051 = state_29974__$1;
(statearr_30011_30051[(2)] = null);

(statearr_30011_30051[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (18))){
var inst_29944 = (state_29974[(2)]);
var state_29974__$1 = state_29974;
var statearr_30012_30052 = state_29974__$1;
(statearr_30012_30052[(2)] = inst_29944);

(statearr_30012_30052[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_29975 === (8))){
var inst_29917 = (state_29974[(12)]);
var inst_29918 = (state_29974[(16)]);
var inst_29920 = (inst_29918 < inst_29917);
var inst_29921 = inst_29920;
var state_29974__$1 = state_29974;
if(cljs.core.truth_(inst_29921)){
var statearr_30013_30053 = state_29974__$1;
(statearr_30013_30053[(1)] = (10));

} else {
var statearr_30014_30054 = state_29974__$1;
(statearr_30014_30054[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___30026,mults,ensure_mult,p))
;
return ((function (switch__27965__auto__,c__28077__auto___30026,mults,ensure_mult,p){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_30018 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30018[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_30018[(1)] = (1));

return statearr_30018;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_29974){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_29974);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e30019){if((e30019 instanceof Object)){
var ex__27969__auto__ = e30019;
var statearr_30020_30055 = state_29974;
(statearr_30020_30055[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_29974);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30019;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30056 = state_29974;
state_29974 = G__30056;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_29974){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_29974);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___30026,mults,ensure_mult,p))
})();
var state__28079__auto__ = (function (){var statearr_30021 = f__28078__auto__.call(null);
(statearr_30021[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___30026);

return statearr_30021;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___30026,mults,ensure_mult,p))
);


return p;
});

cljs.core.async.pub.cljs$lang$maxFixedArity = 3;

/**
 * Subscribes a channel to a topic of a pub.
 * 
 *   By default the channel will be closed when the source closes,
 *   but can be determined by the close? parameter.
 */
cljs.core.async.sub = (function cljs$core$async$sub(var_args){
var args30057 = [];
var len__26974__auto___30060 = arguments.length;
var i__26975__auto___30061 = (0);
while(true){
if((i__26975__auto___30061 < len__26974__auto___30060)){
args30057.push((arguments[i__26975__auto___30061]));

var G__30062 = (i__26975__auto___30061 + (1));
i__26975__auto___30061 = G__30062;
continue;
} else {
}
break;
}

var G__30059 = args30057.length;
switch (G__30059) {
case 3:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
case 4:
return cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4((arguments[(0)]),(arguments[(1)]),(arguments[(2)]),(arguments[(3)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30057.length)].join('')));

}
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$3 = (function (p,topic,ch){
return cljs.core.async.sub.call(null,p,topic,ch,true);
});

cljs.core.async.sub.cljs$core$IFn$_invoke$arity$4 = (function (p,topic,ch,close_QMARK_){
return cljs.core.async.sub_STAR_.call(null,p,topic,ch,close_QMARK_);
});

cljs.core.async.sub.cljs$lang$maxFixedArity = 4;

/**
 * Unsubscribes a channel from a topic of a pub
 */
cljs.core.async.unsub = (function cljs$core$async$unsub(p,topic,ch){
return cljs.core.async.unsub_STAR_.call(null,p,topic,ch);
});
/**
 * Unsubscribes all channels from a pub, or a topic of a pub
 */
cljs.core.async.unsub_all = (function cljs$core$async$unsub_all(var_args){
var args30064 = [];
var len__26974__auto___30067 = arguments.length;
var i__26975__auto___30068 = (0);
while(true){
if((i__26975__auto___30068 < len__26974__auto___30067)){
args30064.push((arguments[i__26975__auto___30068]));

var G__30069 = (i__26975__auto___30068 + (1));
i__26975__auto___30068 = G__30069;
continue;
} else {
}
break;
}

var G__30066 = args30064.length;
switch (G__30066) {
case 1:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30064.length)].join('')));

}
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$1 = (function (p){
return cljs.core.async.unsub_all_STAR_.call(null,p);
});

cljs.core.async.unsub_all.cljs$core$IFn$_invoke$arity$2 = (function (p,topic){
return cljs.core.async.unsub_all_STAR_.call(null,p,topic);
});

cljs.core.async.unsub_all.cljs$lang$maxFixedArity = 2;

/**
 * Takes a function and a collection of source channels, and returns a
 *   channel which contains the values produced by applying f to the set
 *   of first items taken from each source channel, followed by applying
 *   f to the set of second items from each channel, until any one of the
 *   channels is closed, at which point the output channel will be
 *   closed. The returned channel will be unbuffered by default, or a
 *   buf-or-n can be supplied
 */
cljs.core.async.map = (function cljs$core$async$map(var_args){
var args30071 = [];
var len__26974__auto___30142 = arguments.length;
var i__26975__auto___30143 = (0);
while(true){
if((i__26975__auto___30143 < len__26974__auto___30142)){
args30071.push((arguments[i__26975__auto___30143]));

var G__30144 = (i__26975__auto___30143 + (1));
i__26975__auto___30143 = G__30144;
continue;
} else {
}
break;
}

var G__30073 = args30071.length;
switch (G__30073) {
case 2:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.map.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30071.length)].join('')));

}
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$2 = (function (f,chs){
return cljs.core.async.map.call(null,f,chs,null);
});

cljs.core.async.map.cljs$core$IFn$_invoke$arity$3 = (function (f,chs,buf_or_n){
var chs__$1 = cljs.core.vec.call(null,chs);
var out = cljs.core.async.chan.call(null,buf_or_n);
var cnt = cljs.core.count.call(null,chs__$1);
var rets = cljs.core.object_array.call(null,cnt);
var dchan = cljs.core.async.chan.call(null,(1));
var dctr = cljs.core.atom.call(null,null);
var done = cljs.core.mapv.call(null,((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (i){
return ((function (chs__$1,out,cnt,rets,dchan,dctr){
return (function (ret){
(rets[i] = ret);

if((cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec) === (0))){
return cljs.core.async.put_BANG_.call(null,dchan,rets.slice((0)));
} else {
return null;
}
});
;})(chs__$1,out,cnt,rets,dchan,dctr))
});})(chs__$1,out,cnt,rets,dchan,dctr))
,cljs.core.range.call(null,cnt));
var c__28077__auto___30146 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___30146,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___30146,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function (state_30112){
var state_val_30113 = (state_30112[(1)]);
if((state_val_30113 === (7))){
var state_30112__$1 = state_30112;
var statearr_30114_30147 = state_30112__$1;
(statearr_30114_30147[(2)] = null);

(statearr_30114_30147[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (1))){
var state_30112__$1 = state_30112;
var statearr_30115_30148 = state_30112__$1;
(statearr_30115_30148[(2)] = null);

(statearr_30115_30148[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (4))){
var inst_30076 = (state_30112[(7)]);
var inst_30078 = (inst_30076 < cnt);
var state_30112__$1 = state_30112;
if(cljs.core.truth_(inst_30078)){
var statearr_30116_30149 = state_30112__$1;
(statearr_30116_30149[(1)] = (6));

} else {
var statearr_30117_30150 = state_30112__$1;
(statearr_30117_30150[(1)] = (7));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (15))){
var inst_30108 = (state_30112[(2)]);
var state_30112__$1 = state_30112;
var statearr_30118_30151 = state_30112__$1;
(statearr_30118_30151[(2)] = inst_30108);

(statearr_30118_30151[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (13))){
var inst_30101 = cljs.core.async.close_BANG_.call(null,out);
var state_30112__$1 = state_30112;
var statearr_30119_30152 = state_30112__$1;
(statearr_30119_30152[(2)] = inst_30101);

(statearr_30119_30152[(1)] = (15));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (6))){
var state_30112__$1 = state_30112;
var statearr_30120_30153 = state_30112__$1;
(statearr_30120_30153[(2)] = null);

(statearr_30120_30153[(1)] = (11));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (3))){
var inst_30110 = (state_30112[(2)]);
var state_30112__$1 = state_30112;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30112__$1,inst_30110);
} else {
if((state_val_30113 === (12))){
var inst_30098 = (state_30112[(8)]);
var inst_30098__$1 = (state_30112[(2)]);
var inst_30099 = cljs.core.some.call(null,cljs.core.nil_QMARK_,inst_30098__$1);
var state_30112__$1 = (function (){var statearr_30121 = state_30112;
(statearr_30121[(8)] = inst_30098__$1);

return statearr_30121;
})();
if(cljs.core.truth_(inst_30099)){
var statearr_30122_30154 = state_30112__$1;
(statearr_30122_30154[(1)] = (13));

} else {
var statearr_30123_30155 = state_30112__$1;
(statearr_30123_30155[(1)] = (14));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (2))){
var inst_30075 = cljs.core.reset_BANG_.call(null,dctr,cnt);
var inst_30076 = (0);
var state_30112__$1 = (function (){var statearr_30124 = state_30112;
(statearr_30124[(7)] = inst_30076);

(statearr_30124[(9)] = inst_30075);

return statearr_30124;
})();
var statearr_30125_30156 = state_30112__$1;
(statearr_30125_30156[(2)] = null);

(statearr_30125_30156[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (11))){
var inst_30076 = (state_30112[(7)]);
var _ = cljs.core.async.impl.ioc_helpers.add_exception_frame.call(null,state_30112,(10),Object,null,(9));
var inst_30085 = chs__$1.call(null,inst_30076);
var inst_30086 = done.call(null,inst_30076);
var inst_30087 = cljs.core.async.take_BANG_.call(null,inst_30085,inst_30086);
var state_30112__$1 = state_30112;
var statearr_30126_30157 = state_30112__$1;
(statearr_30126_30157[(2)] = inst_30087);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30112__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (9))){
var inst_30076 = (state_30112[(7)]);
var inst_30089 = (state_30112[(2)]);
var inst_30090 = (inst_30076 + (1));
var inst_30076__$1 = inst_30090;
var state_30112__$1 = (function (){var statearr_30127 = state_30112;
(statearr_30127[(10)] = inst_30089);

(statearr_30127[(7)] = inst_30076__$1);

return statearr_30127;
})();
var statearr_30128_30158 = state_30112__$1;
(statearr_30128_30158[(2)] = null);

(statearr_30128_30158[(1)] = (4));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (5))){
var inst_30096 = (state_30112[(2)]);
var state_30112__$1 = (function (){var statearr_30129 = state_30112;
(statearr_30129[(11)] = inst_30096);

return statearr_30129;
})();
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30112__$1,(12),dchan);
} else {
if((state_val_30113 === (14))){
var inst_30098 = (state_30112[(8)]);
var inst_30103 = cljs.core.apply.call(null,f,inst_30098);
var state_30112__$1 = state_30112;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30112__$1,(16),out,inst_30103);
} else {
if((state_val_30113 === (16))){
var inst_30105 = (state_30112[(2)]);
var state_30112__$1 = (function (){var statearr_30130 = state_30112;
(statearr_30130[(12)] = inst_30105);

return statearr_30130;
})();
var statearr_30131_30159 = state_30112__$1;
(statearr_30131_30159[(2)] = null);

(statearr_30131_30159[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (10))){
var inst_30080 = (state_30112[(2)]);
var inst_30081 = cljs.core.swap_BANG_.call(null,dctr,cljs.core.dec);
var state_30112__$1 = (function (){var statearr_30132 = state_30112;
(statearr_30132[(13)] = inst_30080);

return statearr_30132;
})();
var statearr_30133_30160 = state_30112__$1;
(statearr_30133_30160[(2)] = inst_30081);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30112__$1);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30113 === (8))){
var inst_30094 = (state_30112[(2)]);
var state_30112__$1 = state_30112;
var statearr_30134_30161 = state_30112__$1;
(statearr_30134_30161[(2)] = inst_30094);

(statearr_30134_30161[(1)] = (5));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___30146,chs__$1,out,cnt,rets,dchan,dctr,done))
;
return ((function (switch__27965__auto__,c__28077__auto___30146,chs__$1,out,cnt,rets,dchan,dctr,done){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_30138 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30138[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_30138[(1)] = (1));

return statearr_30138;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_30112){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_30112);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e30139){if((e30139 instanceof Object)){
var ex__27969__auto__ = e30139;
var statearr_30140_30162 = state_30112;
(statearr_30140_30162[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30112);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30139;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30163 = state_30112;
state_30112 = G__30163;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_30112){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_30112);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___30146,chs__$1,out,cnt,rets,dchan,dctr,done))
})();
var state__28079__auto__ = (function (){var statearr_30141 = f__28078__auto__.call(null);
(statearr_30141[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___30146);

return statearr_30141;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___30146,chs__$1,out,cnt,rets,dchan,dctr,done))
);


return out;
});

cljs.core.async.map.cljs$lang$maxFixedArity = 3;

/**
 * Takes a collection of source channels and returns a channel which
 *   contains all values taken from them. The returned channel will be
 *   unbuffered by default, or a buf-or-n can be supplied. The channel
 *   will close after all the source channels have closed.
 */
cljs.core.async.merge = (function cljs$core$async$merge(var_args){
var args30165 = [];
var len__26974__auto___30223 = arguments.length;
var i__26975__auto___30224 = (0);
while(true){
if((i__26975__auto___30224 < len__26974__auto___30223)){
args30165.push((arguments[i__26975__auto___30224]));

var G__30225 = (i__26975__auto___30224 + (1));
i__26975__auto___30224 = G__30225;
continue;
} else {
}
break;
}

var G__30167 = args30165.length;
switch (G__30167) {
case 1:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30165.length)].join('')));

}
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$1 = (function (chs){
return cljs.core.async.merge.call(null,chs,null);
});

cljs.core.async.merge.cljs$core$IFn$_invoke$arity$2 = (function (chs,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28077__auto___30227 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___30227,out){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___30227,out){
return (function (state_30199){
var state_val_30200 = (state_30199[(1)]);
if((state_val_30200 === (7))){
var inst_30178 = (state_30199[(7)]);
var inst_30179 = (state_30199[(8)]);
var inst_30178__$1 = (state_30199[(2)]);
var inst_30179__$1 = cljs.core.nth.call(null,inst_30178__$1,(0),null);
var inst_30180 = cljs.core.nth.call(null,inst_30178__$1,(1),null);
var inst_30181 = (inst_30179__$1 == null);
var state_30199__$1 = (function (){var statearr_30201 = state_30199;
(statearr_30201[(9)] = inst_30180);

(statearr_30201[(7)] = inst_30178__$1);

(statearr_30201[(8)] = inst_30179__$1);

return statearr_30201;
})();
if(cljs.core.truth_(inst_30181)){
var statearr_30202_30228 = state_30199__$1;
(statearr_30202_30228[(1)] = (8));

} else {
var statearr_30203_30229 = state_30199__$1;
(statearr_30203_30229[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30200 === (1))){
var inst_30168 = cljs.core.vec.call(null,chs);
var inst_30169 = inst_30168;
var state_30199__$1 = (function (){var statearr_30204 = state_30199;
(statearr_30204[(10)] = inst_30169);

return statearr_30204;
})();
var statearr_30205_30230 = state_30199__$1;
(statearr_30205_30230[(2)] = null);

(statearr_30205_30230[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30200 === (4))){
var inst_30169 = (state_30199[(10)]);
var state_30199__$1 = state_30199;
return cljs.core.async.ioc_alts_BANG_.call(null,state_30199__$1,(7),inst_30169);
} else {
if((state_val_30200 === (6))){
var inst_30195 = (state_30199[(2)]);
var state_30199__$1 = state_30199;
var statearr_30206_30231 = state_30199__$1;
(statearr_30206_30231[(2)] = inst_30195);

(statearr_30206_30231[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30200 === (3))){
var inst_30197 = (state_30199[(2)]);
var state_30199__$1 = state_30199;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30199__$1,inst_30197);
} else {
if((state_val_30200 === (2))){
var inst_30169 = (state_30199[(10)]);
var inst_30171 = cljs.core.count.call(null,inst_30169);
var inst_30172 = (inst_30171 > (0));
var state_30199__$1 = state_30199;
if(cljs.core.truth_(inst_30172)){
var statearr_30208_30232 = state_30199__$1;
(statearr_30208_30232[(1)] = (4));

} else {
var statearr_30209_30233 = state_30199__$1;
(statearr_30209_30233[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30200 === (11))){
var inst_30169 = (state_30199[(10)]);
var inst_30188 = (state_30199[(2)]);
var tmp30207 = inst_30169;
var inst_30169__$1 = tmp30207;
var state_30199__$1 = (function (){var statearr_30210 = state_30199;
(statearr_30210[(10)] = inst_30169__$1);

(statearr_30210[(11)] = inst_30188);

return statearr_30210;
})();
var statearr_30211_30234 = state_30199__$1;
(statearr_30211_30234[(2)] = null);

(statearr_30211_30234[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30200 === (9))){
var inst_30179 = (state_30199[(8)]);
var state_30199__$1 = state_30199;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30199__$1,(11),out,inst_30179);
} else {
if((state_val_30200 === (5))){
var inst_30193 = cljs.core.async.close_BANG_.call(null,out);
var state_30199__$1 = state_30199;
var statearr_30212_30235 = state_30199__$1;
(statearr_30212_30235[(2)] = inst_30193);

(statearr_30212_30235[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30200 === (10))){
var inst_30191 = (state_30199[(2)]);
var state_30199__$1 = state_30199;
var statearr_30213_30236 = state_30199__$1;
(statearr_30213_30236[(2)] = inst_30191);

(statearr_30213_30236[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30200 === (8))){
var inst_30169 = (state_30199[(10)]);
var inst_30180 = (state_30199[(9)]);
var inst_30178 = (state_30199[(7)]);
var inst_30179 = (state_30199[(8)]);
var inst_30183 = (function (){var cs = inst_30169;
var vec__30174 = inst_30178;
var v = inst_30179;
var c = inst_30180;
return ((function (cs,vec__30174,v,c,inst_30169,inst_30180,inst_30178,inst_30179,state_val_30200,c__28077__auto___30227,out){
return (function (p1__30164_SHARP_){
return cljs.core.not_EQ_.call(null,c,p1__30164_SHARP_);
});
;})(cs,vec__30174,v,c,inst_30169,inst_30180,inst_30178,inst_30179,state_val_30200,c__28077__auto___30227,out))
})();
var inst_30184 = cljs.core.filterv.call(null,inst_30183,inst_30169);
var inst_30169__$1 = inst_30184;
var state_30199__$1 = (function (){var statearr_30214 = state_30199;
(statearr_30214[(10)] = inst_30169__$1);

return statearr_30214;
})();
var statearr_30215_30237 = state_30199__$1;
(statearr_30215_30237[(2)] = null);

(statearr_30215_30237[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___30227,out))
;
return ((function (switch__27965__auto__,c__28077__auto___30227,out){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_30219 = [null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30219[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_30219[(1)] = (1));

return statearr_30219;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_30199){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_30199);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e30220){if((e30220 instanceof Object)){
var ex__27969__auto__ = e30220;
var statearr_30221_30238 = state_30199;
(statearr_30221_30238[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30199);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30220;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30239 = state_30199;
state_30199 = G__30239;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_30199){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_30199);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___30227,out))
})();
var state__28079__auto__ = (function (){var statearr_30222 = f__28078__auto__.call(null);
(statearr_30222[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___30227);

return statearr_30222;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___30227,out))
);


return out;
});

cljs.core.async.merge.cljs$lang$maxFixedArity = 2;

/**
 * Returns a channel containing the single (collection) result of the
 *   items taken from the channel conjoined to the supplied
 *   collection. ch must close before into produces a result.
 */
cljs.core.async.into = (function cljs$core$async$into(coll,ch){
return cljs.core.async.reduce.call(null,cljs.core.conj,coll,ch);
});
/**
 * Returns a channel that will return, at most, n items from ch. After n items
 * have been returned, or ch has been closed, the return chanel will close.
 * 
 *   The output channel is unbuffered by default, unless buf-or-n is given.
 */
cljs.core.async.take = (function cljs$core$async$take(var_args){
var args30240 = [];
var len__26974__auto___30289 = arguments.length;
var i__26975__auto___30290 = (0);
while(true){
if((i__26975__auto___30290 < len__26974__auto___30289)){
args30240.push((arguments[i__26975__auto___30290]));

var G__30291 = (i__26975__auto___30290 + (1));
i__26975__auto___30290 = G__30291;
continue;
} else {
}
break;
}

var G__30242 = args30240.length;
switch (G__30242) {
case 2:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.take.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30240.length)].join('')));

}
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.take.call(null,n,ch,null);
});

cljs.core.async.take.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28077__auto___30293 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___30293,out){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___30293,out){
return (function (state_30266){
var state_val_30267 = (state_30266[(1)]);
if((state_val_30267 === (7))){
var inst_30248 = (state_30266[(7)]);
var inst_30248__$1 = (state_30266[(2)]);
var inst_30249 = (inst_30248__$1 == null);
var inst_30250 = cljs.core.not.call(null,inst_30249);
var state_30266__$1 = (function (){var statearr_30268 = state_30266;
(statearr_30268[(7)] = inst_30248__$1);

return statearr_30268;
})();
if(inst_30250){
var statearr_30269_30294 = state_30266__$1;
(statearr_30269_30294[(1)] = (8));

} else {
var statearr_30270_30295 = state_30266__$1;
(statearr_30270_30295[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30267 === (1))){
var inst_30243 = (0);
var state_30266__$1 = (function (){var statearr_30271 = state_30266;
(statearr_30271[(8)] = inst_30243);

return statearr_30271;
})();
var statearr_30272_30296 = state_30266__$1;
(statearr_30272_30296[(2)] = null);

(statearr_30272_30296[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30267 === (4))){
var state_30266__$1 = state_30266;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30266__$1,(7),ch);
} else {
if((state_val_30267 === (6))){
var inst_30261 = (state_30266[(2)]);
var state_30266__$1 = state_30266;
var statearr_30273_30297 = state_30266__$1;
(statearr_30273_30297[(2)] = inst_30261);

(statearr_30273_30297[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30267 === (3))){
var inst_30263 = (state_30266[(2)]);
var inst_30264 = cljs.core.async.close_BANG_.call(null,out);
var state_30266__$1 = (function (){var statearr_30274 = state_30266;
(statearr_30274[(9)] = inst_30263);

return statearr_30274;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30266__$1,inst_30264);
} else {
if((state_val_30267 === (2))){
var inst_30243 = (state_30266[(8)]);
var inst_30245 = (inst_30243 < n);
var state_30266__$1 = state_30266;
if(cljs.core.truth_(inst_30245)){
var statearr_30275_30298 = state_30266__$1;
(statearr_30275_30298[(1)] = (4));

} else {
var statearr_30276_30299 = state_30266__$1;
(statearr_30276_30299[(1)] = (5));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30267 === (11))){
var inst_30243 = (state_30266[(8)]);
var inst_30253 = (state_30266[(2)]);
var inst_30254 = (inst_30243 + (1));
var inst_30243__$1 = inst_30254;
var state_30266__$1 = (function (){var statearr_30277 = state_30266;
(statearr_30277[(8)] = inst_30243__$1);

(statearr_30277[(10)] = inst_30253);

return statearr_30277;
})();
var statearr_30278_30300 = state_30266__$1;
(statearr_30278_30300[(2)] = null);

(statearr_30278_30300[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30267 === (9))){
var state_30266__$1 = state_30266;
var statearr_30279_30301 = state_30266__$1;
(statearr_30279_30301[(2)] = null);

(statearr_30279_30301[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30267 === (5))){
var state_30266__$1 = state_30266;
var statearr_30280_30302 = state_30266__$1;
(statearr_30280_30302[(2)] = null);

(statearr_30280_30302[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30267 === (10))){
var inst_30258 = (state_30266[(2)]);
var state_30266__$1 = state_30266;
var statearr_30281_30303 = state_30266__$1;
(statearr_30281_30303[(2)] = inst_30258);

(statearr_30281_30303[(1)] = (6));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30267 === (8))){
var inst_30248 = (state_30266[(7)]);
var state_30266__$1 = state_30266;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30266__$1,(11),out,inst_30248);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___30293,out))
;
return ((function (switch__27965__auto__,c__28077__auto___30293,out){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_30285 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_30285[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_30285[(1)] = (1));

return statearr_30285;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_30266){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_30266);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e30286){if((e30286 instanceof Object)){
var ex__27969__auto__ = e30286;
var statearr_30287_30304 = state_30266;
(statearr_30287_30304[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30266);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30286;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30305 = state_30266;
state_30266 = G__30305;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_30266){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_30266);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___30293,out))
})();
var state__28079__auto__ = (function (){var statearr_30288 = f__28078__auto__.call(null);
(statearr_30288[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___30293);

return statearr_30288;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___30293,out))
);


return out;
});

cljs.core.async.take.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_LT_ = (function cljs$core$async$map_LT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async30313 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30313 = (function (map_LT_,f,ch,meta30314){
this.map_LT_ = map_LT_;
this.f = f;
this.ch = ch;
this.meta30314 = meta30314;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async30313.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30315,meta30314__$1){
var self__ = this;
var _30315__$1 = this;
return (new cljs.core.async.t_cljs$core$async30313(self__.map_LT_,self__.f,self__.ch,meta30314__$1));
});

cljs.core.async.t_cljs$core$async30313.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30315){
var self__ = this;
var _30315__$1 = this;
return self__.meta30314;
});

cljs.core.async.t_cljs$core$async30313.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t_cljs$core$async30313.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async30313.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async30313.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t_cljs$core$async30313.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
var ret = cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,(function (){
if(typeof cljs.core.async.t_cljs$core$async30316 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Handler}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30316 = (function (map_LT_,f,ch,meta30314,_,fn1,meta30317){
this.map_LT_ = map_LT_;
this.f = f;
this.ch = ch;
this.meta30314 = meta30314;
this._ = _;
this.fn1 = fn1;
this.meta30317 = meta30317;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async30316.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = ((function (___$1){
return (function (_30318,meta30317__$1){
var self__ = this;
var _30318__$1 = this;
return (new cljs.core.async.t_cljs$core$async30316(self__.map_LT_,self__.f,self__.ch,self__.meta30314,self__._,self__.fn1,meta30317__$1));
});})(___$1))
;

cljs.core.async.t_cljs$core$async30316.prototype.cljs$core$IMeta$_meta$arity$1 = ((function (___$1){
return (function (_30318){
var self__ = this;
var _30318__$1 = this;
return self__.meta30317;
});})(___$1))
;

cljs.core.async.t_cljs$core$async30316.prototype.cljs$core$async$impl$protocols$Handler$ = true;

cljs.core.async.t_cljs$core$async30316.prototype.cljs$core$async$impl$protocols$Handler$active_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return cljs.core.async.impl.protocols.active_QMARK_.call(null,self__.fn1);
});})(___$1))
;

cljs.core.async.t_cljs$core$async30316.prototype.cljs$core$async$impl$protocols$Handler$blockable_QMARK_$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
return true;
});})(___$1))
;

cljs.core.async.t_cljs$core$async30316.prototype.cljs$core$async$impl$protocols$Handler$commit$arity$1 = ((function (___$1){
return (function (___$1){
var self__ = this;
var ___$2 = this;
var f1 = cljs.core.async.impl.protocols.commit.call(null,self__.fn1);
return ((function (f1,___$2,___$1){
return (function (p1__30306_SHARP_){
return f1.call(null,(((p1__30306_SHARP_ == null))?null:self__.f.call(null,p1__30306_SHARP_)));
});
;})(f1,___$2,___$1))
});})(___$1))
;

cljs.core.async.t_cljs$core$async30316.getBasis = ((function (___$1){
return (function (){
return new cljs.core.PersistentVector(null, 7, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"map<","map<",-1235808357,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta30314","meta30314",-1722219703,null),cljs.core.with_meta(new cljs.core.Symbol(null,"_","_",-1201019570,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"tag","tag",-1290361223),new cljs.core.Symbol("cljs.core.async","t_cljs$core$async30313","cljs.core.async/t_cljs$core$async30313",2047256623,null)], null)),new cljs.core.Symbol(null,"fn1","fn1",895834444,null),new cljs.core.Symbol(null,"meta30317","meta30317",745668197,null)], null);
});})(___$1))
;

cljs.core.async.t_cljs$core$async30316.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30316.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30316";

cljs.core.async.t_cljs$core$async30316.cljs$lang$ctorPrWriter = ((function (___$1){
return (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.core.async/t_cljs$core$async30316");
});})(___$1))
;

cljs.core.async.__GT_t_cljs$core$async30316 = ((function (___$1){
return (function cljs$core$async$map_LT__$___GT_t_cljs$core$async30316(map_LT___$1,f__$1,ch__$1,meta30314__$1,___$2,fn1__$1,meta30317){
return (new cljs.core.async.t_cljs$core$async30316(map_LT___$1,f__$1,ch__$1,meta30314__$1,___$2,fn1__$1,meta30317));
});})(___$1))
;

}

return (new cljs.core.async.t_cljs$core$async30316(self__.map_LT_,self__.f,self__.ch,self__.meta30314,___$1,fn1,cljs.core.PersistentArrayMap.EMPTY));
})()
);
if(cljs.core.truth_((function (){var and__25887__auto__ = ret;
if(cljs.core.truth_(and__25887__auto__)){
return !((cljs.core.deref.call(null,ret) == null));
} else {
return and__25887__auto__;
}
})())){
return cljs.core.async.impl.channels.box.call(null,self__.f.call(null,cljs.core.deref.call(null,ret)));
} else {
return ret;
}
});

cljs.core.async.t_cljs$core$async30313.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t_cljs$core$async30313.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
});

cljs.core.async.t_cljs$core$async30313.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"map<","map<",-1235808357,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta30314","meta30314",-1722219703,null)], null);
});

cljs.core.async.t_cljs$core$async30313.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30313.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30313";

cljs.core.async.t_cljs$core$async30313.cljs$lang$ctorPrWriter = (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.core.async/t_cljs$core$async30313");
});

cljs.core.async.__GT_t_cljs$core$async30313 = (function cljs$core$async$map_LT__$___GT_t_cljs$core$async30313(map_LT___$1,f__$1,ch__$1,meta30314){
return (new cljs.core.async.t_cljs$core$async30313(map_LT___$1,f__$1,ch__$1,meta30314));
});

}

return (new cljs.core.async.t_cljs$core$async30313(cljs$core$async$map_LT_,f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.map_GT_ = (function cljs$core$async$map_GT_(f,ch){
if(typeof cljs.core.async.t_cljs$core$async30322 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30322 = (function (map_GT_,f,ch,meta30323){
this.map_GT_ = map_GT_;
this.f = f;
this.ch = ch;
this.meta30323 = meta30323;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async30322.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30324,meta30323__$1){
var self__ = this;
var _30324__$1 = this;
return (new cljs.core.async.t_cljs$core$async30322(self__.map_GT_,self__.f,self__.ch,meta30323__$1));
});

cljs.core.async.t_cljs$core$async30322.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30324){
var self__ = this;
var _30324__$1 = this;
return self__.meta30323;
});

cljs.core.async.t_cljs$core$async30322.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t_cljs$core$async30322.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async30322.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t_cljs$core$async30322.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async30322.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t_cljs$core$async30322.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,self__.f.call(null,val),fn1);
});

cljs.core.async.t_cljs$core$async30322.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"map>","map>",1676369295,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"f","f",43394975,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta30323","meta30323",611866832,null)], null);
});

cljs.core.async.t_cljs$core$async30322.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30322.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30322";

cljs.core.async.t_cljs$core$async30322.cljs$lang$ctorPrWriter = (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.core.async/t_cljs$core$async30322");
});

cljs.core.async.__GT_t_cljs$core$async30322 = (function cljs$core$async$map_GT__$___GT_t_cljs$core$async30322(map_GT___$1,f__$1,ch__$1,meta30323){
return (new cljs.core.async.t_cljs$core$async30322(map_GT___$1,f__$1,ch__$1,meta30323));
});

}

return (new cljs.core.async.t_cljs$core$async30322(cljs$core$async$map_GT_,f,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_GT_ = (function cljs$core$async$filter_GT_(p,ch){
if(typeof cljs.core.async.t_cljs$core$async30328 !== 'undefined'){
} else {

/**
* @constructor
 * @implements {cljs.core.async.impl.protocols.Channel}
 * @implements {cljs.core.async.impl.protocols.WritePort}
 * @implements {cljs.core.async.impl.protocols.ReadPort}
 * @implements {cljs.core.IMeta}
 * @implements {cljs.core.IWithMeta}
*/
cljs.core.async.t_cljs$core$async30328 = (function (filter_GT_,p,ch,meta30329){
this.filter_GT_ = filter_GT_;
this.p = p;
this.ch = ch;
this.meta30329 = meta30329;
this.cljs$lang$protocol_mask$partition0$ = 393216;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.core.async.t_cljs$core$async30328.prototype.cljs$core$IWithMeta$_with_meta$arity$2 = (function (_30330,meta30329__$1){
var self__ = this;
var _30330__$1 = this;
return (new cljs.core.async.t_cljs$core$async30328(self__.filter_GT_,self__.p,self__.ch,meta30329__$1));
});

cljs.core.async.t_cljs$core$async30328.prototype.cljs$core$IMeta$_meta$arity$1 = (function (_30330){
var self__ = this;
var _30330__$1 = this;
return self__.meta30329;
});

cljs.core.async.t_cljs$core$async30328.prototype.cljs$core$async$impl$protocols$Channel$ = true;

cljs.core.async.t_cljs$core$async30328.prototype.cljs$core$async$impl$protocols$Channel$close_BANG_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.close_BANG_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async30328.prototype.cljs$core$async$impl$protocols$Channel$closed_QMARK_$arity$1 = (function (_){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch);
});

cljs.core.async.t_cljs$core$async30328.prototype.cljs$core$async$impl$protocols$ReadPort$ = true;

cljs.core.async.t_cljs$core$async30328.prototype.cljs$core$async$impl$protocols$ReadPort$take_BANG_$arity$2 = (function (_,fn1){
var self__ = this;
var ___$1 = this;
return cljs.core.async.impl.protocols.take_BANG_.call(null,self__.ch,fn1);
});

cljs.core.async.t_cljs$core$async30328.prototype.cljs$core$async$impl$protocols$WritePort$ = true;

cljs.core.async.t_cljs$core$async30328.prototype.cljs$core$async$impl$protocols$WritePort$put_BANG_$arity$3 = (function (_,val,fn1){
var self__ = this;
var ___$1 = this;
if(cljs.core.truth_(self__.p.call(null,val))){
return cljs.core.async.impl.protocols.put_BANG_.call(null,self__.ch,val,fn1);
} else {
return cljs.core.async.impl.channels.box.call(null,cljs.core.not.call(null,cljs.core.async.impl.protocols.closed_QMARK_.call(null,self__.ch)));
}
});

cljs.core.async.t_cljs$core$async30328.getBasis = (function (){
return new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.core.with_meta(new cljs.core.Symbol(null,"filter>","filter>",-37644455,null),new cljs.core.PersistentArrayMap(null, 2, [new cljs.core.Keyword(null,"arglists","arglists",1661989754),cljs.core.list(new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.list(new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null)], null))),new cljs.core.Keyword(null,"doc","doc",1913296891),"Deprecated - this function will be removed. Use transducer instead"], null)),new cljs.core.Symbol(null,"p","p",1791580836,null),new cljs.core.Symbol(null,"ch","ch",1085813622,null),new cljs.core.Symbol(null,"meta30329","meta30329",-1769616714,null)], null);
});

cljs.core.async.t_cljs$core$async30328.cljs$lang$type = true;

cljs.core.async.t_cljs$core$async30328.cljs$lang$ctorStr = "cljs.core.async/t_cljs$core$async30328";

cljs.core.async.t_cljs$core$async30328.cljs$lang$ctorPrWriter = (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.core.async/t_cljs$core$async30328");
});

cljs.core.async.__GT_t_cljs$core$async30328 = (function cljs$core$async$filter_GT__$___GT_t_cljs$core$async30328(filter_GT___$1,p__$1,ch__$1,meta30329){
return (new cljs.core.async.t_cljs$core$async30328(filter_GT___$1,p__$1,ch__$1,meta30329));
});

}

return (new cljs.core.async.t_cljs$core$async30328(cljs$core$async$filter_GT_,p,ch,cljs.core.PersistentArrayMap.EMPTY));
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_GT_ = (function cljs$core$async$remove_GT_(p,ch){
return cljs.core.async.filter_GT_.call(null,cljs.core.complement.call(null,p),ch);
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.filter_LT_ = (function cljs$core$async$filter_LT_(var_args){
var args30331 = [];
var len__26974__auto___30375 = arguments.length;
var i__26975__auto___30376 = (0);
while(true){
if((i__26975__auto___30376 < len__26974__auto___30375)){
args30331.push((arguments[i__26975__auto___30376]));

var G__30377 = (i__26975__auto___30376 + (1));
i__26975__auto___30376 = G__30377;
continue;
} else {
}
break;
}

var G__30333 = args30331.length;
switch (G__30333) {
case 2:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30331.length)].join('')));

}
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.filter_LT_.call(null,p,ch,null);
});

cljs.core.async.filter_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28077__auto___30379 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___30379,out){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___30379,out){
return (function (state_30354){
var state_val_30355 = (state_30354[(1)]);
if((state_val_30355 === (7))){
var inst_30350 = (state_30354[(2)]);
var state_30354__$1 = state_30354;
var statearr_30356_30380 = state_30354__$1;
(statearr_30356_30380[(2)] = inst_30350);

(statearr_30356_30380[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30355 === (1))){
var state_30354__$1 = state_30354;
var statearr_30357_30381 = state_30354__$1;
(statearr_30357_30381[(2)] = null);

(statearr_30357_30381[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30355 === (4))){
var inst_30336 = (state_30354[(7)]);
var inst_30336__$1 = (state_30354[(2)]);
var inst_30337 = (inst_30336__$1 == null);
var state_30354__$1 = (function (){var statearr_30358 = state_30354;
(statearr_30358[(7)] = inst_30336__$1);

return statearr_30358;
})();
if(cljs.core.truth_(inst_30337)){
var statearr_30359_30382 = state_30354__$1;
(statearr_30359_30382[(1)] = (5));

} else {
var statearr_30360_30383 = state_30354__$1;
(statearr_30360_30383[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30355 === (6))){
var inst_30336 = (state_30354[(7)]);
var inst_30341 = p.call(null,inst_30336);
var state_30354__$1 = state_30354;
if(cljs.core.truth_(inst_30341)){
var statearr_30361_30384 = state_30354__$1;
(statearr_30361_30384[(1)] = (8));

} else {
var statearr_30362_30385 = state_30354__$1;
(statearr_30362_30385[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30355 === (3))){
var inst_30352 = (state_30354[(2)]);
var state_30354__$1 = state_30354;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30354__$1,inst_30352);
} else {
if((state_val_30355 === (2))){
var state_30354__$1 = state_30354;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30354__$1,(4),ch);
} else {
if((state_val_30355 === (11))){
var inst_30344 = (state_30354[(2)]);
var state_30354__$1 = state_30354;
var statearr_30363_30386 = state_30354__$1;
(statearr_30363_30386[(2)] = inst_30344);

(statearr_30363_30386[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30355 === (9))){
var state_30354__$1 = state_30354;
var statearr_30364_30387 = state_30354__$1;
(statearr_30364_30387[(2)] = null);

(statearr_30364_30387[(1)] = (10));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30355 === (5))){
var inst_30339 = cljs.core.async.close_BANG_.call(null,out);
var state_30354__$1 = state_30354;
var statearr_30365_30388 = state_30354__$1;
(statearr_30365_30388[(2)] = inst_30339);

(statearr_30365_30388[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30355 === (10))){
var inst_30347 = (state_30354[(2)]);
var state_30354__$1 = (function (){var statearr_30366 = state_30354;
(statearr_30366[(8)] = inst_30347);

return statearr_30366;
})();
var statearr_30367_30389 = state_30354__$1;
(statearr_30367_30389[(2)] = null);

(statearr_30367_30389[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30355 === (8))){
var inst_30336 = (state_30354[(7)]);
var state_30354__$1 = state_30354;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30354__$1,(11),out,inst_30336);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___30379,out))
;
return ((function (switch__27965__auto__,c__28077__auto___30379,out){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_30371 = [null,null,null,null,null,null,null,null,null];
(statearr_30371[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_30371[(1)] = (1));

return statearr_30371;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_30354){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_30354);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e30372){if((e30372 instanceof Object)){
var ex__27969__auto__ = e30372;
var statearr_30373_30390 = state_30354;
(statearr_30373_30390[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30354);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30372;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30391 = state_30354;
state_30354 = G__30391;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_30354){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_30354);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___30379,out))
})();
var state__28079__auto__ = (function (){var statearr_30374 = f__28078__auto__.call(null);
(statearr_30374[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___30379);

return statearr_30374;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___30379,out))
);


return out;
});

cljs.core.async.filter_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.remove_LT_ = (function cljs$core$async$remove_LT_(var_args){
var args30392 = [];
var len__26974__auto___30395 = arguments.length;
var i__26975__auto___30396 = (0);
while(true){
if((i__26975__auto___30396 < len__26974__auto___30395)){
args30392.push((arguments[i__26975__auto___30396]));

var G__30397 = (i__26975__auto___30396 + (1));
i__26975__auto___30396 = G__30397;
continue;
} else {
}
break;
}

var G__30394 = args30392.length;
switch (G__30394) {
case 2:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30392.length)].join('')));

}
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$2 = (function (p,ch){
return cljs.core.async.remove_LT_.call(null,p,ch,null);
});

cljs.core.async.remove_LT_.cljs$core$IFn$_invoke$arity$3 = (function (p,ch,buf_or_n){
return cljs.core.async.filter_LT_.call(null,cljs.core.complement.call(null,p),ch,buf_or_n);
});

cljs.core.async.remove_LT_.cljs$lang$maxFixedArity = 3;

cljs.core.async.mapcat_STAR_ = (function cljs$core$async$mapcat_STAR_(f,in$,out){
var c__28077__auto__ = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto__){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto__){
return (function (state_30564){
var state_val_30565 = (state_30564[(1)]);
if((state_val_30565 === (7))){
var inst_30560 = (state_30564[(2)]);
var state_30564__$1 = state_30564;
var statearr_30566_30607 = state_30564__$1;
(statearr_30566_30607[(2)] = inst_30560);

(statearr_30566_30607[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (20))){
var inst_30530 = (state_30564[(7)]);
var inst_30541 = (state_30564[(2)]);
var inst_30542 = cljs.core.next.call(null,inst_30530);
var inst_30516 = inst_30542;
var inst_30517 = null;
var inst_30518 = (0);
var inst_30519 = (0);
var state_30564__$1 = (function (){var statearr_30567 = state_30564;
(statearr_30567[(8)] = inst_30517);

(statearr_30567[(9)] = inst_30518);

(statearr_30567[(10)] = inst_30519);

(statearr_30567[(11)] = inst_30516);

(statearr_30567[(12)] = inst_30541);

return statearr_30567;
})();
var statearr_30568_30608 = state_30564__$1;
(statearr_30568_30608[(2)] = null);

(statearr_30568_30608[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (1))){
var state_30564__$1 = state_30564;
var statearr_30569_30609 = state_30564__$1;
(statearr_30569_30609[(2)] = null);

(statearr_30569_30609[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (4))){
var inst_30505 = (state_30564[(13)]);
var inst_30505__$1 = (state_30564[(2)]);
var inst_30506 = (inst_30505__$1 == null);
var state_30564__$1 = (function (){var statearr_30570 = state_30564;
(statearr_30570[(13)] = inst_30505__$1);

return statearr_30570;
})();
if(cljs.core.truth_(inst_30506)){
var statearr_30571_30610 = state_30564__$1;
(statearr_30571_30610[(1)] = (5));

} else {
var statearr_30572_30611 = state_30564__$1;
(statearr_30572_30611[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (15))){
var state_30564__$1 = state_30564;
var statearr_30576_30612 = state_30564__$1;
(statearr_30576_30612[(2)] = null);

(statearr_30576_30612[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (21))){
var state_30564__$1 = state_30564;
var statearr_30577_30613 = state_30564__$1;
(statearr_30577_30613[(2)] = null);

(statearr_30577_30613[(1)] = (23));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (13))){
var inst_30517 = (state_30564[(8)]);
var inst_30518 = (state_30564[(9)]);
var inst_30519 = (state_30564[(10)]);
var inst_30516 = (state_30564[(11)]);
var inst_30526 = (state_30564[(2)]);
var inst_30527 = (inst_30519 + (1));
var tmp30573 = inst_30517;
var tmp30574 = inst_30518;
var tmp30575 = inst_30516;
var inst_30516__$1 = tmp30575;
var inst_30517__$1 = tmp30573;
var inst_30518__$1 = tmp30574;
var inst_30519__$1 = inst_30527;
var state_30564__$1 = (function (){var statearr_30578 = state_30564;
(statearr_30578[(8)] = inst_30517__$1);

(statearr_30578[(9)] = inst_30518__$1);

(statearr_30578[(10)] = inst_30519__$1);

(statearr_30578[(11)] = inst_30516__$1);

(statearr_30578[(14)] = inst_30526);

return statearr_30578;
})();
var statearr_30579_30614 = state_30564__$1;
(statearr_30579_30614[(2)] = null);

(statearr_30579_30614[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (22))){
var state_30564__$1 = state_30564;
var statearr_30580_30615 = state_30564__$1;
(statearr_30580_30615[(2)] = null);

(statearr_30580_30615[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (6))){
var inst_30505 = (state_30564[(13)]);
var inst_30514 = f.call(null,inst_30505);
var inst_30515 = cljs.core.seq.call(null,inst_30514);
var inst_30516 = inst_30515;
var inst_30517 = null;
var inst_30518 = (0);
var inst_30519 = (0);
var state_30564__$1 = (function (){var statearr_30581 = state_30564;
(statearr_30581[(8)] = inst_30517);

(statearr_30581[(9)] = inst_30518);

(statearr_30581[(10)] = inst_30519);

(statearr_30581[(11)] = inst_30516);

return statearr_30581;
})();
var statearr_30582_30616 = state_30564__$1;
(statearr_30582_30616[(2)] = null);

(statearr_30582_30616[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (17))){
var inst_30530 = (state_30564[(7)]);
var inst_30534 = cljs.core.chunk_first.call(null,inst_30530);
var inst_30535 = cljs.core.chunk_rest.call(null,inst_30530);
var inst_30536 = cljs.core.count.call(null,inst_30534);
var inst_30516 = inst_30535;
var inst_30517 = inst_30534;
var inst_30518 = inst_30536;
var inst_30519 = (0);
var state_30564__$1 = (function (){var statearr_30583 = state_30564;
(statearr_30583[(8)] = inst_30517);

(statearr_30583[(9)] = inst_30518);

(statearr_30583[(10)] = inst_30519);

(statearr_30583[(11)] = inst_30516);

return statearr_30583;
})();
var statearr_30584_30617 = state_30564__$1;
(statearr_30584_30617[(2)] = null);

(statearr_30584_30617[(1)] = (8));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (3))){
var inst_30562 = (state_30564[(2)]);
var state_30564__$1 = state_30564;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30564__$1,inst_30562);
} else {
if((state_val_30565 === (12))){
var inst_30550 = (state_30564[(2)]);
var state_30564__$1 = state_30564;
var statearr_30585_30618 = state_30564__$1;
(statearr_30585_30618[(2)] = inst_30550);

(statearr_30585_30618[(1)] = (9));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (2))){
var state_30564__$1 = state_30564;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30564__$1,(4),in$);
} else {
if((state_val_30565 === (23))){
var inst_30558 = (state_30564[(2)]);
var state_30564__$1 = state_30564;
var statearr_30586_30619 = state_30564__$1;
(statearr_30586_30619[(2)] = inst_30558);

(statearr_30586_30619[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (19))){
var inst_30545 = (state_30564[(2)]);
var state_30564__$1 = state_30564;
var statearr_30587_30620 = state_30564__$1;
(statearr_30587_30620[(2)] = inst_30545);

(statearr_30587_30620[(1)] = (16));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (11))){
var inst_30530 = (state_30564[(7)]);
var inst_30516 = (state_30564[(11)]);
var inst_30530__$1 = cljs.core.seq.call(null,inst_30516);
var state_30564__$1 = (function (){var statearr_30588 = state_30564;
(statearr_30588[(7)] = inst_30530__$1);

return statearr_30588;
})();
if(inst_30530__$1){
var statearr_30589_30621 = state_30564__$1;
(statearr_30589_30621[(1)] = (14));

} else {
var statearr_30590_30622 = state_30564__$1;
(statearr_30590_30622[(1)] = (15));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (9))){
var inst_30552 = (state_30564[(2)]);
var inst_30553 = cljs.core.async.impl.protocols.closed_QMARK_.call(null,out);
var state_30564__$1 = (function (){var statearr_30591 = state_30564;
(statearr_30591[(15)] = inst_30552);

return statearr_30591;
})();
if(cljs.core.truth_(inst_30553)){
var statearr_30592_30623 = state_30564__$1;
(statearr_30592_30623[(1)] = (21));

} else {
var statearr_30593_30624 = state_30564__$1;
(statearr_30593_30624[(1)] = (22));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (5))){
var inst_30508 = cljs.core.async.close_BANG_.call(null,out);
var state_30564__$1 = state_30564;
var statearr_30594_30625 = state_30564__$1;
(statearr_30594_30625[(2)] = inst_30508);

(statearr_30594_30625[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (14))){
var inst_30530 = (state_30564[(7)]);
var inst_30532 = cljs.core.chunked_seq_QMARK_.call(null,inst_30530);
var state_30564__$1 = state_30564;
if(inst_30532){
var statearr_30595_30626 = state_30564__$1;
(statearr_30595_30626[(1)] = (17));

} else {
var statearr_30596_30627 = state_30564__$1;
(statearr_30596_30627[(1)] = (18));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (16))){
var inst_30548 = (state_30564[(2)]);
var state_30564__$1 = state_30564;
var statearr_30597_30628 = state_30564__$1;
(statearr_30597_30628[(2)] = inst_30548);

(statearr_30597_30628[(1)] = (12));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30565 === (10))){
var inst_30517 = (state_30564[(8)]);
var inst_30519 = (state_30564[(10)]);
var inst_30524 = cljs.core._nth.call(null,inst_30517,inst_30519);
var state_30564__$1 = state_30564;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30564__$1,(13),out,inst_30524);
} else {
if((state_val_30565 === (18))){
var inst_30530 = (state_30564[(7)]);
var inst_30539 = cljs.core.first.call(null,inst_30530);
var state_30564__$1 = state_30564;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30564__$1,(20),out,inst_30539);
} else {
if((state_val_30565 === (8))){
var inst_30518 = (state_30564[(9)]);
var inst_30519 = (state_30564[(10)]);
var inst_30521 = (inst_30519 < inst_30518);
var inst_30522 = inst_30521;
var state_30564__$1 = state_30564;
if(cljs.core.truth_(inst_30522)){
var statearr_30598_30629 = state_30564__$1;
(statearr_30598_30629[(1)] = (10));

} else {
var statearr_30599_30630 = state_30564__$1;
(statearr_30599_30630[(1)] = (11));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto__))
;
return ((function (switch__27965__auto__,c__28077__auto__){
return (function() {
var cljs$core$async$mapcat_STAR__$_state_machine__27966__auto__ = null;
var cljs$core$async$mapcat_STAR__$_state_machine__27966__auto____0 = (function (){
var statearr_30603 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30603[(0)] = cljs$core$async$mapcat_STAR__$_state_machine__27966__auto__);

(statearr_30603[(1)] = (1));

return statearr_30603;
});
var cljs$core$async$mapcat_STAR__$_state_machine__27966__auto____1 = (function (state_30564){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_30564);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e30604){if((e30604 instanceof Object)){
var ex__27969__auto__ = e30604;
var statearr_30605_30631 = state_30564;
(statearr_30605_30631[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30564);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30604;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30632 = state_30564;
state_30564 = G__30632;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$mapcat_STAR__$_state_machine__27966__auto__ = function(state_30564){
switch(arguments.length){
case 0:
return cljs$core$async$mapcat_STAR__$_state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$mapcat_STAR__$_state_machine__27966__auto____1.call(this,state_30564);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$mapcat_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$mapcat_STAR__$_state_machine__27966__auto____0;
cljs$core$async$mapcat_STAR__$_state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$mapcat_STAR__$_state_machine__27966__auto____1;
return cljs$core$async$mapcat_STAR__$_state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto__))
})();
var state__28079__auto__ = (function (){var statearr_30606 = f__28078__auto__.call(null);
(statearr_30606[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto__);

return statearr_30606;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto__))
);

return c__28077__auto__;
});
/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_LT_ = (function cljs$core$async$mapcat_LT_(var_args){
var args30633 = [];
var len__26974__auto___30636 = arguments.length;
var i__26975__auto___30637 = (0);
while(true){
if((i__26975__auto___30637 < len__26974__auto___30636)){
args30633.push((arguments[i__26975__auto___30637]));

var G__30638 = (i__26975__auto___30637 + (1));
i__26975__auto___30637 = G__30638;
continue;
} else {
}
break;
}

var G__30635 = args30633.length;
switch (G__30635) {
case 2:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30633.length)].join('')));

}
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$2 = (function (f,in$){
return cljs.core.async.mapcat_LT_.call(null,f,in$,null);
});

cljs.core.async.mapcat_LT_.cljs$core$IFn$_invoke$arity$3 = (function (f,in$,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return out;
});

cljs.core.async.mapcat_LT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.mapcat_GT_ = (function cljs$core$async$mapcat_GT_(var_args){
var args30640 = [];
var len__26974__auto___30643 = arguments.length;
var i__26975__auto___30644 = (0);
while(true){
if((i__26975__auto___30644 < len__26974__auto___30643)){
args30640.push((arguments[i__26975__auto___30644]));

var G__30645 = (i__26975__auto___30644 + (1));
i__26975__auto___30644 = G__30645;
continue;
} else {
}
break;
}

var G__30642 = args30640.length;
switch (G__30642) {
case 2:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30640.length)].join('')));

}
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$2 = (function (f,out){
return cljs.core.async.mapcat_GT_.call(null,f,out,null);
});

cljs.core.async.mapcat_GT_.cljs$core$IFn$_invoke$arity$3 = (function (f,out,buf_or_n){
var in$ = cljs.core.async.chan.call(null,buf_or_n);
cljs.core.async.mapcat_STAR_.call(null,f,in$,out);

return in$;
});

cljs.core.async.mapcat_GT_.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.unique = (function cljs$core$async$unique(var_args){
var args30647 = [];
var len__26974__auto___30698 = arguments.length;
var i__26975__auto___30699 = (0);
while(true){
if((i__26975__auto___30699 < len__26974__auto___30698)){
args30647.push((arguments[i__26975__auto___30699]));

var G__30700 = (i__26975__auto___30699 + (1));
i__26975__auto___30699 = G__30700;
continue;
} else {
}
break;
}

var G__30649 = args30647.length;
switch (G__30649) {
case 1:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1((arguments[(0)]));

break;
case 2:
return cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30647.length)].join('')));

}
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$1 = (function (ch){
return cljs.core.async.unique.call(null,ch,null);
});

cljs.core.async.unique.cljs$core$IFn$_invoke$arity$2 = (function (ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28077__auto___30702 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___30702,out){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___30702,out){
return (function (state_30673){
var state_val_30674 = (state_30673[(1)]);
if((state_val_30674 === (7))){
var inst_30668 = (state_30673[(2)]);
var state_30673__$1 = state_30673;
var statearr_30675_30703 = state_30673__$1;
(statearr_30675_30703[(2)] = inst_30668);

(statearr_30675_30703[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30674 === (1))){
var inst_30650 = null;
var state_30673__$1 = (function (){var statearr_30676 = state_30673;
(statearr_30676[(7)] = inst_30650);

return statearr_30676;
})();
var statearr_30677_30704 = state_30673__$1;
(statearr_30677_30704[(2)] = null);

(statearr_30677_30704[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30674 === (4))){
var inst_30653 = (state_30673[(8)]);
var inst_30653__$1 = (state_30673[(2)]);
var inst_30654 = (inst_30653__$1 == null);
var inst_30655 = cljs.core.not.call(null,inst_30654);
var state_30673__$1 = (function (){var statearr_30678 = state_30673;
(statearr_30678[(8)] = inst_30653__$1);

return statearr_30678;
})();
if(inst_30655){
var statearr_30679_30705 = state_30673__$1;
(statearr_30679_30705[(1)] = (5));

} else {
var statearr_30680_30706 = state_30673__$1;
(statearr_30680_30706[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30674 === (6))){
var state_30673__$1 = state_30673;
var statearr_30681_30707 = state_30673__$1;
(statearr_30681_30707[(2)] = null);

(statearr_30681_30707[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30674 === (3))){
var inst_30670 = (state_30673[(2)]);
var inst_30671 = cljs.core.async.close_BANG_.call(null,out);
var state_30673__$1 = (function (){var statearr_30682 = state_30673;
(statearr_30682[(9)] = inst_30670);

return statearr_30682;
})();
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30673__$1,inst_30671);
} else {
if((state_val_30674 === (2))){
var state_30673__$1 = state_30673;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30673__$1,(4),ch);
} else {
if((state_val_30674 === (11))){
var inst_30653 = (state_30673[(8)]);
var inst_30662 = (state_30673[(2)]);
var inst_30650 = inst_30653;
var state_30673__$1 = (function (){var statearr_30683 = state_30673;
(statearr_30683[(10)] = inst_30662);

(statearr_30683[(7)] = inst_30650);

return statearr_30683;
})();
var statearr_30684_30708 = state_30673__$1;
(statearr_30684_30708[(2)] = null);

(statearr_30684_30708[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30674 === (9))){
var inst_30653 = (state_30673[(8)]);
var state_30673__$1 = state_30673;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30673__$1,(11),out,inst_30653);
} else {
if((state_val_30674 === (5))){
var inst_30653 = (state_30673[(8)]);
var inst_30650 = (state_30673[(7)]);
var inst_30657 = cljs.core._EQ_.call(null,inst_30653,inst_30650);
var state_30673__$1 = state_30673;
if(inst_30657){
var statearr_30686_30709 = state_30673__$1;
(statearr_30686_30709[(1)] = (8));

} else {
var statearr_30687_30710 = state_30673__$1;
(statearr_30687_30710[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30674 === (10))){
var inst_30665 = (state_30673[(2)]);
var state_30673__$1 = state_30673;
var statearr_30688_30711 = state_30673__$1;
(statearr_30688_30711[(2)] = inst_30665);

(statearr_30688_30711[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30674 === (8))){
var inst_30650 = (state_30673[(7)]);
var tmp30685 = inst_30650;
var inst_30650__$1 = tmp30685;
var state_30673__$1 = (function (){var statearr_30689 = state_30673;
(statearr_30689[(7)] = inst_30650__$1);

return statearr_30689;
})();
var statearr_30690_30712 = state_30673__$1;
(statearr_30690_30712[(2)] = null);

(statearr_30690_30712[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___30702,out))
;
return ((function (switch__27965__auto__,c__28077__auto___30702,out){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_30694 = [null,null,null,null,null,null,null,null,null,null,null];
(statearr_30694[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_30694[(1)] = (1));

return statearr_30694;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_30673){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_30673);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e30695){if((e30695 instanceof Object)){
var ex__27969__auto__ = e30695;
var statearr_30696_30713 = state_30673;
(statearr_30696_30713[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30673);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30695;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30714 = state_30673;
state_30673 = G__30714;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_30673){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_30673);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___30702,out))
})();
var state__28079__auto__ = (function (){var statearr_30697 = f__28078__auto__.call(null);
(statearr_30697[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___30702);

return statearr_30697;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___30702,out))
);


return out;
});

cljs.core.async.unique.cljs$lang$maxFixedArity = 2;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition = (function cljs$core$async$partition(var_args){
var args30715 = [];
var len__26974__auto___30785 = arguments.length;
var i__26975__auto___30786 = (0);
while(true){
if((i__26975__auto___30786 < len__26974__auto___30785)){
args30715.push((arguments[i__26975__auto___30786]));

var G__30787 = (i__26975__auto___30786 + (1));
i__26975__auto___30786 = G__30787;
continue;
} else {
}
break;
}

var G__30717 = args30715.length;
switch (G__30717) {
case 2:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30715.length)].join('')));

}
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$2 = (function (n,ch){
return cljs.core.async.partition.call(null,n,ch,null);
});

cljs.core.async.partition.cljs$core$IFn$_invoke$arity$3 = (function (n,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28077__auto___30789 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___30789,out){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___30789,out){
return (function (state_30755){
var state_val_30756 = (state_30755[(1)]);
if((state_val_30756 === (7))){
var inst_30751 = (state_30755[(2)]);
var state_30755__$1 = state_30755;
var statearr_30757_30790 = state_30755__$1;
(statearr_30757_30790[(2)] = inst_30751);

(statearr_30757_30790[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30756 === (1))){
var inst_30718 = (new Array(n));
var inst_30719 = inst_30718;
var inst_30720 = (0);
var state_30755__$1 = (function (){var statearr_30758 = state_30755;
(statearr_30758[(7)] = inst_30719);

(statearr_30758[(8)] = inst_30720);

return statearr_30758;
})();
var statearr_30759_30791 = state_30755__$1;
(statearr_30759_30791[(2)] = null);

(statearr_30759_30791[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30756 === (4))){
var inst_30723 = (state_30755[(9)]);
var inst_30723__$1 = (state_30755[(2)]);
var inst_30724 = (inst_30723__$1 == null);
var inst_30725 = cljs.core.not.call(null,inst_30724);
var state_30755__$1 = (function (){var statearr_30760 = state_30755;
(statearr_30760[(9)] = inst_30723__$1);

return statearr_30760;
})();
if(inst_30725){
var statearr_30761_30792 = state_30755__$1;
(statearr_30761_30792[(1)] = (5));

} else {
var statearr_30762_30793 = state_30755__$1;
(statearr_30762_30793[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30756 === (15))){
var inst_30745 = (state_30755[(2)]);
var state_30755__$1 = state_30755;
var statearr_30763_30794 = state_30755__$1;
(statearr_30763_30794[(2)] = inst_30745);

(statearr_30763_30794[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30756 === (13))){
var state_30755__$1 = state_30755;
var statearr_30764_30795 = state_30755__$1;
(statearr_30764_30795[(2)] = null);

(statearr_30764_30795[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30756 === (6))){
var inst_30720 = (state_30755[(8)]);
var inst_30741 = (inst_30720 > (0));
var state_30755__$1 = state_30755;
if(cljs.core.truth_(inst_30741)){
var statearr_30765_30796 = state_30755__$1;
(statearr_30765_30796[(1)] = (12));

} else {
var statearr_30766_30797 = state_30755__$1;
(statearr_30766_30797[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30756 === (3))){
var inst_30753 = (state_30755[(2)]);
var state_30755__$1 = state_30755;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30755__$1,inst_30753);
} else {
if((state_val_30756 === (12))){
var inst_30719 = (state_30755[(7)]);
var inst_30743 = cljs.core.vec.call(null,inst_30719);
var state_30755__$1 = state_30755;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30755__$1,(15),out,inst_30743);
} else {
if((state_val_30756 === (2))){
var state_30755__$1 = state_30755;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30755__$1,(4),ch);
} else {
if((state_val_30756 === (11))){
var inst_30735 = (state_30755[(2)]);
var inst_30736 = (new Array(n));
var inst_30719 = inst_30736;
var inst_30720 = (0);
var state_30755__$1 = (function (){var statearr_30767 = state_30755;
(statearr_30767[(10)] = inst_30735);

(statearr_30767[(7)] = inst_30719);

(statearr_30767[(8)] = inst_30720);

return statearr_30767;
})();
var statearr_30768_30798 = state_30755__$1;
(statearr_30768_30798[(2)] = null);

(statearr_30768_30798[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30756 === (9))){
var inst_30719 = (state_30755[(7)]);
var inst_30733 = cljs.core.vec.call(null,inst_30719);
var state_30755__$1 = state_30755;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30755__$1,(11),out,inst_30733);
} else {
if((state_val_30756 === (5))){
var inst_30728 = (state_30755[(11)]);
var inst_30719 = (state_30755[(7)]);
var inst_30723 = (state_30755[(9)]);
var inst_30720 = (state_30755[(8)]);
var inst_30727 = (inst_30719[inst_30720] = inst_30723);
var inst_30728__$1 = (inst_30720 + (1));
var inst_30729 = (inst_30728__$1 < n);
var state_30755__$1 = (function (){var statearr_30769 = state_30755;
(statearr_30769[(11)] = inst_30728__$1);

(statearr_30769[(12)] = inst_30727);

return statearr_30769;
})();
if(cljs.core.truth_(inst_30729)){
var statearr_30770_30799 = state_30755__$1;
(statearr_30770_30799[(1)] = (8));

} else {
var statearr_30771_30800 = state_30755__$1;
(statearr_30771_30800[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30756 === (14))){
var inst_30748 = (state_30755[(2)]);
var inst_30749 = cljs.core.async.close_BANG_.call(null,out);
var state_30755__$1 = (function (){var statearr_30773 = state_30755;
(statearr_30773[(13)] = inst_30748);

return statearr_30773;
})();
var statearr_30774_30801 = state_30755__$1;
(statearr_30774_30801[(2)] = inst_30749);

(statearr_30774_30801[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30756 === (10))){
var inst_30739 = (state_30755[(2)]);
var state_30755__$1 = state_30755;
var statearr_30775_30802 = state_30755__$1;
(statearr_30775_30802[(2)] = inst_30739);

(statearr_30775_30802[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30756 === (8))){
var inst_30728 = (state_30755[(11)]);
var inst_30719 = (state_30755[(7)]);
var tmp30772 = inst_30719;
var inst_30719__$1 = tmp30772;
var inst_30720 = inst_30728;
var state_30755__$1 = (function (){var statearr_30776 = state_30755;
(statearr_30776[(7)] = inst_30719__$1);

(statearr_30776[(8)] = inst_30720);

return statearr_30776;
})();
var statearr_30777_30803 = state_30755__$1;
(statearr_30777_30803[(2)] = null);

(statearr_30777_30803[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___30789,out))
;
return ((function (switch__27965__auto__,c__28077__auto___30789,out){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_30781 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30781[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_30781[(1)] = (1));

return statearr_30781;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_30755){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_30755);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e30782){if((e30782 instanceof Object)){
var ex__27969__auto__ = e30782;
var statearr_30783_30804 = state_30755;
(statearr_30783_30804[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30755);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30782;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30805 = state_30755;
state_30755 = G__30805;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_30755){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_30755);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___30789,out))
})();
var state__28079__auto__ = (function (){var statearr_30784 = f__28078__auto__.call(null);
(statearr_30784[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___30789);

return statearr_30784;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___30789,out))
);


return out;
});

cljs.core.async.partition.cljs$lang$maxFixedArity = 3;

/**
 * Deprecated - this function will be removed. Use transducer instead
 */
cljs.core.async.partition_by = (function cljs$core$async$partition_by(var_args){
var args30806 = [];
var len__26974__auto___30880 = arguments.length;
var i__26975__auto___30881 = (0);
while(true){
if((i__26975__auto___30881 < len__26974__auto___30880)){
args30806.push((arguments[i__26975__auto___30881]));

var G__30882 = (i__26975__auto___30881 + (1));
i__26975__auto___30881 = G__30882;
continue;
} else {
}
break;
}

var G__30808 = args30806.length;
switch (G__30808) {
case 2:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2((arguments[(0)]),(arguments[(1)]));

break;
case 3:
return cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3((arguments[(0)]),(arguments[(1)]),(arguments[(2)]));

break;
default:
throw (new Error([cljs.core.str("Invalid arity: "),cljs.core.str(args30806.length)].join('')));

}
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$2 = (function (f,ch){
return cljs.core.async.partition_by.call(null,f,ch,null);
});

cljs.core.async.partition_by.cljs$core$IFn$_invoke$arity$3 = (function (f,ch,buf_or_n){
var out = cljs.core.async.chan.call(null,buf_or_n);
var c__28077__auto___30884 = cljs.core.async.chan.call(null,(1));
cljs.core.async.impl.dispatch.run.call(null,((function (c__28077__auto___30884,out){
return (function (){
var f__28078__auto__ = (function (){var switch__27965__auto__ = ((function (c__28077__auto___30884,out){
return (function (state_30850){
var state_val_30851 = (state_30850[(1)]);
if((state_val_30851 === (7))){
var inst_30846 = (state_30850[(2)]);
var state_30850__$1 = state_30850;
var statearr_30852_30885 = state_30850__$1;
(statearr_30852_30885[(2)] = inst_30846);

(statearr_30852_30885[(1)] = (3));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30851 === (1))){
var inst_30809 = [];
var inst_30810 = inst_30809;
var inst_30811 = new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123);
var state_30850__$1 = (function (){var statearr_30853 = state_30850;
(statearr_30853[(7)] = inst_30810);

(statearr_30853[(8)] = inst_30811);

return statearr_30853;
})();
var statearr_30854_30886 = state_30850__$1;
(statearr_30854_30886[(2)] = null);

(statearr_30854_30886[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30851 === (4))){
var inst_30814 = (state_30850[(9)]);
var inst_30814__$1 = (state_30850[(2)]);
var inst_30815 = (inst_30814__$1 == null);
var inst_30816 = cljs.core.not.call(null,inst_30815);
var state_30850__$1 = (function (){var statearr_30855 = state_30850;
(statearr_30855[(9)] = inst_30814__$1);

return statearr_30855;
})();
if(inst_30816){
var statearr_30856_30887 = state_30850__$1;
(statearr_30856_30887[(1)] = (5));

} else {
var statearr_30857_30888 = state_30850__$1;
(statearr_30857_30888[(1)] = (6));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30851 === (15))){
var inst_30840 = (state_30850[(2)]);
var state_30850__$1 = state_30850;
var statearr_30858_30889 = state_30850__$1;
(statearr_30858_30889[(2)] = inst_30840);

(statearr_30858_30889[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30851 === (13))){
var state_30850__$1 = state_30850;
var statearr_30859_30890 = state_30850__$1;
(statearr_30859_30890[(2)] = null);

(statearr_30859_30890[(1)] = (14));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30851 === (6))){
var inst_30810 = (state_30850[(7)]);
var inst_30835 = inst_30810.length;
var inst_30836 = (inst_30835 > (0));
var state_30850__$1 = state_30850;
if(cljs.core.truth_(inst_30836)){
var statearr_30860_30891 = state_30850__$1;
(statearr_30860_30891[(1)] = (12));

} else {
var statearr_30861_30892 = state_30850__$1;
(statearr_30861_30892[(1)] = (13));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30851 === (3))){
var inst_30848 = (state_30850[(2)]);
var state_30850__$1 = state_30850;
return cljs.core.async.impl.ioc_helpers.return_chan.call(null,state_30850__$1,inst_30848);
} else {
if((state_val_30851 === (12))){
var inst_30810 = (state_30850[(7)]);
var inst_30838 = cljs.core.vec.call(null,inst_30810);
var state_30850__$1 = state_30850;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30850__$1,(15),out,inst_30838);
} else {
if((state_val_30851 === (2))){
var state_30850__$1 = state_30850;
return cljs.core.async.impl.ioc_helpers.take_BANG_.call(null,state_30850__$1,(4),ch);
} else {
if((state_val_30851 === (11))){
var inst_30814 = (state_30850[(9)]);
var inst_30818 = (state_30850[(10)]);
var inst_30828 = (state_30850[(2)]);
var inst_30829 = [];
var inst_30830 = inst_30829.push(inst_30814);
var inst_30810 = inst_30829;
var inst_30811 = inst_30818;
var state_30850__$1 = (function (){var statearr_30862 = state_30850;
(statearr_30862[(11)] = inst_30830);

(statearr_30862[(7)] = inst_30810);

(statearr_30862[(12)] = inst_30828);

(statearr_30862[(8)] = inst_30811);

return statearr_30862;
})();
var statearr_30863_30893 = state_30850__$1;
(statearr_30863_30893[(2)] = null);

(statearr_30863_30893[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30851 === (9))){
var inst_30810 = (state_30850[(7)]);
var inst_30826 = cljs.core.vec.call(null,inst_30810);
var state_30850__$1 = state_30850;
return cljs.core.async.impl.ioc_helpers.put_BANG_.call(null,state_30850__$1,(11),out,inst_30826);
} else {
if((state_val_30851 === (5))){
var inst_30814 = (state_30850[(9)]);
var inst_30811 = (state_30850[(8)]);
var inst_30818 = (state_30850[(10)]);
var inst_30818__$1 = f.call(null,inst_30814);
var inst_30819 = cljs.core._EQ_.call(null,inst_30818__$1,inst_30811);
var inst_30820 = cljs.core.keyword_identical_QMARK_.call(null,inst_30811,new cljs.core.Keyword("cljs.core.async","nothing","cljs.core.async/nothing",-69252123));
var inst_30821 = (inst_30819) || (inst_30820);
var state_30850__$1 = (function (){var statearr_30864 = state_30850;
(statearr_30864[(10)] = inst_30818__$1);

return statearr_30864;
})();
if(cljs.core.truth_(inst_30821)){
var statearr_30865_30894 = state_30850__$1;
(statearr_30865_30894[(1)] = (8));

} else {
var statearr_30866_30895 = state_30850__$1;
(statearr_30866_30895[(1)] = (9));

}

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30851 === (14))){
var inst_30843 = (state_30850[(2)]);
var inst_30844 = cljs.core.async.close_BANG_.call(null,out);
var state_30850__$1 = (function (){var statearr_30868 = state_30850;
(statearr_30868[(13)] = inst_30843);

return statearr_30868;
})();
var statearr_30869_30896 = state_30850__$1;
(statearr_30869_30896[(2)] = inst_30844);

(statearr_30869_30896[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30851 === (10))){
var inst_30833 = (state_30850[(2)]);
var state_30850__$1 = state_30850;
var statearr_30870_30897 = state_30850__$1;
(statearr_30870_30897[(2)] = inst_30833);

(statearr_30870_30897[(1)] = (7));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
if((state_val_30851 === (8))){
var inst_30814 = (state_30850[(9)]);
var inst_30810 = (state_30850[(7)]);
var inst_30818 = (state_30850[(10)]);
var inst_30823 = inst_30810.push(inst_30814);
var tmp30867 = inst_30810;
var inst_30810__$1 = tmp30867;
var inst_30811 = inst_30818;
var state_30850__$1 = (function (){var statearr_30871 = state_30850;
(statearr_30871[(14)] = inst_30823);

(statearr_30871[(7)] = inst_30810__$1);

(statearr_30871[(8)] = inst_30811);

return statearr_30871;
})();
var statearr_30872_30898 = state_30850__$1;
(statearr_30872_30898[(2)] = null);

(statearr_30872_30898[(1)] = (2));


return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
return null;
}
}
}
}
}
}
}
}
}
}
}
}
}
}
}
});})(c__28077__auto___30884,out))
;
return ((function (switch__27965__auto__,c__28077__auto___30884,out){
return (function() {
var cljs$core$async$state_machine__27966__auto__ = null;
var cljs$core$async$state_machine__27966__auto____0 = (function (){
var statearr_30876 = [null,null,null,null,null,null,null,null,null,null,null,null,null,null,null];
(statearr_30876[(0)] = cljs$core$async$state_machine__27966__auto__);

(statearr_30876[(1)] = (1));

return statearr_30876;
});
var cljs$core$async$state_machine__27966__auto____1 = (function (state_30850){
while(true){
var ret_value__27967__auto__ = (function (){try{while(true){
var result__27968__auto__ = switch__27965__auto__.call(null,state_30850);
if(cljs.core.keyword_identical_QMARK_.call(null,result__27968__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
continue;
} else {
return result__27968__auto__;
}
break;
}
}catch (e30877){if((e30877 instanceof Object)){
var ex__27969__auto__ = e30877;
var statearr_30878_30899 = state_30850;
(statearr_30878_30899[(5)] = ex__27969__auto__);


cljs.core.async.impl.ioc_helpers.process_exception.call(null,state_30850);

return new cljs.core.Keyword(null,"recur","recur",-437573268);
} else {
throw e30877;

}
}})();
if(cljs.core.keyword_identical_QMARK_.call(null,ret_value__27967__auto__,new cljs.core.Keyword(null,"recur","recur",-437573268))){
var G__30900 = state_30850;
state_30850 = G__30900;
continue;
} else {
return ret_value__27967__auto__;
}
break;
}
});
cljs$core$async$state_machine__27966__auto__ = function(state_30850){
switch(arguments.length){
case 0:
return cljs$core$async$state_machine__27966__auto____0.call(this);
case 1:
return cljs$core$async$state_machine__27966__auto____1.call(this,state_30850);
}
throw(new Error('Invalid arity: ' + arguments.length));
};
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$0 = cljs$core$async$state_machine__27966__auto____0;
cljs$core$async$state_machine__27966__auto__.cljs$core$IFn$_invoke$arity$1 = cljs$core$async$state_machine__27966__auto____1;
return cljs$core$async$state_machine__27966__auto__;
})()
;})(switch__27965__auto__,c__28077__auto___30884,out))
})();
var state__28079__auto__ = (function (){var statearr_30879 = f__28078__auto__.call(null);
(statearr_30879[cljs.core.async.impl.ioc_helpers.USER_START_IDX] = c__28077__auto___30884);

return statearr_30879;
})();
return cljs.core.async.impl.ioc_helpers.run_state_machine_wrapped.call(null,state__28079__auto__);
});})(c__28077__auto___30884,out))
);


return out;
});

cljs.core.async.partition_by.cljs$lang$maxFixedArity = 3;


//# sourceMappingURL=async.js.map?rel=1496955718919