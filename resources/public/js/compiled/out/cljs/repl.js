// Compiled by ClojureScript 1.9.229 {}
goog.provide('cljs.repl');
goog.require('cljs.core');
goog.require('cljs.spec');
cljs.repl.print_doc = (function cljs$repl$print_doc(p__35147){
var map__35172 = p__35147;
var map__35172__$1 = ((((!((map__35172 == null)))?((((map__35172.cljs$lang$protocol_mask$partition0$ & (64))) || (map__35172.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__35172):map__35172);
var m = map__35172__$1;
var n = cljs.core.get.call(null,map__35172__$1,new cljs.core.Keyword(null,"ns","ns",441598760));
var nm = cljs.core.get.call(null,map__35172__$1,new cljs.core.Keyword(null,"name","name",1843675177));
cljs.core.println.call(null,"-------------------------");

cljs.core.println.call(null,[cljs.core.str((function (){var temp__4657__auto__ = new cljs.core.Keyword(null,"ns","ns",441598760).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(temp__4657__auto__)){
var ns = temp__4657__auto__;
return [cljs.core.str(ns),cljs.core.str("/")].join('');
} else {
return null;
}
})()),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Protocol");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m))){
var seq__35174_35196 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"forms","forms",2045992350).cljs$core$IFn$_invoke$arity$1(m));
var chunk__35175_35197 = null;
var count__35176_35198 = (0);
var i__35177_35199 = (0);
while(true){
if((i__35177_35199 < count__35176_35198)){
var f_35200 = cljs.core._nth.call(null,chunk__35175_35197,i__35177_35199);
cljs.core.println.call(null,"  ",f_35200);

var G__35201 = seq__35174_35196;
var G__35202 = chunk__35175_35197;
var G__35203 = count__35176_35198;
var G__35204 = (i__35177_35199 + (1));
seq__35174_35196 = G__35201;
chunk__35175_35197 = G__35202;
count__35176_35198 = G__35203;
i__35177_35199 = G__35204;
continue;
} else {
var temp__4657__auto___35205 = cljs.core.seq.call(null,seq__35174_35196);
if(temp__4657__auto___35205){
var seq__35174_35206__$1 = temp__4657__auto___35205;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__35174_35206__$1)){
var c__26710__auto___35207 = cljs.core.chunk_first.call(null,seq__35174_35206__$1);
var G__35208 = cljs.core.chunk_rest.call(null,seq__35174_35206__$1);
var G__35209 = c__26710__auto___35207;
var G__35210 = cljs.core.count.call(null,c__26710__auto___35207);
var G__35211 = (0);
seq__35174_35196 = G__35208;
chunk__35175_35197 = G__35209;
count__35176_35198 = G__35210;
i__35177_35199 = G__35211;
continue;
} else {
var f_35212 = cljs.core.first.call(null,seq__35174_35206__$1);
cljs.core.println.call(null,"  ",f_35212);

var G__35213 = cljs.core.next.call(null,seq__35174_35206__$1);
var G__35214 = null;
var G__35215 = (0);
var G__35216 = (0);
seq__35174_35196 = G__35213;
chunk__35175_35197 = G__35214;
count__35176_35198 = G__35215;
i__35177_35199 = G__35216;
continue;
}
} else {
}
}
break;
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m))){
var arglists_35217 = new cljs.core.Keyword(null,"arglists","arglists",1661989754).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_((function (){var or__25899__auto__ = new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m);
if(cljs.core.truth_(or__25899__auto__)){
return or__25899__auto__;
} else {
return new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m);
}
})())){
cljs.core.prn.call(null,arglists_35217);
} else {
cljs.core.prn.call(null,((cljs.core._EQ_.call(null,new cljs.core.Symbol(null,"quote","quote",1377916282,null),cljs.core.first.call(null,arglists_35217)))?cljs.core.second.call(null,arglists_35217):arglists_35217));
}
} else {
}
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"special-form","special-form",-1326536374).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Special Form");

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.contains_QMARK_.call(null,m,new cljs.core.Keyword(null,"url","url",276297046))){
if(cljs.core.truth_(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))){
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/"),cljs.core.str(new cljs.core.Keyword(null,"url","url",276297046).cljs$core$IFn$_invoke$arity$1(m))].join(''));
} else {
return null;
}
} else {
return cljs.core.println.call(null,[cljs.core.str("\n  Please see http://clojure.org/special_forms#"),cljs.core.str(new cljs.core.Keyword(null,"name","name",1843675177).cljs$core$IFn$_invoke$arity$1(m))].join(''));
}
} else {
if(cljs.core.truth_(new cljs.core.Keyword(null,"macro","macro",-867863404).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"Macro");
} else {
}

if(cljs.core.truth_(new cljs.core.Keyword(null,"repl-special-function","repl-special-function",1262603725).cljs$core$IFn$_invoke$arity$1(m))){
cljs.core.println.call(null,"REPL Special Function");
} else {
}

cljs.core.println.call(null," ",new cljs.core.Keyword(null,"doc","doc",1913296891).cljs$core$IFn$_invoke$arity$1(m));

if(cljs.core.truth_(new cljs.core.Keyword(null,"protocol","protocol",652470118).cljs$core$IFn$_invoke$arity$1(m))){
var seq__35178_35218 = cljs.core.seq.call(null,new cljs.core.Keyword(null,"methods","methods",453930866).cljs$core$IFn$_invoke$arity$1(m));
var chunk__35179_35219 = null;
var count__35180_35220 = (0);
var i__35181_35221 = (0);
while(true){
if((i__35181_35221 < count__35180_35220)){
var vec__35182_35222 = cljs.core._nth.call(null,chunk__35179_35219,i__35181_35221);
var name_35223 = cljs.core.nth.call(null,vec__35182_35222,(0),null);
var map__35185_35224 = cljs.core.nth.call(null,vec__35182_35222,(1),null);
var map__35185_35225__$1 = ((((!((map__35185_35224 == null)))?((((map__35185_35224.cljs$lang$protocol_mask$partition0$ & (64))) || (map__35185_35224.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__35185_35224):map__35185_35224);
var doc_35226 = cljs.core.get.call(null,map__35185_35225__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_35227 = cljs.core.get.call(null,map__35185_35225__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_35223);

cljs.core.println.call(null," ",arglists_35227);

if(cljs.core.truth_(doc_35226)){
cljs.core.println.call(null," ",doc_35226);
} else {
}

var G__35228 = seq__35178_35218;
var G__35229 = chunk__35179_35219;
var G__35230 = count__35180_35220;
var G__35231 = (i__35181_35221 + (1));
seq__35178_35218 = G__35228;
chunk__35179_35219 = G__35229;
count__35180_35220 = G__35230;
i__35181_35221 = G__35231;
continue;
} else {
var temp__4657__auto___35232 = cljs.core.seq.call(null,seq__35178_35218);
if(temp__4657__auto___35232){
var seq__35178_35233__$1 = temp__4657__auto___35232;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__35178_35233__$1)){
var c__26710__auto___35234 = cljs.core.chunk_first.call(null,seq__35178_35233__$1);
var G__35235 = cljs.core.chunk_rest.call(null,seq__35178_35233__$1);
var G__35236 = c__26710__auto___35234;
var G__35237 = cljs.core.count.call(null,c__26710__auto___35234);
var G__35238 = (0);
seq__35178_35218 = G__35235;
chunk__35179_35219 = G__35236;
count__35180_35220 = G__35237;
i__35181_35221 = G__35238;
continue;
} else {
var vec__35187_35239 = cljs.core.first.call(null,seq__35178_35233__$1);
var name_35240 = cljs.core.nth.call(null,vec__35187_35239,(0),null);
var map__35190_35241 = cljs.core.nth.call(null,vec__35187_35239,(1),null);
var map__35190_35242__$1 = ((((!((map__35190_35241 == null)))?((((map__35190_35241.cljs$lang$protocol_mask$partition0$ & (64))) || (map__35190_35241.cljs$core$ISeq$))?true:false):false))?cljs.core.apply.call(null,cljs.core.hash_map,map__35190_35241):map__35190_35241);
var doc_35243 = cljs.core.get.call(null,map__35190_35242__$1,new cljs.core.Keyword(null,"doc","doc",1913296891));
var arglists_35244 = cljs.core.get.call(null,map__35190_35242__$1,new cljs.core.Keyword(null,"arglists","arglists",1661989754));
cljs.core.println.call(null);

cljs.core.println.call(null," ",name_35240);

cljs.core.println.call(null," ",arglists_35244);

if(cljs.core.truth_(doc_35243)){
cljs.core.println.call(null," ",doc_35243);
} else {
}

var G__35245 = cljs.core.next.call(null,seq__35178_35233__$1);
var G__35246 = null;
var G__35247 = (0);
var G__35248 = (0);
seq__35178_35218 = G__35245;
chunk__35179_35219 = G__35246;
count__35180_35220 = G__35247;
i__35181_35221 = G__35248;
continue;
}
} else {
}
}
break;
}
} else {
}

if(cljs.core.truth_(n)){
var temp__4657__auto__ = cljs.spec.get_spec.call(null,cljs.core.symbol.call(null,[cljs.core.str(cljs.core.ns_name.call(null,n))].join(''),cljs.core.name.call(null,nm)));
if(cljs.core.truth_(temp__4657__auto__)){
var fnspec = temp__4657__auto__;
cljs.core.print.call(null,"Spec");

var seq__35192 = cljs.core.seq.call(null,new cljs.core.PersistentVector(null, 3, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Keyword(null,"args","args",1315556576),new cljs.core.Keyword(null,"ret","ret",-468222814),new cljs.core.Keyword(null,"fn","fn",-1175266204)], null));
var chunk__35193 = null;
var count__35194 = (0);
var i__35195 = (0);
while(true){
if((i__35195 < count__35194)){
var role = cljs.core._nth.call(null,chunk__35193,i__35195);
var temp__4657__auto___35249__$1 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__4657__auto___35249__$1)){
var spec_35250 = temp__4657__auto___35249__$1;
cljs.core.print.call(null,[cljs.core.str("\n "),cljs.core.str(cljs.core.name.call(null,role)),cljs.core.str(":")].join(''),cljs.spec.describe.call(null,spec_35250));
} else {
}

var G__35251 = seq__35192;
var G__35252 = chunk__35193;
var G__35253 = count__35194;
var G__35254 = (i__35195 + (1));
seq__35192 = G__35251;
chunk__35193 = G__35252;
count__35194 = G__35253;
i__35195 = G__35254;
continue;
} else {
var temp__4657__auto____$1 = cljs.core.seq.call(null,seq__35192);
if(temp__4657__auto____$1){
var seq__35192__$1 = temp__4657__auto____$1;
if(cljs.core.chunked_seq_QMARK_.call(null,seq__35192__$1)){
var c__26710__auto__ = cljs.core.chunk_first.call(null,seq__35192__$1);
var G__35255 = cljs.core.chunk_rest.call(null,seq__35192__$1);
var G__35256 = c__26710__auto__;
var G__35257 = cljs.core.count.call(null,c__26710__auto__);
var G__35258 = (0);
seq__35192 = G__35255;
chunk__35193 = G__35256;
count__35194 = G__35257;
i__35195 = G__35258;
continue;
} else {
var role = cljs.core.first.call(null,seq__35192__$1);
var temp__4657__auto___35259__$2 = cljs.core.get.call(null,fnspec,role);
if(cljs.core.truth_(temp__4657__auto___35259__$2)){
var spec_35260 = temp__4657__auto___35259__$2;
cljs.core.print.call(null,[cljs.core.str("\n "),cljs.core.str(cljs.core.name.call(null,role)),cljs.core.str(":")].join(''),cljs.spec.describe.call(null,spec_35260));
} else {
}

var G__35261 = cljs.core.next.call(null,seq__35192__$1);
var G__35262 = null;
var G__35263 = (0);
var G__35264 = (0);
seq__35192 = G__35261;
chunk__35193 = G__35262;
count__35194 = G__35263;
i__35195 = G__35264;
continue;
}
} else {
return null;
}
}
break;
}
} else {
return null;
}
} else {
return null;
}
}
});

//# sourceMappingURL=repl.js.map?rel=1496955726572