// Compiled by ClojureScript 1.9.229 {}
goog.provide('cljs.spec.impl.gen');
goog.require('cljs.core');
goog.require('cljs.core');

/**
* @constructor
 * @implements {cljs.core.IDeref}
*/
cljs.spec.impl.gen.LazyVar = (function (f,cached){
this.f = f;
this.cached = cached;
this.cljs$lang$protocol_mask$partition0$ = 32768;
this.cljs$lang$protocol_mask$partition1$ = 0;
})
cljs.spec.impl.gen.LazyVar.prototype.cljs$core$IDeref$_deref$arity$1 = (function (this$){
var self__ = this;
var this$__$1 = this;
if(!((self__.cached == null))){
return self__.cached;
} else {
var x = self__.f.call(null);
if((x == null)){
} else {
self__.cached = x;
}

return x;
}
});

cljs.spec.impl.gen.LazyVar.getBasis = (function (){
return new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [new cljs.core.Symbol(null,"f","f",43394975,null),cljs.core.with_meta(new cljs.core.Symbol(null,"cached","cached",-1216707864,null),new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"mutable","mutable",875778266),true], null))], null);
});

cljs.spec.impl.gen.LazyVar.cljs$lang$type = true;

cljs.spec.impl.gen.LazyVar.cljs$lang$ctorStr = "cljs.spec.impl.gen/LazyVar";

cljs.spec.impl.gen.LazyVar.cljs$lang$ctorPrWriter = (function (this__26505__auto__,writer__26506__auto__,opt__26507__auto__){
return cljs.core._write.call(null,writer__26506__auto__,"cljs.spec.impl.gen/LazyVar");
});

cljs.spec.impl.gen.__GT_LazyVar = (function cljs$spec$impl$gen$__GT_LazyVar(f,cached){
return (new cljs.spec.impl.gen.LazyVar(f,cached));
});

cljs.spec.impl.gen.quick_check_ref = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check.quick_check !== 'undefined')){
return clojure.test.check.quick_check;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check","quick-check","clojure.test.check/quick-check",-810344251,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check","quick-check","clojure.test.check/quick-check",-810344251,null))),cljs.core.str(" never required")].join('')));
}
}),null));
cljs.spec.impl.gen.quick_check = (function cljs$spec$impl$gen$quick_check(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33882 = arguments.length;
var i__26975__auto___33883 = (0);
while(true){
if((i__26975__auto___33883 < len__26974__auto___33882)){
args__26981__auto__.push((arguments[i__26975__auto___33883]));

var G__33884 = (i__26975__auto___33883 + (1));
i__26975__auto___33883 = G__33884;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.quick_check.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

cljs.spec.impl.gen.quick_check.cljs$core$IFn$_invoke$arity$variadic = (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,cljs.spec.impl.gen.quick_check_ref),args);
});

cljs.spec.impl.gen.quick_check.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.quick_check.cljs$lang$applyTo = (function (seq33881){
return cljs.spec.impl.gen.quick_check.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33881));
});

cljs.spec.impl.gen.for_all_STAR__ref = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.properties.for_all_STAR_ !== 'undefined')){
return clojure.test.check.properties.for_all_STAR_;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.properties","for-all*","clojure.test.check.properties/for-all*",67088845,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.properties","for-all*","clojure.test.check.properties/for-all*",67088845,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Dynamically loaded clojure.test.check.properties/for-all*.
 */
cljs.spec.impl.gen.for_all_STAR_ = (function cljs$spec$impl$gen$for_all_STAR_(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33886 = arguments.length;
var i__26975__auto___33887 = (0);
while(true){
if((i__26975__auto___33887 < len__26974__auto___33886)){
args__26981__auto__.push((arguments[i__26975__auto___33887]));

var G__33888 = (i__26975__auto___33887 + (1));
i__26975__auto___33887 = G__33888;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.for_all_STAR_.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

cljs.spec.impl.gen.for_all_STAR_.cljs$core$IFn$_invoke$arity$variadic = (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,cljs.spec.impl.gen.for_all_STAR__ref),args);
});

cljs.spec.impl.gen.for_all_STAR_.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.for_all_STAR_.cljs$lang$applyTo = (function (seq33885){
return cljs.spec.impl.gen.for_all_STAR_.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33885));
});

var g_QMARK__33889 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.generator_QMARK_ !== 'undefined')){
return clojure.test.check.generators.generator_QMARK_;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","generator?","clojure.test.check.generators/generator?",-1378210460,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","generator?","clojure.test.check.generators/generator?",-1378210460,null))),cljs.core.str(" never required")].join('')));
}
}),null));
var g_33890 = (new cljs.spec.impl.gen.LazyVar(((function (g_QMARK__33889){
return (function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.generate !== 'undefined')){
return clojure.test.check.generators.generate;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","generate","clojure.test.check.generators/generate",-690390711,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","generate","clojure.test.check.generators/generate",-690390711,null))),cljs.core.str(" never required")].join('')));
}
});})(g_QMARK__33889))
,null));
var mkg_33891 = (new cljs.spec.impl.gen.LazyVar(((function (g_QMARK__33889,g_33890){
return (function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.__GT_Generator !== 'undefined')){
return clojure.test.check.generators.__GT_Generator;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","->Generator","clojure.test.check.generators/->Generator",-1179475051,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","->Generator","clojure.test.check.generators/->Generator",-1179475051,null))),cljs.core.str(" never required")].join('')));
}
});})(g_QMARK__33889,g_33890))
,null));
cljs.spec.impl.gen.generator_QMARK_ = ((function (g_QMARK__33889,g_33890,mkg_33891){
return (function cljs$spec$impl$gen$generator_QMARK_(x){
return cljs.core.deref.call(null,g_QMARK__33889).call(null,x);
});})(g_QMARK__33889,g_33890,mkg_33891))
;

cljs.spec.impl.gen.generator = ((function (g_QMARK__33889,g_33890,mkg_33891){
return (function cljs$spec$impl$gen$generator(gfn){
return cljs.core.deref.call(null,mkg_33891).call(null,gfn);
});})(g_QMARK__33889,g_33890,mkg_33891))
;

/**
 * Generate a single value using generator.
 */
cljs.spec.impl.gen.generate = ((function (g_QMARK__33889,g_33890,mkg_33891){
return (function cljs$spec$impl$gen$generate(generator){
return cljs.core.deref.call(null,g_33890).call(null,generator);
});})(g_QMARK__33889,g_33890,mkg_33891))
;
cljs.spec.impl.gen.delay_impl = (function cljs$spec$impl$gen$delay_impl(gfnd){
return cljs.spec.impl.gen.generator.call(null,(function (rnd,size){
return new cljs.core.Keyword(null,"gen","gen",142575302).cljs$core$IFn$_invoke$arity$1(cljs.core.deref.call(null,gfnd)).call(null,rnd,size);
}));
});
var g__33853__auto___33910 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.hash_map !== 'undefined')){
return clojure.test.check.generators.hash_map;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","hash-map","clojure.test.check.generators/hash-map",1961346626,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","hash-map","clojure.test.check.generators/hash-map",1961346626,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/hash-map
 */
cljs.spec.impl.gen.hash_map = ((function (g__33853__auto___33910){
return (function cljs$spec$impl$gen$hash_map(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33911 = arguments.length;
var i__26975__auto___33912 = (0);
while(true){
if((i__26975__auto___33912 < len__26974__auto___33911)){
args__26981__auto__.push((arguments[i__26975__auto___33912]));

var G__33913 = (i__26975__auto___33912 + (1));
i__26975__auto___33912 = G__33913;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.hash_map.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33910))
;

cljs.spec.impl.gen.hash_map.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33910){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33910),args);
});})(g__33853__auto___33910))
;

cljs.spec.impl.gen.hash_map.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.hash_map.cljs$lang$applyTo = ((function (g__33853__auto___33910){
return (function (seq33892){
return cljs.spec.impl.gen.hash_map.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33892));
});})(g__33853__auto___33910))
;


var g__33853__auto___33914 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.list !== 'undefined')){
return clojure.test.check.generators.list;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","list","clojure.test.check.generators/list",506971058,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","list","clojure.test.check.generators/list",506971058,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/list
 */
cljs.spec.impl.gen.list = ((function (g__33853__auto___33914){
return (function cljs$spec$impl$gen$list(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33915 = arguments.length;
var i__26975__auto___33916 = (0);
while(true){
if((i__26975__auto___33916 < len__26974__auto___33915)){
args__26981__auto__.push((arguments[i__26975__auto___33916]));

var G__33917 = (i__26975__auto___33916 + (1));
i__26975__auto___33916 = G__33917;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.list.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33914))
;

cljs.spec.impl.gen.list.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33914){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33914),args);
});})(g__33853__auto___33914))
;

cljs.spec.impl.gen.list.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.list.cljs$lang$applyTo = ((function (g__33853__auto___33914){
return (function (seq33893){
return cljs.spec.impl.gen.list.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33893));
});})(g__33853__auto___33914))
;


var g__33853__auto___33918 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.map !== 'undefined')){
return clojure.test.check.generators.map;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","map","clojure.test.check.generators/map",45738796,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","map","clojure.test.check.generators/map",45738796,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/map
 */
cljs.spec.impl.gen.map = ((function (g__33853__auto___33918){
return (function cljs$spec$impl$gen$map(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33919 = arguments.length;
var i__26975__auto___33920 = (0);
while(true){
if((i__26975__auto___33920 < len__26974__auto___33919)){
args__26981__auto__.push((arguments[i__26975__auto___33920]));

var G__33921 = (i__26975__auto___33920 + (1));
i__26975__auto___33920 = G__33921;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.map.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33918))
;

cljs.spec.impl.gen.map.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33918){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33918),args);
});})(g__33853__auto___33918))
;

cljs.spec.impl.gen.map.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.map.cljs$lang$applyTo = ((function (g__33853__auto___33918){
return (function (seq33894){
return cljs.spec.impl.gen.map.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33894));
});})(g__33853__auto___33918))
;


var g__33853__auto___33922 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.not_empty !== 'undefined')){
return clojure.test.check.generators.not_empty;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","not-empty","clojure.test.check.generators/not-empty",-876211682,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","not-empty","clojure.test.check.generators/not-empty",-876211682,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/not-empty
 */
cljs.spec.impl.gen.not_empty = ((function (g__33853__auto___33922){
return (function cljs$spec$impl$gen$not_empty(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33923 = arguments.length;
var i__26975__auto___33924 = (0);
while(true){
if((i__26975__auto___33924 < len__26974__auto___33923)){
args__26981__auto__.push((arguments[i__26975__auto___33924]));

var G__33925 = (i__26975__auto___33924 + (1));
i__26975__auto___33924 = G__33925;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.not_empty.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33922))
;

cljs.spec.impl.gen.not_empty.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33922){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33922),args);
});})(g__33853__auto___33922))
;

cljs.spec.impl.gen.not_empty.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.not_empty.cljs$lang$applyTo = ((function (g__33853__auto___33922){
return (function (seq33895){
return cljs.spec.impl.gen.not_empty.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33895));
});})(g__33853__auto___33922))
;


var g__33853__auto___33926 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.set !== 'undefined')){
return clojure.test.check.generators.set;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","set","clojure.test.check.generators/set",-1027639543,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","set","clojure.test.check.generators/set",-1027639543,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/set
 */
cljs.spec.impl.gen.set = ((function (g__33853__auto___33926){
return (function cljs$spec$impl$gen$set(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33927 = arguments.length;
var i__26975__auto___33928 = (0);
while(true){
if((i__26975__auto___33928 < len__26974__auto___33927)){
args__26981__auto__.push((arguments[i__26975__auto___33928]));

var G__33929 = (i__26975__auto___33928 + (1));
i__26975__auto___33928 = G__33929;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.set.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33926))
;

cljs.spec.impl.gen.set.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33926){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33926),args);
});})(g__33853__auto___33926))
;

cljs.spec.impl.gen.set.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.set.cljs$lang$applyTo = ((function (g__33853__auto___33926){
return (function (seq33896){
return cljs.spec.impl.gen.set.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33896));
});})(g__33853__auto___33926))
;


var g__33853__auto___33930 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.vector !== 'undefined')){
return clojure.test.check.generators.vector;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","vector","clojure.test.check.generators/vector",1081775325,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","vector","clojure.test.check.generators/vector",1081775325,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/vector
 */
cljs.spec.impl.gen.vector = ((function (g__33853__auto___33930){
return (function cljs$spec$impl$gen$vector(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33931 = arguments.length;
var i__26975__auto___33932 = (0);
while(true){
if((i__26975__auto___33932 < len__26974__auto___33931)){
args__26981__auto__.push((arguments[i__26975__auto___33932]));

var G__33933 = (i__26975__auto___33932 + (1));
i__26975__auto___33932 = G__33933;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.vector.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33930))
;

cljs.spec.impl.gen.vector.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33930){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33930),args);
});})(g__33853__auto___33930))
;

cljs.spec.impl.gen.vector.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.vector.cljs$lang$applyTo = ((function (g__33853__auto___33930){
return (function (seq33897){
return cljs.spec.impl.gen.vector.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33897));
});})(g__33853__auto___33930))
;


var g__33853__auto___33934 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.vector_distinct !== 'undefined')){
return clojure.test.check.generators.vector_distinct;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","vector-distinct","clojure.test.check.generators/vector-distinct",1656877834,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","vector-distinct","clojure.test.check.generators/vector-distinct",1656877834,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/vector-distinct
 */
cljs.spec.impl.gen.vector_distinct = ((function (g__33853__auto___33934){
return (function cljs$spec$impl$gen$vector_distinct(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33935 = arguments.length;
var i__26975__auto___33936 = (0);
while(true){
if((i__26975__auto___33936 < len__26974__auto___33935)){
args__26981__auto__.push((arguments[i__26975__auto___33936]));

var G__33937 = (i__26975__auto___33936 + (1));
i__26975__auto___33936 = G__33937;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.vector_distinct.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33934))
;

cljs.spec.impl.gen.vector_distinct.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33934){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33934),args);
});})(g__33853__auto___33934))
;

cljs.spec.impl.gen.vector_distinct.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.vector_distinct.cljs$lang$applyTo = ((function (g__33853__auto___33934){
return (function (seq33898){
return cljs.spec.impl.gen.vector_distinct.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33898));
});})(g__33853__auto___33934))
;


var g__33853__auto___33938 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.fmap !== 'undefined')){
return clojure.test.check.generators.fmap;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","fmap","clojure.test.check.generators/fmap",1957997092,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","fmap","clojure.test.check.generators/fmap",1957997092,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/fmap
 */
cljs.spec.impl.gen.fmap = ((function (g__33853__auto___33938){
return (function cljs$spec$impl$gen$fmap(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33939 = arguments.length;
var i__26975__auto___33940 = (0);
while(true){
if((i__26975__auto___33940 < len__26974__auto___33939)){
args__26981__auto__.push((arguments[i__26975__auto___33940]));

var G__33941 = (i__26975__auto___33940 + (1));
i__26975__auto___33940 = G__33941;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.fmap.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33938))
;

cljs.spec.impl.gen.fmap.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33938){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33938),args);
});})(g__33853__auto___33938))
;

cljs.spec.impl.gen.fmap.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.fmap.cljs$lang$applyTo = ((function (g__33853__auto___33938){
return (function (seq33899){
return cljs.spec.impl.gen.fmap.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33899));
});})(g__33853__auto___33938))
;


var g__33853__auto___33942 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.elements !== 'undefined')){
return clojure.test.check.generators.elements;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","elements","clojure.test.check.generators/elements",438991326,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","elements","clojure.test.check.generators/elements",438991326,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/elements
 */
cljs.spec.impl.gen.elements = ((function (g__33853__auto___33942){
return (function cljs$spec$impl$gen$elements(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33943 = arguments.length;
var i__26975__auto___33944 = (0);
while(true){
if((i__26975__auto___33944 < len__26974__auto___33943)){
args__26981__auto__.push((arguments[i__26975__auto___33944]));

var G__33945 = (i__26975__auto___33944 + (1));
i__26975__auto___33944 = G__33945;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.elements.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33942))
;

cljs.spec.impl.gen.elements.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33942){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33942),args);
});})(g__33853__auto___33942))
;

cljs.spec.impl.gen.elements.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.elements.cljs$lang$applyTo = ((function (g__33853__auto___33942){
return (function (seq33900){
return cljs.spec.impl.gen.elements.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33900));
});})(g__33853__auto___33942))
;


var g__33853__auto___33946 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.bind !== 'undefined')){
return clojure.test.check.generators.bind;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","bind","clojure.test.check.generators/bind",-361313906,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","bind","clojure.test.check.generators/bind",-361313906,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/bind
 */
cljs.spec.impl.gen.bind = ((function (g__33853__auto___33946){
return (function cljs$spec$impl$gen$bind(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33947 = arguments.length;
var i__26975__auto___33948 = (0);
while(true){
if((i__26975__auto___33948 < len__26974__auto___33947)){
args__26981__auto__.push((arguments[i__26975__auto___33948]));

var G__33949 = (i__26975__auto___33948 + (1));
i__26975__auto___33948 = G__33949;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.bind.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33946))
;

cljs.spec.impl.gen.bind.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33946){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33946),args);
});})(g__33853__auto___33946))
;

cljs.spec.impl.gen.bind.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.bind.cljs$lang$applyTo = ((function (g__33853__auto___33946){
return (function (seq33901){
return cljs.spec.impl.gen.bind.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33901));
});})(g__33853__auto___33946))
;


var g__33853__auto___33950 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.choose !== 'undefined')){
return clojure.test.check.generators.choose;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","choose","clojure.test.check.generators/choose",909997832,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","choose","clojure.test.check.generators/choose",909997832,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/choose
 */
cljs.spec.impl.gen.choose = ((function (g__33853__auto___33950){
return (function cljs$spec$impl$gen$choose(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33951 = arguments.length;
var i__26975__auto___33952 = (0);
while(true){
if((i__26975__auto___33952 < len__26974__auto___33951)){
args__26981__auto__.push((arguments[i__26975__auto___33952]));

var G__33953 = (i__26975__auto___33952 + (1));
i__26975__auto___33952 = G__33953;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.choose.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33950))
;

cljs.spec.impl.gen.choose.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33950){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33950),args);
});})(g__33853__auto___33950))
;

cljs.spec.impl.gen.choose.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.choose.cljs$lang$applyTo = ((function (g__33853__auto___33950){
return (function (seq33902){
return cljs.spec.impl.gen.choose.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33902));
});})(g__33853__auto___33950))
;


var g__33853__auto___33954 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.one_of !== 'undefined')){
return clojure.test.check.generators.one_of;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","one-of","clojure.test.check.generators/one-of",-183339191,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","one-of","clojure.test.check.generators/one-of",-183339191,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/one-of
 */
cljs.spec.impl.gen.one_of = ((function (g__33853__auto___33954){
return (function cljs$spec$impl$gen$one_of(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33955 = arguments.length;
var i__26975__auto___33956 = (0);
while(true){
if((i__26975__auto___33956 < len__26974__auto___33955)){
args__26981__auto__.push((arguments[i__26975__auto___33956]));

var G__33957 = (i__26975__auto___33956 + (1));
i__26975__auto___33956 = G__33957;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.one_of.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33954))
;

cljs.spec.impl.gen.one_of.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33954){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33954),args);
});})(g__33853__auto___33954))
;

cljs.spec.impl.gen.one_of.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.one_of.cljs$lang$applyTo = ((function (g__33853__auto___33954){
return (function (seq33903){
return cljs.spec.impl.gen.one_of.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33903));
});})(g__33853__auto___33954))
;


var g__33853__auto___33958 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.such_that !== 'undefined')){
return clojure.test.check.generators.such_that;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","such-that","clojure.test.check.generators/such-that",-1754178732,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","such-that","clojure.test.check.generators/such-that",-1754178732,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/such-that
 */
cljs.spec.impl.gen.such_that = ((function (g__33853__auto___33958){
return (function cljs$spec$impl$gen$such_that(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33959 = arguments.length;
var i__26975__auto___33960 = (0);
while(true){
if((i__26975__auto___33960 < len__26974__auto___33959)){
args__26981__auto__.push((arguments[i__26975__auto___33960]));

var G__33961 = (i__26975__auto___33960 + (1));
i__26975__auto___33960 = G__33961;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.such_that.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33958))
;

cljs.spec.impl.gen.such_that.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33958){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33958),args);
});})(g__33853__auto___33958))
;

cljs.spec.impl.gen.such_that.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.such_that.cljs$lang$applyTo = ((function (g__33853__auto___33958){
return (function (seq33904){
return cljs.spec.impl.gen.such_that.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33904));
});})(g__33853__auto___33958))
;


var g__33853__auto___33962 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.tuple !== 'undefined')){
return clojure.test.check.generators.tuple;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","tuple","clojure.test.check.generators/tuple",-143711557,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","tuple","clojure.test.check.generators/tuple",-143711557,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/tuple
 */
cljs.spec.impl.gen.tuple = ((function (g__33853__auto___33962){
return (function cljs$spec$impl$gen$tuple(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33963 = arguments.length;
var i__26975__auto___33964 = (0);
while(true){
if((i__26975__auto___33964 < len__26974__auto___33963)){
args__26981__auto__.push((arguments[i__26975__auto___33964]));

var G__33965 = (i__26975__auto___33964 + (1));
i__26975__auto___33964 = G__33965;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.tuple.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33962))
;

cljs.spec.impl.gen.tuple.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33962){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33962),args);
});})(g__33853__auto___33962))
;

cljs.spec.impl.gen.tuple.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.tuple.cljs$lang$applyTo = ((function (g__33853__auto___33962){
return (function (seq33905){
return cljs.spec.impl.gen.tuple.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33905));
});})(g__33853__auto___33962))
;


var g__33853__auto___33966 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.sample !== 'undefined')){
return clojure.test.check.generators.sample;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","sample","clojure.test.check.generators/sample",-382944992,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","sample","clojure.test.check.generators/sample",-382944992,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/sample
 */
cljs.spec.impl.gen.sample = ((function (g__33853__auto___33966){
return (function cljs$spec$impl$gen$sample(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33967 = arguments.length;
var i__26975__auto___33968 = (0);
while(true){
if((i__26975__auto___33968 < len__26974__auto___33967)){
args__26981__auto__.push((arguments[i__26975__auto___33968]));

var G__33969 = (i__26975__auto___33968 + (1));
i__26975__auto___33968 = G__33969;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.sample.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33966))
;

cljs.spec.impl.gen.sample.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33966){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33966),args);
});})(g__33853__auto___33966))
;

cljs.spec.impl.gen.sample.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.sample.cljs$lang$applyTo = ((function (g__33853__auto___33966){
return (function (seq33906){
return cljs.spec.impl.gen.sample.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33906));
});})(g__33853__auto___33966))
;


var g__33853__auto___33970 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.return$ !== 'undefined')){
return clojure.test.check.generators.return$;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","return","clojure.test.check.generators/return",1744522038,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","return","clojure.test.check.generators/return",1744522038,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/return
 */
cljs.spec.impl.gen.return$ = ((function (g__33853__auto___33970){
return (function cljs$spec$impl$gen$return(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33971 = arguments.length;
var i__26975__auto___33972 = (0);
while(true){
if((i__26975__auto___33972 < len__26974__auto___33971)){
args__26981__auto__.push((arguments[i__26975__auto___33972]));

var G__33973 = (i__26975__auto___33972 + (1));
i__26975__auto___33972 = G__33973;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.return$.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33970))
;

cljs.spec.impl.gen.return$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33970){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33970),args);
});})(g__33853__auto___33970))
;

cljs.spec.impl.gen.return$.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.return$.cljs$lang$applyTo = ((function (g__33853__auto___33970){
return (function (seq33907){
return cljs.spec.impl.gen.return$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33907));
});})(g__33853__auto___33970))
;


var g__33853__auto___33974 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.large_integer_STAR_ !== 'undefined')){
return clojure.test.check.generators.large_integer_STAR_;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","large-integer*","clojure.test.check.generators/large-integer*",-437830670,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","large-integer*","clojure.test.check.generators/large-integer*",-437830670,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/large-integer*
 */
cljs.spec.impl.gen.large_integer_STAR_ = ((function (g__33853__auto___33974){
return (function cljs$spec$impl$gen$large_integer_STAR_(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33975 = arguments.length;
var i__26975__auto___33976 = (0);
while(true){
if((i__26975__auto___33976 < len__26974__auto___33975)){
args__26981__auto__.push((arguments[i__26975__auto___33976]));

var G__33977 = (i__26975__auto___33976 + (1));
i__26975__auto___33976 = G__33977;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.large_integer_STAR_.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33974))
;

cljs.spec.impl.gen.large_integer_STAR_.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33974){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33974),args);
});})(g__33853__auto___33974))
;

cljs.spec.impl.gen.large_integer_STAR_.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.large_integer_STAR_.cljs$lang$applyTo = ((function (g__33853__auto___33974){
return (function (seq33908){
return cljs.spec.impl.gen.large_integer_STAR_.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33908));
});})(g__33853__auto___33974))
;


var g__33853__auto___33978 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.double_STAR_ !== 'undefined')){
return clojure.test.check.generators.double_STAR_;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","double*","clojure.test.check.generators/double*",841542265,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","double*","clojure.test.check.generators/double*",841542265,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Lazy loaded version of clojure.test.check.generators/double*
 */
cljs.spec.impl.gen.double_STAR_ = ((function (g__33853__auto___33978){
return (function cljs$spec$impl$gen$double_STAR_(var_args){
var args__26981__auto__ = [];
var len__26974__auto___33979 = arguments.length;
var i__26975__auto___33980 = (0);
while(true){
if((i__26975__auto___33980 < len__26974__auto___33979)){
args__26981__auto__.push((arguments[i__26975__auto___33980]));

var G__33981 = (i__26975__auto___33980 + (1));
i__26975__auto___33980 = G__33981;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.double_STAR_.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33853__auto___33978))
;

cljs.spec.impl.gen.double_STAR_.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33853__auto___33978){
return (function (args){
return cljs.core.apply.call(null,cljs.core.deref.call(null,g__33853__auto___33978),args);
});})(g__33853__auto___33978))
;

cljs.spec.impl.gen.double_STAR_.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.double_STAR_.cljs$lang$applyTo = ((function (g__33853__auto___33978){
return (function (seq33909){
return cljs.spec.impl.gen.double_STAR_.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33909));
});})(g__33853__auto___33978))
;

var g__33866__auto___34003 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.any !== 'undefined')){
return clojure.test.check.generators.any;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","any","clojure.test.check.generators/any",1883743710,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","any","clojure.test.check.generators/any",1883743710,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/any
 */
cljs.spec.impl.gen.any = ((function (g__33866__auto___34003){
return (function cljs$spec$impl$gen$any(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34004 = arguments.length;
var i__26975__auto___34005 = (0);
while(true){
if((i__26975__auto___34005 < len__26974__auto___34004)){
args__26981__auto__.push((arguments[i__26975__auto___34005]));

var G__34006 = (i__26975__auto___34005 + (1));
i__26975__auto___34005 = G__34006;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.any.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34003))
;

cljs.spec.impl.gen.any.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34003){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34003);
});})(g__33866__auto___34003))
;

cljs.spec.impl.gen.any.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.any.cljs$lang$applyTo = ((function (g__33866__auto___34003){
return (function (seq33982){
return cljs.spec.impl.gen.any.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33982));
});})(g__33866__auto___34003))
;


var g__33866__auto___34007 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.any_printable !== 'undefined')){
return clojure.test.check.generators.any_printable;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","any-printable","clojure.test.check.generators/any-printable",-1570493991,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","any-printable","clojure.test.check.generators/any-printable",-1570493991,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/any-printable
 */
cljs.spec.impl.gen.any_printable = ((function (g__33866__auto___34007){
return (function cljs$spec$impl$gen$any_printable(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34008 = arguments.length;
var i__26975__auto___34009 = (0);
while(true){
if((i__26975__auto___34009 < len__26974__auto___34008)){
args__26981__auto__.push((arguments[i__26975__auto___34009]));

var G__34010 = (i__26975__auto___34009 + (1));
i__26975__auto___34009 = G__34010;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.any_printable.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34007))
;

cljs.spec.impl.gen.any_printable.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34007){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34007);
});})(g__33866__auto___34007))
;

cljs.spec.impl.gen.any_printable.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.any_printable.cljs$lang$applyTo = ((function (g__33866__auto___34007){
return (function (seq33983){
return cljs.spec.impl.gen.any_printable.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33983));
});})(g__33866__auto___34007))
;


var g__33866__auto___34011 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.boolean$ !== 'undefined')){
return clojure.test.check.generators.boolean$;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","boolean","clojure.test.check.generators/boolean",1586992347,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","boolean","clojure.test.check.generators/boolean",1586992347,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/boolean
 */
cljs.spec.impl.gen.boolean$ = ((function (g__33866__auto___34011){
return (function cljs$spec$impl$gen$boolean(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34012 = arguments.length;
var i__26975__auto___34013 = (0);
while(true){
if((i__26975__auto___34013 < len__26974__auto___34012)){
args__26981__auto__.push((arguments[i__26975__auto___34013]));

var G__34014 = (i__26975__auto___34013 + (1));
i__26975__auto___34013 = G__34014;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.boolean$.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34011))
;

cljs.spec.impl.gen.boolean$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34011){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34011);
});})(g__33866__auto___34011))
;

cljs.spec.impl.gen.boolean$.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.boolean$.cljs$lang$applyTo = ((function (g__33866__auto___34011){
return (function (seq33984){
return cljs.spec.impl.gen.boolean$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33984));
});})(g__33866__auto___34011))
;


var g__33866__auto___34015 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char$ !== 'undefined')){
return clojure.test.check.generators.char$;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","char","clojure.test.check.generators/char",-1426343459,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char","clojure.test.check.generators/char",-1426343459,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char
 */
cljs.spec.impl.gen.char$ = ((function (g__33866__auto___34015){
return (function cljs$spec$impl$gen$char(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34016 = arguments.length;
var i__26975__auto___34017 = (0);
while(true){
if((i__26975__auto___34017 < len__26974__auto___34016)){
args__26981__auto__.push((arguments[i__26975__auto___34017]));

var G__34018 = (i__26975__auto___34017 + (1));
i__26975__auto___34017 = G__34018;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.char$.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34015))
;

cljs.spec.impl.gen.char$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34015){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34015);
});})(g__33866__auto___34015))
;

cljs.spec.impl.gen.char$.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.char$.cljs$lang$applyTo = ((function (g__33866__auto___34015){
return (function (seq33985){
return cljs.spec.impl.gen.char$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33985));
});})(g__33866__auto___34015))
;


var g__33866__auto___34019 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char_alpha !== 'undefined')){
return clojure.test.check.generators.char_alpha;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","char-alpha","clojure.test.check.generators/char-alpha",615785796,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char-alpha","clojure.test.check.generators/char-alpha",615785796,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char-alpha
 */
cljs.spec.impl.gen.char_alpha = ((function (g__33866__auto___34019){
return (function cljs$spec$impl$gen$char_alpha(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34020 = arguments.length;
var i__26975__auto___34021 = (0);
while(true){
if((i__26975__auto___34021 < len__26974__auto___34020)){
args__26981__auto__.push((arguments[i__26975__auto___34021]));

var G__34022 = (i__26975__auto___34021 + (1));
i__26975__auto___34021 = G__34022;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.char_alpha.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34019))
;

cljs.spec.impl.gen.char_alpha.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34019){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34019);
});})(g__33866__auto___34019))
;

cljs.spec.impl.gen.char_alpha.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.char_alpha.cljs$lang$applyTo = ((function (g__33866__auto___34019){
return (function (seq33986){
return cljs.spec.impl.gen.char_alpha.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33986));
});})(g__33866__auto___34019))
;


var g__33866__auto___34023 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char_alphanumeric !== 'undefined')){
return clojure.test.check.generators.char_alphanumeric;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","char-alphanumeric","clojure.test.check.generators/char-alphanumeric",1383091431,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char-alphanumeric","clojure.test.check.generators/char-alphanumeric",1383091431,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char-alphanumeric
 */
cljs.spec.impl.gen.char_alphanumeric = ((function (g__33866__auto___34023){
return (function cljs$spec$impl$gen$char_alphanumeric(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34024 = arguments.length;
var i__26975__auto___34025 = (0);
while(true){
if((i__26975__auto___34025 < len__26974__auto___34024)){
args__26981__auto__.push((arguments[i__26975__auto___34025]));

var G__34026 = (i__26975__auto___34025 + (1));
i__26975__auto___34025 = G__34026;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.char_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34023))
;

cljs.spec.impl.gen.char_alphanumeric.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34023){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34023);
});})(g__33866__auto___34023))
;

cljs.spec.impl.gen.char_alphanumeric.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.char_alphanumeric.cljs$lang$applyTo = ((function (g__33866__auto___34023){
return (function (seq33987){
return cljs.spec.impl.gen.char_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33987));
});})(g__33866__auto___34023))
;


var g__33866__auto___34027 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.char_ascii !== 'undefined')){
return clojure.test.check.generators.char_ascii;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","char-ascii","clojure.test.check.generators/char-ascii",-899908538,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","char-ascii","clojure.test.check.generators/char-ascii",-899908538,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/char-ascii
 */
cljs.spec.impl.gen.char_ascii = ((function (g__33866__auto___34027){
return (function cljs$spec$impl$gen$char_ascii(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34028 = arguments.length;
var i__26975__auto___34029 = (0);
while(true){
if((i__26975__auto___34029 < len__26974__auto___34028)){
args__26981__auto__.push((arguments[i__26975__auto___34029]));

var G__34030 = (i__26975__auto___34029 + (1));
i__26975__auto___34029 = G__34030;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.char_ascii.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34027))
;

cljs.spec.impl.gen.char_ascii.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34027){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34027);
});})(g__33866__auto___34027))
;

cljs.spec.impl.gen.char_ascii.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.char_ascii.cljs$lang$applyTo = ((function (g__33866__auto___34027){
return (function (seq33988){
return cljs.spec.impl.gen.char_ascii.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33988));
});})(g__33866__auto___34027))
;


var g__33866__auto___34031 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.double$ !== 'undefined')){
return clojure.test.check.generators.double$;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","double","clojure.test.check.generators/double",668331090,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","double","clojure.test.check.generators/double",668331090,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/double
 */
cljs.spec.impl.gen.double$ = ((function (g__33866__auto___34031){
return (function cljs$spec$impl$gen$double(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34032 = arguments.length;
var i__26975__auto___34033 = (0);
while(true){
if((i__26975__auto___34033 < len__26974__auto___34032)){
args__26981__auto__.push((arguments[i__26975__auto___34033]));

var G__34034 = (i__26975__auto___34033 + (1));
i__26975__auto___34033 = G__34034;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.double$.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34031))
;

cljs.spec.impl.gen.double$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34031){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34031);
});})(g__33866__auto___34031))
;

cljs.spec.impl.gen.double$.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.double$.cljs$lang$applyTo = ((function (g__33866__auto___34031){
return (function (seq33989){
return cljs.spec.impl.gen.double$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33989));
});})(g__33866__auto___34031))
;


var g__33866__auto___34035 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.int$ !== 'undefined')){
return clojure.test.check.generators.int$;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","int","clojure.test.check.generators/int",1756228469,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","int","clojure.test.check.generators/int",1756228469,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/int
 */
cljs.spec.impl.gen.int$ = ((function (g__33866__auto___34035){
return (function cljs$spec$impl$gen$int(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34036 = arguments.length;
var i__26975__auto___34037 = (0);
while(true){
if((i__26975__auto___34037 < len__26974__auto___34036)){
args__26981__auto__.push((arguments[i__26975__auto___34037]));

var G__34038 = (i__26975__auto___34037 + (1));
i__26975__auto___34037 = G__34038;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.int$.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34035))
;

cljs.spec.impl.gen.int$.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34035){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34035);
});})(g__33866__auto___34035))
;

cljs.spec.impl.gen.int$.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.int$.cljs$lang$applyTo = ((function (g__33866__auto___34035){
return (function (seq33990){
return cljs.spec.impl.gen.int$.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33990));
});})(g__33866__auto___34035))
;


var g__33866__auto___34039 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.keyword !== 'undefined')){
return clojure.test.check.generators.keyword;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","keyword","clojure.test.check.generators/keyword",24530530,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","keyword","clojure.test.check.generators/keyword",24530530,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/keyword
 */
cljs.spec.impl.gen.keyword = ((function (g__33866__auto___34039){
return (function cljs$spec$impl$gen$keyword(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34040 = arguments.length;
var i__26975__auto___34041 = (0);
while(true){
if((i__26975__auto___34041 < len__26974__auto___34040)){
args__26981__auto__.push((arguments[i__26975__auto___34041]));

var G__34042 = (i__26975__auto___34041 + (1));
i__26975__auto___34041 = G__34042;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.keyword.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34039))
;

cljs.spec.impl.gen.keyword.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34039){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34039);
});})(g__33866__auto___34039))
;

cljs.spec.impl.gen.keyword.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.keyword.cljs$lang$applyTo = ((function (g__33866__auto___34039){
return (function (seq33991){
return cljs.spec.impl.gen.keyword.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33991));
});})(g__33866__auto___34039))
;


var g__33866__auto___34043 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.keyword_ns !== 'undefined')){
return clojure.test.check.generators.keyword_ns;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","keyword-ns","clojure.test.check.generators/keyword-ns",-1492628482,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","keyword-ns","clojure.test.check.generators/keyword-ns",-1492628482,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/keyword-ns
 */
cljs.spec.impl.gen.keyword_ns = ((function (g__33866__auto___34043){
return (function cljs$spec$impl$gen$keyword_ns(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34044 = arguments.length;
var i__26975__auto___34045 = (0);
while(true){
if((i__26975__auto___34045 < len__26974__auto___34044)){
args__26981__auto__.push((arguments[i__26975__auto___34045]));

var G__34046 = (i__26975__auto___34045 + (1));
i__26975__auto___34045 = G__34046;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.keyword_ns.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34043))
;

cljs.spec.impl.gen.keyword_ns.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34043){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34043);
});})(g__33866__auto___34043))
;

cljs.spec.impl.gen.keyword_ns.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.keyword_ns.cljs$lang$applyTo = ((function (g__33866__auto___34043){
return (function (seq33992){
return cljs.spec.impl.gen.keyword_ns.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33992));
});})(g__33866__auto___34043))
;


var g__33866__auto___34047 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.large_integer !== 'undefined')){
return clojure.test.check.generators.large_integer;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","large-integer","clojure.test.check.generators/large-integer",-865967138,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","large-integer","clojure.test.check.generators/large-integer",-865967138,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/large-integer
 */
cljs.spec.impl.gen.large_integer = ((function (g__33866__auto___34047){
return (function cljs$spec$impl$gen$large_integer(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34048 = arguments.length;
var i__26975__auto___34049 = (0);
while(true){
if((i__26975__auto___34049 < len__26974__auto___34048)){
args__26981__auto__.push((arguments[i__26975__auto___34049]));

var G__34050 = (i__26975__auto___34049 + (1));
i__26975__auto___34049 = G__34050;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.large_integer.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34047))
;

cljs.spec.impl.gen.large_integer.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34047){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34047);
});})(g__33866__auto___34047))
;

cljs.spec.impl.gen.large_integer.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.large_integer.cljs$lang$applyTo = ((function (g__33866__auto___34047){
return (function (seq33993){
return cljs.spec.impl.gen.large_integer.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33993));
});})(g__33866__auto___34047))
;


var g__33866__auto___34051 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.ratio !== 'undefined')){
return clojure.test.check.generators.ratio;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","ratio","clojure.test.check.generators/ratio",1540966915,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","ratio","clojure.test.check.generators/ratio",1540966915,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/ratio
 */
cljs.spec.impl.gen.ratio = ((function (g__33866__auto___34051){
return (function cljs$spec$impl$gen$ratio(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34052 = arguments.length;
var i__26975__auto___34053 = (0);
while(true){
if((i__26975__auto___34053 < len__26974__auto___34052)){
args__26981__auto__.push((arguments[i__26975__auto___34053]));

var G__34054 = (i__26975__auto___34053 + (1));
i__26975__auto___34053 = G__34054;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.ratio.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34051))
;

cljs.spec.impl.gen.ratio.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34051){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34051);
});})(g__33866__auto___34051))
;

cljs.spec.impl.gen.ratio.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.ratio.cljs$lang$applyTo = ((function (g__33866__auto___34051){
return (function (seq33994){
return cljs.spec.impl.gen.ratio.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33994));
});})(g__33866__auto___34051))
;


var g__33866__auto___34055 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.simple_type !== 'undefined')){
return clojure.test.check.generators.simple_type;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","simple-type","clojure.test.check.generators/simple-type",892572284,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","simple-type","clojure.test.check.generators/simple-type",892572284,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/simple-type
 */
cljs.spec.impl.gen.simple_type = ((function (g__33866__auto___34055){
return (function cljs$spec$impl$gen$simple_type(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34056 = arguments.length;
var i__26975__auto___34057 = (0);
while(true){
if((i__26975__auto___34057 < len__26974__auto___34056)){
args__26981__auto__.push((arguments[i__26975__auto___34057]));

var G__34058 = (i__26975__auto___34057 + (1));
i__26975__auto___34057 = G__34058;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.simple_type.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34055))
;

cljs.spec.impl.gen.simple_type.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34055){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34055);
});})(g__33866__auto___34055))
;

cljs.spec.impl.gen.simple_type.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.simple_type.cljs$lang$applyTo = ((function (g__33866__auto___34055){
return (function (seq33995){
return cljs.spec.impl.gen.simple_type.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33995));
});})(g__33866__auto___34055))
;


var g__33866__auto___34059 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.simple_type_printable !== 'undefined')){
return clojure.test.check.generators.simple_type_printable;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","simple-type-printable","clojure.test.check.generators/simple-type-printable",-58489962,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","simple-type-printable","clojure.test.check.generators/simple-type-printable",-58489962,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/simple-type-printable
 */
cljs.spec.impl.gen.simple_type_printable = ((function (g__33866__auto___34059){
return (function cljs$spec$impl$gen$simple_type_printable(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34060 = arguments.length;
var i__26975__auto___34061 = (0);
while(true){
if((i__26975__auto___34061 < len__26974__auto___34060)){
args__26981__auto__.push((arguments[i__26975__auto___34061]));

var G__34062 = (i__26975__auto___34061 + (1));
i__26975__auto___34061 = G__34062;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.simple_type_printable.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34059))
;

cljs.spec.impl.gen.simple_type_printable.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34059){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34059);
});})(g__33866__auto___34059))
;

cljs.spec.impl.gen.simple_type_printable.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.simple_type_printable.cljs$lang$applyTo = ((function (g__33866__auto___34059){
return (function (seq33996){
return cljs.spec.impl.gen.simple_type_printable.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33996));
});})(g__33866__auto___34059))
;


var g__33866__auto___34063 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.string !== 'undefined')){
return clojure.test.check.generators.string;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","string","clojure.test.check.generators/string",-1704750979,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","string","clojure.test.check.generators/string",-1704750979,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/string
 */
cljs.spec.impl.gen.string = ((function (g__33866__auto___34063){
return (function cljs$spec$impl$gen$string(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34064 = arguments.length;
var i__26975__auto___34065 = (0);
while(true){
if((i__26975__auto___34065 < len__26974__auto___34064)){
args__26981__auto__.push((arguments[i__26975__auto___34065]));

var G__34066 = (i__26975__auto___34065 + (1));
i__26975__auto___34065 = G__34066;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.string.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34063))
;

cljs.spec.impl.gen.string.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34063){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34063);
});})(g__33866__auto___34063))
;

cljs.spec.impl.gen.string.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.string.cljs$lang$applyTo = ((function (g__33866__auto___34063){
return (function (seq33997){
return cljs.spec.impl.gen.string.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33997));
});})(g__33866__auto___34063))
;


var g__33866__auto___34067 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.string_ascii !== 'undefined')){
return clojure.test.check.generators.string_ascii;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","string-ascii","clojure.test.check.generators/string-ascii",-2009877640,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","string-ascii","clojure.test.check.generators/string-ascii",-2009877640,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/string-ascii
 */
cljs.spec.impl.gen.string_ascii = ((function (g__33866__auto___34067){
return (function cljs$spec$impl$gen$string_ascii(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34068 = arguments.length;
var i__26975__auto___34069 = (0);
while(true){
if((i__26975__auto___34069 < len__26974__auto___34068)){
args__26981__auto__.push((arguments[i__26975__auto___34069]));

var G__34070 = (i__26975__auto___34069 + (1));
i__26975__auto___34069 = G__34070;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.string_ascii.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34067))
;

cljs.spec.impl.gen.string_ascii.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34067){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34067);
});})(g__33866__auto___34067))
;

cljs.spec.impl.gen.string_ascii.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.string_ascii.cljs$lang$applyTo = ((function (g__33866__auto___34067){
return (function (seq33998){
return cljs.spec.impl.gen.string_ascii.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33998));
});})(g__33866__auto___34067))
;


var g__33866__auto___34071 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.string_alphanumeric !== 'undefined')){
return clojure.test.check.generators.string_alphanumeric;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","string-alphanumeric","clojure.test.check.generators/string-alphanumeric",836374939,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","string-alphanumeric","clojure.test.check.generators/string-alphanumeric",836374939,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/string-alphanumeric
 */
cljs.spec.impl.gen.string_alphanumeric = ((function (g__33866__auto___34071){
return (function cljs$spec$impl$gen$string_alphanumeric(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34072 = arguments.length;
var i__26975__auto___34073 = (0);
while(true){
if((i__26975__auto___34073 < len__26974__auto___34072)){
args__26981__auto__.push((arguments[i__26975__auto___34073]));

var G__34074 = (i__26975__auto___34073 + (1));
i__26975__auto___34073 = G__34074;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.string_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34071))
;

cljs.spec.impl.gen.string_alphanumeric.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34071){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34071);
});})(g__33866__auto___34071))
;

cljs.spec.impl.gen.string_alphanumeric.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.string_alphanumeric.cljs$lang$applyTo = ((function (g__33866__auto___34071){
return (function (seq33999){
return cljs.spec.impl.gen.string_alphanumeric.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq33999));
});})(g__33866__auto___34071))
;


var g__33866__auto___34075 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.symbol !== 'undefined')){
return clojure.test.check.generators.symbol;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","symbol","clojure.test.check.generators/symbol",-1305461065,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","symbol","clojure.test.check.generators/symbol",-1305461065,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/symbol
 */
cljs.spec.impl.gen.symbol = ((function (g__33866__auto___34075){
return (function cljs$spec$impl$gen$symbol(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34076 = arguments.length;
var i__26975__auto___34077 = (0);
while(true){
if((i__26975__auto___34077 < len__26974__auto___34076)){
args__26981__auto__.push((arguments[i__26975__auto___34077]));

var G__34078 = (i__26975__auto___34077 + (1));
i__26975__auto___34077 = G__34078;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.symbol.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34075))
;

cljs.spec.impl.gen.symbol.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34075){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34075);
});})(g__33866__auto___34075))
;

cljs.spec.impl.gen.symbol.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.symbol.cljs$lang$applyTo = ((function (g__33866__auto___34075){
return (function (seq34000){
return cljs.spec.impl.gen.symbol.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq34000));
});})(g__33866__auto___34075))
;


var g__33866__auto___34079 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.symbol_ns !== 'undefined')){
return clojure.test.check.generators.symbol_ns;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","symbol-ns","clojure.test.check.generators/symbol-ns",-862629490,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","symbol-ns","clojure.test.check.generators/symbol-ns",-862629490,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/symbol-ns
 */
cljs.spec.impl.gen.symbol_ns = ((function (g__33866__auto___34079){
return (function cljs$spec$impl$gen$symbol_ns(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34080 = arguments.length;
var i__26975__auto___34081 = (0);
while(true){
if((i__26975__auto___34081 < len__26974__auto___34080)){
args__26981__auto__.push((arguments[i__26975__auto___34081]));

var G__34082 = (i__26975__auto___34081 + (1));
i__26975__auto___34081 = G__34082;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.symbol_ns.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34079))
;

cljs.spec.impl.gen.symbol_ns.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34079){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34079);
});})(g__33866__auto___34079))
;

cljs.spec.impl.gen.symbol_ns.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.symbol_ns.cljs$lang$applyTo = ((function (g__33866__auto___34079){
return (function (seq34001){
return cljs.spec.impl.gen.symbol_ns.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq34001));
});})(g__33866__auto___34079))
;


var g__33866__auto___34083 = (new cljs.spec.impl.gen.LazyVar((function (){
if((typeof clojure.test !== 'undefined') && (typeof clojure.test.check !== 'undefined') && (typeof clojure.test.check.generators.uuid !== 'undefined')){
return clojure.test.check.generators.uuid;
} else {
throw (new Error([cljs.core.str("Var "),cljs.core.str(new cljs.core.Symbol("clojure.test.check.generators","uuid","clojure.test.check.generators/uuid",1589373144,null)),cljs.core.str(" does not exist, "),cljs.core.str(cljs.core.namespace.call(null,new cljs.core.Symbol("clojure.test.check.generators","uuid","clojure.test.check.generators/uuid",1589373144,null))),cljs.core.str(" never required")].join('')));
}
}),null));
/**
 * Fn returning clojure.test.check.generators/uuid
 */
cljs.spec.impl.gen.uuid = ((function (g__33866__auto___34083){
return (function cljs$spec$impl$gen$uuid(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34084 = arguments.length;
var i__26975__auto___34085 = (0);
while(true){
if((i__26975__auto___34085 < len__26974__auto___34084)){
args__26981__auto__.push((arguments[i__26975__auto___34085]));

var G__34086 = (i__26975__auto___34085 + (1));
i__26975__auto___34085 = G__34086;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.uuid.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});})(g__33866__auto___34083))
;

cljs.spec.impl.gen.uuid.cljs$core$IFn$_invoke$arity$variadic = ((function (g__33866__auto___34083){
return (function (args){
return cljs.core.deref.call(null,g__33866__auto___34083);
});})(g__33866__auto___34083))
;

cljs.spec.impl.gen.uuid.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.uuid.cljs$lang$applyTo = ((function (g__33866__auto___34083){
return (function (seq34002){
return cljs.spec.impl.gen.uuid.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq34002));
});})(g__33866__auto___34083))
;

/**
 * Returns a generator of a sequence catenated from results of
 * gens, each of which should generate something sequential.
 */
cljs.spec.impl.gen.cat = (function cljs$spec$impl$gen$cat(var_args){
var args__26981__auto__ = [];
var len__26974__auto___34089 = arguments.length;
var i__26975__auto___34090 = (0);
while(true){
if((i__26975__auto___34090 < len__26974__auto___34089)){
args__26981__auto__.push((arguments[i__26975__auto___34090]));

var G__34091 = (i__26975__auto___34090 + (1));
i__26975__auto___34090 = G__34091;
continue;
} else {
}
break;
}

var argseq__26982__auto__ = ((((0) < args__26981__auto__.length))?(new cljs.core.IndexedSeq(args__26981__auto__.slice((0)),(0),null)):null);
return cljs.spec.impl.gen.cat.cljs$core$IFn$_invoke$arity$variadic(argseq__26982__auto__);
});

cljs.spec.impl.gen.cat.cljs$core$IFn$_invoke$arity$variadic = (function (gens){
return cljs.spec.impl.gen.fmap.call(null,(function (p1__34087_SHARP_){
return cljs.core.apply.call(null,cljs.core.concat,p1__34087_SHARP_);
}),cljs.core.apply.call(null,cljs.spec.impl.gen.tuple,gens));
});

cljs.spec.impl.gen.cat.cljs$lang$maxFixedArity = (0);

cljs.spec.impl.gen.cat.cljs$lang$applyTo = (function (seq34088){
return cljs.spec.impl.gen.cat.cljs$core$IFn$_invoke$arity$variadic(cljs.core.seq.call(null,seq34088));
});

cljs.spec.impl.gen.qualified_QMARK_ = (function cljs$spec$impl$gen$qualified_QMARK_(ident){
return !((cljs.core.namespace.call(null,ident) == null));
});
cljs.spec.impl.gen.gen_builtins = (new cljs.core.Delay((function (){
var simple = cljs.spec.impl.gen.simple_type_printable.call(null);
return cljs.core.PersistentHashMap.fromArrays([cljs.core.qualified_keyword_QMARK_,cljs.core.seq_QMARK_,cljs.core.vector_QMARK_,cljs.core.any_QMARK_,cljs.core.boolean_QMARK_,cljs.core.char_QMARK_,cljs.core.inst_QMARK_,cljs.core.simple_symbol_QMARK_,cljs.core.sequential_QMARK_,cljs.core.set_QMARK_,cljs.core.map_QMARK_,cljs.core.empty_QMARK_,cljs.core.string_QMARK_,cljs.core.int_QMARK_,cljs.core.associative_QMARK_,cljs.core.keyword_QMARK_,cljs.core.indexed_QMARK_,cljs.core.zero_QMARK_,cljs.core.simple_keyword_QMARK_,cljs.core.neg_int_QMARK_,cljs.core.nil_QMARK_,cljs.core.ident_QMARK_,cljs.core.qualified_ident_QMARK_,cljs.core.true_QMARK_,cljs.core.integer_QMARK_,cljs.core.nat_int_QMARK_,cljs.core.pos_int_QMARK_,cljs.core.uuid_QMARK_,cljs.core.false_QMARK_,cljs.core.list_QMARK_,cljs.core.simple_ident_QMARK_,cljs.core.number_QMARK_,cljs.core.qualified_symbol_QMARK_,cljs.core.seqable_QMARK_,cljs.core.symbol_QMARK_,cljs.core.coll_QMARK_],[cljs.spec.impl.gen.such_that.call(null,cljs.spec.impl.gen.qualified_QMARK_,cljs.spec.impl.gen.keyword_ns.call(null)),cljs.spec.impl.gen.list.call(null,simple),cljs.spec.impl.gen.vector.call(null,simple),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.return$.call(null,null),cljs.spec.impl.gen.any_printable.call(null)], null)),cljs.spec.impl.gen.boolean$.call(null),cljs.spec.impl.gen.char$.call(null),cljs.spec.impl.gen.fmap.call(null,((function (simple){
return (function (p1__34092_SHARP_){
return (new Date(p1__34092_SHARP_));
});})(simple))
,cljs.spec.impl.gen.large_integer.call(null)),cljs.spec.impl.gen.symbol.call(null),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.list.call(null,simple),cljs.spec.impl.gen.vector.call(null,simple)], null)),cljs.spec.impl.gen.set.call(null,simple),cljs.spec.impl.gen.map.call(null,simple,simple),cljs.spec.impl.gen.elements.call(null,new cljs.core.PersistentVector(null, 5, 5, cljs.core.PersistentVector.EMPTY_NODE, [null,cljs.core.List.EMPTY,cljs.core.PersistentVector.EMPTY,cljs.core.PersistentArrayMap.EMPTY,cljs.core.PersistentHashSet.EMPTY], null)),cljs.spec.impl.gen.string_alphanumeric.call(null),cljs.spec.impl.gen.large_integer.call(null),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.map.call(null,simple,simple),cljs.spec.impl.gen.vector.call(null,simple)], null)),cljs.spec.impl.gen.keyword_ns.call(null),cljs.spec.impl.gen.vector.call(null,simple),cljs.spec.impl.gen.return$.call(null,(0)),cljs.spec.impl.gen.keyword.call(null),cljs.spec.impl.gen.large_integer_STAR_.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"max","max",61366548),(-1)], null)),cljs.spec.impl.gen.return$.call(null,null),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.keyword_ns.call(null),cljs.spec.impl.gen.symbol_ns.call(null)], null)),cljs.spec.impl.gen.such_that.call(null,cljs.spec.impl.gen.qualified_QMARK_,cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.keyword_ns.call(null),cljs.spec.impl.gen.symbol_ns.call(null)], null))),cljs.spec.impl.gen.return$.call(null,true),cljs.spec.impl.gen.large_integer.call(null),cljs.spec.impl.gen.large_integer_STAR_.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"min","min",444991522),(0)], null)),cljs.spec.impl.gen.large_integer_STAR_.call(null,new cljs.core.PersistentArrayMap(null, 1, [new cljs.core.Keyword(null,"min","min",444991522),(1)], null)),cljs.spec.impl.gen.uuid.call(null),cljs.spec.impl.gen.return$.call(null,false),cljs.spec.impl.gen.list.call(null,simple),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.keyword.call(null),cljs.spec.impl.gen.symbol.call(null)], null)),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 2, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.large_integer.call(null),cljs.spec.impl.gen.double$.call(null)], null)),cljs.spec.impl.gen.such_that.call(null,cljs.spec.impl.gen.qualified_QMARK_,cljs.spec.impl.gen.symbol_ns.call(null)),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 6, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.return$.call(null,null),cljs.spec.impl.gen.list.call(null,simple),cljs.spec.impl.gen.vector.call(null,simple),cljs.spec.impl.gen.map.call(null,simple,simple),cljs.spec.impl.gen.set.call(null,simple),cljs.spec.impl.gen.string_alphanumeric.call(null)], null)),cljs.spec.impl.gen.symbol_ns.call(null),cljs.spec.impl.gen.one_of.call(null,new cljs.core.PersistentVector(null, 4, 5, cljs.core.PersistentVector.EMPTY_NODE, [cljs.spec.impl.gen.map.call(null,simple,simple),cljs.spec.impl.gen.list.call(null,simple),cljs.spec.impl.gen.vector.call(null,simple),cljs.spec.impl.gen.set.call(null,simple)], null))]);
}),null));
/**
 * Given a predicate, returns a built-in generator if one exists.
 */
cljs.spec.impl.gen.gen_for_pred = (function cljs$spec$impl$gen$gen_for_pred(pred){
if(cljs.core.set_QMARK_.call(null,pred)){
return cljs.spec.impl.gen.elements.call(null,pred);
} else {
return cljs.core.get.call(null,cljs.core.deref.call(null,cljs.spec.impl.gen.gen_builtins),pred);
}
});

//# sourceMappingURL=gen.js.map?rel=1496955724577