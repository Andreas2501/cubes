// Compiled by ClojureScript 1.9.229 {}
goog.provide('metropolis.core');
goog.require('cljs.core');
goog.require('cljsjs.three');
cljs.core.enable_console_print_BANG_.call(null);
metropolis.core.init = (function metropolis$core$init(){
var scene = (new THREE.Scene());
var p_camera = (new THREE.PerspectiveCamera(metropolis.core.view_angle,metropolis.core.aspect,metropolis.core.near,metropolis.core.far));
var box = (new THREE.BoxGeometry((200),(200),(200)));
var mat = (new THREE.MeshBasicMaterial((function (){var obj27031 = {"color":(16711680),"wireframe":true};
return obj27031;
})()));
var mesh = (new THREE.Mesh(box,mat));
var renderer = (new THREE.WebGLRenderer());
(p_camera["name"] = "p-camera");

(p_camera["position"]["z"] = (500));

(mesh["rotation"]["x"] = (45));

(mesh["rotation"]["y"] = (0));

renderer.setSize((500),(500));

scene.add(p_camera);

scene.add(mesh);

document.body.appendChild(renderer.domElement);

metropolis.core.render = ((function (scene,p_camera,box,mat,mesh,renderer){
return (function metropolis$core$init_$_render(){
(mesh["rotation"]["y"] = (0.01 + mesh.rotation.y));

return renderer.render(scene,p_camera);
});})(scene,p_camera,box,mat,mesh,renderer))
;

metropolis.core.animate = ((function (scene,p_camera,box,mat,mesh,renderer){
return (function metropolis$core$init_$_animate(){
window.requestAnimationFrame(metropolis$core$init_$_animate);

return metropolis.core.render.call(null);
});})(scene,p_camera,box,mat,mesh,renderer))
;

return metropolis.core.animate.call(null);
});
metropolis.core.init.call(null);

//# sourceMappingURL=core.js.map?rel=1496955715219